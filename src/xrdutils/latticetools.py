"""
Created on Thu Jul  9 11:48:24 2020

@author: prevot
"""


# -*- coding: utf-8 -*-
"""
Created on Fri Dec 12 13:42:19 2014

@author: Prevot
"""
#import tracemalloc
#tracemalloc.start()

import numpy as np
from math import sqrt, sin, cos, pi
from matplotlib.path import Path as mplPath
from lmfit import minimize
import os.path as osp
import os
import pickle
from PIL import Image
import scipy.optimize as opt
from scipy import ndimage

from guidata.configtools import get_icon
from guidata.dataset.datatypes import (DataSet, ObjectItem, BeginGroup,ItemProperty,
                                       EndGroup, Obj, DataSetGroup,
                                       BeginTabGroup, EndTabGroup,
                                       GetAttrProp, NotProp)
from guidata.dataset.dataitems import (ChoiceItem, BoolItem, FloatItem, IntItem,
                                       ImageChoiceItem, ColorItem, StringItem,
                                       ButtonItem, FloatArrayItem, TextItem)
from sys import maxsize
from qtpy import PYQT5
from qtpy.QtGui import QColor, QPen, QBrush, QPolygonF,  QTransform, QPainter, QPainterPath
from qtpy.QtCore import Qt, QRectF, QPointF, QPoint, QLineF, QSize, QRect, Signal
from qtpy.QtWidgets import (QSplitter,QCheckBox,
                              QComboBox, QPushButton, QDialog, QLabel, QLineEdit, QMessageBox,
                              QSizePolicy, QApplication, QFileDialog, QMenuBar, QMenu, QAction)
from guidata.utils import assert_interfaces_valid
from guidata.utils import update_dataset
from guidata.qthelpers import add_actions
from guiqwt.baseplot import canvas_to_axes
from guiqwt.builder import PlotItemBuilder as GuiPlotItemBuilder
from guiqwt.config import CONF, _
from guiqwt.curve import PlotItemList
from guiqwt.geometry import (vector_norm, vector_projection, vector_rotation, translate, scale, rotate, colvector,
                             compute_center)
from guiqwt.histogram import lut_range_threshold
from guiqwt.image import ImagePlot,BaseImageItem,ImageItem,MaskedImageItem
from guiqwt.interfaces import IBasePlotItem, IShapeItemType, IImageItemType, ISerializableType,IColormapImageItemType
from guiqwt.plot import ImageDialog,configure_plot_splitter
from guiqwt._scaler import _histogram
from guiqwt.shapes import AbstractShape,PolygonShape,RectangleShape,EllipseShape,PointShape,Marker
from guiqwt.styles import (MarkerParam, ShapeParam, RangeShapeParam, ItemParameters, MaskedImageParam,
                           AxesShapeParam, MARKERSTYLES, ImageParam)
from guiqwt.tools import (RectangleTool, RectangularShapeTool, EllipseTool, HRangeTool, PlaceAxesTool,
                          MultiLineTool, FreeFormTool, SegmentTool, CircleTool, PointTool,
                          AnnotatedRectangleTool, AnnotatedEllipseTool,
                          AnnotatedSegmentTool, AnnotatedCircleTool, LabelTool,
                          AnnotatedPointTool, CommandTool,RectangularActionTool,
                          VCursorTool, HCursorTool, XCursorTool,ImageMaskTool,
                          ObliqueRectangleTool, AnnotatedObliqueRectangleTool,DefaultToolbarID)
from guiqwt.transitional import QwtPlotItem, QwtSymbol, QwtPlotMarker
from guiqwt import io

#Modification of guiqwt.baseplot :
#  def add_item_with_z_offset(self, item, zoffset):
# z = zlist[dzlist.flat[0]]+1  instead of z = zlist[dzlist]+1




_fromUtf8 = lambda s: s



from guidata.configtools import add_image_path
abspath = osp.abspath(__file__)
dirpath = osp.dirname(abspath)
add_image_path(dirpath)

#these lines are needed to correct an error in guidata
import inspect
from guidata.dataset.qtitemwidgets import ChoiceWidget
string = inspect.getsource(ChoiceWidget.get)
ind0 = string.index('self._first_call = False')
ind1 = string.index('self.index_changed(idx)')
txt = "error not corrected in python module guidata.dataset.qtitemwidgets\n"
txt += "in ChoiceWidget.get(self):"
txt +=  "inverting lines 'self._first_call = False' and 'self.index_changed(idx)' is needed"
if 'XRDUTILS_NO_GUIDATA_CHECK' not in os.environ:
  assert (ind0 < ind1),txt


cos_60 = 0.5
sin_60 = np.sqrt(3.)/2.

cos_120 = -0.5
sin_120 = np.sqrt(3.)/2.

"""default parameters for new plot items (unitcell, latticegrip, spot, reconstructiongrid)"""
DEFAULTS = {
            'plot' :
             {

              "shape/unicell/line/style" : 'NoPen',
              "shape/unicell/line/color" : "#ff0000",
              "shape/unicell/line/width" : 1,
              "shape/unicell/fill/style" : "SolidPattern",
              "shape/unicell/fill/color" : "white",
              "shape/unicell/fill/alpha" : 0.1,
              "shape/unicell/symbol/marker" : 'Ellipse',
              "shape/unicell/symbol/size" : 10,
              "shape/unicell/symbol/edgecolor" : "#ff0000",
              "shape/unicell/symbol/facecolor" : "#ff0000",
              "shape/unicell/symbol/alpha" : 0.,
              "shape/unicell/sel_line/style" : 'NoPen',
              "shape/unicell/sel_line/color" : "#00ff00",
              "shape/unicell/sel_line/width" : 1,
              "shape/unicell/sel_fill/style" : "SolidPattern",
              "shape/unicell/sel_fill/color" : "white",
              "shape/unicell/sel_fill/alpha" : 0.1,
              "shape/unicell/sel_symbol/marker" : 'NoSymbol',
              "shape/unicell/sel_symbol/size" : 10,
              "shape/unicell/sel_symbol/edgecolor" : "#00aa00",
              "shape/unicell/sel_symbol/facecolor" : "#00ff00",
              "shape/unicell/sel_symbol/alpha" : .7,
              "shape/unicell/xarrow_pen/style" : 'NoPen',
              "shape/unicell/xarrow_pen/color" : "red",
              "shape/unicell/xarrow_pen/width" : 1,
              "shape/unicell/xarrow_brush/color" : "red",
              "shape/unicell/xarrow_brush/alpha" : 0.2,
              "shape/unicell/yarrow_pen/style" : 'NoPen',
              "shape/unicell/yarrow_pen/color" : "blue",
              "shape/unicell/yarrow_pen/width" : 1,
              "shape/unicell/yarrow_brush/color" : "blue",
              "shape/unicell/yarrow_brush/alpha" : 0.2,

              "shape/latticegrid/line/style" : 'SolidLine',
              "shape/latticegrid/line/color" : "#ff0000",
              "shape/latticegrid/line/width" : 1,
              "shape/latticegrid/fill/style" : "SolidPattern",
              "shape/latticegrid/fill/color" : "white",
              "shape/latticegrid/fill/alpha" : 0.1,
              "shape/latticegrid/symbol/marker" : 'Ellipse',
              "shape/latticegrid/symbol/size" : 10,
              "shape/latticegrid/symbol/edgecolor" : "#ff0000",
              "shape/latticegrid/symbol/facecolor" : "#ff0000",
              "shape/latticegrid/symbol/alpha" : 0.,
              "shape/latticegrid/sel_line/style" : 'SolidLine',
              "shape/latticegrid/sel_line/color" : "#00ff00",
              "shape/latticegrid/sel_line/width" : 1,
              "shape/latticegrid/sel_fill/style" : "SolidPattern",
              "shape/latticegrid/sel_fill/color" : "white",
              "shape/latticegrid/sel_fill/alpha" : 0.1,
              "shape/latticegrid/sel_symbol/marker" : 'Ellipse',
              "shape/latticegrid/sel_symbol/size" : 10,
              "shape/latticegrid/sel_symbol/edgecolor" : "#00aa00",
              "shape/latticegrid/sel_symbol/facecolor" : "#00ff00",
              "shape/latticegrid/sel_symbol/alpha" : .7,
              "shape/latticegrid/xarrow_pen/style" : 'SolidLine',
              "shape/latticegrid/xarrow_pen/color" : "red",
              "shape/latticegrid/xarrow_pen/width" : 1,
              "shape/latticegrid/xarrow_brush/color" : "red",
              "shape/latticegrid/xarrow_brush/alpha" : 0.2,
              "shape/latticegrid/yarrow_pen/style" : 'SolidLine',
              "shape/latticegrid/yarrow_pen/color" : "green",
              "shape/latticegrid/yarrow_pen/width" : 1,
              "shape/latticegrid/yarrow_brush/color" : "green",
              "shape/latticegrid/yarrow_brush/alpha" : 0.2,

              "shape/spot/line/style" : 'SolidLine',
              "shape/spot/line/color" : "#48b427",
              "shape/spot/line/width" : 1,
              "shape/spot/sel_line/style" : 'SolidLine',
              "shape/spot/sel_line/color" : "#00ff00",
              "shape/spot/sel_line/width" : 1,
              "shape/spot/fill/style" : "NoBrush",
              "shape/spot/sel_fill/style" : "NoBrush",
              "shape/spot/symbol/marker" : 'XCross',
              "shape/spot/symbol/size" : 9,
              "shape/spot/symbol/edgecolor" : "#48b427",
              "shape/spot/symbol/facecolor" : "#48b427",
              "shape/spot/symbol/alpha" : 1.,
              "shape/spot/sel_symbol/marker" : 'XCross',
              "shape/spot/sel_symbol/size" : 12,
              "shape/spot/sel_symbol/edgecolor" : "#00aa00",
              "shape/spot/sel_symbol/facecolor" : "#00ff00",
              "shape/spot/sel_symbol/alpha" : .7,

              "shape/reconstructiongrid/line/style" : 'NoPen',
              "shape/reconstructiongrid/line/color" : "#0000ff",
              "shape/reconstructiongrid/line/width" : 2,
              "shape/reconstructiongrid/fill/style" : "SolidPattern",
              "shape/reconstructiongrid/fill/color" : "white",
              "shape/reconstructiongrid/fill/alpha" : 0.1,
              "shape/reconstructiongrid/symbol/marker" : 'Ellipse',
              "shape/reconstructiongrid/symbol/size" : 8,
              "shape/reconstructiongrid/symbol/edgecolor" : "#0000ff",
              "shape/reconstructiongrid/symbol/facecolor" : "#0000ff",
              "shape/reconstructiongrid/symbol/alpha" : 0.,
              "shape/reconstructiongrid/sel_line/style" : 'DashLine',
              "shape/reconstructiongrid/sel_line/color" : "#00ff00",
              "shape/reconstructiongrid/sel_line/width" : 1,
              "shape/reconstructiongrid/sel_fill/style" : "SolidPattern",
              "shape/reconstructiongrid/sel_fill/color" : "white",
              "shape/reconstructiongrid/sel_fill/alpha" : 0.1,
              "shape/reconstructiongrid/sel_symbol/marker" : 'Ellipse',
              "shape/reconstructiongrid/sel_symbol/size" : 8,
              "shape/reconstructiongrid/sel_symbol/edgecolor" : "#00aa00",
              "shape/reconstructiongrid/sel_symbol/facecolor" : "#00ff00",
              "shape/reconstructiongrid/sel_symbol/alpha" : .7,
              "shape/reconstructiongrid/xarrow_pen/style" : 'SolidLine',
              "shape/reconstructiongrid/xarrow_pen/color" : "red",
              "shape/reconstructiongrid/xarrow_pen/width" : 1,
              "shape/reconstructiongrid/xarrow_brush/color" : "red",
              "shape/reconstructiongrid/xarrow_brush/alpha" : 0.2,
              "shape/reconstructiongrid/yarrow_pen/style" : 'SolidLine',
              "shape/reconstructiongrid/yarrow_pen/color" : "green",
              "shape/reconstructiongrid/yarrow_pen/width" : 1,
              "shape/reconstructiongrid/yarrow_brush/color" : "green",
              "shape/reconstructiongrid/yarrow_brush/alpha" : 0.2,

                            },

            }

CONF.update_defaults(DEFAULTS)

SPACEGROUP_CHOICES = [("p1", _("p1"), "none.png"),
                  ("p2", "p2", "none.png"),
                  ("pm", "pm", "none.png"),
                  ("pg", "pg", "none.png"),
                  ("cm", "cm", "none.png"),
                  ("p1m", "p1m", "none.png"),
                  ("p1g", "p1g", "none.png"),
                  ("c1m", "c1m", "none.png"),
                  ("p2mm", "p2mm", "none.png"),
                  ("p2mg", "p2mg", "none.png"),
                  ("p2gg", "p2gg", "none.png"),
                  ("c2mm", "c2mm", "none.png"),
                  ("p4", "p4", "none.png"),
                  ("p4mm", "p4mm", "none.png"),
                  ("p4gm", "p4gm", "none.png"),
                  ("p3", "p3", "none.png"),
                  ("p3m1", "p3m1", "none.png"),
                  ("p31m", "p31m", "none.png"),
                  ("p6", "p6", "none.png"),
                  ("p6mm", "p6mm", "none.png"),
                   ]
"""
MATRICES = {}
MATRICES['p1'] = []
MATRICES['p2'] = []
MATRICES['pm'] = [np.matrix([[1,0],[0,-1]])]
MATRICES['pg'] = MATRICES['pm']
MATRICES['cm'] = MATRICES['pm']
MATRICES['p1m'] = [np.matrix([[-1,0],[0,1]])]
MATRICES['p1g'] = MATRICES['p1m']
MATRICES['c1m'] = MATRICES['p1m']
MATRICES['p2mm'] = [np.matrix([[1,0],[0,-1]])]
"""











SHAPE_Z_OFFSET = 1000

def xy_to_angle(ux,uy):
  return np.sqrt(ux**2+uy**2),np.rad2deg(np.arctan2(uy,ux))

def angle_to_xy(r,t):
  tt =  np.deg2rad(t)
  return r*np.cos(tt),r*np.sin(tt)

def corr_lin(p,x,y):
   #hi est soit h,soit k en pixel, retourne ki qui est soit kx, soit ky
    return p[0]+p[1]*x+p[2]*y

def corr_quad(p,x,y):
    #h et k sont les pixels, retourne la valeur soit de kx, soit de ky
    return p[0]+p[1]*x+p[2]*y+p[3]*x*x+p[4]*x*y+p[5]*x*y

def corr_cub(p,x,y):
    #h et k sont les pixels, retourne la valeur soit de kx, soit de ky
    return p[0]+p[1]*x+p[2]*y+p[3]*x*x+p[4]*x*y+p[5]*x*y+p[6]*x*x*x+p[7]*x*x*y+p[8]*x*y*y+p[9]*y*y*y

def compute_distortion(xexp,yexp,xth,yth):
    #fit the quadratic deformation to go from (xth,yth) to (xexp,yexp)
    nref = len(xexp)
    xexp = np.array(xexp)
    yexp = np.array(yexp)
    xth = np.array(xth)
    yth = np.array(yth)
    p1x = [0.,1.,0.]
    p1y = [0.,0.,1.]

    if nref>2:
        p1x, success = opt.leastsq(erreur_lin, p1x, args = (xth,yth,xexp), maxfev = 10000)
        p1y, success = opt.leastsq(erreur_lin, p1y, args = (xth,yth,yexp), maxfev = 10000)

        p2x = list(p1x)+[0.,0.,0.]
        p2y = list(p1y)+[0.,0.,0.]

        if nref>5:          # on ajoute des termes quadratiques

            p2x, success = opt.leastsq(erreur_quad, p2x, args = (xth,yth,xexp), maxfev = 10000)
            p2y, success = opt.leastsq(erreur_quad, p2y, args = (xth,yth,yexp), maxfev = 10000)

            p3x = list(p2x)+[0.,0.,0.,0.]
            p3y = list(p2y)+[0.,0.,0.,0.]

            if nref>9:      # on ajoute des termes cubiques
                p3x, success = opt.leastsq(erreur_cub, p3x, args = (xth,yth,xexp), maxfev = 10000)
                p3y, success = opt.leastsq(erreur_cub, p3y, args = (xth,yth,yexp), maxfev = 10000)

        else:
            p3x = list(p2x)+[0.,0.,0.,0.]
            p3y = list(p2y)+[0.,0.,0.,0.]

    else:
        p3x = list(p1x)+[0.,0.,0.,0.,0.,0.,0.]
        p3y = list(p1y)+[0.,0.,0.,0.,0.,0.,0.]

    return p3x,p3y

def erreur_lin(p,x,y,s):
    #x et y en pixel, s est la coordonnee corrigee soit x, soit y
    return corr_lin(p,x,y)-s

def erreur_quad(p,x,y,s):
    #x et y en pixel, s est la coordonnee corrigee soit x, soit y
    return corr_quad(p,x,y)-s

def erreur_cub(p,x,y,s):
    #x et y en pixel, s est la coordonnee corrigee soit x, soit y
    return corr_cub(p,x,y)-s

def Gaussian(x, amplitude, xo, sigma, offset):
    g = offset + amplitude*np.exp(-((x-xo)/sigma)**2)
    return g.ravel()

def twoD_Gaussian(u, amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
    x = u[0]
    y = u[1]
    xo = float(xo)
    yo = float(yo)
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo)
                            + c*((y-yo)**2)))
    return g.ravel()

def twoD_iso_Gaussian(u, amplitude, xo, yo, sigma, offset):
    x = u[0]
    y = u[1]
    xo = float(xo)
    yo = float(yo)
    a = 1./(2*sigma**2)
    g = offset + amplitude*np.exp(-a*(((x-xo)**2) +((y-yo)**2)))
    return g.ravel()

def _nanmin(data):
    if data.dtype.name in ("float32","float64", "float128"):
        return np.nanmin(data)
    else:
        return data.min()

def _nanmax(data):
    if data.dtype.name in ("float32","float64", "float128"):
        return np.nanmax(data)
    else:
        return data.max()

def array2d_to_qpolygonf(points):
    """
    Utility function to convert a 2D-NumPy arrays representing points
    (X-axis, Y-axis data) into a single polyline (QtGui.PolygonF object).
    This feature is compatible with PyQt4, PyQt5

    License/copyright: MIT License © Pierre Raybaut 2020.

    :param numpy.ndarray xdata: 1D-NumPy array (numpy.float64)
    :param numpy.ndarray ydata: 1D-NumPy array (numpy.float64)
    :return: Polyline
    :rtype: QtGui.QPolygonF
    """
    dtype = np.float
    if not (points.shape[1] ==  2  and points.dtype ==   dtype
    ):
        raise ValueError("Arguments must be 2D, float64 NumPy arrays with same size")
    size = points.shape[0]
    polyline = QPolygonF(size)
    buffer = polyline.data()
    buffer.setsize(2 * size * np.finfo(dtype).dtype.itemsize)
    memory = np.frombuffer(buffer, dtype)
    memory[: (size - 1) * 2 + 1 : 2] = points[:,0]
    memory[1 : (size - 1) * 2 + 2 : 2] = points[:,1]
    return polyline

def append_domain(domains,domain):
    #append a domain to a list of domains if not already in list
    for domain1 in domains:
        if not check_different(domain1, domain):  #a matrix is similar, do nothing
            return
    domains.append(domain)  #add the domain to the list

def check_different(M1,M2):
    #determine if M1 and M2 matrices define the same lattice
    u1x,u1y,v1x,v1y=M1[0,0],M1[1,0],M1[0,1],M1[1,1]
    u2x,u2y,v2x,v2y=M2[0,0],M2[1,0],M2[0,1],M2[1,1]


    delta2=u2x*v2y-u2y*v2x
    nA1=(u1x*v2y-u1y*v2x)/delta2
    pA1=(u1y*u2x-u1x*u2y)/delta2
    nB1=(v1x*v2y-v1y*v2x)/delta2
    pB1=(v1y*u2x-v1x*u2y)/delta2

    if nA1%1==0 and pA1%1==0 and nB1%1==0 and pB1%1==0:
        return False
    else:
        return True


def fit_spot_position(params,xy_mes,xy_th):
    #ecart entre positions theoriques et mesurees pour des noeuds d'un reseau
    a,bsa,gamma,theta,x0,y0 = params

    uth = np.deg2rad(theta)
    vth = np.deg2rad(theta+gamma)
    ux = a*np.cos(uth)
    uy = a*np.sin(uth)
    b = a*bsa
    vx = a*np.cos(vth)
    vy = a*np.sin(vth)






def guess_lattice(plot, x0, y0):
    """guess the nearest node from position (x0, y0) found among all lattices drawn in plot"""
    from xrdutils.latticetools import LatticeGrid as OLatticeGrid
    from xrdutils.latticetools import ReconstructionGrid as OReconstructionGrid
    lattices = [item for item in plot.get_items() if (isinstance(item,LatticeGrid)  or isinstance(item, ReconstructionGrid)
                                                      or isinstance(item,OLatticeGrid)  or isinstance(item, OReconstructionGrid))]

    dmin = np.inf

    lattice_opt = None
    idom_opt = None

    for lattice in lattices:
        for idom in range(lattice.ndomains):
            hx, kx = lattice.get_node_coordinates(x0, y0, idom)
            h = np.round(hx)
            k = np.round(kx)
            _x0,_y0  = lattice.get_plot_coordinates(h, k, idom)
            d = (x0 - _x0)**2 + (y0 - _y0)**2
            if d < dmin:
                dmin = d
                lattice_opt = lattice
                idom_opt = idom
    return (lattice_opt, idom_opt)

def guess_directlattice(plot, x0, y0):
    """guess the nearest node from position (x0, y0) found among all lattices drawn in plot"""
    from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
    from xrdutils.latticetools import DirectReconstructionGrid as ODirectReconstructionGrid
    lattices = [item for item in plot.get_items() if (isinstance(item,DirectLatticeGrid)  or isinstance(item, DirectReconstructionGrid)
                                                      or isinstance(item,ODirectLatticeGrid)  or isinstance(item, ODirectReconstructionGrid))]

    dmin = np.inf

    lattice_opt = None
    iatom_opt = None

    for lattice in lattices:
        natoms = lattice.atoms.shape[0]
        for iatom in range(natoms):
            hx, kx = lattice.get_node_coordinates(x0, y0, iatom)
            h = np.round(hx)
            k = np.round(kx)
            _x0,_y0  = lattice.get_plot_coordinates(h, k, iatom)
            d = (x0 - _x0)**2 + (y0 - _y0)**2
            if d < dmin:
                dmin = d
                lattice_opt = lattice
                iatom_opt = iatom
    return (lattice_opt, iatom_opt)

def set_identical_shapeparam(item1, item2):
    #take shapeparam of item1 and apply to item2
    shapeparam1 = item1.shapeparam
    shapeparam2 = item2.shapeparam
    shapeparam1.update_param(item1) #update shapeparam1 from item1
    shapeparam1.update_shape(item2) #update item2 from shapeparam1
    shapeparam2.update_param(item2) #update shapeparam2

class MyItemParameters(ItemParameters):
    """a derived class that return the result of DataSetGroup().edit"""
    def edit(self, plot, title, icon):
        paramdict = self.paramdict.copy()
        ending_parameters = []
        for key in self.ENDING_PARAMETERS:
            if key in paramdict:
                ending_parameters.append(paramdict.pop(key))
        parameters = list(paramdict.values())+ending_parameters
        dset = DataSetGroup(parameters, title = title.rstrip('.'), icon = icon)
        doit = dset.edit(parent = plot, apply = lambda dset: self.update(plot))
        if doit:
            self.update(plot)
        return doit


class ImageNan(ImageItem):
    """a derived class with a better management of nan"""
    def get_histogram(self, nbins):
        """interface de IHistDataSource"""
        if self.data is None:
            return [0,], [0,1]
        if self.histogram_cache is None or nbins !=   self.histogram_cache[0].shape[0]:
            #from guidata.utils import tic, toc
            if False:
                #tic("histo1")
                res = np.histogram(self.data[~np.isnan(self.data)], nbins)
                #toc("histo1")
            else:
                #TODO: _histogram is faster, but caching is buggy
                # in this version
                #tic("histo2")
                _min = _nanmin(self.data)
                _max = _nanmax(self.data)
                if self.data.dtype in (np.float64, np.float32):
                    bins = np.unique(np.array(np.linspace(_min, _max, nbins+1),
                                              dtype = self.data.dtype))
                else:
                    bins = np.arange(_min, _max+2,
                                     dtype = self.data.dtype)
                res2 = np.zeros((bins.size+1,), np.uint32)
                _histogram(self.data.flatten(), bins, res2)
                #toc("histo2")
                res = res2[1:-1], bins
            self.histogram_cache = res
        else:
            res = self.histogram_cache
        return res

    def hit_test(self, pos):
        plot = self.plot()
        ax = self.xAxis()
        ay = self.yAxis()
        _sd, _h, _i, _u = self.border_rect.poly_hit_test(plot, ax, ay, pos)
        if _i:
            #check if self.data is not nan at this point
            x0 = plot.invTransform(ax, pos.x())
            y0 = plot.invTransform(ay, pos.y())
            if not np.isfinite(self.get_data(x0, y0)):
                _i = False
        return _sd, _h, _i, _u

class MaskedArea:
    """Defines masked areas for a masked image item"""
    """geometry can be rectangular, elliptical or polygonal"""
    """mask can be applied inside or outside the shape"""

    def __init__(self, geometry=None, pts = None,  inside=None, mask=None, gradient =None):
        self.geometry = geometry
        self.pts = pts
        self.inside = inside
        self.mask = mask

    def __eq__(self, other):
        return (self.geometry == other.geometry and np.array_equal(self.pts, other.pts) and
               self.inside == other.inside and self.mask == self.mask)

    def serialize(self, writer):
        """Serialize object to HDF5 writer"""
        for name in ('geometry', 'inside', 'mask', 'pts'):
            writer.write(getattr(self, name), name)

    def deserialize(self, reader):
        """Deserialize object from HDF5 reader"""
        self.geometry = reader.read('geometry')
        self.inside = reader.read('inside')
        self.mask = reader.read('mask')
        self.pts =  reader.read(group_name='pts', func=reader.read_array)


class MaskedImageNan(MaskedImageItem):
    """a derived class with a better management of nan"""
    def load_data(self, lut_range=None):
        """
        Load data from *filename* and eventually apply specified lut_range
        *filename* has been set using method 'set_filename'
        """
        data = io.imread(self.get_filename(), to_grayscale=True)
        data = np.flipud(data)
        self.set_data(data, lut_range=lut_range)

    def get_histogram(self, nbins):
        """interface de IHistDataSource"""
        if self.data is None:
            return [0,], [0,1]
        if self.histogram_cache is None or nbins !=   self.histogram_cache[0].shape[0]:
            #from guidata.utils import tic, toc
            if False:
                #tic("histo1")
                res = np.histogram(self.data[~np.isnan(self.data)], nbins)
                #toc("histo1")
            else:
                #TODO: _histogram is faster, but caching is buggy
                # in this version
                #tic("histo2")
                _min = _nanmin(self.data)
                _max = _nanmax(self.data)
                if self.data.dtype in (np.float64, np.float32):
                    bins = np.unique(np.array(np.linspace(_min, _max, nbins+1),
                                              dtype = self.data.dtype))
                else:
                    bins = np.arange(_min, _max+2,
                                     dtype = self.data.dtype)
                res2 = np.zeros((bins.size+1,), np.uint32)
                _histogram(self.data.flatten(), bins, res2)
                #toc("histo2")
                res = res2[1:-1], bins
            self.histogram_cache = res
        else:
            res = self.histogram_cache
        return res

    def hit_test(self, pos):
        plot = self.plot()
        ax = self.xAxis()
        ay = self.yAxis()
        _sd, _h, _i, _u = self.border_rect.poly_hit_test(plot, ax, ay, pos)
        if _i:
            #check if self.data is not nan at this point
            x0 = plot.invTransform(ax, pos.x())
            y0 = plot.invTransform(ay, pos.y())
            if not np.isfinite(self.get_data(x0, y0)):
                _i = False
        return _sd, _h, _i, _u

    def add_masked_area(self, geometry, pts, inside, mask):
        area = MaskedArea(geometry=geometry, pts=pts, inside=inside, mask=mask, gradient=None)
        for _area in self._masked_areas:
            if area == _area:
                return
        self._masked_areas.append(area)

    def mask_rectangular_area(self, x0, y0, x1, y1, inside=True,
                              trace=True, do_signal=True):
        """
        Mask rectangular area
        If inside is True (default), mask the inside of the area
        Otherwise, mask the outside
        """
        ix0, iy0, ix1, iy1 = self.get_closest_index_rect(x0, y0, x1, y1)
        if inside:
            self.data[iy0:iy1, ix0:ix1] = np.ma.masked
        else:
            indexes = np.ones(self.data.shape, dtype=bool)
            indexes[iy0:iy1, ix0:ix1] = False
            self.data[indexes] = np.ma.masked
        if trace:
            self.add_masked_area('rectangular', np.array([[x0, y0],[x1, y0],[x1, y1],[x0, y1]]), inside, mask=True)
        if do_signal:
            self._mask_changed()

    def mask_circular_area(self, x0, y0, x1, y1, inside=True,
                           trace=True, do_signal=True):
        """
        Mask circular area, inside the rectangle (x0, y0, x1, y1), i.e.
        circle with a radius of .5*(x1-x0)
        If inside is True (default), mask the inside of the area
        Otherwise, mask the outside
        """
        ix0, iy0, ix1, iy1 = self.get_closest_index_rect(x0, y0, x1, y1)
        xc, yc = .5*(x0+x1), .5*(y0+y1)
        radius = .5*(x1-x0)
        xdata, ydata = self.get_x_values(ix0, ix1), self.get_y_values(iy0, iy1)
        for ix in range(ix0, ix1):
            for iy in range(iy0, iy1):
                distance = np.sqrt((xdata[ix-ix0]-xc)**2+(ydata[iy-iy0]-yc)**2)
                if inside:
                    if distance <= radius:
                        self.data[iy, ix] = np.ma.masked
                elif distance > radius:
                    self.data[iy, ix] = np.ma.masked
        if not inside:
            self.mask_rectangular_area(x0, y0, x1, y1, inside, trace=False)
        if trace:
            xc=(x0+x1)/2.
            yc=(y0+y1)/2.
            dx=abs(x1-x0)/2.
            dy=abs(y1-y0)/2.
            self.add_masked_area('circular', np.array([[xc, yc+dy],[xc, yc-dy],[xc+dx, yc],[xc-dx, yc]]), inside, mask=True)
        if do_signal:
            self._mask_changed()

    def mask_polygonal_area(self, pts, inside=True,
                           trace=True, do_signal=True):
        """
        Mask polygonal area, inside the rectangle (x0, y0, x1, y1)
        #points is a np array of the polygon points
        """
        x0,y0=np.min(pts,axis=0)
        x1,y1=np.max(pts,axis=0)

        if not self.plot():
            return

        #we construct a QpolygonF to use the containsPoint function of PyQt
        poly = QPolygonF()

        for i in range(pts.shape[0]):
          poly.append(QPointF(pts[i, 0], pts[i, 1]))

        ix0, iy0, ix1, iy1 = self.get_closest_index_rect(x0, y0, x1, y1)

        xdata, ydata = self.get_x_values(ix0, ix1), self.get_y_values(iy0, iy1)  #values in the axis referential
        for ix in range(ix0, ix1):
            for iy in range(iy0, iy1):
                inside_poly = poly.containsPoint(QPointF(xdata[ix-ix0],ydata[iy-iy0]), Qt.OddEvenFill)

                if inside:
                  if inside_poly:
                    self.data[iy, ix] = np.ma.masked
                elif not inside_poly:
                  self.data[iy, ix] = np.ma.masked
        if not inside:
            self.mask_rectangular_area(x0, y0, x1, y1, inside, trace=False)
        if trace:
            self.add_masked_area('polygonal', pts, inside, mask=True)
        if do_signal:
            self._mask_changed()

class PlotItemBuilder(GuiPlotItemBuilder):
    """derived class to easily build imagenan"""
    def __init__(self):
        super().__init__()

    def imagenan(self, data = None, filename = None, title = None, alpha_mask = None,
              alpha = None, background_color = None, colormap = None,
              xdata = [None, None], ydata = [None, None],
              pixel_size = None, center_on = None,
              interpolation = 'nearest', eliminate_outliers = None,
              readonly = False,
              xformat = '%.1f', yformat = '%.1f', zformat = '%.1f'):
        """
        Make an image `plot item` from data
        (:py:class:`guiqwt.image.ImageItem` object or
        :py:class:`guiqwt.image.RGBImageItem` object if data has 3 dimensions)
        """
        assert isinstance(xdata, (tuple, list)) and len(xdata) ==   2
        assert isinstance(ydata, (tuple, list)) and len(ydata) ==   2
        param = ImageParam(title = _("Image"), icon = 'image.png')
        data, filename, title = self._get_image_data(data, filename, title,
                                                     to_grayscale = True)
        data = np.flipud(data)
        if data.ndim ==   3:
            return self.rgbimage(data = data, filename = filename, title = title,
                                 alpha_mask = alpha_mask, alpha = alpha)
        assert data.ndim ==   2, "Data must have 2 dimensions"
        if pixel_size is None:
            assert center_on is None, "Ambiguous parameters: both `center_on`"\
                                      " and `xdata`/`ydata` were specified"
            xmin, xmax = xdata
            ymin, ymax = ydata
        else:
            xmin, xmax, ymin, ymax = self.compute_bounds(data, pixel_size,
                                                         center_on)
        self.__set_image_param(param, title, alpha_mask, alpha, interpolation,
                               background = background_color,
                               colormap = colormap,
                               xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax,
                               xformat = xformat, yformat = yformat,
                               zformat = zformat)
        image = ImageNan(data, param)
        image.set_readonly(readonly)
        image.set_filename(filename)
        if eliminate_outliers is not None:
            image.set_lut_range(lut_range_threshold(image, 256,
                                                    eliminate_outliers))
        return image

    def maskedimagenan(self, data=None, mask=None, filename=None, title=None, alpha_mask=False, alpha=1.0,
                    xdata=[None, None], ydata=[None, None], pixel_size=None, center_on=None, background_color=None,
                    colormap=None, show_mask=False, fill_value=None, interpolation="nearest", eliminate_outliers=None,
                    readonly=False, xformat="%.1f", yformat="%.1f", zformat="%.1f"):
        """
        Make a masked image `plot item` from data
        (:py:class:`guiqwt.image.MaskedImageItem` object)
        """
        assert isinstance(xdata, (tuple, list)) and len(xdata) == 2
        assert isinstance(ydata, (tuple, list)) and len(ydata) == 2
        param = MaskedImageParam(title=_("Image"), icon="image.png")
        data, filename, title = self._get_image_data(
            data, filename, title, to_grayscale=True
        )
        data = np.flipud(data)
        assert data.ndim == 2, "Data must have 2 dimensions"
        if pixel_size is None:
            assert center_on is None, (
                "Ambiguous parameters: both `center_on`"
                " and `xdata`/`ydata` were specified"
            )
            xmin, xmax = xdata
            ymin, ymax = ydata
        else:
            xmin, xmax, ymin, ymax = self.compute_bounds(data, pixel_size, center_on)
        self.__set_image_param(param, title, alpha_mask, alpha, interpolation,
            background=background_color, colormap=colormap, xmin=xmin, xmax=xmax,
            ymin=ymin, ymax=ymax, show_mask=show_mask, fill_value=fill_value,
            xformat=xformat, yformat=yformat, zformat=zformat)
        image = MaskedImageNan(data, mask, param)
        image.set_readonly(readonly)
        image.set_filename(filename)
        if eliminate_outliers is not None:
            image.set_lut_range(lut_range_threshold(image, 256, eliminate_outliers))
        return image

make = PlotItemBuilder()

class LatticeGridParam(DataSet):
    """Set LatticeGrid parameters"""
    th = '\u03B8'
    O0 = FloatItem(_("Ox"), default = 00.)
    O1 = FloatItem(_("Oy"), default = 00.).set_pos(col = 1)

    RT = BoolItem(_("(R,"+th+")"), default = False)
    vm = BoolItem(_("v master"), default = False).set_pos(col = 1)

    ux = FloatItem(_("ux"), default = 100.)
    uR = FloatItem(_("uR"), default = 100.).set_pos(col = 1)
    uy = FloatItem(_("uy"), default = 100.)
    uT = FloatItem(_("u"+th), default = 100.).set_pos(col = 1)

    vx = FloatItem(_("vx"), default = 100.)
    vR = FloatItem(_("vR"), default = 100.).set_pos(col = 1)

    vy = FloatItem(_("vy"), default = 100.)
    vT = FloatItem(_("v"+th), default = 100.).set_pos(col = 1)

    sg = ImageChoiceItem(_("SpaceGroup"), SPACEGROUP_CHOICES,
                             default = "NoSymbol")
    can_move = BoolItem(_("movable"), default = True)
    centered = BoolItem(_("centered cell"), default = False)
    maxnpt = IntItem(_("maximum points drawn"), default = 1000)

    def update_param(self, obj):
        self.O0 = obj.O[0]
        self.O1 =  obj.O[1]
        self.ux = obj.u[0]
        self.uy =  obj.u[1]
        self.vx = obj.v[0]
        self.vy = obj.v[1]
        self.uR = obj.uR
        self.uT =  obj.uT
        self.vR = obj.vR
        self.vT = obj.vT
        self.sg = obj.sg
        self.can_move = obj.can_move()
        self.centered = obj.centered
        self.maxnpt = obj.maxnpt

    def update_shape(self, obj):

        obj.O[0] = self.O0
        obj.O[1] = self.O1

        obj.u[0] = self.ux
        obj.u[1] = self.uy
        obj.v[0] = self.vx
        obj.v[1] = self.vy
        obj.uR = self.uR
        obj.uT = self.uT
        obj.vR = self.vR
        obj.vT = self.vT

        obj.sg = self.sg
        if self.vm:
            obj.set_space_group_constraints(master = 'v',RT = self.RT)
        else:
            obj.set_space_group_constraints(master = 'u',RT = self.RT)

        obj.set_movable(self.can_move)
        obj.centered = self.centered
        obj.maxnpt = self.maxnpt

        #self.update_param(obj)

        obj.set_matrix()  #here we set also slaves

        for slave in obj.slaves:
              slave.set_matrix()

class DirectLatticeGridParam(DataSet):
    """Set LatticeGrid parameters"""
    th = '\u03B8'
    O0 = FloatItem(_("Ox"), default = 00.)
    O1 = FloatItem(_("Oy"), default = 00.).set_pos(col = 1)

    RT = BoolItem(_("(R,"+th+")"), default = False)
    vm = BoolItem(_("v master"), default = False).set_pos(col = 1)

    ux = FloatItem(_("ux"), default = 100.)
    uR = FloatItem(_("uR"), default = 100.).set_pos(col = 1)
    uy = FloatItem(_("uy"), default = 100.)
    uT = FloatItem(_("u"+th), default = 100.).set_pos(col = 1)

    vx = FloatItem(_("vx"), default = 100.)
    vR = FloatItem(_("vR"), default = 100.).set_pos(col = 1)

    vy = FloatItem(_("vy"), default = 100.)
    vT = FloatItem(_("v"+th), default = 100.).set_pos(col = 1)

    sg = ImageChoiceItem(_("SpaceGroup"), SPACEGROUP_CHOICES,
                             default = "NoSymbol")
    can_move = BoolItem(_("movable"), default = True)
    maxrad = IntItem(_("maximum radius"), default = 100)
    natoms = IntItem(_("atoms per unit cell"), default = 1)
    atoms = FloatArrayItem(_("atomic positions"),np.zeros((1,2)))

    def update_param(self, obj):
        self.O0 = obj.O[0]
        self.O1 =  obj.O[1]
        self.ux = obj.u[0]
        self.uy =  obj.u[1]
        self.vx = obj.v[0]
        self.vy = obj.v[1]
        self.uR = obj.uR
        self.uT =  obj.uT
        self.vR = obj.vR
        self.vT = obj.vT
        self.sg = obj.sg
        self.can_move = obj.can_move()
        self.maxrad = obj.maxrad
        self.natoms =  obj.atoms.shape[0]
        self.atoms =  obj.atoms

    def update_shape(self, obj):

        obj.O[0] = self.O0
        obj.O[1] = self.O1

        obj.u[0] = self.ux
        obj.u[1] = self.uy
        obj.v[0] = self.vx
        obj.v[1] = self.vy
        obj.uR = self.uR
        obj.uT = self.uT
        obj.vR = self.vR
        obj.vT = self.vT

        obj.sg = self.sg

        if self.sg in ["cm","c2mm"]:
            obj.centered == True
        else:
            obj.centered == False

        if self.vm:
            obj.set_space_group_constraints(master = 'v',RT = self.RT)
        else:
            obj.set_space_group_constraints(master = 'u',RT = self.RT)

        obj.set_movable(self.can_move)
        obj.maxrad = self.maxrad

        #self.update_param(obj)

        obj.set_matrix()  #here we set also slaves

        #we fill the atoms of the directlatticegrid with zeros, and update the value with the float in the np array self.atoms
        atoms = np.zeros((self.natoms,2))
        n = min(self.natoms,self.atoms.shape[0])
        atoms[:n] = self.atoms[:n]
        self.atoms = atoms

        obj.atoms = atoms

        for slave in obj.slaves:
              slave.set_matrix()




def check_already_done(M,dataset):
    #check if a matrix has already been computed
    #we assume that M[0,0]>=0,M[1,0]>=0,M[0,1]>=0
    #we perform several operations on M: M1 = MS.M
    #if M1 < M and M1[0]>=0 then return true
    n = np.max(abs(M))

    if M[0,1] == 0:
        #we check the matrix A-B,B or A+B,B
        if M[1,0]-abs(M[1,1])>=0:
            return True

    else:  #M[0,1] > 0, we check the matrix A-B,B
        if M[1,0]-M[1,1]<0 or M[1,0]-M[1,1]>n:
            return True

    #we check the matrix A,B-A
    if M[0,1]-M[0,0]>=0 and M[1,1]-M[1,0]>=-n:
        return True

    #we check the matrix B,A
    if M[1,1]>=0:
        if M[0,0]>M[0,1]:
            return True
        elif M[0,0]==M[0,1]:
            if M[1,0]>M[1,1]:
                return True

    return False







def guess_reconstruction(dataset,item,value,parent):
    #explore all possibilities of reconstruction, starting from
    #A=[1,0],B=[0,1], we assume that Ax>=0, Ay>=0
    #print (dataset,item,value,parent)
    M = np.matrix([[dataset.A0,dataset.B0,0],
                   [dataset.A1,dataset.B1,0],
                   [0         ,0         ,1]])
    n = np.max(abs(M))
    doit=True
    while doit:
        M[1,1]=M[1,1]+1    #increase B1
        if M[1,1]>n:
            M[1,1]=-n
            M[0,1]=M[0,1]+1     #increase B0
            if M[0,1]>n:
                M[0,1]=0
                M[1,0]=M[1,0]+1   #increase A1
                if M[1,0]>n:
                    M[1,0]=0
                    M[0,0]=M[0,0]+1   #increase A0
                    if M[0,0]>n:
                        n=n+1
                        M=np.matrix([[0,0],[0,-n]])
        if np.max(np.abs(M)) == n:
            if np.linalg.det(M)!=0:
                doit = check_already_done(M,dataset)   #check if any equivalent matrix has been already studied


    dataset.A0 = M[0,0]
    dataset.A1 = M[1,0]
    dataset.B0 = M[0,1]
    dataset.B1 = M[1,1]

    widget=parent
    from guidata.dataset.qtwidgets import DataSetGroupEditDialog
    doit = True
    while doit:
        if isinstance(widget,DataSetGroupEditDialog):
            doit = False
            done = True
        else:
            try:
                widget=widget.parent()
            except:
                doit = False
                done = False
    if done:
        widget.apply_func(dataset)







class ReconstructionGridParam(DataSet):
    """Set ReconstructionGrid parameters"""
    A0 = FloatItem(_("Ax"), default = 2.)
    B0 = FloatItem(_("Bx"), default = 0.).set_pos(col = 1)
    A1 = FloatItem(_("Ay"), default = 0.)
    B1 = FloatItem(_("By"), default = 2.).set_pos(col = 1)
    master = ChoiceItem(_("master lattice"),(None,'None'))
    sym = BoolItem(_("symmetrical domains"), default = False)
    can_move = BoolItem(_("movable"), default = False)
    centered = BoolItem(_("centered cell"), default = False)
    maxnpt = IntItem(_("maximum points drawn"), default = 1000)
    guess = ButtonItem(_("try another reconstruction"), guess_reconstruction,default=0)
    """
    Construct a simple button that calls a method when hit
        * label [string]: text shown on the button
        * callback [function]: function with four parameters (dataset, item, value, parent)
            - dataset [DataSet]: instance of the parent dataset
            - item [DataItem]: instance of ButtonItem (i.e. self)
            - value [unspecified]: value of ButtonItem (default ButtonItem
              value or last value returned by the callback)
            - parent [QObject]: button's parent widget
        * icon [QIcon or string]: icon show on the button (optional)
          (string: icon filename as in guidata/guiqwt image search paths)
        * default [unspecified]: default value passed to the callback (optional)
        * help [string]: text shown in button's tooltip (optional)
        * check [bool]: if False, value is not checked (optional, default=True)
    """

    def update_param(self, obj):
        self.A0 = obj.A[0]
        self.B0 = obj.B[0]
        self.A1 = obj.A[1]
        self.B1 = obj.B[1]
        self.can_move = obj.can_move()
        self.centered = obj.centered
        self.master = obj.master
        self.sym = obj.sym
        self.maxnpt = obj.maxnpt

    def update_shape(self, obj):
        obj.A[0] = self.A0
        obj.B[0] = self.B0
        obj.A[1] = self.A1
        obj.B[1] = self.B1

        if (self.A0*self.B1-self.A1*self.B0) == 0:
            return

        obj.set_movable(self.can_move)
        obj.centered = self.centered
        obj.sym = self.sym
        obj.maxnpt = self.maxnpt

        if self.master !=  obj.master:
            obj.set_master(self.master)

        obj.set_domains()
        obj.set_matrix()


    def update_choices(self, lattices):
        #lattices is the list of lattices in the plot
        _choices = [self._items[4]._normalize_choice(None,'None')]
        for lattice in lattices:
            label = lattice.shapeparam.label
            _choices.append(self._items[4]._normalize_choice(lattice, label))
        self._items[4].set_prop("data", choices = _choices)

class DirectReconstructionGridParam(DataSet):
    """Set ReconstructionGrid parameters"""
    O0 = FloatItem(_("Ox"), default = 00.)
    O1 = FloatItem(_("Oy"), default = 00.).set_pos(col = 1)
    A0 = FloatItem(_("Ax"), default = 2.)
    B0 = FloatItem(_("Bx"), default = 0.).set_pos(col = 1)
    A1 = FloatItem(_("Ay"), default = 0.)
    B1 = FloatItem(_("By"), default = 2.).set_pos(col = 1)
    master = ChoiceItem(_("master lattice"),(None,'None'))
    can_move = BoolItem(_("movable"), default = False)
    force_master = BoolItem(_("force master"), default = False).set_pos(col = 1)
    maxrad = FloatItem(_("maximum radius"), default = 200)
    natoms = IntItem(_("atoms per unit cell"), default = 1)
    atoms = FloatArrayItem(_("atomic positions"),np.zeros((1,2)))

    def update_param(self, obj):
        self.O0 = obj.O[0]
        self.O1 =  obj.O[1]
        self.A0 = obj.A[0]
        self.B0 = obj.B[0]
        self.A1 = obj.A[1]
        self.B1 = obj.B[1]
        self.can_move = obj.can_move()
        self.force_master = obj.force_master
        self.master = obj.master
        self.maxrad = obj.maxrad
        self.natoms = len(obj.atoms)
        self.atoms =  obj.atoms

    def update_shape(self, obj):
        obj.O[0] = self.O0
        obj.O[1] = self.O1
        obj.A[0] = self.A0
        obj.B[0] = self.B0
        obj.A[1] = self.A1
        obj.B[1] = self.B1

        if (self.A0*self.B1-self.A1*self.B0) == 0:
            return

        obj.set_movable(self.can_move)
        obj.maxrad = self.maxrad

        if self.master !=  obj.master:
            obj.set_master(self.master)
        obj.force_master = self.force_master

        obj.set_matrix()
        atoms = np.zeros((self.natoms,2))
        n = min(self.natoms,self.atoms.shape[0])
        atoms[:n] = self.atoms[:n]

        self.atoms = atoms
        obj.atoms = atoms

    def update_choices(self, lattices):
        #lattices is the list of lattices in the plot
        _choices = [self._items[6]._normalize_choice(None,'None')]
        for lattice in lattices:
            label = lattice.shapeparam.label
            _choices.append(self._items[6]._normalize_choice(lattice, label))
        self._items[6].set_prop("data", choices = _choices)

class UpdateProp(ItemProperty):
    """An 'operator property'
    prop: ItemProperty instance
    func: function
    """
    def __init__(self, prop = None, func = None):
        self.property = prop
        self.function = func

    def __call__(self, instance, item, value):   #DataSet instance, DataItem instance, value of the dataitem
        self.function(instance, item, value)
        return self

def update_domain(latticeparam, choice_item, latticedomain):
    """update a SpotShapeParam instance parameters when lattice has been changed"""
    if latticeparam.lattice is not None:
        if latticedomain is not None:
            hx, kx = latticeparam.lattice.get_node_coordinates(latticeparam.x0, latticeparam.y0, latticedomain)
            h = np.round(hx)
            k = np.round(kx)
            latticeparam._items[2].set_from_string(latticeparam, '%d'%h)
            latticeparam._items[3].set_from_string(latticeparam, '%d'%k)
            latticeparam._items[4].set_from_string(latticeparam, '%g'%hx)
            latticeparam._items[5].set_from_string(latticeparam, '%g'%kx)

def update_lattice(latticeparam, choice_item, lattice):
    """update a SpotShapeParam instance parameters when lattice has been changed"""
    if lattice is not None:
        #update the value of h and k with the corresponding lattice
        latticeparam.latticedomain = min(latticeparam.latticedomain,lattice.ndomains-1)
        latticeparam.update_domain_choices(lattice)
        #hx, kx = lattice.get_node_coordinates(latticeparam.x0, latticeparam.y0, self.latticedomain)
        if latticeparam.latticedomain is not None:
            hx, kx = lattice.get_node_coordinates(latticeparam.x0, latticeparam.y0, latticeparam.latticedomain)
            h = np.round(hx)
            k = np.round(kx)
            latticeparam._items[2].set_from_string(latticeparam, '%d'%h)
            latticeparam._items[3].set_from_string(latticeparam, '%d'%k)
            latticeparam._items[4].set_from_string(latticeparam, '%g'%hx)
            latticeparam._items[5].set_from_string(latticeparam, '%g'%kx)

def update_atom(latticeparam, choice_item, latticeatom):
    """update a SpotShapeParam instance parameters when lattice has been changed"""
    if latticeparam.lattice is not None:
        if latticeatom is not None:
            hx, kx = latticeparam.lattice.get_node_coordinates(latticeparam.x0, latticeparam.y0, latticeatom)
            h = np.round(hx)
            k = np.round(kx)
            latticeparam._items[2].set_from_string(latticeparam, '%d'%h)
            latticeparam._items[3].set_from_string(latticeparam, '%d'%k)
            latticeparam._items[4].set_from_string(latticeparam, '%g'%hx)
            latticeparam._items[5].set_from_string(latticeparam, '%g'%kx)

def update_directlattice(latticeparam, choice_item, lattice):
    """update a SpotShapeParam instance parameters when lattice has been changed"""
    if lattice is not None:
        #update the value of h and k with the corresponding lattice
        latticeparam.latticeatom = min(latticeparam.latticeatom,lattice.atoms.shape[0]-1)
        latticeparam.update_atom_choices(lattice)
        #hx, kx = lattice.get_node_coordinates(latticeparam.x0, latticeparam.y0, self.latticedomain)
        if latticeparam.latticeatom is not None:
            hx, kx = lattice.get_node_coordinates(latticeparam.x0, latticeparam.y0, latticeparam.latticeatom)
            h = np.round(hx)
            k = np.round(kx)
            latticeparam._items[2].set_from_string(latticeparam, '%d'%h)
            latticeparam._items[3].set_from_string(latticeparam, '%d'%k)
            latticeparam._items[4].set_from_string(latticeparam, '%g'%hx)
            latticeparam._items[5].set_from_string(latticeparam, '%g'%kx)


class SpotShapeParam(DataSet):
    """DataSet for defining parameters of a SpotShape instance"""
    x0 = FloatItem(_("x0"), default = 1.)
    y0 = FloatItem(_("y0"), default = 0.)
    h = IntItem(_("h_integer"), default = 0)
    k = IntItem(_("k_integer"), default = 0)
    hx = FloatItem(_("h_float"), default = 0.)
    kx = FloatItem(_("k_float"), default = 0.)
    lattice = ChoiceItem(_("associated lattice"),(None,'None'))     #instance of LatticeGrid or ReconstructionGrid
    lattice.set_prop("display", callback = UpdateProp(func = update_lattice))
    latticedomain = ChoiceItem(_("domain index"),(None,'None'))
    latticedomain.set_prop("display", callback = UpdateProp(func = update_domain))

    def update_param(self, obj):
        self.x0 = obj.x0
        self.y0 = obj.y0
        self.h = obj.h
        self.k = obj.k
        self.hx = obj.hx
        self.kx = obj.kx
        self.lattice = obj.lattice
        self.latticedomain = obj.latticedomain

    def update_shape(self, obj):
        obj.set_pos(self.x0, self.y0)
        obj.h = self.h
        obj.k = self.k
        obj.hx = self.hx
        obj.kx = self.kx
        obj.lattice = self.lattice
        obj.latticedomain = self.latticedomain
        self.update_param(obj)

    def update_choices(self,lattices):
        #lattices is the list of lattices and reconstructions in the plot
        _choices = [self._items[6]._normalize_choice(None,'None')]     # self._items[6] is a 'guidata.dataset.dataitems.ChoiceItem
        for lattice in lattices:
            label = lattice.shapeparam.label
            _choices.append(self._items[6]._normalize_choice(lattice, label))
        self._items[6].set_prop("data", choices = _choices)    #we update the property of the associated widget

    def update_domain_choices(self, lattice):
        #when a LatticeGrid/ReconstructionGrid instance has been chosen, update the choices of domains
        if lattice is None:
            _choices = [(None,'None')]
        else:
            _choices = []
            for idom in range(lattice.ndomains):
                _choices.append(self._items[7]._normalize_choice(idom,'%d'%idom))

        self._items[7].set_prop("data", choices = _choices)


class SpotShape(PointShape):
    """a shape indicating a point in the direct space or reciprocal space
       derived from PointShape with (h,k) reference to a lattice (latticegrid or reconstructiongrid)"""
    CLOSED = False
    def __init__(self, x0 = 0, y0 = 0, shapeparam = None, latticeparam = None, lattice = None, latticedomain = None, h = 0, k = 0, hx = 0., kx = 0.):
        super().__init__(x0,y0,shapeparam = shapeparam)
        self.x0 = x0          #position in plot coordinates
        self.y0 = y0

        self.lattice = lattice  #must be an instance of LatticeGrid or ReconstructionGrid
        self.latticedomain = latticedomain

        self.h = h  #integer value of spot position in the lattice referential
        self.k = k
        self.hx = hx #real value of spot position in the lattice referential
        self.kx = kx

        if latticeparam is None:
            self.latticeparam = SpotShapeParam(_("SpotShape"), icon = "point.png")
        else:
            self.latticeparam = latticeparam
            self.latticeparam.update_shape(self)

    def move_point_to(self, handle, pos, ctrl = None):
        pass
        #nx, ny = pos
        #self.points[0] = (nx, ny)

    def set_pos(self, x0, y0):
        """Set the point coordinates to (x, y)"""
        self.set_points([(x0, y0)])
        self.x0 = x0
        self.y0 = y0

    #pickle methods
    def __reduce__(self):
        self.shapeparam.update_param(self)
        self.latticeparam.update_param(self)
        state = (self.shapeparam, self.points, self.latticeparam, self.z())
        return (SpotShape, (), state)

    def __getstate__(self):
        return self.shapeparam, self.points, self.latticeparam, self.z()

    def __setstate__(self, state):
        param, points, latticeparam, z = state
        self.points = points
        self.setZ(z)
        self.shapeparam = param
        self.latticeparam = latticeparam
        self.shapeparam.update_shape(self)
        self.latticeparam.update_shape(self)

    def guess(self):
        #estimate latticeparams from nearest spot found in all present lattice
        self.lattice, self.latticedomain = guess_lattice(self.plot(), self.x0, self.y0)

    def get_item_parameters(self, itemparams):

        self.shapeparam.update_param(self)
        itemparams.add("ShapeParam", self, self.shapeparam)

        if self.plot()!=None:
            from xrdutils.latticetools import LatticeGrid as OLatticeGrid
            from xrdutils.latticetools import ReconstructionGrid as OReconstructionGrid
            lattices = [item for item in self.plot().get_items() if (isinstance(item,LatticeGrid)  or isinstance(item, ReconstructionGrid)
                                                  or isinstance(item,OLatticeGrid)  or isinstance(item, OReconstructionGrid))]
            self.latticeparam.update_choices(lattices)
        else:
            print (self,"has not been added to a plot, lattice cannot be set")
        self.latticeparam.update_param(self)
        itemparams.add("SpotShapeParam", self, self.latticeparam)

    def set_lattice_from_latticename(self, latticename):
        #permet de recuperer le reseau a partir de son nom s'il existe
        assert self.plot() != None,"item must be added to a plot"
        from xrdutils.latticetools import LatticeGrid as OLatticeGrid
        from xrdutils.latticetools import ReconstructionGrid as OReconstructionGrid
        items = list(item for item in self.plot().get_items() if (isinstance(item, LatticeGrid) or isinstance(item, ReconstructionGrid)
                                                                   or isinstance(item,OLatticeGrid)  or isinstance(item, OReconstructionGrid)))
        itemnames = list(item.title().text() for item in items)
        if latticename in itemnames:
            self.lattice = items[itemnames.index(latticename)]

    def set_title_from_lattice(self):
        lattice = self.latticeparam.lattice
        if lattice is not None:
            latticedomain = self.latticeparam.latticedomain
            label = lattice.shapeparam.label
            title = label+"_dom_%d_(%d,%d)"%(latticedomain,self.latticeparam.h,self.latticeparam.k)
        else:
            title = "spot_(%g,%g)"%(self.latticeparam.x0,self.latticeparam.y0)

        self.setTitle(title)
        self.shapeparam.label = title

    def set_item_parameters(self, itemparams):
        update_dataset(self.shapeparam, itemparams.get("ShapeParam"),
                       visible_only = True)

        self.shapeparam.update_shape(self)
        self.latticeparam.update_shape(self)
        self.set_title_from_lattice()

assert_interfaces_valid(SpotShape)

class AtomShapeParam(DataSet):
    """DataSet for defining parameters of a SpotShape instance"""
    x0 = FloatItem(_("x0"), default = 1.)
    y0 = FloatItem(_("y0"), default = 0.)
    h = IntItem(_("h_integer"), default = 0)
    k = IntItem(_("k_integer"), default = 0)
    hx = FloatItem(_("h_float"), default = 0.)
    kx = FloatItem(_("k_float"), default = 0.)
    lattice = ChoiceItem(_("associated lattice"),(None,'None'))     #instance of LatticeGrid or ReconstructionGrid
    lattice.set_prop("display", callback = UpdateProp(func = update_directlattice))
    latticeatom = ChoiceItem(_("domain index"),(None,'None'))
    latticeatom.set_prop("display", callback = UpdateProp(func = update_atom))

    def update_param(self, obj):
        self.x0 = obj.x0
        self.y0 = obj.y0
        self.h = obj.h
        self.k = obj.k
        self.hx = obj.hx
        self.kx = obj.kx
        self.lattice = obj.lattice
        self.latticeatom = obj.latticeatom

    def update_shape(self, obj):
        obj.set_pos(self.x0, self.y0)
        obj.h = self.h
        obj.k = self.k
        obj.hx = self.hx
        obj.kx = self.kx
        obj.lattice = self.lattice
        obj.latticeatom = self.latticeatom

        self.update_param(obj)

    def update_choices(self,lattices):
        #lattices is the list of direct lattices and reconstructions in the plot
        _choices = [self._items[6]._normalize_choice(None,'None')]     # self._items[6] is a 'guidata.dataset.dataitems.ChoiceItem
        for lattice in lattices:
            label = lattice.shapeparam.label
            _choices.append(self._items[6]._normalize_choice(lattice, label))
        self._items[6].set_prop("data", choices = _choices)    #we update the property of the associated widget

    def update_atom_choices(self, lattice):
        #when a DirectLatticeGrid/DirectReconstructionGrid instance has been chosen, update the choices of atoms
        if lattice is None:
            _choices = [(None,'None')]
        else:
            _choices = []
            natoms = lattice.atoms.shape[0]

            for latticeatom in range(natoms):
                _choices.append(self._items[7]._normalize_choice(latticeatom,'%d'%latticeatom))

        self._items[7].set_prop("data", choices = _choices)

class AtomShape(PointShape):
    """a shape indicating a point in the direct space or reciprocal space
       derived from PointShape with (h,k) reference to a lattice (latticegrid or reconstructiongrid)"""
    CLOSED = False
    def __init__(self, x0 = 0, y0 = 0, shapeparam = None, latticeparam = None, lattice = None, h = 0, k = 0, hx = 0., kx = 0., latticeatom=0):
        super().__init__(x0,y0,shapeparam = shapeparam)
        self.x0 = x0          #position in plot coordinates
        self.y0 = y0

        self.lattice = lattice  #must be an instance of LatticeGrid or ReconstructionGrid
        self.latticeatom = latticeatom # indice of atom in the unit cell

        self.h = h
        self.k = k
        self.hx = hx
        self.kx = kx

        if latticeparam is None:
            self.latticeparam = AtomShapeParam(_("AtomShape"), icon = "point.png")
        else:
            self.latticeparam = latticeparam
            self.latticeparam.update_shape(self)

    def move_point_to(self, handle, pos, ctrl = None):
        pass
        #nx, ny = pos
        #self.points[0] = (nx, ny)

    def set_pos(self, x0, y0):
        """Set the point coordinates to (x, y)"""
        self.set_points([(x0, y0)])
        self.x0 = x0
        self.y0 = y0

    #pickle methods
    def __reduce__(self):
        self.shapeparam.update_param(self)
        self.latticeparam.update_param(self)
        state = (self.shapeparam, self.points, self.latticeparam, self.z())
        return (AtomShape, (), state)

    def __getstate__(self):
        return self.latticeparam, self.points, self.latticeparam, self.z()

    def __setstate__(self, state):
        param, points, latticeparam, z = state
        self.points = points
        self.setZ(z)
        self.shapeparam = param
        self.latticeparam = latticeparam
        self.latticeparam.update_shape(self)
        self.latticeparam.update_shape(self)

    def guess(self):
        #estimate latticeparams from nearest atom found in all present lattice
        self.lattice, self.latticeatom = guess_directlattice(self.plot(), self.x0, self.y0)

    def get_item_parameters(self, itemparams):

        self.shapeparam.update_param(self)
        itemparams.add("ShapeParam", self, self.shapeparam)

        if self.plot()!=None:
            from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
            from xrdutils.latticetools import DirectReconstructionGrid as ODirectReconstructionGrid
            lattices = [item for item in self.plot().get_items() if (isinstance(item,DirectLatticeGrid)  or isinstance(item, DirectReconstructionGrid)
                                                  or isinstance(item,ODirectLatticeGrid)  or isinstance(item, ODirectReconstructionGrid))]
            self.latticeparam.update_choices(lattices)
        else:
            print (self,"has not been added to a plot, lattice cannot be set")
        self.latticeparam.update_param(self)
        itemparams.add("SpotShapeParam", self, self.latticeparam)

    def set_lattice_from_latticename(self, latticename):
        #permet de recuperer le reseau a partir de son nom s'il existe
        assert self.plot() != None,"item must be added to a plot"
        from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
        from xrdutils.latticetools import DirectReconstructionGrid as ODirectReconstructionGrid
        items = list(item for item in self.plot().get_items() if (isinstance(item, DirectLatticeGrid) or isinstance(item, DirectReconstructionGrid)
                                                                   or isinstance(item,ODirectLatticeGrid)  or isinstance(item, ODirectReconstructionGrid)))
        itemnames = list(item.title().text() for item in items)
        if latticename in itemnames:
            self.lattice = items[itemnames.index(latticename)]

    def set_title_from_lattice(self):
        lattice = self.latticeparam.lattice
        if lattice is not None:
            latticeatom = self.latticeparam.latticeatom
            label = lattice.shapeparam.label
            title = label+"_atom_%d_(%d,%d)"%(latticeatom,self.latticeparam.h,self.latticeparam.k)
        else:
            title = "atom_(%g,%g)"%(self.latticeparam.x0,self.latticeparam.y0)

        self.setTitle(title)
        self.shapeparam.label = title

    def set_item_parameters(self, itemparams):
        update_dataset(self.shapeparam, itemparams.get("ShapeParam"),
                       visible_only = True)

        self.shapeparam.update_shape(self)
        self.latticeparam.update_shape(self)
        self.set_title_from_lattice()

assert_interfaces_valid(AtomShape)


class LatticeGrid(PolygonShape):
    """a shape for drawing a lattice in the reciprocal space, combining two unit cell vectors and associated grid"""
    _can_move = True
    maxnpt = 10000
    def __init__(self, O = [0,0], u = [100,0], v = [0,100], sg = 'p3m1', centered = False, shapeparam = None, latticeparam = None):
        super().__init__(shapeparam = shapeparam)
        #self.setItemInterest(QwtPlotItem.ScaleInterest, True)
        self.arrow_angle = 15 # degrees
        self.arrow_size = 10 # canvas pixels

        if shapeparam is None:
            self.shapeparam = ShapeParam(_("Shape"), icon = "rectangle.png")
        else:
            self.shapeparam = shapeparam
            self.shapeparam.update_shape(self)

        if latticeparam is None:
            self.latticeparam = LatticeGridParam(_("LatticeGrid"), icon = "rectangle.png")
        else:
            self.latticeparam = latticeparam
            self.latticeparam.update_shape(self)

        self.ndomains = 1   #only one domain

        self.O = np.array(O,float)   #lattice origin
        self.u = np.array(u,float)   #vector 1
        self.v = np.array(v,float)   #vector 2

        self.uR,self.uT = xy_to_angle(self.u[0],self.u[1])
        self.vR,self.vT = xy_to_angle(self.v[0],self.v[1])

        self.sg = sg   #space group
        self.centered = centered   #centered unit cell
        self.slaves = list()  #associated reconstructions

        self.set_matrix()

    def detach(self):
        for slave in self.slaves:
            slave.set_master(None)
        super().detach()

    #pickle methods
    def __reduce__(self):
        self.shapeparam.update_param(self)
        self.latticeparam.update_param(self)
        state = (self.shapeparam, self.z(), self.latticeparam, self.slaves)
        return (LatticeGrid, (), state)

    def __getstate__(self):
        return self.shapeparam, self.z(), self.latticeparam, self.slaves

    def __setstate__(self, state):
        #with pickle, slaves are charged
        param, z, latticeparam, slaves = state

        self.setZ(z)
        self.shapeparam = param
        self.shapeparam.update_shape(self)
        self.latticeparam = latticeparam
        self.latticeparam.update_shape(self)
        self.slaves = slaves
        for slave in self.slaves:
            slave.set_master(self)
            slave.latticeparam.update_shape(slave)

    def add_slave(self,slave):
        if slave not in self.slaves:
            self.slaves.append(slave)

    def remove_slave(self,slave):
        try:
            self.slaves.remove(slave)
        except ValueError:
            print ("cannot remove ",slave," , not found in slaves of master",self)

    def set_matrix(self):
        #set the matrix to go from plot coordinates to node coordinates (tr)
        #and the inverse matrix of it (itr)
        self.itr = np.mat([[self.u[0],self.v[0],self.O[0]]
                         ,[self.u[1],self.v[1],self.O[1]]
                         ,[0.,       0.,       1.       ]])
        self.tr = self.itr.I
        _x0, _y0 = self.get_plot_coordinates(0,0)
        _x1, _y1 = self.get_plot_coordinates(1,0)
        _x2, _y2 = self.get_plot_coordinates(0,1)
        self.points = np.array([[_x0, _y0],[_x1, _y1],[_x2, _y2]])
        for slave in self.slaves:
            slave.set_matrix()

    def get_node_coordinates(self, xplot, yplot, idom = 0):
        """Return node coordinates (from plot coordinates)"""
        v = self.tr*colvector(xplot, yplot)
        xpixel, ypixel, _ = v[:, 0].A.ravel()
        return xpixel, ypixel

    def get_plot_coordinates(self, xnode, ynode, idom = 0):
        """Return plot coordinates (from node coordinates)"""
        v0 = self.itr*colvector(xnode, ynode)
        xplot, yplot, _ = v0[:, 0].A.ravel()
        return xplot, yplot

    def set_space_group_constraints(self,master = 'u',RT = False):

        if RT:
            self.u[0],self.u[1] = angle_to_xy(self.uR,self.uT)
            self.v[0],self.v[1] = angle_to_xy(self.vR,self.vT)

        if master ==   'u':
            if self.sg in ["pm","p2mm","pg","p2mg","p2gg"]:
                #les axes doivent etre perpendiculaires
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(v2/u2)
                self.v[0] = -self.u[1]*ratio
                self.v[1] = self.u[0]*ratio

            elif self.sg in ["cm","c2mm"]:
                #les axes doivent etre orthonormes
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(u2/v2)
                self.v[0] = self.v[0]*ratio
                self.v[1] = self.v[1]*ratio

            elif self.sg in ["p4","p4mm","p4gm"]:
                #les axes doivent etre orthonormes
                self.v[0] = -self.u[1]
                self.v[1] = self.u[0]

            elif self.sg in ["p3","p3m1","p31m","p6","p6mm"]:
                #les axes doivent etre orthonormes
                self.v[0] = self.u[0]*cos_60-self.u[1]*sin_60
                self.v[1] = self.u[0]*sin_60+self.u[1]*cos_60
        else:
            if self.sg in ["pm","p2mm","pg","p2mg","p2gg"]:
                #les axes doivent etre perpendiculaires
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(u2/v2)
                self.u[0] = self.v[1]*ratio
                self.u[1] = -self.v[0]*ratio

            if self.sg in ["cm","c2mm"]:
                #les axes doivent etre orthonormes
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(v2/u2)
                self.u[0] = self.u[0]*ratio
                self.u[1] = self.u[1]*ratio

            if self.sg in ["p4","p4mm","p4gm"]:
                #les axes doivent etre orthonormes
                self.u[0] = self.v[1]
                self.u[1] = -self.v[0]

            if self.sg in ["p3","p3m1","p31m","p6","p6mm"]:
                #les axes doivent etre orthonormes
                self.u[0] = self.v[0]*cos_60+self.v[1]*sin_60
                self.u[1] = -self.v[0]*sin_60+self.v[1]*cos_60

        self.uR,self.uT = xy_to_angle(self.u[0],self.u[1])
        self.vR,self.vT = xy_to_angle(self.v[0],self.v[1])

    def move_shape(self, old_pos, new_pos):
        pass
        #super().move_shape(old_pos,new_pos)
        #for slave in self.slaves:
        #    slave.set_points()

    def move_point_to(self, handle, pos, ctrl = None):
        if self.can_move():
          if handle == 0:
              self.O = np.array(pos,float)
              self.set_matrix()

          elif handle == 1:
              #il s'agit de l'axe des x
              x0 = self.points[0,0]
              y0 = self.points[0,1]
              x1 = pos[0]
              y1 = pos[1]
              self.u[0] = x1-x0
              self.u[1] = y1-y0

              #ici on ecrit le comportement pour assurer que le groupe d'espace est le bon
              self.set_space_group_constraints('u')
              self.set_matrix()

          elif handle == 2:
              #il s'agit de l'axe des x
              x0 = self.points[0,0]
              y0 = self.points[0,1]
              x2 = pos[0]
              y2 = pos[1]
              self.v[0] = x2-x0
              self.v[1] = y2-y0
              self.set_space_group_constraints('v')
              self.set_matrix()


    def draw(self, painter, xMap, yMap, canvasRect):
        plot = self.plot()
        x1, x2 = plot.get_axis_limits('bottom')
        y1, y2 = plot.get_axis_limits('left')

        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)

        if symbol != QwtSymbol.NoSymbol:
            spen = symbol.pen()
            sbrush = symbol.brush()
            col_h1,col_s1,col_v1,col_a1 = spen.color().getHsv()  #symbol edge color
            col_h2,col_s2,col_v2,col_a2 = sbrush.color().getHsv()   #symbol face color

        style0 = pen.style()

        col_h0,col_s0,col_v0,col_a0 = pen.color().getHsv()   #line color

        h1,k1 = self.get_node_coordinates(x1,y1)
        h2,k2 = self.get_node_coordinates(x2,y1)
        h3,k3 = self.get_node_coordinates(x1,y2)
        h4,k4 = self.get_node_coordinates(x2,y2)

        hh = np.array([h1, h2, h3, h4])
        kk = np.array([k1, k2, k3, k4])
        hmin = int(np.amin(np.round(hh+0.5)))   #integer values inside the plot
        hmax = int(np.amax(np.round(hh-0.5)))   #integer values inside the plot
        kmin = int(np.amin(np.round(kk+0.5)))   #integer values inside the plot
        kmax = int(np.amax(np.round(kk-0.5)))   #integer values inside the plot

        npt = (hmax-hmin)*(kmax-kmin)
        if (not (style0 ==  0 or col_a0 == 0)) and npt<self.maxnpt:
            for h in range(hmin-1,hmax+2):
                x1,y1 = self.get_plot_coordinates(h,kmin-1)
                x2,y2 = self.get_plot_coordinates(h,kmax+2)
                p1,p2 = int(round(xMap.transform(x1))), int(round(yMap.transform(y1)))
                p3,p4 = int(round(xMap.transform(x2))), int(round(yMap.transform(y2)))
                painter.drawLine(p1, p2, p3, p4)


            for k in range(kmin-1,kmax+2):
                x1,y1 = self.get_plot_coordinates(hmin-1,k)
                x2,y2 = self.get_plot_coordinates(hmax+2,k)
                p1,p2 = int(round(xMap.transform(x1))), int(round(yMap.transform(y1)))
                p3,p4 = int(round(xMap.transform(x2))), int(round(yMap.transform(y2)))
                painter.drawLine(p1, p2, p3, p4)

        #we change the style for painting basis vectors with continuous line
        pen.setStyle(1)
        painter.setPen(pen)
        self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[1])
        self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[2])
        pen.setStyle(style0)

        if symbol != QwtSymbol.NoSymbol:
            painter.setPen(spen)
            painter.setBrush(sbrush)

            if (not (spen.style() ==  0 or col_a1 ==  0) or (sbrush.style() ==  0 or col_a2 ==  0)) and  npt<self.maxnpt:
                for h in range(hmin,hmax+1):
                    for k in range(kmin,kmax+1):
                        if not self.centered or (h+k)%2 ==   0:
                            x, y = self.get_plot_coordinates(h,k)
                            p1,p2 = xMap.transform(x), yMap.transform(y)
                            painter.drawEllipse(QPointF(p1,p2),5,5)


    def draw_arrow(self, painter, xMap, yMap, p0, p1):

        sz = self.arrow_size
        angle = pi*self.arrow_angle/180.
        ca, sa = cos(angle), sin(angle)
        d1x = (xMap.transform(p1[0])-xMap.transform(p0[0]))
        d1y = (yMap.transform(p1[1])-yMap.transform(p0[1]))
        norm = sqrt(d1x**2+d1y**2)
        if abs(norm) < 1e-6:
            return
        d1x *=   sz/norm
        d1y *=   sz/norm
        n1x = -d1y
        n1y = d1x
        # arrow : a0 - a1 ==   p1 - a2
        a1x = xMap.transform(p1[0])
        a1y = yMap.transform(p1[1])
        a0x = a1x - ca*d1x + sa*n1x
        a0y = a1y - ca*d1y + sa*n1y
        a2x = a1x - ca*d1x - sa*n1x
        a2y = a1y - ca*d1y - sa*n1y

        poly = QPolygonF()
        poly.append(QPointF(a0x, a0y))
        poly.append(QPointF(a1x, a1y))
        poly.append(QPointF(a2x, a2y))
        painter.drawPolygon(poly)
        d0x = xMap.transform(p0[0])
        d0y = yMap.transform(p0[1])
        painter.drawLine(QPointF(d0x,d0y),QPointF(a1x,a1y))

    def get_item_parameters(self, itemparams):
        self.shapeparam.update_param(self)
        itemparams.add("ShapeParam", self, self.shapeparam)
        self.latticeparam.update_param(self)
        itemparams.add("LatticeGridParam", self, self.latticeparam)

    def set_item_parameters(self, itemparams):
        update_dataset(self.shapeparam, itemparams.get("ShapeParam"),
                       visible_only = True)
        self.shapeparam.update_shape(self)
        self.latticeparam.update_shape(self)

class DirectLatticeGrid(LatticeGrid):
    """a shape for drawing a lattice with atoms, combining two unit cell vectors and associated grid"""
    _can_move = True
    maxrad = 200  #maximum radius for drawing points
    def __init__(self, O = [0,0], u = [100,0], v = [0,100], sg = 'p4', shapeparam = None, latticeparam = None, atoms = [[0,0]]):
        self.atoms = np.array(atoms,dtype=float)

        super().__init__(O = O, u = u, v=v, sg = sg, centered = False, shapeparam = shapeparam, latticeparam = latticeparam)

        if latticeparam == None:
            self.latticeparam = DirectLatticeGridParam(_("LatticeGrid"), icon = "rectangle.png")
        else:
            self.latticeparam = latticeparam
            self.latticeparam.update_shape(self)

    def detach(self):
        for slave in self.slaves:
            slave.set_master(None)
        super().detach()

    #pickle methods
    def __reduce__(self):
        self.shapeparam.update_param(self)
        self.latticeparam.update_param(self)
        state = (self.shapeparam, self.z(), self.latticeparam, self.slaves, self.atoms)
        return (DirectLatticeGrid, (), state)

    def __getstate__(self):
        return self.shapeparam, self.z(), self.latticeparam, self.slaves, self.atoms

    def __setstate__(self, state):
        #with pickle, slaves are charged

        param, z, latticeparam, slaves, atoms = state

        self.setZ(z)
        self.shapeparam = param
        self.shapeparam.update_shape(self)
        self.latticeparam = latticeparam
        self.latticeparam.update_shape(self)
        self.slaves = slaves
        for slave in self.slaves:
            slave.set_master(self)
            slave.latticeparam.update_shape(slave)

    def get_node_coordinates(self, xplot, yplot, iatom = -1):
        """if iatom<0, return node coordinates (from plot coordinates)
           else, return atom coordinates"""

        v = self.tr*colvector(xplot, yplot)
        xreduce, yreduce, _ = v[:, 0].A.ravel()

        if iatom < 0:
            return xreduce, yreduce
        else:
            return xreduce-self.atoms[iatom,0], yreduce-self.atoms[iatom,1]

    def get_plot_coordinates(self, xnode, ynode, iatom = -1):
        """if iatom<0, Return plot coordinates of given node)
        else, return atom number iatom  coordinates"""
        if iatom<0:
            v0 = self.itr*colvector(xnode, ynode)
        else:
            v0 = self.itr*colvector(xnode+self.atoms[iatom,0], ynode+self.atoms[iatom,1])
        xplot, yplot, _ = v0[:, 0].A.ravel()
        return xplot, yplot

    def set_space_group_constraints(self,master = 'u',RT = False):

        if RT:
          self.u[0],self.u[1] = angle_to_xy(self.uR,self.uT)
          self.v[0],self.v[1] = angle_to_xy(self.vR,self.vT)

        if master ==   'u':
            if self.sg in ["pm","p2mm","pg","p2mg","p2gg"]:
                #les axes doivent etre perpendiculaires
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(v2/u2)
                self.v[0] = -self.u[1]*ratio
                self.v[1] = self.u[0]*ratio

            elif self.sg in ["cm","c2mm"]:
                #les axes doivent etre orthonormes
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(u2/v2)
                self.v[0] = self.v[0]*ratio
                self.v[1] = self.v[1]*ratio

            elif self.sg in ["p4","p4mm","p4gm"]:
                #les axes doivent etre orthonormes
                self.v[0] = -self.u[1]
                self.v[1] = self.u[0]

            elif self.sg in ["p3","p3m1","p31m","p6","p6mm"]:
                #les axes doivent etre orthonormes
                self.v[0] = self.u[0]*cos_120-self.u[1]*sin_120
                self.v[1] = self.u[0]*sin_120+self.u[1]*cos_120
        else:
            if self.sg in ["pm","p2mm","pg","p2mg","p2gg"]:
                #les axes doivent etre perpendiculaires
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(u2/v2)
                self.u[0] = self.v[1]*ratio
                self.u[1] = -self.v[0]*ratio

            if self.sg in ["cm","c2mm"]:
                #les axes doivent etre orthonormes
                u2 = self.u[0]*self.u[0]+self.u[1]*self.u[1]
                v2 = self.v[0]*self.v[0]+self.v[1]*self.v[1]
                ratio = np.sqrt(v2/u2)
                self.u[0] = self.u[0]*ratio
                self.u[1] = self.u[1]*ratio

            if self.sg in ["p4","p4mm","p4gm"]:
                #les axes doivent etre orthonormes
                self.u[0] = self.v[1]
                self.u[1] = -self.v[0]

            if self.sg in ["p3","p3m1","p31m","p6","p6mm"]:
                #les axes doivent etre orthonormes
                self.u[0] = self.v[0]*cos_120+self.v[1]*sin_120
                self.u[1] = -self.v[0]*sin_120+self.v[1]*cos_120

        self.uR,self.uT = xy_to_angle(self.u[0],self.u[1])
        self.vR,self.vT = xy_to_angle(self.v[0],self.v[1])

    def move_shape(self, old_pos, new_pos):
        pass
        #super().move_shape(old_pos,new_pos)
        #for slave in self.slaves:
        #    slave.set_points()

    def move_point_to(self, handle, pos, ctrl = None):
        if self.can_move():
          if handle == 0:
              self.O = np.array(pos,float)
              self.set_matrix()

          elif handle == 1:
              #il s'agit de l'axe des x
              x0 = self.points[0,0]
              y0 = self.points[0,1]
              x1 = pos[0]
              y1 = pos[1]
              self.u[0] = x1-x0
              self.u[1] = y1-y0

              #ici on ecrit le comportement pour assurer que le groupe d'espace est le bon
              self.set_space_group_constraints('u')
              self.set_matrix()

          elif handle == 2:
              #il s'agit de l'axe des x
              x0 = self.points[0,0]
              y0 = self.points[0,1]
              x2 = pos[0]
              y2 = pos[1]
              self.v[0] = x2-x0
              self.v[1] = y2-y0
              self.set_space_group_constraints('v')
              self.set_matrix()

    def set_matrix(self):
        #set the matrix to go from plot coordinates to node coordinates (tr)
        #and the inverse matrix of it (itr)
        self.itr = np.mat([[self.u[0],self.v[0],self.O[0]]
                         ,[self.u[1],self.v[1],self.O[1]]
                         ,[0.,       0.,       1.       ]])
        self.tr = self.itr.I
        _x0, _y0 = self.get_plot_coordinates(0,0)
        _x1, _y1 = self.get_plot_coordinates(1,0)
        _x2, _y2 = self.get_plot_coordinates(0,1)
        self.points = np.array([[_x0, _y0],[_x1, _y1],[_x2, _y2]])
        for slave in self.slaves:
            slave.set_matrix()

        self.set_plotted_nodes()

    def set_plotted_nodes(self):
        #set the lines and points to be drawn
        self.plotted_lines = []
        self.plotted_atoms = []
        x0, y0 = self.O[0], self.O[1]
        x1, x2 = x0 - self.maxrad, x0 + self.maxrad
        y1, y2 = y0 - self.maxrad, y0 + self.maxrad
        r2 = self.maxrad**2

        h1,k1 = self.get_node_coordinates(x1,y1)
        h2,k2 = self.get_node_coordinates(x2,y1)
        h3,k3 = self.get_node_coordinates(x1,y2)
        h4,k4 = self.get_node_coordinates(x2,y2)

        hh = np.array([h1, h2, h3, h4])
        kk = np.array([k1, k2, k3, k4])

        hmin = int(np.amin(np.round(hh+0.5)))   #integer values inside the plot
        hmax = int(np.amax(np.round(hh-0.5)))   #integer values inside the plot
        kmin = int(np.amin(np.round(kk+0.5)))   #integer values inside the plot
        kmax = int(np.amax(np.round(kk-0.5)))   #integer values inside the plot

        for h in range(hmin-1,hmax+2):
            x1,y1 = self.get_plot_coordinates(h,kmin-1)
            x2,y2 = self.get_plot_coordinates(h,kmax+2)
            a = (x2-x1)**2 + (y2-y1)**2
            b = (x2-x1)*(x1-x0) + (y2-y1)*(y1-y0)
            c = (x1-x0)**2 + (y1-y0)**2 - r2
            delta = b*b - a*c
            if delta>0:
                sq = np.sqrt(delta)
                t1 = (-b - sq)/a
                t2 = (-b + sq)/a
                x11 = x1 + t1*(x2-x1)
                y11 = y1 + t1*(y2-y1)
                x22 = x1 + t2*(x2-x1)
                y22 = y1 + t2*(y2-y1)
                self.plotted_lines.append((x11,y11,x22,y22))

        for k in range(kmin-1,kmax+2):
            x1,y1 = self.get_plot_coordinates(hmin-1,k)
            x2,y2 = self.get_plot_coordinates(hmax+2,k)
            a = (x2-x1)**2 + (y2-y1)**2
            b = (x2-x1)*(x1-x0) + (y2-y1)*(y1-y0)
            c = (x1-x0)**2 + (y1-y0)**2 - r2
            delta = b*b - a*c
            if delta>0:
                sq = np.sqrt(delta)
                t1 = (-b - sq)/a
                t2 = (-b + sq)/a
                x11 = x1 + t1*(x2-x1)
                y11 = y1 + t1*(y2-y1)
                x22 = x1 + t2*(x2-x1)
                y22 = y1 + t2*(y2-y1)
                self.plotted_lines.append((x11,y11,x22,y22))

        for h in range(hmin,hmax+1):
            for k in range(kmin,kmax+1):

                for iatom, atom in enumerate(self.atoms):
                    x, y = self.get_plot_coordinates(h,k,iatom)
                    if (x-x0)**2+(y-y0)**2 < r2:
                        self.plotted_atoms.append((x, y))


    def draw(self, painter, xMap, yMap, canvasRect):
        plot = self.plot()

        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)

        if symbol != QwtSymbol.NoSymbol:
            spen = symbol.pen()
            sbrush = symbol.brush()
            col_h1,col_s1,col_v1,col_a1 = spen.color().getHsv()  #symbol edge color
            col_h2,col_s2,col_v2,col_a2 = sbrush.color().getHsv()   #symbol face color


        style0 = pen.style()

        col_h0,col_s0,col_v0,col_a0 = pen.color().getHsv()   #line color

        if (not (style0 ==  0 or col_a0 == 0)):
            for line in self.plotted_lines:
                p1,p2 = int(round(xMap.transform(line[0]))), int(round(yMap.transform(line[1])))
                p3,p4 = int(round(xMap.transform(line[2]))), int(round(yMap.transform(line[3])))
                painter.drawLine(p1, p2, p3, p4)

        #we change the style for painting basis vectors with continuous line
        pen.setStyle(1)
        painter.setPen(pen)
        self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[1])
        self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[2])
        pen.setStyle(style0)

        if symbol != QwtSymbol.NoSymbol:
            painter.setPen(spen)
            painter.setBrush(sbrush)


            if (not (spen.style() ==  0 or col_a1 ==  0) or (sbrush.style() ==  0 or col_a2 ==  0)):
                for atom in self.plotted_atoms:
                    p1,p2 = xMap.transform(atom[0]), yMap.transform(atom[1])
                    painter.drawEllipse(QPointF(p1,p2),5,5)


    def get_item_parameters(self, itemparams):
        self.shapeparam.update_param(self)
        itemparams.add("Shape Parameters", self, self.shapeparam)
        self.latticeparam.update_param(self)
        itemparams.add("Lattice Parameters", self, self.latticeparam)

    def set_item_parameters(self, itemparams):
        update_dataset(self.shapeparam, itemparams.get("ShapeParam"),
                       visible_only = True)
        self.shapeparam.update_shape(self)
        self.latticeparam.update_shape(self)



class ReconstructionGrid(PolygonShape):
    """a shape for drawing a reconstruction lattice, combining unit cell vectors and associated grid"""
    _can_move = False
    master = None
    maxnpt = 10000

    def __init__(self, A = [1,0], B = [0,1], master = None, sym = True, centered = False, shapeparam = None, latticeparam = None):
        super().__init__(shapeparam = shapeparam)

        self.arrow_angle = 15 # degrees
        self.arrow_size = 10 # pixels

        self.sym = sym
        self.centered = centered
        self.A = A.copy()  #first base vector of the reconstruction in direct space
        self.B = B.copy()
        self.trs = [np.identity(3)]  #transformation matrices for each domain to go from plot to node
        self.itrs = [np.identity(3)]
        self.domains = [np.identity(3)]  #matrices in direct space for each symmetrical domain, in coordinates of the master in direct space
        self.ndomains = 1

        self.set_master(master)   #set the master to master and update matrices

        if shapeparam is None:
            self.shapeparam = ShapeParam(_("Shape"), icon = "rectangle.png")
        else:
            self.shapeparam = shapeparam
            self.shapeparam.update_shape(self)

        if latticeparam is None:
            self.latticeparam = ReconstructionGridParam(_("ReconstructionShape"), icon = "rectangle.png")
        else:
            self.latticeparam = latticeparam
            self.latticeparam.update_shape(self)


    def detach(self):
        if self.master is not None:
            self.master.remove_slave(self)
        super().detach()

    def types(self):
        # we avoid direct serialization of the item through baseplot.save_items() method
        return (IShapeItemType, )

    def __reduce__(self):
        self.shapeparam.update_param(self)
        self.latticeparam.update_param(self)
        state = (self.shapeparam, self.z(), self.latticeparam)
        return (ReconstructionGrid, (), state)

    def __getstate__(self):
        return self.shapeparam, self.z(), self.latticeparam

    def __setstate__(self, state):
        param, z, latticeparam = state
        self.setZ(z)
        self.shapeparam = param
        self.shapeparam.update_shape(self)
        self.latticeparam = latticeparam
        self.latticeparam.update_shape(self)

    def set_master(self,master):
        if master == self.master:
            return

        if self.master is not None:   #first remove reference to self in old master
            self.master.remove_slave(self)

        if master != None:
            from xrdutils.latticetools import LatticeGrid as OLatticeGrid
            assert (isinstance(master,LatticeGrid) or isinstance(master,OLatticeGrid)),"master must be a LatticeGrid instance or None"

            self.master = master
            self.set_domains()
            self.master.add_slave(self)    #add reference to self in new master
            self.set_domains()
            self.set_matrix()

        else:
            self.master = master

    def get_node_coordinates(self, xplot, yplot, idom = 0):
        """Return node coordinates (from plot coordinates)"""
        v = self.trs[idom]*colvector(xplot, yplot)
        xpixel, ypixel, _ = v[:, 0].A.ravel()
        return xpixel, ypixel

    def get_plot_coordinates(self, xnode, ynode, idom = 0):
        """Return plot coordinates (from node coordinates)"""
        v0 = self.itrs[idom]*colvector(xnode, ynode)
        xplot, yplot, _ = v0[:, 0].A.ravel()
        return xplot, yplot

    def set_domains(self):
        #we first list the domains in direct space
        mat = np.matrix([[self.A[0],self.B[0],0.],
                         [self.A[1],self.B[1],0.],
                         [0.       ,0.       ,1.]])
        self.domains = [mat]
        if self.sym:
            #we add all independent reconstructions matrices (i.e. that are not multiple)
            if self.master.sg in ["pm",'pg','cm','pmm','p2mm','p2mg','p2gg','c2mm','p4mm','p4gm']:
                #mirror along x axis (y->-y)
                S = np.matrix([[1,0,0],[0,-1,0],[0,0,1]])*mat
                append_domain(self.domains,S)

            if self.master.sg in ["p1m",'p1g','c1m']:
                #mirror along y axis (x->-x)
                S = np.matrix([[-1,0,0],[0,1,0],[0,0,1]])*mat
                append_domain(self.domains,S)

            if self.master.sg in ['p4','p4mm','p4gm']:
                #rotation 90°
                S = np.matrix([[0,1,0],[-1,0,0],[0,0,1]])*mat
                append_domain(self.domains,S)


            if self.master.sg in ['p4mm','p4gm']:
                #mirror xy
                S = np.matrix([[0,1,0],[1,0,0],[0,0,1]])*mat
                append_domain(self.domains,S)

            if self.master.sg in ['p3','p6','p3m1','p31m','p6mm']:
                #note that rotation 60° is equivalent to rotation -120° (+180°)
                #rotation 120°
                S = np.matrix([[0,-1,0],[1,-1,0],[0,0,1]])*mat
                append_domain(self.domains,S)

                #rotation -120°
                S = np.matrix([[-1,1,0],[-1,0,0],[0,0,1]])*mat
                append_domain(self.domains,S)

            if self.master.sg in ['p3m1','p6mm']:
                #mirror 1 along y = x/2 axis
                S = np.matrix([[1,0,0],[1,-1,0],[0,0,1]])*mat
                append_domain(self.domains,S)

                #mirror 2 along x = y/2 axis
                S = np.matrix([[-1,1,0],[0,1,0],[0,0,1]])*mat
                append_domain(self.domains,S)

                #mirror 3 along y = -x axis
                S = np.matrix([[0,-1,0],[-1,0,0],[0,0,1]])*mat
                append_domain(self.domains,S)


            if self.master.sg in ['p31m',]:
                #mirror 1 along x axis
                S = np.matrix([[1,-1,0],[0,-1,0],[0,0,1]])*mat
                append_domain(self.domains,S)

                #mirror2 (R*mirror = mirror along y = x)
                S = np.matrix([[0,1,0],[1,0,0],[0,0,1]])*mat
                append_domain(self.domains,S)

                #mirror3 (R*R*mirror = mirror along y)
                S = np.matrix([[-1,0,0],[-1,1,0],[0,0,1]])*mat
                append_domain(self.domains,S)

        self.ndomains = len(self.domains)
        self.set_matrix()

    def set_matrix(self):
        #define the transformation matrices in reciprocal space
        self.trs = []
        self.itrs = []

        self.points = [[self.master.points[0,0],self.master.points[0,1]],]  #origin
        try:
            for idom in range(self.ndomains):
                itr = self.master.itr * self.domains[idom].I.T     #to go from node to plot coordinates
                tr = itr.I
                self.trs.append(tr)
                self.itrs.append(itr)

                _x1, _y1 = self.get_plot_coordinates(1,0,idom)
                _x2, _y2 = self.get_plot_coordinates(0,1,idom)
                self.points +=   [[_x1, _y1],[_x2, _y2]]
        except np.linalg.LinAlgError:
            pass
        self.points = np.array(self.points)

    def move_point_to(self, handle, pos, ctrl = None):
        #self.points[handle, :] = pos
        #ici il faut ecrire le comportement de la shape
        if self.can_move() and handle>0 and self.master is not None:

            mat = self.master.itr * self.domains[0].I.T

            if handle ==  1:
                 #il s'agit de l'axe des x
                x0 = self.points[0,0]
                y0 = self.points[0,1]
                x1 = pos[0]
                y1 = pos[1]
                u0 = x1-x0
                u1 = y1-y0

                mat[0,0] = u0
                mat[1,0] = u1

                domain = ((self.master.itr).I*mat).T.I
                self.A[0] = domain[0,0]
                self.A[1] = domain[1,0]
                self.B[0] = domain[0,1]
                self.B[1] = domain[1,1]
                self.set_domains()

            elif handle ==  2:
                #il s'agit de l'axe des y
                x0 = self.points[0,0]
                y0 = self.points[0,1]
                x2 = pos[0]
                y2 = pos[1]
                v0 = x2-x0
                v1 = y2-y0

                mat[0,1] = v0
                mat[1,1] = v1

                domain = ((self.master.itr).I*mat).T.I
                self.A[0] = domain[0,0]
                self.A[1] = domain[1,0]
                self.B[0] = domain[0,1]
                self.B[1] = domain[1,1]
                self.set_domains()


    def move_shape(self, old_pos, new_pos):
        return

    def draw(self, painter, xMap, yMap, canvasRect):

        plot = self.plot()
        x_min, x_max = plot.get_axis_limits('bottom')
        y_min, y_max = plot.get_axis_limits('left')

        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)

        if symbol != QwtSymbol.NoSymbol:
            spen = symbol.pen()   #PyQt5.QtGui.QPen object
            sbrush = symbol.brush()   #PyQt5.QtGui.QBrush
            col_h1,col_s1,col_v1,col_a1 = spen.color().getHsv()  #symbol edge color
            col_h2,col_s2,col_v2,col_a2 = sbrush.color().getHsv()   #symbol face color
            spcolor = spen.color()
            sbcolor = sbrush.color()

        col_h0,col_s0,col_v0,col_a0 = pen.color().getHsv()   #line color
        style0 = pen.style()
        color = pen.color()

        for idom in range(self.ndomains):

            #setting color for each domain line
            color.setHsv((col_h0+40*idom)%256,col_s0,col_v0,col_a0)
            pen.setColor(color)
            pen.setStyle(style0)
            painter.setPen(pen)

            h1,k1 = self.get_node_coordinates(x_min,y_min,idom)
            h2,k2 = self.get_node_coordinates(x_max,y_min,idom)
            h3,k3 = self.get_node_coordinates(x_min,y_max,idom)
            h4,k4 = self.get_node_coordinates(x_max,y_max,idom)

            hh = np.array([h1, h2, h3, h4])
            kk = np.array([k1, k2, k3, k4])
            hmin = int(np.amin(np.round(hh+0.5)))   #integer values inside the plot
            hmax = int(np.amax(np.round(hh-0.5)))   #integer values inside the plot
            kmin = int(np.amin(np.round(kk+0.5)))   #integer values inside the plot
            kmax = int(np.amax(np.round(kk-0.5)))   #integer values inside the plot

            npt = (hmax-hmin)*(kmax-kmin)
            if not (pen.style ==  0 or col_a0 ==  0) and npt<self.maxnpt:
                for h in range(hmin-1,hmax+2):
                    x1,y1 = self.get_plot_coordinates(h,kmin-1,idom)
                    x2,y2 = self.get_plot_coordinates(h,kmax+2,idom)
                    p1,p2 = int(round(xMap.transform(x1))), int(round(yMap.transform(y1)))
                    p3,p4 = int(round(xMap.transform(x2))), int(round(yMap.transform(y2)))
                    painter.drawLine(p1, p2, p3, p4)


                for k in range(kmin-1,kmax+2):
                    x1,y1 = self.get_plot_coordinates(hmin-1,k,idom)
                    x2,y2 = self.get_plot_coordinates(hmax+2,k,idom)
                    p1,p2 = int(round(xMap.transform(x1))), int(round(yMap.transform(y1)))
                    p3,p4 = int(round(xMap.transform(x2))), int(round(yMap.transform(y2)))
                    painter.drawLine(p1, p2, p3, p4)

            pen.setStyle(1)
            painter.setPen(pen)
            self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[2*idom+1])
            self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[2*idom+2])
            pen.setStyle(style0)

            if symbol != QwtSymbol.NoSymbol:
                #setting color for each domain symbol
                spcolor.setHsv((col_h1+40*idom)%256,col_s1,col_v1,col_a1)
                sbcolor.setHsv((col_h2+40*idom)%256,col_s2,col_v2,col_a2)
                spen.setColor(spcolor)
                sbrush.setColor(sbcolor)
                painter.setPen(spen)
                painter.setBrush(sbrush)

                if (not (spen.style() ==  0 or col_a1 ==  0) or (sbrush.style() ==  0 or col_a2 ==  0)) and npt<self.maxnpt :
                    for h in range(hmin,hmax+1):
                        for k in range(kmin,kmax+1):
                            if not self.centered or (h+k)%2 ==   0:
                                x, y = self.get_plot_coordinates(h,k,idom)
                                p1,p2 = xMap.transform(x), yMap.transform(y)
                                painter.drawEllipse(QPointF(p1,p2),5,5)

        #back to initial colors
        color.setHsv(col_h0,col_s0,col_v0,col_a0)

        if symbol != QwtSymbol.NoSymbol:
            spcolor.setHsv(col_h1,col_s1,col_v1,col_a1)
            sbcolor.setHsv(col_h2,col_s2,col_v2,col_a2)

        pen.setColor(color)
        spen.setColor(spcolor)
        sbrush.setColor(sbcolor)

    def draw_arrow(self, painter, xMap, yMap, p0, p1):
        sz = self.arrow_size
        angle = pi*self.arrow_angle/180.
        ca, sa = cos(angle), sin(angle)
        d1x = (xMap.transform(p1[0])-xMap.transform(p0[0]))
        d1y = (yMap.transform(p1[1])-yMap.transform(p0[1]))
        norm = sqrt(d1x**2+d1y**2)
        if abs(norm) < 1e-6:
            return
        d1x *=   sz/norm
        d1y *=   sz/norm
        n1x = -d1y
        n1y = d1x
        # arrow : a0 - a1 ==   p1 - a2
        a1x = xMap.transform(p1[0])
        a1y = yMap.transform(p1[1])
        a0x = a1x - ca*d1x + sa*n1x
        a0y = a1y - ca*d1y + sa*n1y
        a2x = a1x - ca*d1x - sa*n1x
        a2y = a1y - ca*d1y - sa*n1y

        poly = QPolygonF()
        poly.append(QPointF(a0x, a0y))
        poly.append(QPointF(a1x, a1y))
        poly.append(QPointF(a2x, a2y))
        painter.drawPolygon(poly)
        d0x = xMap.transform(p0[0])
        d0y = yMap.transform(p0[1])
        painter.drawLine(QPointF(d0x,d0y),QPointF(a1x,a1y))

    def get_item_parameters(self, itemparams):
        self.shapeparam.update_param(self)
        itemparams.add("ShapeParam", self, self.shapeparam)
        from xrdutils.latticetools import LatticeGrid as OLatticeGrid
        lattices = [item for item in self.plot().get_items() if (isinstance(item,LatticeGrid) or isinstance(item,OLatticeGrid)) ]
        self.latticeparam.update_choices(lattices)

        self.latticeparam.update_param(self)
        itemparams.add("ReconstructionGridParam", self, self.latticeparam)

    def set_item_parameters(self, itemparams):
        update_dataset(self.shapeparam, itemparams.get("ShapeParam"),
                       visible_only = True)
        self.shapeparam.update_shape(self)
        #modification des parametres specifiques de la latticeshape
        self.latticeparam.update_shape(self)

class DirectReconstructionGrid(ReconstructionGrid):
    maxrad = 100   #maximum radius for drawing points

    def __init__(self, O=[0,0], A = [1,0], B = [0,1], master = None, force_master = None, shapeparam = None, latticeparam = None, atoms=[[0,0]]):
        self.atoms = np.array(atoms,dtype=float)

        self.O = np.array(O,float)   #lattice origin can be shifted for separating different domains
        self.force_master = force_master
        super().__init__(A = A, B = B, master = master, sym = None, centered = None, shapeparam = shapeparam, latticeparam = latticeparam)
        if latticeparam == None:
            self.latticeparam = DirectReconstructionGridParam(_("DirectLatticeGrid"), icon = "rectangle.png")
        else:
            self.latticeparam = latticeparam
            self.latticeparam.update_shape(self)

    def __reduce__(self):
        self.shapeparam.update_param(self)
        self.latticeparam.update_param(self)
        state = (self.shapeparam, self.z(), self.latticeparam)
        return (DirectReconstructionGrid, (), state)

    def set_master(self,master):
        if master == self.master:
            return

        if self.master is not None:   #first remove reference to self in old master
            self.master.remove_slave(self)

        if master != None:
            from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
            assert (isinstance(master,DirectLatticeGrid) or isinstance(master,ODirectLatticeGrid)),"master must be a DirectLatticeGrid instance or None"

            self.master = master
            self.set_domains()
            self.master.add_slave(self)    #add reference to self in new master
            self.set_domains()
            self.set_matrix()

        else:
            self.master = master

    def get_node_coordinates(self, xplot, yplot, iatom = -1):
        """if iatom<0, return node coordinates (from plot coordinates)
           else, return atom coordinates"""

        v = self.tr*colvector(xplot, yplot)
        xreduce, yreduce, _ = v[:, 0].A.ravel()

        if iatom < 0:
            return xreduce, yreduce
        else:
            return xreduce-self.atoms[iatom,0], yreduce-self.atoms[iatom,1]

    def get_plot_coordinates(self, xnode, ynode, iatom = -1):
        """if iatom<0, Return plot coordinates of given node)
        else, return atom number iatom  coordinates"""
        if iatom<0:
            v0 = self.itr*colvector(xnode, ynode)
        else:
            v0 = self.itr*colvector(xnode+self.atoms[iatom,0], ynode+self.atoms[iatom,1])
        xplot, yplot, _ = v0[:, 0].A.ravel()
        return xplot, yplot

    def set_matrix(self):
        #define the transformation matrices in reciprocal space
        self.mat = np.matrix([[self.A[0],self.B[0],0],
                         [self.A[1],self.B[1],0],
                         [0.       ,0.       ,1.]])
        self.trs = []
        self.itrs = []


        trans = np.matrix([[1., 0., self.O[0]],
                         [0., 1., self.O[1]],
                         [0. ,0. ,1.]])

        self.points = []

        try:
            self.itr = trans*self.master.itr*self.mat      #to go from node to plot coordinates
            self.tr = self.itr.I

            _x0, _y0 = self.get_plot_coordinates(0,0,-1)  #origin
            _x1, _y1 = self.get_plot_coordinates(1,0,-1)
            _x2, _y2 = self.get_plot_coordinates(0,1,-1)

            self.points =   [[_x0, _y0],[_x1, _y1],[_x2, _y2]]

        except np.linalg.LinAlgError:
            pass

        self.points = np.array(self.points)
        self.set_plotted_nodes()

    def move_point_to(self, handle, pos, ctrl = None):
        #self.points[handle, :] = pos
        #ici il faut ecrire le comportement de la shape
        if self.can_move() and self.master is not None:
            if not self.force_master:
                #we change reconstruction parameters
                if handle == 0:
                    x0 = self.points[0,0]
                    y0 = self.points[0,1]
                    x1 = pos[0]
                    y1 = pos[1]
                    dx = x1 - x0
                    dy = y1 - y0
                    self.O[0] += dx
                    self.O[1] += dy
                    self.set_matrix()

                elif handle ==  1:
                    #il s'agit de l'axe des x
                    x1 = pos[0] - self.O[0]  #position with respect to the master lattice, corrected from the shift between master and reconstruction
                    y1 = pos[1] - self.O[1]
                    u0, v0 = self.master.get_node_coordinates(x1, y1)
                    self.A[0] = u0
                    self.A[1] = v0
                    self.set_matrix()


                elif handle ==  2:
                    #il s'agit de l'axe des x
                    x1 = pos[0] - self.O[0]  #position en enlevant le décalage de reseau
                    y1 = pos[1] - self.O[1]
                    u0, v0 = self.master.get_node_coordinates(x1, y1)
                    self.B[0] = u0
                    self.B[1] = v0
                    self.set_matrix()

            elif self.master.can_move():
                if handle == 0:
                    x0 = self.points[0,0]
                    y0 = self.points[0,1]
                    x1 = pos[0]
                    y1 = pos[1]
                    dx = x1 - x0
                    dy = y1 - y0
                    self.master.O[0] += dx
                    self.master.O[1] += dy
                    self.master.set_matrix()

                elif handle ==  1:
                    #il s'agit de l'axe des x
                    x0,y0 = self.get_plot_coordinates(0, 0)
                    x1,y1 = pos[0],pos[1]
                    x2,y2 = self.get_plot_coordinates(0, 1)

                    M = np.matrix([[x1-x0,x2-x0,0],[y1-y0,y2-y0,0],[0,0,1]])*self.mat.I  #matrix with vectors of the reconstruction

                    self.master.u = [M[0,0],M[1,0]]
                    self.master.v = [M[0,1],M[1,1]]
                    self.master.set_space_group_constraints('u')
                    self.master.set_matrix()

                elif handle ==  2:
                    #il s'agit de l'axe des x
                    x0,y0 = self.get_plot_coordinates(0, 0)
                    x1,y1 = self.get_plot_coordinates(1, 0)
                    x2,y2 = pos[0],pos[1]

                    M = np.matrix([[x1-x0,x2-x0,0],[y1-y0,y2-y0,0],[0,0,1]])*self.mat.I  #matrix with vectors of the reconstruction

                    self.master.u = [M[0,0],M[1,0]]
                    self.master.v = [M[0,1],M[1,1]]
                    self.master.set_space_group_constraints('v')
                    self.master.set_matrix()




    def set_plotted_nodes(self):
        #set the lines and points to be drawn
        self.plotted_lines = []
        self.plotted_atoms = []

        x0, y0 = self.get_plot_coordinates(0,0)

        x1, x2 = x0 - self.maxrad, x0 + self.maxrad
        y1, y2 = y0 - self.maxrad, y0 + self.maxrad
        r2 = self.maxrad**2

        h1,k1 = self.get_node_coordinates(x1,y1)
        h2,k2 = self.get_node_coordinates(x2,y1)
        h3,k3 = self.get_node_coordinates(x1,y2)
        h4,k4 = self.get_node_coordinates(x2,y2)

        hh = np.array([h1, h2, h3, h4])
        kk = np.array([k1, k2, k3, k4])

        hmin = int(np.amin(np.round(hh+0.5)))   #integer values inside the plot
        hmax = int(np.amax(np.round(hh-0.5)))   #integer values inside the plot
        kmin = int(np.amin(np.round(kk+0.5)))   #integer values inside the plot
        kmax = int(np.amax(np.round(kk-0.5)))   #integer values inside the plot

        for h in range(hmin-1,hmax+2):
            x1,y1 = self.get_plot_coordinates(h,kmin-1)
            x2,y2 = self.get_plot_coordinates(h,kmax+2)
            a = (x2-x1)**2 + (y2-y1)**2
            b = (x2-x1)*(x1-x0) + (y2-y1)*(y1-y0)
            c = (x1-x0)**2 + (y1-y0)**2 - r2
            delta = b*b - a*c
            if delta>0:
                sq = np.sqrt(delta)
                t1 = (-b - sq)/a
                t2 = (-b + sq)/a
                x11 = x1 + t1*(x2-x1)
                y11 = y1 + t1*(y2-y1)
                x22 = x1 + t2*(x2-x1)
                y22 = y1 + t2*(y2-y1)
                self.plotted_lines.append((x11,y11,x22,y22))

        for k in range(kmin-1,kmax+2):
            x1,y1 = self.get_plot_coordinates(hmin-1,k)
            x2,y2 = self.get_plot_coordinates(hmax+2,k)
            a = (x2-x1)**2 + (y2-y1)**2
            b = (x2-x1)*(x1-x0) + (y2-y1)*(y1-y0)
            c = (x1-x0)**2 + (y1-y0)**2 - r2
            delta = b*b - a*c
            if delta>0:
                sq = np.sqrt(delta)
                t1 = (-b - sq)/a
                t2 = (-b + sq)/a
                x11 = x1 + t1*(x2-x1)
                y11 = y1 + t1*(y2-y1)
                x22 = x1 + t2*(x2-x1)
                y22 = y1 + t2*(y2-y1)
                self.plotted_lines.append((x11,y11,x22,y22))

        for h in range(hmin,hmax+1):
            for k in range(kmin,kmax+1):

                for iatom, atom in enumerate(self.atoms):
                    x, y = self.get_plot_coordinates(h,k,iatom)
                    if (x-x0)**2+(y-y0)**2 < r2:
                        self.plotted_atoms.append((x, y))

    def draw(self, painter, xMap, yMap, canvasRect):
        plot = self.plot()

        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)

        if symbol != QwtSymbol.NoSymbol:
            spen = symbol.pen()
            sbrush = symbol.brush()
            col_h1,col_s1,col_v1,col_a1 = spen.color().getHsv()  #symbol edge color
            col_h2,col_s2,col_v2,col_a2 = sbrush.color().getHsv()   #symbol face color

        style0 = pen.style()

        col_h0,col_s0,col_v0,col_a0 = pen.color().getHsv()   #line color

        if (not (style0 ==  0 or col_a0 == 0)):
            for line in self.plotted_lines:
                p1,p2 = int(round(xMap.transform(line[0]))), int(round(yMap.transform(line[1])))
                p3,p4 = int(round(xMap.transform(line[2]))), int(round(yMap.transform(line[3])))
                painter.drawLine(p1, p2, p3, p4)

        #we change the style for painting basis vectors with continuous line
        pen.setStyle(1)
        painter.setPen(pen)
        self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[1])
        self.draw_arrow(painter, xMap, yMap, self.points[0], self.points[2])
        pen.setStyle(style0)

        if symbol != QwtSymbol.NoSymbol:
            painter.setPen(spen)
            painter.setBrush(sbrush)

            if (not (spen.style() ==  0 or col_a1 ==  0) or (sbrush.style() ==  0 or col_a2 ==  0)):
                for atom in self.plotted_atoms:
                    p1,p2 = xMap.transform(atom[0]), yMap.transform(atom[1])
                    painter.drawEllipse(QPointF(p1,p2),5,5)

    def get_item_parameters(self, itemparams):
        self.shapeparam.update_param(self)
        itemparams.add("ShapeParam", self, self.shapeparam)
        from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
        lattices = [item for item in self.plot().get_items() if (isinstance(item,DirectLatticeGrid) or isinstance(item,ODirectLatticeGrid)) ]
        self.latticeparam.update_choices(lattices)

        self.latticeparam.update_param(self)
        itemparams.add("ReconstructionGridParam", self, self.latticeparam)

class LatticeGridTool(RectangularShapeTool):
    """A tool for drawing a lattice on a plot"""
    TITLE = _("Lattice")
    ICON = 'lattice.png'
    SHAPE_STYLE_KEY = "shape/latticegrid"

    def activate(self):
        """Activate tool"""
        for baseplot, start_state in self.start_state.items():
            baseplot.filter.set_state(start_state, None)
        self.action.setChecked(True)
        self.manager.set_active_tool(self)

    def create_shape(self):
        shape = LatticeGrid()
        shape.select()
        self.set_shape_style(shape)
        return shape, 0, 1

    def add_shape_to_plot(self, plot, p0, p1):
        """
        Method called when shape's rectangular area
        has just been drawn on screen.
        Adding the final shape to plot and returning it.
        """
        shape = self.get_final_shape(plot, p0, p1)
        shape.unselect()
        #on donne a la shape le dernier numero  de la liste des EllipseStatShape
        from xrdutils.latticetools import LatticeGrid as OLatticeGrid
        items = list(item for item in plot.get_items() if isinstance(item, LatticeGrid) or isinstance(item,OLatticeGrid))
        N = len(items)
        shape.setTitle("Lattice %d"%N)
        shape.shapeparam.update_param(shape)
        #shape.set_color(QColor('#ffff00'))  #yellow
        plot.SIG_ITEMS_CHANGED.emit(plot)
        self.handle_final_shape(shape)
        plot.replot()

    def handle_final_shape(self, shape):
        super().handle_final_shape(shape)

class DirectLatticeGridTool(LatticeGridTool):
    """A tool for drawing a lattice on a plot"""
    def create_shape(self):
        shape = DirectLatticeGrid()
        shape.select()
        self.set_shape_style(shape)
        return shape, 0, 1

    def add_shape_to_plot(self, plot, p0, p1):
        """
        Method called when shape's rectangular area
        has just been drawn on screen.
        Adding the final shape to plot and returning it.
        """
        shape = self.get_final_shape(plot, p0, p1)
        shape.unselect()
        #on donne a la shape le dernier numero  de la liste des EllipseStatShape
        from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
        items = list(item for item in plot.get_items() if isinstance(item, DirectLatticeGrid) or isinstance(item,ODirectLatticeGrid))
        N = len(items)
        shape.setTitle("Lattice %d"%N)
        shape.shapeparam.update_param(shape)
        #shape.set_color(QColor('#ffff00'))  #yellow
        plot.SIG_ITEMS_CHANGED.emit(plot)
        self.handle_final_shape(shape)
        plot.replot()


class ReciprocalMarker(Marker):
    """a cross Marker for displaying coordinates relative to the different lattices and reconstructions that are visible"""
    def __init__(self, label_cb = None, constraint_cb = None,
                 markerparam = None):
        self.xunits = 'x'
        self.yunits = 'y'
        self.mode = -1
        super().__init__(label_cb = label_cb, constraint_cb = constraint_cb, markerparam = markerparam)

    def update_units(self, xunits, yunits):
        self.xunits = xunits
        self.yunits = yunits

    def get_all_coordinates(self, xplot, yplot):
        coords = []
        plot = self.plot()
        if plot is not None:
            from xrdutils.latticetools import LatticeGrid as OLatticeGrid
            from xrdutils.latticetools import ReconstructionGrid as OReconstructionGrid
            lattices = list(item for item in plot.get_items() if ((isinstance(item, LatticeGrid) or isinstance(item,ReconstructionGrid)
                                                                    or isinstance(item, OLatticeGrid) or isinstance(item,OReconstructionGrid))
                                                                   and item.isVisible()))
            for lattice in lattices:
                for idom in range(lattice.ndomains):
                    h,k = lattice.get_node_coordinates(xplot, yplot,idom)
                    title = lattice.shapeparam.label+"_dom_%d"%idom
                    coords.append((title, h, k))
        return coords

    def update_label(self):
        x, y = self.xValue(), self.yValue()
        if self.label_cb:
            label = self.label_cb(x, y)
            if label is None:
                return
        elif self.is_vertical():
            label = self.xunits+" = %g" % x
        elif self.is_horizontal():
            label = self.yunits+" = %g" % y
        else:
            coords = self.get_all_coordinates(x, y)
            label = f"{self.xunits} = {x:g}, {self.yunits} = {y:g}"

            if self.mode > len(coords):
                self.mode = -1

            if self.mode < -1:
                self.mode = len(coords)

            if self.mode ==   -1:  #display all
                for cc in coords:
                    label = label+f"\nh = {cc[1]:g}, k = {cc[2]:g} ({cc[0]})"

            elif self.mode>0:
                cc = coords[self.mode-1]
                label = f"h = {cc[1]:g}, k = {cc[2]:g} ({cc[0]})"

        text = self.label()
        text.setText(label)
        self.setLabel(text)
        plot = self.plot()
        if plot is not None:
            xaxis = plot.axisScaleDiv(self.xAxis())
            if x < (xaxis.upperBound()+xaxis.lowerBound())/2:
                hor_alignment = Qt.AlignRight
            else:
                hor_alignment = Qt.AlignLeft
            yaxis = plot.axisScaleDiv(self.yAxis())
            ymap = plot.canvasMap(self.yAxis())
            y_top, y_bottom = ymap.s1(), ymap.s2()
            if y < .5*(yaxis.upperBound()+yaxis.lowerBound()):
                if y_top > y_bottom:
                    ver_alignment = Qt.AlignBottom
                else:
                    ver_alignment = Qt.AlignTop
            else:
                if y_top > y_bottom:
                    ver_alignment = Qt.AlignTop
                else:
                    ver_alignment = Qt.AlignBottom
            self.setLabelAlignment(hor_alignment|ver_alignment)

class MyImageMaskTool(ImageMaskTool):
    def create_action_menu(self, manager):
        """Create and return menu for the tool's action"""
        rect_tool = manager.add_tool(
            RectangleTool,
            toolbar_id=None,
            handle_final_shape_cb=lambda shape: self.handle_shape(shape, inside=True),
            title=_("Mask rectangular area (inside)"),
            icon="mask_rectangle.png",
        )
        rect_out_tool = manager.add_tool(
            RectangleTool,
            toolbar_id=None,
            handle_final_shape_cb=lambda shape: self.handle_shape(shape, inside=False),
            title=_("Mask rectangular area (outside)"),
            icon="mask_rectangle_outside.png",
        )

        ellipse_tool = manager.add_tool(
            CircleTool,
            toolbar_id=None,
            handle_final_shape_cb=lambda shape: self.handle_shape(shape, inside=True),
            title=_("Mask circular area (inside)"),
            icon="mask_circle.png",
        )
        ellipse_out_tool = manager.add_tool(
            CircleTool,
            toolbar_id=None,
            handle_final_shape_cb=lambda shape: self.handle_shape(shape, inside=False),
            title=_("Mask circular area (outside)"),
            icon="mask_circle_outside.png",
        )
        polygon_tool = manager.add_tool(
            FreeFormTool,
            toolbar_id=None,
            handle_final_shape_cb=lambda shape: self.handle_shape(shape, inside=True),
            title=_("Mask polygonal area (inside)"),
            icon="mask_polyon.png",
        )
        polygon_out_tool = manager.add_tool(
            FreeFormTool,
            toolbar_id=None,
            handle_final_shape_cb=lambda shape: self.handle_shape(shape, inside=False),
            title=_("Mask polygonal area (outside)"),
            icon="mask_polygon_outside.png",
        )

        menu = QMenu()
        self.showmask_action = manager.create_action(
            _("Show image mask"), toggled=self.show_mask
        )
        showshapes_action = manager.create_action(
            _("Show masking shapes"), toggled=self.show_shapes
        )
        showshapes_action.setChecked(True)
        applymask_a = manager.create_action(
            _("Apply mask"), icon=get_icon("apply.png"), triggered=self.apply_mask
        )
        clearmask_a = manager.create_action(
            _("Clear mask"), icon=get_icon("delete.png"), triggered=self.clear_mask
        )
        removeshapes_a = manager.create_action(
            _("Remove all masking shapes"),
            icon=get_icon("delete.png"),
            triggered=self.remove_all_shapes,
        )
        add_actions(
            menu,
            (
                self.showmask_action,
                None,
                showshapes_action,
                rect_tool.action,
                ellipse_tool.action,
                polygon_tool.action,
                rect_out_tool.action,
                ellipse_out_tool.action,
                polygon_out_tool.action,
                applymask_a,
                None,
                clearmask_a,
                removeshapes_a,
            ),
        )
        self.action.setMenu(menu)
        return menu

    def apply_mask(self):
        mask = self.masked_image.get_mask()
        plot = self.get_active_plot()
        for shape, inside in self._mask_shapes[plot]:
            if isinstance(shape, RectangleShape):
                self.masked_image.align_rectangular_shape(shape)
                x0, y0, x1, y1 = shape.get_rect()
                self.masked_image.mask_rectangular_area(x0, y0, x1, y1, inside=inside)
            elif isinstance(shape, EllipseShape):
                x0, y0, x1, y1 = shape.get_rect()
                self.masked_image.mask_circular_area(x0, y0, x1, y1, inside=inside)
            else:
                self.masked_image.mask_polygonal_area(shape.points, inside=inside)
        self.masked_image.set_mask(mask)
        plot.replot()
        self.SIG_APPLIED_MASK_TOOL.emit()


class RectanglePeakTool(RectangularShapeTool):
    """a tool for fitting a diffraction peak"""
    TITLE = None
    ICON = 'peakfind.png'
    PEAK = SpotShape

    def __init__(self, manager, setup_shape_cb = None,
                 handle_final_shape_cb = None, shape_style = None,
                 toolbar_id = DefaultToolbarID, title = "find a peak", icon = None, tip = None,
                 switch_to_default_tool = None):
        super().__init__(manager,
                       self.add_shape_to_plot, shape_style,
                       toolbar_id = toolbar_id, title = title, icon = icon, tip = tip,
                       switch_to_default_tool = switch_to_default_tool)
        self.setup_shape_cb = setup_shape_cb
        self.handle_final_shape_cb = handle_final_shape_cb

    def add_shape_to_plot(self, plot, p0, p1):
        """
        Method called when shape's rectangular area
        has just been drawn on screen.
        Adding the final shape to plot and returning it.
        """
        shape = self.get_final_shape(plot, p0, p1)
        plotpoints = shape.get_points()

        image = None
        for item in plot.get_items(z_sorted=True):
            if item.isVisible() and IImageItemType in item.types():
                image = item
                break
        if image is None:
            plot.del_item(shape)
            plot.replot()
            return

        #conversion des unites du plot en pixel de l'image
        points = np.empty(plotpoints.shape)
        for i in range(4):
            points[i,0],points[i,1] = image.get_pixel_coordinates(plotpoints[i,0],plotpoints[i,1])
        #image.get_plot_coordinates give the coordinate of the BL corner of the pixel

        x1 = min(points[:,0])
        x2 = max(points[:,0])
        y1 = min(points[:,1])
        y2 = max(points[:,1])

        i_max = image.data.shape[1] - 1
        j_max = image.data.shape[0] - 1
        ix1 = max([0, min([i_max, int(round(x1))])])
        iy1 = max([0, min([j_max, int(round(y1))])])
        ix2 = max([0, min([i_max, int(round(x2))])])
        iy2 = max([0, min([j_max, int(round(y2))])])

        ix2 += 1
        iy2 += 1

        data2 = np.array(image.data[iy1:iy2,ix1:ix2])

        sx = ix2-ix1
        sy = iy2-iy1
        # Create x and y indices
        x = np.linspace(ix1, ix2-1, sx)   #ix2-1 is included
        y = np.linspace(iy1, iy2-1, sy)

        self.handle_final_shape(shape)

        isnan_data2 = np.isnan(data2).ravel()

        try:
            if isnan_data2.any():
                #we suppress nan entries and go to 2D fit directly
                x, y = np.meshgrid(x, y)
                x = x.ravel()
                y = y.ravel()
                datafitted = data2.ravel()
                datafitted = datafitted[~isnan_data2]
                x = x[~isnan_data2]
                y = y[~isnan_data2]
                i0 = np.argmax(datafitted)
                x0 = x[i0]
                y0 = y[i0]
                fond = np.amin(datafitted)
                pic = np.amax(datafitted)-fond
                sigma = ((ix2-1-ix1)+(iy2-1-iy1))/8.
                initial_guess = (pic,x0,y0,sigma,fond)

                popt, pcov = opt.curve_fit(twoD_iso_Gaussian, (x, y), datafitted, p0 = initial_guess)
                initial_guess = (popt[0],popt[1],popt[2],popt[3],popt[3],0.,popt[4])
                popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), datafitted, p0 = initial_guess)
                #conversion en unite de plot
                x0,y0 = image.get_plot_coordinates(popt[1]+0.5,popt[2]+0.5)


            else:
                #dans un premier temps on regarde les sommes 1D
                sumx = np.sum(data2,0)
                fond = np.min(sumx)
                pic = np.max(sumx)-fond
                x0 = np.argmax(sumx)+ix1
                sigma = min(1.,sx/10.)
                initial_guess = (pic,x0,sigma,fond)
                popt, pcov = opt.curve_fit(Gaussian, x, sumx, p0 = initial_guess)
                picx = popt[0]/sy
                x0 = popt[1]
                sigmax = popt[2]
                fondx = popt[3]/sy

                sumy = np.sum(data2,1)
                fond = np.min(sumy)
                pic = np.max(sumy)-fond
                y0 = np.argmax(sumy)+iy1
                sigma = min(1.,sy/10.)
                initial_guess = (pic,y0,sigma,fond)
                popt, pcov = opt.curve_fit(Gaussian, y, sumy, p0 = initial_guess)
                picy = popt[0]/sx
                y0 = popt[1]
                sigmay = popt[2]
                fondy = popt[3]/sx

                pic = (picx+picy)/2.
                sigma = (sigmax+sigmay)/2.
                fond = (fondx+fondy)/2.

                initial_guess = (pic,x0,y0,sigma,fond)
                x, y = np.meshgrid(x, y)
                popt, pcov = opt.curve_fit(twoD_iso_Gaussian, (x, y), data2.ravel(), p0 = initial_guess)

                initial_guess = (popt[0],popt[1],popt[2],popt[3],popt[3],0.,popt[4])
                popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), data2.ravel(), p0 = initial_guess)
                #conversion en unite de plot
                x0,y0 = image.get_plot_coordinates(popt[1]+0.5,popt[2]+0.5)

        except RuntimeError:
            #we try to fit with a depression
            data2 = -data2
            x = np.linspace(ix1, ix2-1, sx)   #ix2-1 is included
            y = np.linspace(iy1, iy2-1, sy)

            try:
                if isnan_data2.any():
                    #we suppress nan entries and go to 2D fit directly
                    x, y = np.meshgrid(x, y)
                    x = x.ravel()
                    y = y.ravel()
                    datafitted = data2.ravel()
                    datafitted = datafitted[~isnan_data2]
                    x = x[~isnan_data2]
                    y = y[~isnan_data2]
                    i0 = np.argmax(datafitted)
                    x0 = x[i0]
                    y0 = y[i0]
                    fond = np.amin(datafitted)
                    pic = np.amax(datafitted)-fond
                    sigma = ((ix2-1-ix1)+(iy2-1-iy1))/8.
                    initial_guess = (pic,x0,y0,sigma,fond)

                    popt, pcov = opt.curve_fit(twoD_iso_Gaussian, (x, y), datafitted, p0 = initial_guess)
                    initial_guess = (popt[0],popt[1],popt[2],popt[3],popt[3],0.,popt[4])
                    popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), datafitted, p0 = initial_guess)
                    #conversion en unite de plot
                    x0,y0 = image.get_plot_coordinates(popt[1]+0.5,popt[2]+0.5)


                else:
                    #dans un premier temps on regarde les sommes 1D
                    sumx = np.sum(data2,0)
                    fond = np.min(sumx)
                    pic = np.max(sumx)-fond
                    x0 = np.argmax(sumx)+ix1
                    sigma = min(1.,sx/10.)
                    initial_guess = (pic,x0,sigma,fond)
                    popt, pcov = opt.curve_fit(Gaussian, x, sumx, p0 = initial_guess)
                    picx = popt[0]/sy
                    x0 = popt[1]
                    sigmax = popt[2]
                    fondx = popt[3]/sy

                    sumy = np.sum(data2,1)
                    fond = np.min(sumy)
                    pic = np.max(sumy)-fond
                    y0 = np.argmax(sumy)+iy1
                    sigma = min(1.,sy/10.)
                    initial_guess = (pic,y0,sigma,fond)
                    popt, pcov = opt.curve_fit(Gaussian, y, sumy, p0 = initial_guess)
                    picy = popt[0]/sx
                    y0 = popt[1]
                    sigmay = popt[2]
                    fondy = popt[3]/sx

                    pic = (picx+picy)/2.
                    sigma = (sigmax+sigmay)/2.
                    fond = (fondx+fondy)/2.

                    initial_guess = (pic,x0,y0,sigma,fond)
                    x, y = np.meshgrid(x, y)
                    popt, pcov = opt.curve_fit(twoD_iso_Gaussian, (x, y), data2.ravel(), p0 = initial_guess)

                    initial_guess = (popt[0],popt[1],popt[2],popt[3],popt[3],0.,popt[4])
                    popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), data2.ravel(), p0 = initial_guess)
                    #conversion en unite de plot
                    x0,y0 = image.get_plot_coordinates(popt[1]+0.5,popt[2]+0.5)

            except RuntimeError as e:
                QMessageBox.warning(plot,'RuntimeError',str(e))
                plot.del_item(shape)
                plot.replot()
                return

        except TypeError as e:
            QMessageBox.warning(plot,'TypeError',str(e))
            plot.del_item(shape)
            plot.replot()
            return

        except ValueError as e:
            QMessageBox.warning(plot,'ValueError',str(e))
            plot.del_item(shape)
            plot.replot()
            return


        if  (x1<popt[1]<x2) and (y1<popt[2]<y2):

            spot = self.PEAK(x0,y0)
            spot.set_style("plot", "shape/spot")

            plot.add_item_with_z_offset(spot, SHAPE_Z_OFFSET)
            self.handle_final_shape(spot)
            plot.replot()

            spot.guess()
            itemparams = MyItemParameters(multiselection = False)
            spot.get_item_parameters(itemparams)

            doit = itemparams.edit(plot, "select lattice",icon = None)

            if not doit:
                plot.del_item(spot)

        else:
            QMessageBox.warning(plot, 'Error','Spot not found')

        #except:
        #    QMessageBox.about(plot, 'Error','Spot not found')

        plot.del_item(shape)
        plot.replot()



    def setup_shape(self, shape):
        """To be reimplemented"""
        shape.setTitle(self.TITLE)
        if self.setup_shape_cb is not None:
            self.setup_shape_cb(shape)

    def handle_final_shape(self, shape):
        """To be reimplemented"""
        if self.handle_final_shape_cb is not None:
            self.handle_final_shape_cb(shape)

class RectangleAtomTool(RectanglePeakTool):
    """a tool for fitting an atomic position"""
    TITLE = None
    ICON = 'peakfind.png'
    PEAK = AtomShape





class PeakTool(PointTool):
    """a tool for adding a diffraction peak"""
    TITLE = None

    def __init__(self, manager, setup_shape_cb = None,
                 handle_final_shape_cb = None, shape_style = None,
                 toolbar_id = DefaultToolbarID, title = "add spot position", icon = None, tip = None,
                 switch_to_default_tool = None):
        super().__init__(manager,
                       self.add_shape_to_plot, shape_style,
                       toolbar_id = toolbar_id, title = title, icon = icon, tip = tip,
                       switch_to_default_tool = switch_to_default_tool)
        self.setup_shape_cb = setup_shape_cb
        self.handle_final_shape_cb = handle_final_shape_cb

    def create_shape(self):
        shape = SpotShape(0, 0)
        self.set_shape_style(shape)
        return shape, 0, 0

    def add_shape_to_plot(self, plot, p0, p1):
        """
        Method called when shape's rectangular area
        has just been drawn on screen.
        Adding the final shape to plot and returning it.
        """
        shape = self.get_final_shape(plot, p0, p1)
        x0, y0 = canvas_to_axes(shape, p0)
        shape.set_pos(x0, y0)


        shape.guess()
        itemparams = MyItemParameters(multiselection = False)
        shape.get_item_parameters(itemparams)

        doit = itemparams.edit(plot, "select lattice",icon = None)

        if not doit:
            plot.del_item(shape)
        else:
            self.handle_final_shape(shape)
        plot.replot()


    def setup_shape(self, shape):
        """To be reimplemented"""
        shape.setTitle(self.TITLE)
        if self.setup_shape_cb is not None:
            self.setup_shape_cb(shape)

    def handle_final_shape(self, shape):
        """To be reimplemented"""
        if self.handle_final_shape_cb is not None:
            self.handle_final_shape_cb(shape)

class AtomTool(PeakTool):
    """a tool for adding an atom position"""

    def create_shape(self):
        shape = AtomShape(0, 0)
        self.set_shape_style(shape)
        return shape, 0, 0

class ReconstructionGridTool(CommandTool):
    """a tool for adding a reconstruction grid"""
    ICON = 'reconstruction.png'

    def __init__(self, manager, toolbar_id = DefaultToolbarID, title = _("Reconstruction"), icon = None, tip = None):
        if icon ==  None:
            icon = get_icon(self.ICON)
        super().__init__(manager,
                       toolbar_id = toolbar_id, title = title, icon = icon, tip = tip)


    def activate_command(self, plot, checked):
        #ouvre une fenetre pour les settings de reconstruction
        param = ReconstructionGridParam()

        lattices = [item for item in plot.get_items() if isinstance(item,LatticeGrid)]
        param.update_choices(lattices)
        ok = param.edit()

        if ok:
            self.addreconstruction(plot,param)

    def addreconstruction(self, plot, param):

        shape = ReconstructionGrid(latticeparam = param)
        shape.set_style("plot", "shape/reconstructiongrid")
        #shape.set_color(QColor("#ff5500"))  #orange
        from xrdutils.latticetools import ReconstructionGrid as OReconstructionGrid
        self.items = list(item for item in plot.get_items() if (isinstance(item, ReconstructionGrid) or isinstance(item, OReconstructionGrid)))
        n = len(self.items)+1
        param.update_shape(shape)
        shape.setTitle("Reconstruction %d"%n)
        shape.shapeparam.update_param(shape)  #update title

        plot.add_item_with_z_offset(shape, SHAPE_Z_OFFSET)

        if shape.master is not None:
            shape.master.add_slave(shape)
            z1 = shape.master.z()
            z2 = shape.z()
            shape.setZ(z1)
            shape.master.setZ(z2)

        plot.replot()

class DirectReconstructionGridTool(ReconstructionGridTool):

    def activate_command(self, plot, checked):
        #ouvre une fenetre pour les settings de reconstruction
        param = DirectReconstructionGridParam()

        lattices = [item for item in plot.get_items() if isinstance(item,LatticeGrid)]
        param.update_choices(lattices)
        ok = param.edit()

        if ok:
            self.addreconstruction(plot,param)

    def addreconstruction(self, plot, param):

        shape = DirectReconstructionGrid(latticeparam = param)
        shape.set_style("plot", "shape/reconstructiongrid")
        #shape.set_color(QColor("#ff5500"))  #orange
        from xrdutils.latticetools import DirectReconstructionGrid as ODirectReconstructionGrid
        self.items = list(item for item in plot.get_items() if (isinstance(item, DirectReconstructionGrid) or isinstance(item, ODirectReconstructionGrid)))
        n = len(self.items)+1
        param.update_shape(shape)
        shape.setTitle("Reconstruction %d"%n)
        shape.shapeparam.update_param(shape)  #update title

        plot.add_item_with_z_offset(shape, SHAPE_Z_OFFSET)

        if shape.master is not None:
            shape.master.add_slave(shape)
            z1 = shape.master.z()
            z2 = shape.z()
            shape.setZ(z1)
            shape.master.setZ(z2)

        plot.replot()



class DistorsionCorrectionTool(CommandTool):
    """a tool for applying a polygonal distortion to an image"""
    """first spot shapes need to be defined, then the distortion is applied to put the spots at the integer values of h and k"""
    ICON = 'distortion.png'
    SIG_ACTIVATED = Signal("PyQt_PyObject")

    def __init__(self, manager, toolbar_id = DefaultToolbarID,icon = None):
        if icon ==   None:
            icon = get_icon(self.ICON)
        CommandTool.__init__(self, manager, _("DistorsionCorrection"),icon = icon,
                                            tip = _("Distorsion Correction"),
                                            toolbar_id = toolbar_id)

    def activate_command(self, plot, checked):
        self.SIG_ACTIVATED.emit(plot)

class LatticeFitTool(CommandTool):
    """a tool for fitting a lattice"""
    ICON = 'fit.png'
    SIG_ACTIVATED = Signal("PyQt_PyObject")

    def __init__(self, manager, toolbar_id = DefaultToolbarID,icon = None):
        if icon ==   None:
            icon = get_icon(self.ICON)
        CommandTool.__init__(self, manager, _("Lattice Fit"),icon = icon,
                                            tip = _("Fit lattice"),
                                            toolbar_id = toolbar_id)

    def activate_command(self, plot, checked):
        self.SIG_ACTIVATED.emit(plot)


class ReciprocalPlot(ImagePlot):
    """
    Construct a 2D curve plotting widget
    (this class inherits :py:class:`guiqwt.baseplot.BasePlot`)

        * parent: parent widget
        * title: plot title
        * xlabel: (bottom axis title, top axis title) or bottom axis title only
        * ylabel: (left axis title, right axis title) or left axis title only
        * xunit: (bottom axis unit, top axis unit) or bottom axis unit only
        * yunit: (left axis unit, right axis unit) or left axis unit only
        * gridparam: GridParam instance
        * axes_synchronised: keep all x and y axes synchronised when zomming or
          panning
    """

    def __init__(self, parent = None,
                 title = None, xlabel = None, ylabel = None, zlabel = None,
                 xunit = None, yunit = None, zunit = None, yreverse = True,
                 aspect_ratio = 1.0, lock_aspect_ratio = True,
                 gridparam = None, section = "plot"):
        super().__init__(parent = parent, title = title,
                                        xlabel = xlabel, ylabel = ylabel, zlabel = zlabel,
                                        xunit = xunit, yunit = yunit, zunit = zunit, yreverse = yreverse,
                                        aspect_ratio = aspect_ratio, lock_aspect_ratio = lock_aspect_ratio,
                                        gridparam = gridparam, section = section)

        #we replace the cross_marker with a customised marker
        self.cross_marker.detach()
        self.cross_marker = ReciprocalMarker()
        self.cross_marker.set_style(section, "marker/cross")
        self.cross_marker.setVisible(False)
        self.cross_marker.attach(self)


    def keyPressEvent(self, event):
        if event.key() ==   Qt.Key_Down and event.modifiers() & Qt.AltModifier:
            self.cross_marker.mode -= 1
        if event.key() ==   Qt.Key_Up and event.modifiers() & Qt.AltModifier:
            self.cross_marker.mode +=  1
        self.cross_marker.setZ(self.get_max_z()+1)
        self.cross_marker.setVisible(True)
        self.curve_marker.setVisible(False)
        self.cross_marker.update_label()
        self.replot()


    def do_move_marker(self, event):
        pos = event.pos()
        self.set_marker_axes()
        if event.modifiers() & Qt.ShiftModifier or self.curve_pointer :
            self.curve_marker.setZ(self.get_max_z()+1)
            self.cross_marker.setVisible(False)
            self.curve_marker.setVisible(True)
            self.curve_marker.move_local_point_to(0, pos)
            self.replot()
            #self.move_curve_marker(self.curve_marker, xc, yc)
        elif event.modifiers() & Qt.AltModifier or self.canvas_pointer:
            self.cross_marker.setZ(self.get_max_z()+1)
            self.cross_marker.setVisible(True)
            self.curve_marker.setVisible(False)
            self.cross_marker.move_local_point_to(0, pos)
            self.replot()
            #self.move_canvas_marker(self.cross_marker, xc, yc)
        else:
            vis_cross = self.cross_marker.isVisible()
            vis_curve = self.curve_marker.isVisible()
            self.cross_marker.setVisible(False)
            self.curve_marker.setVisible(False)
            if vis_cross or vis_curve:
                self.replot()

    def get_visible_nodes(self, x1, x2, y1, y2, eps = 1.e-3, lattice = None):
        #return all nodes for lattices in the plot, that have their position within the bounds given
        items = self.get_items()
        if lattice == None:
            from xrdutils.latticetools import LatticeGrid as OLatticeGrid
            from xrdutils.latticetools import ReconstructionGrid as OReconstructionGrid
            lattices = [item for item in items if isinstance(item, LatticeGrid) or isinstance(item, OLatticeGrid)]   #priority to lattices
            lattices +=   [item for item in items if isinstance(item, ReconstructionGrid) or isinstance(item, OReconstructionGrid)]
        else:
            lattices = [lattice]
        nodes =  []
        eps2 = eps*eps
        if x1 > x2:
            x1, x2 = x2, x1
        if y1 > y2:
            y1, y2 = y2, y1

        for lattice in lattices:
            for idom in range(lattice.ndomains):

                h1,k1 = lattice.get_node_coordinates(x1, y1, idom)
                h2,k2 = lattice.get_node_coordinates(x2, y1, idom)
                h3,k3 = lattice.get_node_coordinates(x1, y2, idom)
                h4,k4 = lattice.get_node_coordinates(x2, y2, idom)

                hh = np.array([h1, h2, h3, h4])
                kk = np.array([k1, k2, k3, k4])
                hmin = int(np.amin(np.round(hh+0.5)))   #integer values inside the plot
                hmax = int(np.amax(np.round(hh-0.5)))   #integer values inside the plot
                kmin = int(np.amin(np.round(kk+0.5)))   #integer values inside the plot
                kmax = int(np.amax(np.round(kk-0.5)))   #integer values inside the plot
                #print (lattice, idom, hmin, hmax, kmin, kmax)
                for h in range(hmin,hmax+1):
                    for k in range(kmin,kmax+1):
                        if not lattice.centered or (h+k)%2 ==   0:
                            x, y = lattice.get_plot_coordinates(h, k, idom)
                            if (x1 < x < x2) and (y1 < y < y2):
                                for node in nodes:
                                    if ((x-node[4])**2 + (y-node[5])**2) < eps2:
                                        break
                                else:   #if all distances are > eps
                                    nodes.append((lattice,idom,h,k,x,y))   #thus common spots to reconstructions and lattices are counted only one (for the lattice)

        return nodes


class ReciprocalImageWidget(QSplitter):
    """
    Construct a BaseImageWidget object, which includes:
        * A plot (:py:class:`guiqwt.curve.CurvePlot`)
        * An `item list` panel (:py:class:`guiqwt.curve.PlotItemList`)
        * A `contrast adjustment` panel
          (:py:class:`guiqwt.histogram.ContrastAdjustment`)
        * An `X-axis cross section` panel
          (:py:class:`guiqwt.histogram.XCrossSection`)
        * An `Y-axis cross section` panel
          (:py:class:`guiqwt.histogram.YCrossSection`)

    This object does nothing in itself because plot and panels are not
    connected to each other.
    See children class :py:class:`guiqwt.plot.ImageWidget`
    """
    def __init__(self, parent = None, title = "",
                 xlabel = ("", ""), ylabel = ("", ""), zlabel = None,
                 xunit = ("", ""), yunit = ("", ""), zunit = None, yreverse = True,
                 colormap = "jet", aspect_ratio = 1.0, lock_aspect_ratio = True,
                 show_contrast = False, show_itemlist = False, show_xsection = False,
                 show_ysection = False, xsection_pos = "top", ysection_pos = "right",
                 gridparam = None, curve_antialiasing = None, **kwargs):
        if PYQT5:
            super().__init__(parent, **kwargs)
            self.setOrientation(Qt.Vertical)
        else:
            QSplitter.__init__(self, Qt.Vertical, parent)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.sub_splitter = QSplitter(Qt.Horizontal, self)
        self.plot = ReciprocalPlot(parent = self, title = title,
                              xlabel = xlabel, ylabel = ylabel, zlabel = zlabel,
                              xunit = xunit, yunit = yunit, zunit = zunit,
                              yreverse = yreverse, aspect_ratio = aspect_ratio,
                              lock_aspect_ratio = lock_aspect_ratio,
                              gridparam = gridparam)
        if curve_antialiasing is not None:
            self.plot.set_antialiasing(curve_antialiasing)

        from guiqwt.cross_section import YCrossSection
        self.ycsw = YCrossSection(self, position = ysection_pos,
                                  xsection_pos = xsection_pos)
        self.ycsw.setVisible(show_ysection)

        from guiqwt.cross_section import XCrossSection
        self.xcsw = XCrossSection(self)
        self.xcsw.setVisible(show_xsection)

        self.xcsw.SIG_VISIBILITY_CHANGED.connect(self.xcsw_is_visible)

        self.xcsw_splitter = QSplitter(Qt.Vertical, self)
        if xsection_pos ==   "top":
            self.xcsw_splitter.addWidget(self.xcsw)
            self.xcsw_splitter.addWidget(self.plot)
        else:
            self.xcsw_splitter.addWidget(self.plot)
            self.xcsw_splitter.addWidget(self.xcsw)
        self.xcsw_splitter.splitterMoved.connect(
                                 lambda pos, index: self.adjust_ycsw_height())

        self.ycsw_splitter = QSplitter(Qt.Horizontal, self)
        if ysection_pos ==   "left":
            self.ycsw_splitter.addWidget(self.ycsw)
            self.ycsw_splitter.addWidget(self.xcsw_splitter)
        else:
            self.ycsw_splitter.addWidget(self.xcsw_splitter)
            self.ycsw_splitter.addWidget(self.ycsw)

        configure_plot_splitter(self.xcsw_splitter,
                                decreasing_size = xsection_pos ==   "bottom")
        configure_plot_splitter(self.ycsw_splitter,
                                decreasing_size = ysection_pos ==   "right")

        self.sub_splitter.addWidget(self.ycsw_splitter)

        self.itemlist = PlotItemList(self)
        self.itemlist.setVisible(show_itemlist)
        self.sub_splitter.addWidget(self.itemlist)

        # Contrast adjustment (Levels histogram)
        from guiqwt.histogram import ContrastAdjustment
        self.contrast = ContrastAdjustment(self)
        self.contrast.setVisible(show_contrast)
        self.addWidget(self.contrast)

        configure_plot_splitter(self)
        configure_plot_splitter(self.sub_splitter)

    def adjust_ycsw_height(self, height = None):
        if height is None:
            height = self.xcsw.height()-self.ycsw.toolbar.height()
        self.ycsw.adjust_height(height)
        if height:
            QApplication.processEvents()

    def xcsw_is_visible(self, state):
        if state:
            QApplication.processEvents()
            self.adjust_ycsw_height()
        else:
            self.adjust_ycsw_height(0)


class ReciprocalDialog(ImageDialog):
    """Construct a Reciprocal Dialog which includes a plotting widget"""
    #une fenetre qui permet d'afficher les images du reseau reciproque et gere les datas associees
    def __init__(self, edit = False, toolbar = True,options = {"show_contrast":True,"yreverse":False}, parent = None, data = None):
        ImageDialog.__init__(self, edit = edit, toolbar = toolbar, wintitle = "Reciprocal window",options = options)
        self.setGeometry(QRect(100,100,800, 600))
        self.plot = self.get_plot()
        self.create_menubar()
        self.plot.SIG_ITEM_REMOVED.connect(self.refreshlist)
        self.show()
        self.set_cwd(os.getcwd())

    def register_tools(self):
        """
        Register the plotting dialog box tools: the base implementation
        provides standard, image-related and other tools - i.e. calling
        this method is exactly the same as calling
        :py:meth:`guiqwt.plot.CurveDialog.register_all_image_tools`

        This method may be overriden to provide a fully customized set of tools
        """
        self.register_all_image_tools()
        self.add_tool(MyImageMaskTool)
        self.add_tool(LatticeGridTool)
        self.reconstructiongridtool = self.add_tool(ReconstructionGridTool)
        self.add_tool(RectanglePeakTool)
        self.add_tool(PeakTool)
        self.dctool = self.add_tool(DistorsionCorrectionTool)
        self.dctool.SIG_ACTIVATED.connect(self.apply_distortion_tool)
        self.fittool = self.add_tool(LatticeFitTool)
        self.fittool.SIG_ACTIVATED.connect(self.apply_fit_tool)
        self.add_tool(LabelTool)
        self.add_tool(FreeFormTool)


    def create_menubar(self):
        self.menubar = QMenuBar(self)
        self.layout().setMenuBar(self.menubar)

        """***************File Menu*******************************"""

        self.menuFile = QMenu("File",self.menubar)

        self.actionOpen = QAction("Open plot",self.menuFile)
        self.actionOpen.setShortcut("Ctrl+O")

        self.actionImport = QAction("Import image",self.menuFile)

        self.actionSave = QAction("Save plot",self.menuFile)
        self.actionSave.setShortcut("Ctrl+S")

        self.actionQuit = QAction("Quit",self.menuFile)
        self.actionQuit.setShortcut("Ctrl+Q")

        self.menuFile.addActions((self.actionOpen, self.actionImport, self.actionSave, self.actionQuit))

        """***************Operation Menu*******************************"""

        self.menuOperation = QMenu("Operation",self.menubar)

        self.actionAverageSpots = QAction("Average spots",self.menuOperation)
        self.actionAverageImage = QAction("Average image",self.menuOperation)
        self.menuOperation.addActions((self.actionAverageSpots,self.actionAverageImage))



        """**********************************************"""

        self.menubar.addMenu(self.menuFile)
        self.menubar.addMenu(self.menuOperation)

        self.actionOpen.triggered.connect(self.get_open_filename)
        self.actionImport.triggered.connect(self.get_import_filename)
        self.actionSave.triggered.connect(self.get_save_filename)
        self.actionQuit.triggered.connect(self.close)
        self.actionAverageSpots.triggered.connect(self.average_spots)
        self.actionAverageImage.triggered.connect(self.average_image_from_lattice)

    def create_plot(self, options, row = 0, column = 0, rowspan = 1, columnspan = 1):
        """
        Create the plotting widget (which is an instance of class
        :py:class:`guiqwt.plot.BaseImageWidget`), add it to the dialog box
        main layout (:py:attr:`guiqwt.plot.CurveDialog.plot_layout`) and
        then add the `item list`, `contrast adjustment` and X/Y axes
        cross section panels.

        May be overriden to customize the plot layout
        (:py:attr:`guiqwt.plot.CurveDialog.plot_layout`)
        """
        self.plot_widget = ReciprocalImageWidget(self, **options)
        self.plot_layout.addWidget(self.plot_widget,
                                   row, column, rowspan, columnspan)

        # Configuring plot manager
        self.add_plot(self.plot_widget.plot)
        self.add_panel(self.plot_widget.itemlist)
        self.add_panel(self.plot_widget.xcsw)
        self.add_panel(self.plot_widget.ycsw)
        self.add_panel(self.plot_widget.contrast)

    def import_image(self,image,title = None):
        #this function may be used by another script python to directly import an image in the dialog
        #we import an image from another window, possibly changing the title
        newimage = make.maskedimagenan(image.data,title = title, show_mask=True)
        newimage.imageparam.update_param(image)
        if title is not None:
            newimage.imageparam.label = title
        newimage.imageparam.update_image(newimage)
        self.plot.add_item(newimage)
        self.plot.replot()

    def add_data(self,data,xdata = [None, None], ydata = [None, None],colormap = "bone",transformable = False):
        #data est un tableau 2D qui represente les donnees
        #xdata sont les bornes en x, ydata les bornes en y
        self.data = np.array(data,np.float32)
        #on regarde s'il y a quelque chose de dessine
        listimage = self.plot.get_items(item_type = IColormapImageItemType)
        if len(listimage)>0:
            self.defaultcolormap = listimage[0].get_color_map_name()
        else:
            self.defaultcolormap = "bone"
        if transformable:
            image = make.trimage(self.data, colormap = colormap)
        else:
            image = make.image(self.data, xdata = xdata,ydata = ydata,colormap = colormap)
        self.plot.add_item(image)

    def refreshlist(self,item):
        #quand on efface un item
        from xrdutils.latticetools import LatticeGrid as OLatticeGrid
        from xrdutils.latticetools import ReconstructionGrid as OReconstructionGrid

        if (isinstance(item, LatticeGrid) or isinstance(item, OLatticeGrid)):
            #on supprime referencement aux reconstructions
            if len(item.slaves)>0:
                for slave in item.slaves:
                    slave.master = None
        if (isinstance(item, ReconstructionGrid) or isinstance(item, OReconstructionGrid)):
            if item.master is not None:
                item.master.remove_slave(item)

    def average_spots(self, checked = True):

        spots = list(item for item in self.plot.get_selected_items() if (isinstance(item, SpotShape) and item.selected==True))
        xm = 0.
        ym = 0.
        if len(spots) == 0:
            return

        for spot in spots:
            xm += spot.x0
            ym += spot.y0

        xm = xm/len(spots)
        ym = ym/len(spots)

        spot = SpotShape(xm,ym)
        spot.set_style("plot", "shape/spot")

        self.plot.add_item_with_z_offset(spot, SHAPE_Z_OFFSET)

        self.plot.replot()

        spot.guess()
        itemparams = MyItemParameters(multiselection = False)
        spot.get_item_parameters(itemparams)

        doit = itemparams.edit(self.plot, "select lattice",icon = None)
        for spot in spots:
            self.plot.del_item(spot)


    def average_image_from_lattice(self):

        lattices = list(item for item in self.plot.get_selected_items() if isinstance(item, LatticeGrid))
        if len(lattices) == 0:
            lattices = list(item for item in self.plot.get_items() if isinstance(item, LatticeGrid))
            if len(lattices) == 0:
                return

        images = list(item for item in self.plot.get_selected_items() if isinstance(item, ImageItem))
        if len(images) ==  0:
            images = list(item for item in self.plot.get_items() if isinstance(item, ImageItem))
            if len(images) ==  0:
                QMessageBox.warning(self.plot, "warning","no image found")
                print ("warning, no image found")
                return

        image = images[0]
        print ('active image:',image.imageparam.label)

        lattice = lattices[0]
        O = lattice.O
        u = lattice.u
        v = lattice.v
        sg = lattice.sg
        uR = lattice.uR

        #on determine le rectangle dans lequel la maille va être dessinée
        #on part sur un côté de 512

        hdec = (v[1]*u[1]+v[0]*u[0])/uR
        vperp = (v[1]*u[0]-v[0]*u[1])/uR  #projection of v along the perpendicular of u
        ysize = np.abs(vperp)
        xsize = uR + np.abs(hdec)
        nx = int(round(xsize/uR*512))
        ny = int(round(ysize/uR*512))
        size = (nx,ny)

        pilim = Image.fromarray(np.ma.array(image.data,dtype=float).filled(np.nan))
        #quad in the plot referential




        bound = image.boundingRect()
        x1, x2 = bound.left(), bound.right()
        y1, y2 = bound.top(), bound.bottom()

        lattice2 = LatticeGrid()
        lattice2.latticeparam.update_param(lattice)
        lattice2.latticeparam.RT = True
        lattice2.latticeparam.vm = False

        if lattice.sg in ['p6','p6mm']:
            irmax = 6
        elif lattice.sg in ['p3','p3m1','p31m']:
            irmax = 3
        elif lattice.sg in ['p4','p4mm','p4gm']:
            irmax = 4
        elif lattice.sg in ['p2','p2mm','p2mg','p2gg','c2mm']:
            irmax = 2
        else:
            irmax = 1
        dt = 360/irmax

        averagedata = []

        for ir in range(irmax):
            lattice2.latticeparam.uT = lattice.latticeparam.uT+ir*dt
            lattice2.latticeparam.update_shape(lattice2)

            nodes = self.plot.get_visible_nodes(x1, x2, y1, y2, lattice = lattice2)

            indices = []
            for node in nodes:
                indices.append((node[2],node[3]))
                indices.append((node[2]-1,node[3]))
                indices.append((node[2],node[3]-1))
                indices.append((node[2]-1,node[3]-1))
            indices = set(indices)
            u2 = lattice2.u
            v2 = lattice2.v
            #print (u,v)
            #print (u2,v2)

            hd2 = (v2[1]*u2[1]+v2[0]*u2[0])/uR
            if hd2<0:   #angle>90°
                points = [[O[0]+hd2/uR*u2[0], O[1]+hd2/uR*u2[1]],
                          [O[0]+v2[0], O[1]+v2[1]],
                          [O[0]+v2[0]+u2[0]-hd2/uR*u2[1], O[1]+v2[1]+u2[1]-hd2/uR*u2[1]],
                          [O[0]+u2[0], O[1]+u2[1]]]
            else:   #angle<90°
                points = [[O[0], O[1]],
                          [O[0]+v2[0]-hd2/uR*u2[0], O[1]+v2[1]-hd2/uR*u2[1]],
                          [O[0]+v2[0]+u2[0], O[1]+v2[1]+u2[1]],
                          [O[0]+u2[0]+hd2/uR*u2[0], O[1]+u2[1]+hd2/uR*u2[1]]]

            for i,j in indices:
                try:
                    quad = []
                    for point in points:
                        quad += list(image.get_pixel_coordinates(point[0]+u2[0]*i+v2[0]*j,point[1]+u2[1]*i+v2[1]*j))
                    #print (ir,i,j)
                    #print (quad)

                    pilimt=pilim.transform(size, Image.Transform.QUAD, quad, Image.Transform.AFFINE)
                    mask = Image.new("L", pilim.size, 255)
                    mask = mask.transform(size, Image.Transform.QUAD, quad, Image.Transform.AFFINE)

                    img = Image.new("F", size, np.nan)
                    img.paste(pilimt, mask)

                    newdata = np.ma.masked_invalid(np.asarray(img))
                    averagedata.append(newdata)

                    if newdata.count()>0:
                        title = 'rot_%d_node_%d,%d'%(ir,i,j)
                        newimage = make.maskedimagenan(np.ma.array(newdata, copy=True),title=title)
                        self.plot.add_item(newimage)

                except Exception as e:
                    print (e)
                    print (quad)

        #print (averagedata)
        averagedata = np.ma.masked_array(averagedata)
        averagedata = np.mean(averagedata,axis=0)
        #print (averagedata)

        newimage = make.maskedimagenan(averagedata.filled(np.nan),title='average image',show_mask=True)
        self.plot.add_item(newimage)








    def apply_distortion_tool(self, plot):

        spots = list(item for item in plot.get_items() if isinstance(item, SpotShape))
        from guiqwt.image import ImageItem

        images = list(item for item in plot.get_selected_items() if isinstance(item, ImageItem))
        if len(images) ==  0:
            images = list(item for item in plot.get_items() if isinstance(item, ImageItem))
            if len(images) ==  0:
                QMessageBox.warning(plot, "warning","no image found")
                print ("warning, no image found")
                return

        image = images[0]
        print ('active image:',image.imageparam.label)
        xexp = list()
        yexp = list()
        xth = list()
        yth = list()

        for item in spots:

            x,y = image.get_pixel_coordinates(item.x0,item.y0)
            h = item.h
            k = item.k
            xexp.append(x)
            yexp.append(y)
            if item.lattice is not None:
                _x0,_y0  = item.lattice.get_plot_coordinates(h, k, item.latticedomain)  #plot coordinates of the node
                x1,y1 = image.get_pixel_coordinates(_x0,_y0)

                xth.append(x1)
                yth.append(y1)
        if len(xexp)<=2:
            QMessageBox.warning(plot, "warning","not enough referenced points\n to compute the distortion")
            print ("warning, not enough referenced points")
            return

        p3x,p3y = compute_distortion(xexp,yexp,xth,yth)
        print ('distortion = ',p3x,p3y)

        #corrige la distortion pour cela on mappe l'image d'arrivee avec des rectangles qui correspondent
        #a des quadrilateres sur l'image non deformee. Ceux-ci sont obtenus par la transformation
        #xth->xexp, yth->yexp
        #dans un premier temps, on regarde la valeur de p3x[1] et p3xy[2] pour savoir sur quelle taille d'image
        #faire la correction
        data = np.array(image.data, np.float32)
        pilim = Image.fromarray(data, mode = 'F')  #using PIL image deformation

        w,h = pilim.size
        meshdata = []
        print ("pixel 0,0 ",pilim.getpixel((0,0)))
        #dx = (x2-x1)/10.
        #dy = (y2-y1)/10.

        ninter = 10
        nsteps = ninter+1
        xdest = np.linspace(0,w,nsteps)
        ydest = np.linspace(0,h,nsteps)
        xgrid,ygrid = np.meshgrid(xdest,ydest)   #ensemble des valeurs de x et y de destination
        xgrid=xgrid.ravel()
        ygrid=ygrid.ravel()
        xstart=corr_cub(p3x,xgrid,ygrid)       #ensemble des valeurs de x et y de depart
        ystart=corr_cub(p3y,xgrid,ygrid)
        fic=open('output.txt','w')
        for i in range(len(xgrid)):
          fic.write('%f %f %f %f'%(xgrid[i],ygrid[i],xstart[i],ystart[i]))
          fic.write('\n')
        fic.close()

        xstart=xstart.reshape(nsteps,nsteps)
        ystart=ystart.reshape(nsteps,nsteps)

        xdest=np.array(np.around(xdest),int)
        ydest=np.array(np.around(ydest),int)
        xstart=np.array(np.around(xstart),int)
        ystart=np.array(np.around(ystart),int)

        meshdata=[]
        for j in range(ninter):
            for i in range(ninter):
                xl=xdest[i]
                xr=xdest[i+1]
                yu=ydest[j]
                yl=ydest[j+1]
                xul=xstart[j,i]
                xur=xstart[j,i+1]
                yul=ystart[j,i]
                yur=ystart[j,i+1]
                xll=xstart[j+1,i]
                xlr=xstart[j+1,i+1]
                yll=ystart[j+1,i]
                ylr=ystart[j+1,i+1]
                meshdata.append(((xl,yu,xr,yl),(xul,yul,xll,yll,xlr,ylr,xur,yur)))
        img=pilim.transform((w,h),Image.MESH,meshdata,Image.BICUBIC)
        print ("pixel 100,100 ",img.getpixel((100,100)))

        #data=np.array(img).reshape((w,h))
        data=np.array(img)

        win2 = self.duplicate(plot)
        plot2 = win2.get_plot()

        param = image.imageparam

        image2 = make.maskedimagenan(data, title=None, alpha_mask=param.alpha_mask,
              alpha=param.alpha, colormap=param.colormap,
              xdata=[param.xmin, param.xmax], ydata=[param.ymin, param.ymax],show_mask=True)

        plot2.add_item(image2, z=-1)

        #on effectue la transformation sur les taches identifiees
        spotitems=list(item for item in plot.items if (isinstance(item, SpotShape)))
        spotitems2=list(item for item in plot2.items if (isinstance(item, SpotShape)))

        for i in range(len(spotitems)):
            item = spotitems[i]
            item2 = spotitems2[i]
            #coordonnees pixels
            x0,y0 = image.get_pixel_coordinates(item.x0,item.y0)
            #on recherche le polygone de depart
            jquad = -1
            for j in range(len(meshdata)):   #on pourrait faire plus rapide en affinant a partir de la position supposee....
                quad = mplPath(np.array(meshdata[j][1]).reshape(4,2))
                if quad.contains_point([x0,y0]):
                    jquad = j
                    break
            if jquad ==   -1:
                #le point n'est pas sur l'image d'arrivee, on le supprime
                 win2.plot.del_item(item2)
            else:
                #on resoud le systeme d'equation lineaire pour obtenir les coefficients
                #de la transformation quad->rectangle
                xl,yu,xr,yl = meshdata[j][0]
                xul,yul,xll,yll,xlr,ylr,xur,yur = meshdata[j][1]
                A = [[xul,yul,1.,0.,0.,0.,-xul*xl,-yul*xl],
                   [xur,yur,1.,0.,0.,0.,-xur*xr,-yur*xr],
                   [xll,yll,1.,0.,0.,0.,-xll*xl,-yll*xl],
                   [xlr,ylr,1.,0.,0.,0.,-xlr*xr,-ylr*xr],
                   [0.,0.,0.,xul,yul,1.,-xul*yu,-yul*yu],
                   [0.,0.,0.,xll,yll,1.,-xll*yl,-yll*yl],
                   [0.,0.,0.,xur,yur,1.,-xur*yu,-yur*yu],
                   [0.,0.,0.,xlr,ylr,1.,-xlr*yl,-ylr*yl]]
                B = [xl,xr,xl,xr,yu,yl,yu,yl]

                C = np.linalg.solve(A,B)
                a,b,c,d,e,f,g,h = C
                #transformation quad->rectangle
                x1 = (a*x0+b*y0+c)/(g*x0+h*y0+1.)
                y1 = (d*x0+e*y0+f)/(g*x0+h*y0+1.)
                #coordonnees plot
                x2,y2 = image.get_plot_coordinates(x1,y1)
                item2.set_pos(x2,y2)

        print ("ok")
        win2.show()

    def apply_fit_tool(self, plot):

        lattices = list(item for item in plot.get_selected_items() if isinstance(item, LatticeGrid()))
        lattice = lattices[0]
        spots = list(item for item in plot.get_items() if isinstance(item, SpotShape))
        spots = list(spot for spot in spots if spot.lattice == lattice)

        for item in spots:

            x,y = item.x0,item.y0
            h,k = item.h,item.k

            item.lattice.get_plot_coordinates(h, k, item.latticedomain)



    def duplicate(self, plot = None):
        """duplicate latticegrid, reconstructiongrid and spotshapes"""

        if plot ==   None:
            plot = self.get_plot()
        allitems = plot.items

        from xrdutils.latticetools import LatticeGrid as OLatticeGrid

        latticeitems = list(item for item in allitems if (isinstance(item, LatticeGrid) or isinstance(item, OLatticeGrid)))
        spotitems = list(item for item in allitems if (isinstance(item, SpotShape)))

        win2 = ReciprocalDialog()
        plot2 = win2.get_plot()
        #on rajoute les latticeitems
        for item in latticeitems:
            item2  = LatticeGrid(O = item.O, u = item.u, v = item.v, sg = item.sg, centered = item.centered)
            set_identical_shapeparam(item,item2)
            item2.setZ(item.z())
            plot2.add_item(item2)

            #on rajoute les reconstructionitems avec les nouvelles relations master-slave
            #item2.slaves = list()  #on repart de zero
            for slave in item.slaves:
                slave2 = ReconstructionGrid(A = slave.A, B = slave.B,
                                            master = item2, sym = slave.sym, centered = slave.centered)
                set_identical_shapeparam(slave,slave2)
                slave2.setZ(item.z())
                plot2.add_item(slave2)

        #on rajoute les spotitems
        for item in (atomitems):
            item.shapeparam.update_param(item)#certains parametres ne sont pas forcement actualises

            item2 = AtomShape()
            item.shapeparam.update_shape(item2) #update shape from shapeparam
            item.latticeparam.update_shape(item2)
            item2.setZ(item.z())
            plot2.add_item(item2)
            item2.set_lattice_from_latticename(item2.lattice.shapeparam.label) #needs to be added on a plot

        return win2

    def set_cwd(self,dirname):
        self.cwd = dirname

    def get_open_filename(self, checked = True):
        filename = QFileDialog.getOpenFileName(None,'open pickle file',self.cwd)[0]
        if len(filename) ==  0:
             return
        self.set_cwd(osp.dirname(filename))
        self.load_pickle(filename)

    def get_import_filename(self, checked = True):
        filename = QFileDialog.getOpenFileName(None,'import image file',self.cwd)[0]
        if len(filename) ==  0:
             return
        self.set_cwd(osp.dirname(filename))
        self.open_image(filename)

    def get_save_filename(self, checked = True):
        filename = QFileDialog.getSaveFileName(None, 'save to pickle file',self.cwd)[0]
        if len(filename) ==  0:
            return
        self.set_cwd(osp.dirname(filename))
        self.save_pickle(filename)

    def open_image(self, filename):
        try:
            if filename.endswith(".gsf"):
                from SPMutils.SPMio import read_gsf
                data,metadatas = read_gsf(filename)
                data = np.flipud(data)
                newimage = make.maskedimagenan(data = data, title = filename)
            else:
                newimage = make.maskedimagenan(filename = filename)

            #newimage.imageparam.label = title
            #newimage.imageparam.update_image(newimage)
            self.plot.add_item(newimage)
            self.plot.replot()
        except Exception as e:
            QMessageBox.warning(self,'Import Error',str(e))

    def save_pickle(self,  filename):
        """
        Save (serializable) items to file using the :py:mod:`pickle` protocol
            * iofile: file object or filename
            * selected = False: if True, will save only selected items

        See also :py:meth:`guiqwt.baseplot.BasePlot.restore_items`
        """
        iofile = open( filename, "wb")

        items = self.plot.get_items(item_type = ISerializableType)

        pickle.dump(items, iofile)

    def load_pickle(self, filename):
        """
        Restore items from file using the :py:mod:`pickle` protocol
            * iofile: file object or filename

        See also :py:meth:`guiqwt.baseplot.BasePlot.save_items`
        """
        iofile = open( filename, "rb")
        items = pickle.load(iofile)
        for item in items:
            self.plot.add_item(item)
            slaves = getattr(item, 'slaves', [])
            for slave in slaves:
                self.plot.add_item(slave)


    def closeEvent(self,event):
        result = QMessageBox.question(self,
                      "Confirm Exit...",
                      "Are you sure you want to exit ?",
                      QMessageBox.Yes| QMessageBox.No)

        if result !=   QMessageBox.Yes:
            event.ignore()

    def reject(self):
        #we override the reject method to avoid window closing with esc key
        pass


class DirectSpaceDialog(ReciprocalDialog):
    """Construct a Direct Space Dialog which includes a plotting widget"""
    #une fenetre qui permet d'afficher les images du reseau direct et gere les datas associees
    def __init__(self, edit = False, toolbar = True,options = {"show_contrast":True,"yreverse":False}, parent = None, data = None):
        ImageDialog.__init__(self, edit = edit, toolbar = toolbar, wintitle = "Direct Space window",options = options)
        self.setGeometry(QRect(100,100,800, 600))
        self.plot = self.get_plot()
        self.create_menubar()
        self.plot.SIG_ITEM_REMOVED.connect(self.refreshlist)
        self.show()
        self.set_cwd(os.getcwd())

    def register_tools(self):
        """
        Register the plotting dialog box tools: the base implementation
        provides standard, image-related and other tools - i.e. calling
        this method is exactly the same as calling
        :py:meth:`guiqwt.plot.CurveDialog.register_all_image_tools`

        This method may be overriden to provide a fully customized set of tools
        """
        self.register_all_image_tools()
        self.add_tool(MyImageMaskTool)
        self.add_tool(DirectLatticeGridTool)
        self.reconstructiongridtool = self.add_tool(DirectReconstructionGridTool)
        self.add_tool(RectangleAtomTool)
        self.add_tool(AtomTool)
        self.dctool = self.add_tool(DistorsionCorrectionTool)
        self.dctool.SIG_ACTIVATED.connect(self.apply_distortion_tool)
        self.add_tool(LabelTool)
        self.add_tool(FreeFormTool)


    def create_menubar(self):
        self.menubar = QMenuBar(self)
        self.layout().setMenuBar(self.menubar)

        """***************File Menu*******************************"""

        self.menuFile = QMenu("File",self.menubar)

        self.actionOpen = QAction("Open plot",self.menuFile)
        self.actionOpen.setShortcut("Ctrl+O")

        self.actionImport = QAction("Import image",self.menuFile)

        self.actionSave = QAction("Save plot",self.menuFile)
        self.actionSave.setShortcut("Ctrl+S")

        self.actionQuit = QAction("Quit",self.menuFile)
        self.actionQuit.setShortcut("Ctrl+Q")

        self.menuFile.addActions((self.actionOpen, self.actionImport, self.actionSave, self.actionQuit))

        """***************Operation Menu*******************************"""

        self.menuOperation = QMenu("Operation",self.menubar)
        self.actionAverageImage = QAction("Average image",self.menuOperation)
        self.actionAveragedUnitCell = QAction("Averaged unit cell",self.menuOperation)
        self.menuOperation.addActions((self.actionAverageImage,self.actionAveragedUnitCell))


        """**********************************************"""

        self.menubar.addMenu(self.menuFile)
        self.menubar.addMenu(self.menuOperation)

        self.actionOpen.triggered.connect(self.get_open_filename)
        self.actionImport.triggered.connect(self.get_import_filename)
        self.actionSave.triggered.connect(self.get_save_filename)
        self.actionQuit.triggered.connect(self.close)
        self.actionAverageImage.triggered.connect(self.average_image_from_lattice)
        self.actionAveragedUnitCell.triggered.connect(self.make_averaged_unit_cell)

    def create_plot(self, options, row = 0, column = 0, rowspan = 1, columnspan = 1):
        """
        Create the plotting widget (which is an instance of class
        :py:class:`guiqwt.plot.BaseImageWidget`), add it to the dialog box
        main layout (:py:attr:`guiqwt.plot.CurveDialog.plot_layout`) and
        then add the `item list`, `contrast adjustment` and X/Y axes
        cross section panels.

        May be overriden to customize the plot layout
        (:py:attr:`guiqwt.plot.CurveDialog.plot_layout`)
        """
        self.plot_widget = ReciprocalImageWidget(self, **options)
        self.plot_layout.addWidget(self.plot_widget,
                                   row, column, rowspan, columnspan)

        # Configuring plot manager
        self.add_plot(self.plot_widget.plot)
        self.add_panel(self.plot_widget.itemlist)
        self.add_panel(self.plot_widget.xcsw)
        self.add_panel(self.plot_widget.ycsw)
        self.add_panel(self.plot_widget.contrast)

    def import_image(self,image,title = None):
        #this function may be used by another script python to directly import an image in the dialog
        #we import an image from another window, possibly changing the title
        #we flipud the data since it is flipped in maskedimagenan
        newimage = make.maskedimagenan(np.flipud(image.data),title = title,show_mask=True)
        newimage.imageparam.update_param(image)
        if title is not None:
            newimage.imageparam.label = title
        newimage.imageparam.update_image(newimage)
        self.plot.add_item(newimage)
        print (newimage)
        self.plot.replot()

    def add_data(self,data,xdata = [None, None], ydata = [None, None],colormap = "bone",transformable = False):
        #data est un tableau 2D qui represente les donnees
        #xdata sont les bornes en x, ydata les bornes en y
        self.data = np.array(data,np.float32)
        #on regarde s'il y a quelque chose de dessine
        listimage = self.plot.get_items(item_type = IColormapImageItemType)
        if len(listimage)>0:
            self.defaultcolormap = listimage[0].get_color_map_name()
        else:
            self.defaultcolormap = "bone"
        if transformable:
            image = make.trimage(self.data, colormap = colormap)
        else:
            image = make.image(self.data, xdata = xdata,ydata = ydata,colormap = colormap)
        self.plot.add_item(image)

    def refreshlist(self,item):
        #quand on efface un item
        from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
        from xrdutils.latticetools import DirectReconstructionGrid as ODirectReconstructionGrid

        if (isinstance(item, DirectLatticeGrid) or isinstance(item, ODirectLatticeGrid)):
            #on supprime referencement aux reconstructions
            if len(item.slaves)>0:
                for slave in item.slaves:
                    slave.master = None
        if (isinstance(item, DirectReconstructionGrid) or isinstance(item, ODirectReconstructionGrid)):
            if item.master is not None:
                item.master.remove_slave(item)

    def average_atoms(self, checked = True):

        atoms = list(item for item in self.plot.get_selected_items() if (isinstance(item, AtomShape) and item.selected==True))
        xm = 0.
        ym = 0.
        if len(atoms) == 0:
            return

        for atom in atoms:
            xm += atom.x0
            ym += atom.y0

        xm = xm/len(atoms)
        ym = ym/len(atoms)

        atom = AtomShape(xm,ym)
        atom.set_style("plot", "shape/spot")

        self.plot.add_item_with_z_offset(atom, SHAPE_Z_OFFSET)

        self.plot.replot()

        atom.guess()
        itemparams = MyItemParameters(multiselection = False)
        atom.get_item_parameters(itemparams)

        doit = itemparams.edit(self.plot, "select lattice",icon = None)
        for atom in atoms:
            self.plot.del_item(spot)


    def average_image_from_lattice(self):
        #average image by translation along lattice vectors
        #do not take into account lattice symetries

        lattices = list(item for item in self.plot.get_selected_items() if isinstance(item, DirectLatticeGrid))
        if len(lattices) == 0:
            lattices = list(item for item in self.plot.get_items() if isinstance(item, DirectLatticeGrid))
            if len(lattices) == 0:
                return

        images = list(item for item in self.plot.get_selected_items() if isinstance(item, ImageItem))
        if len(images) ==  0:
            images = list(item for item in self.plot.get_items() if isinstance(item, ImageItem))
            if len(images) ==  0:
                QMessageBox.warning(self.plot, "warning","no image found")
                print ("warning, no image found")
                return

        image = images[0]
        print ('active image:',image.imageparam.label)

        lattice = lattices[0]
        O = lattice.O
        u = lattice.u
        v = lattice.v
        sg = lattice.sg
        uR = lattice.uR

        data = np.ma.filled(image.data, fill_value=0)   #masked array, we replace masked values with zero
        data = np.array(data,dtype = float)
        count = 1-np.array(image.data.mask,dtype = float)  #0 for masked values, 1 otherwise

        shift1 = np.array([u[0],u[1]])
        shift2 = np.array([v[0],v[1]])

        sy = data.shape[0]
        sx = data.shape[1]

        matrix = np.matrix([[u[0],v[0]],[u[1],v[1]]])
        invmatrix = np.linalg.inv(matrix)

        s1 = np.dot(invmatrix,[sx,0]).A1
        s2 = np.dot(invmatrix,[sx,sy]).A1
        s3 = np.dot(invmatrix,[0,sy]).A1

        i1max = max(abs(s1[0]),abs(s2[0]),abs(s3[0]))
        i1max = int(i1max)+1

        i2max = max(abs(s1[1]),abs(s2[1]),abs(s3[1]))
        i2max = int(i2max)+1


        data_sum = np.zeros_like(data)
        count_sum = np.zeros_like(count)

        for i1 in range(-i1max,i1max):
            for i2 in range(-i2max,i2max):
                shift = shift1*i1 + shift2*i2  #raw,column
                if (np.abs(shift[0])<sx) and (np.abs(shift[1])<sy):
                    shift = (shift[1],shift[0])
                    print ("add shifted image",shift)

                    shifted = ndimage.shift(data, shift, output=None, order=1, mode='constant', cval=0.0, prefilter=False)
                    data_sum += shifted

                    shifted = ndimage.shift(count, shift, output=None, order=1, mode='constant', cval=0.0, prefilter=False)
                    count_sum += shifted

                #print (array2[245:250,245:250])
                #print (data[245:250,245:250])
                #item = make.image(array2,  colormap='gray' ,interpolation = "nearest")
                #plot.add_item(item)
        data_sum = data_sum/count_sum

        newimage = make.maskedimagenan(np.flipud(data_sum),title='averaged image',show_mask=True)

        image.imageparam.update_image(newimage)

        self.plot.add_item(newimage)

        plot = win.get_plot()

        win.show()
        win.exec_()






    def make_averaged_unit_cell(self):
        #draw an averaged unit cell in the form of a rectangular item
        lattices = list(item for item in self.plot.get_selected_items() if isinstance(item, DirectLatticeGrid))
        if len(lattices) == 0:
            lattices = list(item for item in self.plot.get_items() if isinstance(item, DirectLatticeGrid))
            if len(lattices) == 0:
                return

        images = list(item for item in self.plot.get_selected_items() if isinstance(item, ImageItem))
        if len(images) ==  0:
            images = list(item for item in self.plot.get_items() if isinstance(item, ImageItem))
            if len(images) ==  0:
                QMessageBox.warning(self.plot, "warning","no image found")
                print ("warning, no image found")
                return

        image = images[0]
        print ('active image:',image.imageparam.label)

        lattice = lattices[0]
        O = lattice.O
        u = lattice.u
        v = lattice.v
        sg = lattice.sg
        uR = lattice.uR

        #on determine le rectangle dans lequel la maille va être dessinée
        #on part sur un côté de 512

        hdec = (v[1]*u[1]+v[0]*u[0])/uR
        vperp = (v[1]*u[0]-v[0]*u[1])/uR  #projection of v along the perpendicular of u
        ysize = np.abs(vperp)
        xsize = uR + np.abs(hdec)
        nx = int(round(xsize/uR*512))
        ny = int(round(ysize/uR*512))
        size = (nx,ny)

        pilim = Image.fromarray(np.ma.array(image.data,dtype=float).filled(np.nan))
        #quad in the plot referential




        bound = image.boundingRect()
        x1, x2 = bound.left(), bound.right()
        y1, y2 = bound.top(), bound.bottom()

        lattice2 = LatticeGrid()
        lattice2.latticeparam.update_param(lattice)
        lattice2.latticeparam.RT = True
        lattice2.latticeparam.vm = False

        if lattice.sg in ['p6','p6mm']:
            irmax = 6
        elif lattice.sg in ['p3','p3m1','p31m']:
            irmax = 3
        elif lattice.sg in ['p4','p4mm','p4gm']:
            irmax = 4
        elif lattice.sg in ['p2','p2mm','p2mg','p2gg','c2mm']:
            irmax = 2
        else:
            irmax = 1
        dt = 360/irmax

        averagedata = []

        for ir in range(irmax):
            lattice2.latticeparam.uT = lattice.latticeparam.uT+ir*dt
            lattice2.latticeparam.update_shape(lattice2)

            nodes = self.plot.get_visible_nodes(x1, x2, y1, y2, lattice = lattice2)

            indices = []
            for node in nodes:
                indices.append((node[2],node[3]))
                indices.append((node[2]-1,node[3]))
                indices.append((node[2],node[3]-1))
                indices.append((node[2]-1,node[3]-1))
            indices = set(indices)
            u2 = lattice2.u
            v2 = lattice2.v
            #print (u,v)
            #print (u2,v2)

            hd2 = (v2[1]*u2[1]+v2[0]*u2[0])/uR
            if hd2<0:   #angle>90°
                points = [[O[0]+hd2/uR*u2[0], O[1]+hd2/uR*u2[1]],
                          [O[0]+v2[0], O[1]+v2[1]],
                          [O[0]+v2[0]+u2[0]-hd2/uR*u2[1], O[1]+v2[1]+u2[1]-hd2/uR*u2[1]],
                          [O[0]+u2[0], O[1]+u2[1]]]
            else:   #angle<90°
                points = [[O[0], O[1]],
                          [O[0]+v2[0]-hd2/uR*u2[0], O[1]+v2[1]-hd2/uR*u2[1]],
                          [O[0]+v2[0]+u2[0], O[1]+v2[1]+u2[1]],
                          [O[0]+u2[0]+hd2/uR*u2[0], O[1]+u2[1]+hd2/uR*u2[1]]]

            #quad is an 8-tuple (x0, y0, x1, y1, x2, y2, y3, y3) which contain the upper left, lower left, lower right, and upper right corner of the source quadrilateral.
            for i,j in indices:
                try:
                    quad = []
                    for point in points:
                        quad += list(image.get_pixel_coordinates(point[0]+u2[0]*i+v2[0]*j,point[1]+u2[1]*i+v2[1]*j))
                    #print (ir,i,j)
                    #print (quad)

                    pilimt=pilim.transform(size, Image.Transform.QUAD, quad, Image.Transform.AFFINE)
                    mask = Image.new("L", pilim.size, 255)
                    mask = mask.transform(size, Image.Transform.QUAD, quad, Image.Transform.AFFINE)

                    img = Image.new("F", size, np.nan)
                    img.paste(pilimt, mask)

                    newdata = np.ma.masked_invalid(np.asarray(img))
                    averagedata.append(newdata)

                    if newdata.count()>0:
                        title = 'rot_%d_node_%d,%d'%(ir,i,j)
                        newimage = make.maskedimagenan(np.ma.array(newdata, copy=True),title=title)
                        self.plot.add_item(newimage)

                except Exception as e:
                    print (e)
                    print (quad)

        #print (averagedata)
        averagedata = np.ma.masked_array(averagedata)
        averagedata = np.mean(averagedata,axis=0)
        #print (averagedata)

        newimage = make.maskedimagenan(averagedata.filled(np.nan),title='average unit cell',show_mask=True)
        self.plot.add_item(newimage)








    def apply_distortion_tool(self, plot):

        spots = list(item for item in plot.get_items() if isinstance(item, AtomShape))
        from guiqwt.image import ImageItem

        images = list(item for item in plot.get_selected_items() if isinstance(item, ImageItem))
        if len(images) ==  0:
            images = list(item for item in plot.get_items() if isinstance(item, ImageItem))
            if len(images) ==  0:
                QMessageBox.warning(plot, "warning","no image found")
                print ("warning, no image found")
                return

        image = images[0]
        print ('active image:',image.imageparam.label)
        xexp = list()
        yexp = list()
        xth = list()
        yth = list()

        for item in spots:

            x,y = image.get_pixel_coordinates(item.x0,item.y0)
            h = item.h
            k = item.k
            xexp.append(x)
            yexp.append(y)
            if item.lattice is not None:
                _x0,_y0  = item.lattice.get_plot_coordinates(h, k, item.latticeatom)  #plot coordinates of the node
                x1,y1 = image.get_pixel_coordinates(_x0,_y0)

                xth.append(x1)
                yth.append(y1)
        if len(xexp)<=2:
            QMessageBox.warning(plot, "warning","not enough referenced points\n to compute the distortion")
            print ("warning, not enough referenced points")
            return

        p3x,p3y = compute_distortion(xexp,yexp,xth,yth)
        print ('distortion = ',p3x,p3y)

        #corrige la distortion pour cela on mappe l'image d'arrivee avec des rectangles qui correspondent
        #a des quadrilateres sur l'image non deformee. Ceux-ci sont obtenus par la transformation
        #xth->xexp, yth->yexp
        #dans un premier temps, on regarde la valeur de p3x[1] et p3xy[2] pour savoir sur quelle taille d'image
        #faire la correction
        data = np.array(image.data, np.float32)
        pilim = Image.fromarray(data, mode = 'F')  #using PIL image deformation

        w,h = pilim.size
        meshdata = []
        print ("pixel 0,0 ",pilim.getpixel((0,0)))
        #dx = (x2-x1)/10.
        #dy = (y2-y1)/10.

        ninter = 10
        nsteps = ninter+1
        xdest = np.linspace(0,w,nsteps)
        ydest = np.linspace(0,h,nsteps)
        xgrid,ygrid = np.meshgrid(xdest,ydest)   #ensemble des valeurs de x et y de destination
        xgrid=xgrid.ravel()
        ygrid=ygrid.ravel()
        xstart=corr_cub(p3x,xgrid,ygrid)       #ensemble des valeurs de x et y de depart
        ystart=corr_cub(p3y,xgrid,ygrid)
        fic=open('output.txt','w')
        for i in range(len(xgrid)):
          fic.write('%f %f %f %f'%(xgrid[i],ygrid[i],xstart[i],ystart[i]))
          fic.write('\n')
        fic.close()

        xstart=xstart.reshape(nsteps,nsteps)
        ystart=ystart.reshape(nsteps,nsteps)

        xdest=np.array(np.around(xdest),int)
        ydest=np.array(np.around(ydest),int)
        xstart=np.array(np.around(xstart),int)
        ystart=np.array(np.around(ystart),int)

        meshdata=[]
        for j in range(ninter):
            for i in range(ninter):
                xl=xdest[i]
                xr=xdest[i+1]
                yu=ydest[j]
                yl=ydest[j+1]
                xul=xstart[j,i]
                xur=xstart[j,i+1]
                yul=ystart[j,i]
                yur=ystart[j,i+1]
                xll=xstart[j+1,i]
                xlr=xstart[j+1,i+1]
                yll=ystart[j+1,i]
                ylr=ystart[j+1,i+1]
                meshdata.append(((xl,yu,xr,yl),(xul,yul,xll,yll,xlr,ylr,xur,yur)))
        img=pilim.transform((w,h),Image.Transform.MESH,meshdata,Image.Resampling.BICUBIC)
        print ("pixel 100,100 ",img.getpixel((100,100)))

        #data=np.array(img).reshape((w,h))
        data=np.array(img)

        data=np.flipud(data)  #needed but it is not clear why!

        print ("duplicate plot")
        win2 = self.duplicate(plot)
        print ("duplicate plot done")
        plot2 = win2.get_plot()

        param = image.imageparam

        image2 = make.maskedimagenan(data, title=None, alpha_mask=param.alpha_mask,
              alpha=param.alpha, colormap=param.colormap,
              xdata=[param.xmin, param.xmax], ydata=[param.ymin, param.ymax],show_mask=True)

        plot2.add_item(image2, z=-1)

        #on effectue la transformation sur les taches identifiees
        atomitems=list(item for item in plot.items if (isinstance(item, AtomShape)))
        atomitems2=list(item for item in plot2.items if (isinstance(item, AtomShape)))

        print ("changing atomshape positions")
        for i in range(len(atomitems)):
            print (i)
            item = atomitems[i]
            item2 = atomitems2[i]
            #coordonnees pixels
            x0,y0 = image.get_pixel_coordinates(item.x0,item.y0)
            #on recherche le polygone de depart
            jquad = -1
            for j in range(len(meshdata)):   #on pourrait faire plus rapide en affinant a partir de la position supposee....
                quad = mplPath(np.array(meshdata[j][1]).reshape(4,2))
                if quad.contains_point([x0,y0]):
                    jquad = j
                    break
            if jquad ==   -1:
                #le point n'est pas sur l'image d'arrivee, on le supprime
                 win2.plot.del_item(item2)
            else:
                #on resoud le systeme d'equation lineaire pour obtenir les coefficients
                #de la transformation quad->rectangle
                xl,yu,xr,yl = meshdata[j][0]
                xul,yul,xll,yll,xlr,ylr,xur,yur = meshdata[j][1]
                A = [[xul,yul,1.,0.,0.,0.,-xul*xl,-yul*xl],
                   [xur,yur,1.,0.,0.,0.,-xur*xr,-yur*xr],
                   [xll,yll,1.,0.,0.,0.,-xll*xl,-yll*xl],
                   [xlr,ylr,1.,0.,0.,0.,-xlr*xr,-ylr*xr],
                   [0.,0.,0.,xul,yul,1.,-xul*yu,-yul*yu],
                   [0.,0.,0.,xll,yll,1.,-xll*yl,-yll*yl],
                   [0.,0.,0.,xur,yur,1.,-xur*yu,-yur*yu],
                   [0.,0.,0.,xlr,ylr,1.,-xlr*yl,-ylr*yl]]
                B = [xl,xr,xl,xr,yu,yl,yu,yl]

                C = np.linalg.solve(A,B)
                a,b,c,d,e,f,g,h = C
                #transformation quad->rectangle
                x1 = (a*x0+b*y0+c)/(g*x0+h*y0+1.)
                y1 = (d*x0+e*y0+f)/(g*x0+h*y0+1.)
                #coordonnees plot
                x2,y2 = image.get_plot_coordinates(x1,y1)
                item2.set_pos(x2,y2)

        print ("ok")
        win2.show()



    def duplicate(self, plot = None):
        """duplicate latticegrid, reconstructiongrid and atomshapes"""

        if plot ==   None:
            plot = self.get_plot()
        allitems = plot.items

        from xrdutils.latticetools import DirectLatticeGrid as ODirectLatticeGrid
        from xrdutils.latticetools import AtomShape as OAtomShape

        latticeitems = list(item for item in allitems if (isinstance(item, DirectLatticeGrid) or isinstance(item, ODirectLatticeGrid)))
        atomitems = list(item for item in allitems if (isinstance(item, AtomShape) or isinstance(item, OAtomShape)))

        win2 = DirectSpaceDialog()
        plot2 = win2.get_plot()
        #on rajoute les latticeitems
        for item in latticeitems:

            item2  = DirectLatticeGrid(O = item.O, u = item.u, v = item.v, sg = item.sg, atoms = item.atoms)
            set_identical_shapeparam(item,item2)
            item2.setZ(item.z())
            plot2.add_item(item2)

            #on rajoute les reconstructionitems avec les nouvelles relations master-slave
            #item2.slaves = list()  #on repart de zero
            for slave in item.slaves:

                slave2 = DirectReconstructionGrid(O=slave.O, A = slave.A, B = slave.B,
                                            master = item2, atoms = slave.atoms)
                set_identical_shapeparam(slave,slave2)
                slave2.setZ(item.z())
                plot2.add_item(slave2)

        #on rajoute les spotitems
        for item in atomitems:
            print (item)
            item.shapeparam.update_param(item)#certains parametres ne sont pas forcement actualises

            item2 = AtomShape()
            item.shapeparam.update_shape(item2) #update shape from shapeparam
            item.latticeparam.update_shape(item2)
            item2.setZ(item.z())
            plot2.add_item(item2)
            item2.set_lattice_from_latticename(item2.lattice.shapeparam.label) #needs to be added on a plot

        return win2




def test(win):
    plot = win.get_plot()
    lattice = DirectLatticeGrid(O = [200,200], u = [100,0], v = [0,100], sg = 'p1', shapeparam = None, latticeparam = None, atoms = [[0,0]])
    reconstruction = DirectReconstructionGrid(O=[0,0], A = [2,1], B = [-1,2], master = lattice, force_master = True, shapeparam = None, latticeparam = None, atoms=[[0,0]])
    lattice.set_movable(True)
    reconstruction.set_movable(True)
    plot.add_item(lattice)
    plot.add_item(reconstruction)

def test2(win):
    x,y = np.meshgrid(np.arange(-10,11),np.arange(-10,11))
    data = np.exp(-(x*x+y*y))
    newimage = make.maskedimagenan(np.flipud(data), xdata=[-10.5, 10.5], ydata=[-10.5, 10.5])

    win.plot.add_item(newimage)
    win.plot.replot()



if __name__ ==   "__main__":
    import guidata
    import sys
    _app = guidata.qapplication()

    from guidata.configtools import add_image_path
    abspath = osp.abspath(__file__)
    dirpath = osp.dirname(abspath)
    add_image_path(dirpath)

    #win = DirectSpaceDialog()
    win = ReciprocalDialog()
    #win.open_image('test.tif')
    test2(win)
    #win.open_image('D:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL Juillet 2020/modeleV109/ImagesSTM/20210427-084440_--STM_Spectroscopy--50_1_TU_filtered_plane.tif')
    #win.load_pickle('D:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL Juillet 2020/modeleV109/ImagesSTM/Plot_20210427-50_1_TU_bis.gui')
    win.show()
    sys.exit(_app.exec_())
