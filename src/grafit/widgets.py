"""
Created on Thu Jun 11 09:25:40 2020

@author: prevot
"""
from os import path,getcwd
import numpy as np
from guidata.configtools import get_icon

from qtpy.QtCore import Qt,Signal
from qtpy.QtWidgets import (QListWidget,QListWidgetItem,QTableWidget,QTableWidgetItem,
                              QLabel,QMenu,QAction,
                              QMessageBox,QFileDialog,QMainWindow,QSplitter,QWidget,
                              QComboBox, QVBoxLayout,QHBoxLayout)
from qtpy.QtGui import QCursor,QColor,QPixmap
from guiqwt.interfaces import IPanel

from grafit.models import minimizationmethods
from grafit.parameters import ExtendedParams
ID_PARAMETER = "parameters"

abspath=path.abspath(__file__)
dirpath=path.dirname(abspath)

colors=[QColor(0,255,255),QColor(255,100,100),QColor(255,255,100),QColor(128,128,255),QColor(255,255,255)]

class RawDataList(QListWidget):
    SIG_DATA_REMOVED = Signal(int)
    SIG_ALL_DATA_REMOVED = Signal()
    ''' A specialized QListWidget that displays the list
        of all data that could be fitted '''
    def __init__(self, parent=None):
        QListWidget.__init__(self, parent)
        self.setSelectionMode(1)
        self.rawdatalist=[]
        self.initcontexts()

    def add_item(self,rawdata):
        ''' add a dataimage item to the list '''
        self.rawdatalist.append(rawdata)
        item = QListWidgetItem(self)
        item.setText(rawdata.title)

    def contextMenuEvent(self, event):
        """Override Qt method"""
        self.context.popup(event.globalPos())

    def initcontexts(self):
        #menu pour la colonne 0
        self.context=QMenu(self)
        self.removeAction = QAction("Remove data",self)
        self.context.addAction(self.removeAction)
        self.removeAction.triggered.connect(self.remove_data)
        self.removeAllAction = QAction("Remove all data",self)
        self.context.addAction(self.removeAllAction)
        self.removeAllAction.triggered.connect(self.remove_all_data)

    def remove_data(self):
        ask=QMessageBox.question(self, "warning", "do you really want to remove this data?", QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        if ask==QMessageBox.Yes:
            i=self.currentRow()
            self.takeItem(i)
            self.rawdatalist.pop(i)
            self.SIG_DATA_REMOVED.emit(i)

    def remove_all_data(self):
        ask=QMessageBox.question(self, "warning", "do you really want to remove all data?", QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        if ask==QMessageBox.Yes:
            for i in range(len(self.rawdatalist)):
                self.takeItem(0)
            self.rawdatalist=[]
            self.SIG_ALL_DATA_REMOVED.emit()

class FunctionList(QListWidget):
    ''' A specialized QListWidget that displays the list
        of functions used in the model '''
    #signal emitted by FunctionList when an item is removed from the list
    SIG_MODEL_REMOVED = Signal()
    SIG_CONFIGURE_MODEL = Signal(int)
    SIG_MODEL_MOVED_UP = Signal(int)
    SIG_MODEL_MOVED_DOWN = Signal(int)
    SIG_MODEL_CONVERTED = Signal(int)
    SIG_MODEL_SHOWN = Signal(bool)

    def __init__(self, extendedparams, shape_list, list_of_models, parent=None):
        self.extendedparams = extendedparams
        self.shape_list = shape_list   #list of the shapes that are present on the plot
        self.list_of_models = list_of_models  #list of model functions that can be used to convert an existing function into another (ex lorentzian into Gaussian)
        QListWidget.__init__(self, parent)
        self.setSelectionMode(3)   #multiple selection allowed with control key
        self.current_item_number = -1
        self.initcontexts()
        self.currentRowChanged.connect(self.row_changed)

    def add_item(self,function_name):
        ''' add a function name to the list '''
        item = QListWidgetItem(self)
        item.setText(function_name)
        self.current_item_number = self.count()-1

    def row_changed(self,i):
        #activated when selected row has changed
        self.current_item_number = i

    def contextMenuEvent(self, event):
        """Override Qt method"""
        try:
            shape = self.shape_list[self.current_item_number]
        except IndexError:
            print ('IndexError: list index out of range')
            print (self.shape_list)
            return

        if shape is not None:
            self.shownAction.setChecked(shape.isVisible())
        else:
            self.shownAction.setChecked(False)
        self.context.popup(event.globalPos())

    def initcontexts(self):
        #menu pour la colonne 0
        self.context=QMenu(self)

        self.removeAction = QAction("Remove curve(s)",self)
        self.context.addAction(self.removeAction)
        self.removeAction.triggered.connect(self.remove_curve)

        self.configureAction = QAction("Redraw curve",self)
        self.context.addAction(self.configureAction)
        self.configureAction.triggered.connect(self.configure_curve)

        self.moveupAction = QAction("Move up curve",self)
        self.context.addAction(self.moveupAction)
        self.moveupAction.triggered.connect(self.moveup_curve)

        self.movedownAction = QAction("Move down curve",self)
        self.context.addAction(self.movedownAction)
        self.movedownAction.triggered.connect(self.movedown_curve)

        self.shownAction = QAction("Show curve(s)",self)
        self.shownAction.setCheckable(True)
        self.shownAction.setChecked(True)
        self.context.addAction(self.shownAction)
        self.shownAction.triggered.connect(self.show_curve)

        self.convertMenu = QMenu("Convert to",self)
        self.context.addMenu(self.convertMenu)
        for i, modelclass in enumerate(self.list_of_models):
            action = QAction(get_icon(modelclass.ICON), modelclass.NAME, self)
            self.convertMenu.addAction(action)
            action.triggered.connect(lambda state, arg=i: self.convert_model(arg))

    def remove_curve(self):

        ask=QMessageBox.question(self, "save", "do you really want to remove selected curve(s)?", QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        if ask==QMessageBox.Yes:
            #self.takeItem(i)
            self.SIG_MODEL_REMOVED.emit()

    def configure_curve(self):
        self.SIG_CONFIGURE_MODEL.emit(self.currentRow())

    def moveup_curve(self):
        i=self.currentRow()
        if i>0:
            self.SIG_MODEL_MOVED_UP.emit(i)

    def movedown_curve(self):
        i=self.currentRow()
        if i<self.count()-1:
            self.SIG_MODEL_MOVED_DOWN.emit(i)

    def show_curve(self):
        self.SIG_MODEL_SHOWN.emit(self.shownAction.isChecked())

    def initialize(self):
        self.clear()
        for model in self.extendedparams.models:
            self.add_item(model.name)

    def convert_model(self,i):
        """convert all models selected into model i"""
        self.SIG_MODEL_CONVERTED.emit(i)


class ParameterTable(QTableWidget):
    __implements__ = (IPanel,)
    PANEL_ID = ID_PARAMETER
    PANEL_TITLE = "parameters"
    PANEL_ICON = None # string

    SIG_TRY_CHANGED=Signal()

    #objet qui gere les entree de parametres, l'affichage des courbes, le lancement des fits, l'enregistrement du resultat
    def __init__(self,parent=None,extendedparams=None):
        QTableWidget.__init__(self,parent=parent)

        self.setColumnCount(8)
        self.setHorizontalHeaderLabels(("Parameters","Estimation","Fit result","Sigma","Restrains","Min","Max","Expression"))

        if extendedparams==None:
            self.extendedparams=ExtendedParams()
        else:
            self.extendedparams=extendedparams
        #on remet le tableau a zero : deux rangees pour un polynome du 1er degre
        self.reset()

        #on definit les differents menus contextuels
        self.initcontexts()

        #au cas ou
        self.int1=0

        #On connecte les signaux
        self.cellChanged.connect(self.cell_changed)
        self.cellPressed.connect(self.cell_pressed)
        self.horizontalHeader().sectionPressed.connect(self.section_clicked)

        #pas de fit sauve
        self.isfitted=False
        self.issaved=False
        self.tags=list()
        self.saveint=False
        self.ints=[]
        self.updatestart=False   #si coche, on active setwholeastry apres enregistrement
        self.cwd=abspath

    def register_plot(self, baseplot):
        pass

    def register_panel(self, manager):
        """Register panel to plot manager"""
        pass

    def configure_panel(self):
        """Configure panel"""
        pass

    def reset(self):
        self.extendedparams.reset()
        self.initparams(settry=True)
        self.savename=None
        self.isfitted=False

    def inititems(self,listrow):
        #met pour les lignes i de listrow un item vide non editable pour colonnes 0,2,3,4
        for i in listrow:
            for j in range(8):
                self.setItem(i,j,QTableWidgetItem(0))
            self.item(i,0).setFlags(Qt.ItemFlags(33))
            self.item(i,2).setFlags(Qt.ItemFlags(33))
            self.item(i,3).setFlags(Qt.ItemFlags(33))
            self.item(i,4).setFlags(Qt.ItemFlags(33))

    def initparams(self,settry=False,setopt=False):
        blocked=self.blockSignals(True)

        #on redessine le tableau a l'aide du dictionnaire extendedparams
        #si settry, on remplit aussi les valeurs d'essai, les bornes, les expressions

        oldrc=self.rowCount()
        newrc=len(self.extendedparams.partry)
        self.setRowCount(newrc)

        if newrc>oldrc:
            #on rajoute des rangees vides
            self.inititems(list(range(oldrc,newrc)))

        n=0
        for entry, value in self.extendedparams.partry.items():
            self.setrow(entry,n,settry,setopt)
            n+=1

        self.blockSignals(blocked)

    def setrow(self,nom,irow,settry=True,setopt=False):
        blocked=self.blockSignals(True)

        ptry=self.extendedparams.partry
        popt=self.extendedparams.paropt
        self.item(irow,0).setText(nom)

        if settry:
            vtry=ptry[nom].value

            mintry=ptry[nom].min
            maxtry=ptry[nom].max
            varytry=ptry[nom].vary
            exprtry=ptry[nom].expr
            self.item(irow,1).setText("%f" % (vtry))

            if exprtry is not None:
                self.item(irow,4).setText("Expr")
                self.set_row_color(irow,2)
                self.item(irow,7).setText(exprtry)

            elif varytry is True:
                self.item(irow,4).setText("Free")
                self.set_row_color(irow,0)
            else:
                self.item(irow,4).setText("Fixed")
                self.set_row_color(irow,1)

            if mintry is not None:
                self.item(irow,5).setText("%f" % (mintry))

            if maxtry is not None:
                self.item(irow,6).setText("%f" % (maxtry))




        if setopt:
            vopt=popt[nom].value
            self.item(irow,2).setText("%f" % (vopt))
            verr=popt[nom].stderr
            if verr is not None:
              self.item(irow,3).setText("%f" % (verr))

        else:
            self.item(irow,2).setText("")
            self.item(irow,3).setText("")

        self.blockSignals(blocked)  #going back to previous setting


    def set_row_color(self,irow,ivar):
        bls=self.signalsBlocked()
        self.blockSignals(True)

        for j in range(8):
          self.item(irow,j).setBackground(colors[ivar])

        """
        if (self.hidefixed.isChecked() and ivar==1) or (self.hideexpr.isChecked() and ivar==2):
          self.hideRow(n)
        else:
          self.showRow(n)
        """
        self.blockSignals(bls)

    def initcontexts(self):
        #initialise une liste de menus contextuels pour les cases, associes a chaque colonne
        self.contexts=list(None for i in range(self.columnCount()))
        #initialise une liste de menus contextuels pour les sectionheaders, associes a chaque colonne
        self.Contexts=list(None for i in range(self.columnCount()))

        #menu pour la colonne 0

        context=QMenu(self)

        self.setastry = QAction("Set as try",self)
        context.addAction(self.setastry)
        self.setastry.triggered.connect(self.set_as_try)

        self.showfit = QAction("Display Fit",self)
        context.addAction(self.showfit)
        self.showfit.triggered.connect(self.show_fit)

        self.showtry = QAction("Display Try",self)
        context.addAction(self.showtry)
        self.showtry.triggered.connect(self.show_try)

        self.contexts[2]=context

        #menu pour la colonne 4
        context=QMenu(self)

        self.setfixed = QAction("Fixed",self)
        context.addAction(self.setfixed)
        self.setfixed.triggered.connect(self.set_fixed)

        self.setfree = QAction("Free",self)
        context.addAction(self.setfree)
        self.setfree.triggered.connect(self.set_free)

        self.useexpr = QAction("Expr",self)
        context.addAction(self.useexpr)
        self.useexpr.triggered.connect(self.use_expr)

        self.contexts[4]=context

        #menu pour le header 2
        context=QMenu(self)
        self.setwholeastry = QAction("Set whole as try",self)
        context.addAction(self.setwholeastry)
        self.setwholeastry.triggered.connect(self.set_whole_as_try)

        self.Contexts[2]=context

        #menu pour le header 4
        context=QMenu(self)
        self.inverserestrains = QAction("Inverse restrains",self)
        context.addAction(self.inverserestrains)
        self.inverserestrains.triggered.connect(self.inverse_restrains)

        self.fixall = QAction("All fixed",self)
        context.addAction(self.fixall)
        self.fixall.triggered.connect(self.fix_all)

        self.releaseall = QAction("All free",self)
        context.addAction(self.releaseall)
        self.releaseall.triggered.connect(self.release_all)

        self.Contexts[4]=context


    def cell_pressed(self,int1,int2):
        self.int1=int1

        context=self.contexts[int2]

        if context is not None:
            context.popup(QCursor.pos())

    def cell_changed(self,int1,int2):
        if int2==1:  #on change la valeur du parametre
            txt= str(self.item(int1,1).text())
            entry= str(self.item(int1,0).text())
            try:
                self.extendedparams.partry[entry].value=float(txt)
                self.SIG_TRY_CHANGED.emit()
            except ValueError:
                QMessageBox.warning(self,'warning','unable to convert text to float')

        if int2==4:
            txt= str(self.item(int1,4).text())
            if txt=='Free':
                self.set_row_color(int1,0)
            elif txt=='Fixed':
                self.set_row_color(int1,1)
            else:
                self.set_row_color(int1,2)

        if int2==5:  #on change la borne inferieure
            txt= str(self.item(int1,5).text())
            entry= str(self.item(int1,0).text())
            try:
                self.extendedparams.partry[entry].min=float(txt)
            except ValueError:
                QMessageBox.warning(self,'warning','unable to convert text to float')

        if int2==6:  #on change la borne superieure
            txt= str(self.item(int1,6).text())
            entry= str(self.item(int1,0).text())
            try:
                self.extendedparams.partry[entry].max=float(txt)
            except ValueError:
                QMessageBox.warning(self,'warning','unable to convert text to float')

        if int2==7:  #on change l'expression
            txt= str(self.item(int1,7).text())
            entry= str(self.item(int1,0).text())
            if len(txt) == 0:
                self.extendedparams.partry[entry].expr=None
                self.extendedparams.partry[entry].vary=True
            else:
                try:
                    self.extendedparams.partry[entry].expr=txt
                except:
                    QMessageBox.warning(self,'warning','unable to set expression')
                self.item(int1,4).setText("Expr")


    def section_clicked(self,int2):
        #quand une colonne entiere est selectionnee valable pour les colonnes 2 et 4
        context=self.Contexts[int2]
        if context is not None:
            context.popup(QCursor.pos())

    def freeze_cv(self) :
        #on met fixe tous les parametres d'une courbe
        entry=str(self.item(self.int1,0).text())
        icurve=self.extendedparams.cv_number(entry)
        self.extendedparams.freezecurve(icurve)
        self.initparams(settry=True,setopt=True)

    def release_cv(self) :
        #on met fixe tous les parametres d'une courbe
        entry=str(self.item(self.int1,0).text())
        icurve=self.extendedparams.cv_number(entry)
        self.extendedparams.releasecurve(icurve)
        self.initparams(settry=True,setopt=True)

    def release_all(self) :
        #on met fixe tous les parametres du fond
        for int1 in range(self.rowCount()):
            item=self.item(int1,4)
            item.setText("Free")
            entry=str(self.item(int1,0).text())
            self.extendedparams.partry[entry].vary=True

    def fix_all(self) :
        #on met fixe tous les parametres du fond
        for int1 in range(self.rowCount()):
            item=self.item(int1,4)
            item.setText("Fixed")
            entry=str(self.item(int1,0).text())
            self.extendedparams.partry[entry].vary=False

    def set_cv(self,curvetype=None,params=None,bg=0.):
        #icurve numero de la courbe a changer le cas echeant -1 c'est la derniere
        pass

    def rm_cv(self):
        #on supprime la courbe selectionnee
        pass

    def ch_cv(self):
        #a partir de la souris, on retrace la courbe selectionnee
        pass

    def show_fit():
        pass

    def show_try():
        pass

    def set_fixed(self):
        item=self.item(self.int1,4)
        item.setText("Fixed")
        entry=str(self.item(self.int1,0).text())
        self.extendedparams.partry[entry].vary=False
        self.extendedparams.partry[entry].expr=None

    def set_free(self):
        item=self.item(self.int1,4)
        item.setText("Free")
        entry=str(self.item(self.int1,0).text())
        self.extendedparams.partry[entry].vary=True
        self.extendedparams.partry[entry].expr=None

    def use_expr(self):
        item=self.item(self.int1,4)
        item.setText("Expr")
        entry=str(self.item(self.int1,0).text())

        txt= str(self.item(self.int1,7).text())
        self.extendedparams.partry[entry].express=txt    #in principle, should have been already set

        if len(txt) != 0:    #on met l'expression indiquee
           self.extendedparams.partry[entry].expr=txt
           self.extendedparams.partry[entry].vary=False

    def inverse_restrains(self):
        #inverse les contraintes: les parametres libres deviennent fixes et vice-versa
        for int1 in range(self.rowCount()):
            item=self.item(int1,4)
            entry=str(self.item(int1,0).text())
            vary=self.extendedparams.partry[entry].vary
            if vary:
                self.extendedparams.partry[entry].vary=False
                item.setText("Fixed")
            else:
                self.extendedparams.partry[entry].vary=True
                item.setText("Free")

    def set_whole_as_try(self):
        self.extendedparams.set_whole_as_try()
        self.initparams(settry=True,setopt=True)

    def set_as_try(self):
        item=self.item(self.int1,1)
        entry=str(self.item(self.int1,0).text())
        if entry in self.extendedparams.paropt:
            self.extendedparams.partry[entry].value=self.extendedparams.paropt[entry].value
            item.setText("%f" %(self.extendedparams.paropt[entry].value))


#this widget does nothing by itself but contains a layout for plots
class LayoutWidget(QWidget):
    def __init__(self,parent,orientation='horizontal'):
        self.parent=parent
        QWidget.__init__(self,parent=parent)
        if orientation == 'horizontal':
            self.layout = QHBoxLayout()
        else:
            self.layout = QVBoxLayout()
        self.setLayout(self.layout)

    def addWidget(self,widget):
        self.layout.addWidget(widget)

    def save_widget(self, fname):
        """Grab widget's window and save it to filename (*.png)"""
        pixmap = self.grab()
        pixmap.save(fname, 'PNG')


class FitWidget(QWidget):
   def __init__(self,parent=None,extendedparams=None):
       QWidget.__init__(self,parent=parent)

       self.extendedparams=extendedparams

       layout=QVBoxLayout()
       self.setLayout(layout)

       self.method=QComboBox(self)
       for couple in minimizationmethods:
           self.method.addItem(couple[1])
       self.method.currentIndexChanged.connect(self.index_changed)

       self.chi2=QLabel('chi2=')
       self.di=QLabel('dI=')

       #signal connected in rodeo
       layout.addWidget(self.method)
       layout.addWidget(self.chi2)
       layout.addWidget(self.di)


   def set_method_number(self,i):
       self.method.setCurrentIndex(i)

   def set_method_name(self):
       i=self.extendedparams.methods.index(self.extendedparams.method)
       self.method.setCurrentIndex(i)

   def index_changed(self,int):
       self.extendedparams.method=minimizationmethods[int][0]

   def set_chi2(self,chi2,di):
       self.chi2.setText('chi2=%g'%chi2)
       self.di.setText('dI=%g'%di)



class DataTable(QTableWidget):
    #a widget to display array of data

    SIG_ROWS_DELETED = Signal(list)
    SIG_COLUMNS_DELETED = Signal(list)
    SIG_ROW_DELETED = Signal(int)
    SIG_COLUMN_DELETED = Signal(int)
    SIG_FIT_DATA = Signal("PyQt_PyObject")

    def __init__(self,parent=None):
        QTableWidget.__init__(self,parent=parent)
        self.init_contexts()

        h_header = self.horizontalHeader()
        h_header.setContextMenuPolicy(Qt.CustomContextMenu)
        h_header.customContextMenuRequested.connect(self.h_header_popup)

        v_header = self.verticalHeader()
        v_header.setContextMenuPolicy(Qt.CustomContextMenu)
        v_header.customContextMenuRequested.connect(self.v_header_popup)
        self.labels=[]
        self.isallselected=0
        #self.itemSelectionChanged.connect(self.test)
        #self.setSelectionMode(4)

    def selectAll(self):
        #catch the selectAll event to allows the possibility to unselect all
        if self.isallselected:
            self.clearSelection()
            self.isallselected=0
        else:
            super().selectAll()
            self.isallselected=1

    def init_contexts(self):
        self.row_context = QMenu(self)
        self.deleterow_a=QAction("Delete selected rows",self)
        self.row_context.addAction(self.deleterow_a)


        self.column_context = QMenu(self)
        self.deletecolumn_a=QAction("Delete selected columns",self)
        self.column_context.addAction(self.deletecolumn_a)

        self.fit_a=QAction("Fit data",self)
        self.column_context.addAction(self.fit_a)

        self.deleterow_a.triggered.connect(self.delete_rows)
        self.deletecolumn_a.triggered.connect(self.delete_columns)
        self.fit_a.triggered.connect(self.fit_data)

    def h_header_popup(self,pos):
        self.column_context.popup(QCursor.pos())
        index = self.indexAt(pos)
        self.current_column = index.column()

    def v_header_popup(self,pos):
        self.row_context.popup(QCursor.pos())
        index = self.indexAt(pos)
        self.current_row = index.row()

    def get_selected_rows(self):
        #rows = list(set(index.row() for index in self.selectedIndexes()))
        indexes = self.selectionModel().selectedRows()
        rows = [index.row() for index in indexes]
        rows.sort()
        return rows

    def get_selected_columns(self):
        #columns = list(set(index.column() for index in self.selectedIndexes()))
        indexes = self.selectionModel().selectedColumns()
        columns = [index.column() for index in indexes]
        columns.sort()
        return columns

    def delete_row(self):
        self.removeRow(self.current_row)
        self.SIG_ROW_DELETED.emit(self.current_row)

    def delete_column(self):
        self.removeColumn(self.current_column)
        self.SIG_COLUMN_DELETED.emit(self.current_column)

    def delete_rows(self):
        rows = self.get_selected_rows()
        rows.reverse()
        for i in rows:
            self.removeRow(i)
        self.SIG_ROWS_DELETED.emit(rows)

    def delete_columns(self):
        columns = self.get_selected_columns()
        columns.reverse()
        for i in columns:
            self.removeColumn(i)
        self.SIG_COLUMNS_DELETED.emit(columns)

    def fit_data(self):
        self.SIG_FIT_DATA.emit(self)

    def init_items(self,old_cc,old_rc,new_cc,new_rc):
        #put a QTableWidgetItem in each new case
        for i in range(new_rc):
            for j in range(new_cc):
                if not (i<old_rc and j<old_cc):
                    self.setItem(i,j,QTableWidgetItem(0))

    def populate(self,columns,labels):
        #populate the table with columns and labels
        #columns contains the text data, same length for all colunms
        old_cc = self.columnCount()
        old_rc = self.rowCount()
        new_cc = len(labels)
        new_rc = len(columns[0])

        self.labels=labels

        self.setColumnCount(new_cc)
        self.setRowCount(new_rc)
        self.init_items(old_cc,old_rc,new_cc,new_rc)
        for i in range(new_rc):
            for j in range(new_cc):
                self.item(i,j).setText(columns[j][i])
        self.setHorizontalHeaderLabels(labels)

    def get_selected_column_labels(self):
        #return the selected cell in column icol
        columns = self.get_selected_columns()
        labels = [self.labels[column] for column in columns]
        return (labels,columns)

    def getColumnValues(self,i):
        #return a np array with values converted to float

        if len(self.selectedIndexes())<=1:
            #nothing or just 1 case is selected, take the whole tab
            v = np.empty(self.rowCount())
            for j in range(self.rowCount()):
                try:
                    v[j] = eval(self.item(j,i).text())
                except:
                    v[j] = float('nan')
            return v
        else:
            #take only selected cells
            v = np.empty(self.rowCount())
            for j in range(self.rowCount()):
                if self.item(j,i).isSelected():
                    try:
                        v[j] = eval(self.item(j,i).text())
                    except:
                        v[j] = float('nan')
                else:
                    v[j] = float('nan')
            return v

class ResultTable(DataTable):

    SIG_ROW_EXPORTED = Signal(int)

    def init_contexts(self):
        #we add the possibility of exporting previous fitted values to the parameter table
        super().init_contexts()

        self.exportrow_a=QAction("Export selected set of parameters",self)
        self.row_context.addAction(self.exportrow_a)
        self.exportrow_a.triggered.connect(self.export_row)

    def export_row(self):
        self.SIG_ROW_EXPORTED.emit(self.current_row)

    def update_results(self, results, result_vars):
        old_cc = self.columnCount()
        old_rc = self.rowCount()

        new_cc = len(result_vars)
        new_rc = len(results)

        self.setColumnCount(new_cc)
        self.setRowCount(new_rc)

        self.setHorizontalHeaderLabels(result_vars)

        self.init_items(old_cc,old_rc,new_cc,new_rc)

        for i,res in enumerate(results):
           dico = res[0]
           for j,var in enumerate(result_vars):
                if var in dico:
                    txt = "%g"%(dico[var])
                else:
                    txt = ''
                self.item(i,j).setText(txt)






if __name__ == "__main__":

    from guidata import qapplication
    _app = qapplication()
    #win = FitDialog()

    win = QMainWindow()
    tab = ResultTable()
    win.setCentralWidget(tab)

    results = [({'x':1,'y':2,'z':5},{'x':1,'y':2,'z':5}),({'x':1,'y':4,'z':8},{'x':2,'y':5,'z':7})]
    result_vars = ['x','y','z']
    tab.update_results(results,result_vars)
    print (tab.columnCount(),tab.rowCount())
    win.show()
    _app.exec_()
