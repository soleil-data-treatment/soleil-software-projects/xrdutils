"""
Created on Fri Feb 21 14:13:29 2020

@author: Prevot
"""

from guidata.configtools import get_icon
from qtpy.QtGui import QColor
from qtpy.QtCore import Qt,QEvent,Signal
from qtpy.QtWidgets import  QAction,QMenu
from guidata.qthelpers import add_actions,get_std_icon

from guiqwt.config import _
from guiqwt.events import QtDragHandler,setup_standard_tool_filter,EventMatch,KeyEventMatch
from guiqwt.tools import CommandTool as GuiqwtCommandTool
from guiqwt.tools import DefaultToolbarID,RectangularActionTool,InteractiveTool,DisplayCoordsTool
from guiqwt.shapes import EllipseShape,PolygonShape,PointShape,Marker,RectangleShape
from guiqwt.styles import FontParam
from grafit.events import MoveHandler,EllipseSelectionHandlerCXY,RectangularSelectionHandlerCX,RectangularSelectionHandlerCXY,CurvePeakSelectionHandler
from grafit.plotitems import RectangleCentredShape,CurveShape,MaskedImageItem,MaskedErrorBarCurveItem

SHAPE_Z_OFFSET = 1000

class CommandTool(GuiqwtCommandTool):
    #: Signal emitted by CommandTool on activation
    SIG_VALIDATE_TOOL = Signal()

    def activate(self, checked=True):
        #class that may be overrided
        self.SIG_VALIDATE_TOOL.emit()

class ToggleTool(CommandTool):
    CHECKABLE = True
    def __init__(self, manager, title, icon=None, tip=None, toolbar_id=None):
        super().__init__(manager, title, icon, tip, toolbar_id)

class KeyMatch(EventMatch):
    """
    A callable returning True if it matches a key event
    keysequence:  integer
    """
    def __init__(self, keysequence):
        super().__init__()
        assert isinstance(keysequence, int)
        self.keyseq = keysequence

    def get_event_types(self):
        return frozenset((QEvent.KeyPress,))

    def __call__(self, event):
        return event.type() == QEvent.KeyPress and event.key()==self.keyseq

class ShowWeightsTool(ToggleTool):
    def __init__(self, manager, title="show weights",icon = "weight.png" ,
                 tip="show weights associated to the data", toolbar_id=DefaultToolbarID):
        super().__init__(manager, title, icon, toolbar_id=toolbar_id)

class RunTool(CommandTool):
    def __init__(self, manager, title="Run",
                 icon="apply.png", tip="run 2D fit", toolbar_id=DefaultToolbarID):
        super().__init__(manager, title, icon, toolbar_id=toolbar_id)

    def setup_context_menu(self, menu, plot):
        pass

class TextSizeTool(GuiqwtCommandTool):
    def __init__(self, manager, title="Plot title size",
                 icon="font.png", tip="plot title size", toolbar_id=DefaultToolbarID):
        super().__init__(manager, title, icon, toolbar_id=toolbar_id)
        self.fontparam = FontParam()

    def activate_command(self, plot, checked=True):
        self.fontparam.update_param(plot.font_title)
        self.fontparam.edit()
        plot.font_title = self.fontparam.build_font()
        plot.set_title(plot.title())





class ExportFitTool(CommandTool):
    def __init__(self, manager, title="Export fit",
                 icon="export_fit.png", tip="export fit result", toolbar_id=DefaultToolbarID):
        super().__init__(manager, title, icon, toolbar_id=toolbar_id)

    def setup_context_menu(self, menu, plot):
        pass

class SaveFitTool(CommandTool):
    def __init__(self, manager, toolbar_id=DefaultToolbarID):
        super().__init__(manager, _("Save fit result"),
                                        get_std_icon("DialogSaveButton", 16),
                                        toolbar_id=toolbar_id)

class PrefTool(CommandTool):
    def __init__(self, manager, toolbar_id=DefaultToolbarID):
        CommandTool.__init__(self, manager, _("Run"),icon="settings.png",
                                            tip=_("Preferences"),
                                            toolbar_id=toolbar_id)
        self.manager=manager

    def create_action_menu(self, manager):
        #Create and return menu for the tool's action
        self.scaleprefaction = QAction("Scale preferences",self)

        self.restartaction = QAction("Restart from last parameters saved",self)
        self.restartaction.setCheckable(True)
        self.restartaction.setChecked(True)

        self.followcurve = QAction("Follow curves",self)
        self.followcurve.setCheckable(True)
        self.followcurve.setChecked(True)

        self.showdiffaction = QAction("Show difference profile(s)",self)
        self.showdiffaction.setCheckable(True)
        self.showdiffaction.setChecked(False)


        menu = QMenu()
        add_actions(menu, (self.scaleprefaction,self.restartaction,self.followcurve,self.showdiffaction))

        self.action.setMenu(menu)
        return menu

class AddModelTool(CommandTool):
    """a tool used for adding a new model curve to the composite model
    initialized with a given list of 1D or 2D models, it returns the
    indice of the model added"""
    SIG_MODEL_ADDED = Signal(int)

    def __init__(self, manager, list_of_models, toolbar_id=DefaultToolbarID):
        self.list_of_models=list_of_models
        CommandTool.__init__(self, manager, _("Fit"),icon= get_icon("edit_add.png"),
                                            tip=_("Add curve for fit"),
                                            toolbar_id=toolbar_id)
        self.manager=manager

    def create_action_menu(self, manager):
        #Create and return menu for the tool's action
        menu = QMenu()
        for i,modelclass in enumerate(self.list_of_models):
            action = QAction(get_icon(modelclass.ICON), modelclass.NAME, self)
            add_actions(menu,(action,))

            action.triggered.connect(lambda state, arg=i: self.add_model(arg))

        self.action.setMenu(menu)
        return menu

    def add_model(self,i):
        self.SIG_MODEL_ADDED.emit(i)


class RectangularActionToolCX(RectangularActionTool):
    """a tool used for setting the position of a CurveShape with a peak on a 1D plot
    draw a rectangle centered along x
    the positions p0,p1 are used for determining the height, position, width of the shape
    using func(plot, p0, p1)"""

    TITLE = _("Rectangle")
    ICON = "rectangle.png"
    SHAPE_STYLE_SECT = "plot"
    SHAPE_STYLE_KEY = "shape/curvedrag"

    def setup_filter(self, baseplot):      #partie utilisee pendant le mouvement a la souris
        #utilise a l'initialisation de la toolbar
        #print "setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()
        handler = RectangularSelectionHandlerCX(filter, Qt.LeftButton,      #gestionnaire du filtre
                                              start_state=start_state)
        shape, h0, h1 = self.get_shape()
        handler.set_shape(shape, h0, h1, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_END_RECT.connect(self.end_rect)
        #self.connect(handler, SIG_CLICK_EVENT, self.start)   #a definir aussi dans RectangularSelectionHandler2
        return setup_standard_tool_filter(filter, start_state)

    def activate(self):
        """Activate tool"""

        #print "commande active",self
        for baseplot, start_state in list(self.start_state.items()):
            baseplot.filter.set_state(start_state, None)
        self.action.setChecked(True)
        self.manager.set_active_tool(self)
        #plot = self.get_active_plot()
        #plot.newcurve=True

    def deactivate(self):
        """Deactivate tool"""
        #print "commande desactivee",self
        self.action.setChecked(False)

    def create_shape(self):
        shape = RectangleShape(0, 0, 1, 1)
        self.set_shape_style(shape)
        return shape, 0, 2


class RectangularActionToolCXY(RectangularActionTool):
    """a tool used for setting the position of a PeakShape on a 2D plot
    draw a rectangle centered along x and y
    the positions p0,p1 are used for determining the position and widths of the shape
    using func(plot, p0, p1)"""

    TITLE = _("Rectangle")
    ICON = "rectangle.png"
    SHAPE_STYLE_SECT = "plot"
    SHAPE_STYLE_KEY = "shape/drag"

    def setup_filter(self, baseplot):      #partie utilisee pendant le mouvement a la souris
        #utilise a l'initialisation de la toolbar
        #print "setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()
        handler = RectangularSelectionHandlerCXY(filter, Qt.LeftButton,      #gestionnaire du filtre
                                              start_state=start_state)

        shape, h0, h1, h2, h3 = self.get_shape()
        handler.set_shape(shape, h0, h1, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_END_RECT.connect(self.end_rect)
        #self.connect(handler, SIG_CLICK_EVENT, self.start)   #a definir aussi dans RectangularSelectionHandler2
        return setup_standard_tool_filter(filter, start_state)

    def activate(self):
        """Activate tool"""

        #print "commande active",self
        for baseplot, start_state in list(self.start_state.items()):
            baseplot.filter.set_state(start_state, None)
        self.action.setChecked(True)
        self.manager.set_active_tool(self)
        #plot = self.get_active_plot()
        #plot.newcurve=True

    def deactivate(self):
        """Deactivate tool"""
        #print "commande desactivee",self
        self.action.setChecked(False)



class CircularActionToolCXY(RectangularActionToolCXY):
    """a tool used for setting the position of a PeakShape on a 2D plot
    draw an ellipse centered along x and y
    the positions p0,p1 are used for determining the position and widths of the shape
    using func(plot, p0, p1)"""

    TITLE = _("Circle")
    ICON = "circle.png"

    def __init__(self, manager, func1, func2, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 fix_orientation=False, switch_to_default_tool=None):

        self.key_func = func2  #function for keys activated with +/-
        super().__init__(manager, func1, shape_style=shape_style, toolbar_id=toolbar_id,
                                 title=title, icon=icon, tip=tip, fix_orientation=fix_orientation,
                                 switch_to_default_tool=switch_to_default_tool)


    def setup_filter(self, baseplot):      #partie utilisee pendant le mouvement a la souris
        #utilise a l'initialisation de la toolbar
        #print "setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()
        handler = EllipseSelectionHandlerCXY(filter, Qt.LeftButton,      #gestionnaire du filtre
                                              start_state=start_state)
        shape, h0, h1, h2, h3 = self.get_shape()
        shape.pen.setColor(QColor("#00bfff"))

        handler.set_shape(shape, h0, h1, h2, h3, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_END_RECT.connect(self.end_rect)
        handler.SIG_KEY_PRESSED_EVENT.connect(self.key_pressed)

        return setup_standard_tool_filter(filter, start_state)

    def key_pressed(self, filter, key):
        plot = filter.plot
        self.key_func(plot,key)

    def get_shape(self):
        """Reimplemented RectangularActionTool method"""
        shape, h0, h1, h2, h3 = self.create_shape()
        self.setup_shape(shape)
        return shape, h0, h1, h2, h3

    def create_shape(self):
        shape = EllipseShape(0, 0, 1, 1)
        self.set_shape_style(shape)
        shape.switch_to_ellipse()
        return shape, 0, 1, 2, 3  #give the values of h0,h1,h2,h3 use to move the shape



class PointTool(InteractiveTool):
    """a tool that changes the position of a given point of a shape for 1D or 2D plot
    the indice of the point is incremented after each mouse press event
    so that all the points can be modified in a cyclic way
    the function func(filter.plot, event.pos(), self.current_handle) is used do the shape modification """

    TITLE = _("Point")
    ICON = "point.png"
    CURSOR = Qt.ArrowCursor

    def __init__(self, manager, func, handle_final_shape_cb=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager, toolbar_id,
                                title=title, icon=icon, tip=tip,
                                switch_to_default_tool=switch_to_default_tool)
        self.handle_final_shape_cb = handle_final_shape_cb
        self.shape = None
        self.current_handle = -1
        self.init_pos = None
        self.move_func = func
        if shape_style is not None:
            self.shape_style_sect = shape_style[0]
            self.shape_style_key = shape_style[1]
        else:
            self.shape_style_sect = "plot"
            self.shape_style_key = "shape/drag"
        self.npoints = 2

    def setup_filter(self, baseplot):
        filter = baseplot.filter
        # Initialisation du filtre
        start_state = filter.new_state()
        # Bouton gauche :
        handler = QtDragHandler(filter, Qt.LeftButton, start_state=start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Enter, Qt.Key_Return,
                                         Qt.Key_Space) ),
                         self.validate, start_state)
        handler.SIG_START_TRACKING.connect(self.mouse_press)
        handler.SIG_MOVE.connect(self.move)
        handler.SIG_STOP_MOVING.connect(self.mouse_release)
        return setup_standard_tool_filter(filter, start_state)

    def mouse_press(self, filter, event):
        """We create a new shape if it's the first point
        otherwise we add a new point
        """
        self.current_handle = self.current_handle+1
        self.move_func(filter.plot, event.pos(), self.current_handle)

    def move(self, filter, event):
        """moving while holding the button down lets the user
        position the last created point
        """
        self.move_func(filter.plot, event.pos(), self.current_handle)

    def mouse_release(self, filter, event):
        """Releasing the mouse button validate the last point position"""
        self.move_func(filter.plot, event.pos(), self.current_handle)

    def deactivate(self):
        """Deactivate tool"""
        self.current_handle = -1
        self.action.setChecked(False)

"""
class DrawingTool(InteractiveTool):
    TITLE = _("Drawing")
    ICON = "pencil.png"
    CURSOR = Qt.CrossCursor
    AVOID_NULL_SHAPE = False

    def __init__(self, manager, handle_final_shape_cb=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager, toolbar_id,
                                title=title, icon=icon, tip=tip,
                                switch_to_default_tool=switch_to_default_tool)
        self.handle_final_shape_cb = handle_final_shape_cb
        self.shape = None
        self.shapes = {}  #dictionary of shapes used for each plot

        if shape_style is not None:
            self.shape_style_sect = shape_style[0]
            self.shape_style_key = shape_style[1]
        else:
            self.shape_style_sect = "plot"
            self.shape_style_key = "shape/drag"


    def reset(self):
        self.shape = None
        self.current_handle = None

    def setup_shape(self, shape):
        #To be reimplemented
        shape.setTitle(self.TITLE)

    def create_shape(self):
        shape = PointShape(0,0)
        self.setup_shape(shape)
        return shape

    def set_shape_style(self, shape):
        shape.set_style(self.shape_style_sect, self.shape_style_key)

    def setup_filter(self, baseplot):
        #filter set for each plot registered to the manager)
        #print self,baseplot,"setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()

        self.shape = self.get_shape()
        self.shapes[baseplot]=self.shape

        handler = MoveHandler(filter, Qt.LeftButton,
                                              start_state=start_state)
        handler.set_shape(self.shape, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_START_TRACKING.connect(self.start)
        handler.SIG_MOVE.connect(self.move)
        handler.SIG_STOP_MOVING.connect(self.stop)
        handler.SIG_STOP_TRACKING.connect(self.stop)
        return setup_standard_tool_filter(filter, start_state)

    def get_shape(self):
        shape = self.create_shape()
        self.setup_shape(shape)
        return shape

    def validate(self, filter, event):
        super(DrawingTool, self).validate(filter, event)
        if self.handle_final_shape_cb is not None:
            self.handle_final_shape_cb(self.shape)
        self.reset()

    def start(self, filter, event):
        print ("tools.DrawingTool.start",self.shape)
        self.shape=self.shapes[filter.plot]
        print ("shape is now",self.shape)

    def move(self, filter, event):
        pass

    def stop(self, filter, event):
        pass

class RectangleDrawingTool(DrawingTool):
    TITLE = _("Drawing")
    ICON = "brush.jpg"
    hpixelsize=1   #size in image pixels
    vpixelsize=1
    hsize=1
    vsize=1

    def set_pixel_size(self,hpixelsize,vpixelsize=None):
        hsize=self.hsize/(self.hpixelsize-0.5)
        vsize=self.vsize/(self.vpixelsize-0.5)
        self.hpixelsize=hpixelsize
        if vpixelsize is None:
            self.vpixelsize=hpixelsize  #square shape
        else:
            self.vpixelsize=vpixelsize
        self.set_size(hsize,vsize)

    def set_size(self,hsize,vsize):
        #size in image coordinates
        self.hsize=hsize*(self.hpixelsize-0.5)
        self.vsize=vsize*(self.vpixelsize-0.5)
        for shape in list(self.shapes.values()):
          if shape is not None:
              shape.set_size(self.hsize,self.vsize)

    def create_shape(self):
        shape = RectangleCentredShape(0,0,self.hsize,self.vsize)
        self.setup_shape(shape)
        return shape

class RectangleMaskTool(RectangleDrawingTool):

    TITLE = _("Square masking brush")
    ICON = "square_sponge.png"
    CURSOR = Qt.CrossCursor
    AVOID_NULL_SHAPE = False
    SHAPE_STYLE_SECT = "plot"
    SHAPE_STYLE_KEY = "shape/drag"

    def __init__(self, manager, handle_final_shape_cb=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager,  toolbar_id=toolbar_id,
                                title=title, icon=icon, tip=tip,
                                switch_to_default_tool=switch_to_default_tool)
        self.handle_final_shape_cb = handle_final_shape_cb
        self.shape = None
        self.masked_image = None # associated masked image item
        self.mode=True

        if shape_style is not None:
            self.shape_style_sect = shape_style[0]
            self.shape_style_key = shape_style[1]
        else:
            self.shape_style_sect = "plot"
            self.shape_style_key = "shape/drag"

    def set_mode(self,mode):
        #set the action mask/unmask
        self.mode=mode

    def start(self, filter, event):
        self.shape=self.shapes[filter.plot]
        self.masked_image=self.find_masked_image(filter.plot)
        if self.masked_image is not None:
          x0, y0, x1, y1 = self.shape.get_rect()
          if self.mode:
            self.masked_image.mask_rectangular_area(x0, y0, x1, y1,trace=False,inside=True)
          else:
            self.masked_image.unmask_rectangular_area(x0, y0, x1, y1,trace=False,inside=True)

          self.masked_image.plot().replot()

    def find_masked_image(self, plot):
        item = plot.get_active_item()
        if isinstance(item, MaskedImageItem):
            return item
        else:
            items = [item for item in plot.get_items()
                     if isinstance(item, MaskedImageItem)]
            if items:
                return items[-1]

    def move(self, filter, event):
        #moving while holding the button down lets the user
        #position the last created point

        if self.masked_image is not None:
          x0, y0, x1, y1 = self.shape.get_rect()
          if self.mode:
            self.masked_image.mask_rectangular_area(x0, y0, x1, y1,trace=False,inside=True)
          else:
            self.masked_image.unmask_rectangular_area(x0, y0, x1, y1,trace=False,inside=True)
          self.masked_image.plot().replot()


class MaskPointTool(InteractiveTool):
    TITLE = _("Mask Point")
    ICON = "eraser.png"
    MARKER_STYLE_SECT = "plot"
    MARKER_STYLE_KEY = "marker/curve"
    CURSOR = Qt.PointingHandCursor

    def __init__(self, manager, mode="reuse", on_active_item=True,
                 title=None, icon=None, tip=None, end_callback=None,
                 toolbar_id=DefaultToolbarID, marker_style=None,
                 switch_to_default_tool=None):
        super().__init__(manager, toolbar_id,
                              title=title, icon=icon, tip=tip,
                              switch_to_default_tool=switch_to_default_tool)
        assert mode in ("reuse", "create")
        self.mode = mode
        self.end_callback = end_callback
        self.marker = None
        self.last_pos = None
        self.on_active_item = on_active_item
        self.maskeditem = None
        if marker_style is not None:
            self.marker_style_sect = marker_style[0]
            self.marker_style_key = marker_style[1]
        else:
            self.marker_style_sect = self.MARKER_STYLE_SECT
            self.marker_style_key = self.MARKER_STYLE_KEY

    def activate(self):
        #Activate tool
        for baseplot, start_state in list(self.start_state.items()):
            baseplot.filter.set_state(start_state, None)
        self.action.setChecked(True)
        self.manager.set_active_tool(self)

    def deactivate(self):
        if self.marker is not None:
            self.marker.setVisible(False)

    def find_masked_curve(self, plot):
        item = plot.get_active_item()
        if isinstance(item, MaskedErrorBarCurveItem):
            #print (item)
            return item
        else:
            items = [item for item in plot.get_items()
                     if isinstance(item, MaskedErrorBarCurveItem)]
            #print (items)
            if items:
                return items[-1]
            else:
                return None

    def set_marker_style(self, marker):
        marker.set_style(self.marker_style_sect, self.marker_style_key)

    def setup_filter(self, baseplot):
        filter = baseplot.filter
        # Initialisation du filtre
        start_state = filter.new_state()
        # Bouton gauche :
        handler = QtDragHandler(filter, Qt.LeftButton, start_state=start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Enter, Qt.Key_Return,
                                         Qt.Key_Space) ),
                         self.validate, start_state)
        handler.SIG_START_TRACKING.connect(self.start)
        handler.SIG_MOVE.connect(self.move)
        handler.SIG_STOP_NOT_MOVING.connect(self.stop)
        handler.SIG_STOP_MOVING.connect(self.stop)
        return setup_standard_tool_filter(filter, start_state)

    def start(self, filter, event):
        self.maskeditem = self.find_masked_curve(filter.plot)
        #print(self.maskeditem)
        if self.maskeditem  == None:
            return
        else:
            filter.plot.select_item(self.maskeditem )
        if self.marker is None:
            title = ""
            if self.TITLE:
                title = "<b>%s</b><br>" % self.TITLE
            if self.on_active_item:
                constraint_cb = filter.plot.on_active_curve
                label_cb = lambda x, y: title + \
                           filter.plot.get_coordinates_str(x, y)
            else:
                constraint_cb = None
                label_cb = lambda x, y: \
                           "%sx = %g<br>y = %g" % (title, x, y)
            self.marker = Marker(label_cb=label_cb,
                                 constraint_cb=constraint_cb)
            self.set_marker_style(self.marker)
        self.marker.attach(filter.plot)
        self.marker.setZ(filter.plot.get_max_z()+1)
        self.marker.setVisible(True)

    def stop(self, filter, event):
        self.move( filter, event )
        if self.mode != "reuse":
            self.marker.detach()
            self.marker = None
        if self.end_callback:
            self.end_callback(self)

    def move(self, filter, event):
        if self.marker is None:
            return # something is wrong ...
        self.marker.move_local_point_to( 0, event.pos() )
        filter.plot.replot()
        self.last_pos = self.marker.xValue(), self.marker.yValue()

    def get_coordinates(self):
        return self.last_pos

    def validate(self, filter, event):
        if self.maskeditem is not None:
            self.maskeditem.toggle_current_point_mask()






class MaskTool2D(CommandTool):

    SIG_SHOW_MASK = Signal(bool)
    SIG_APPLY_MASK = Signal()
    SIG_CLEAR_MASK = Signal()
    SIG_SET_MASK_SIZE = Signal()

    def __init__(self, manager, toolbar_id=DefaultToolbarID):
        CommandTool.__init__(self, manager, _("Fit"),icon= get_icon("eraser.png"),
                                            tip=_("Masking operations"),
                                            toolbar_id=toolbar_id)
        self.manager=manager

    def create_action_menu(self, manager):
        #Create and return menu for the tool's action
        self.rect_tool = manager.add_tool(RectangleMaskTool, toolbar_id=None)

        menu = QMenu()
        self.showmask_action = manager.create_action(_("Show image mask"),
                                                     toggled=self.show_mask)
        self.showmask_action.setChecked(True)
        self.unmask_action = manager.create_action(_("Unmask"),
                                                     toggled=self.set_mask_unmask)
        self.unmask_action.setChecked(False)
        savemask_a = manager.create_action(_("Apply mask to the data"),
                                            icon=get_icon("apply.png"),
                                            triggered=self.apply_mask)
        clearmask_a = manager.create_action(_("Clear mask"),
                                            icon=get_icon("delete.png"),
                                            triggered=self.clear_mask)
        setsize_a = manager.create_action(_("Set tool size"),
                                            triggered=self.set_size)
        add_actions(menu, (self.showmask_action, self.rect_tool.action, self.unmask_action, setsize_a,
                           savemask_a, clearmask_a))
        self.action.setMenu(menu)
        return menu

    def show_mask(self):
        self.SIG_SHOW_MASK.emit(self.showmask_action.isChecked())

    def apply_mask(self):
        self.SIG_APPLY_MASK.emit()

    def clear_mask(self):
        self.SIG_CLEAR_MASK.emit()

    def set_size(self):
        self.SIG_SET_MASK_SIZE.emit()

    def set_mask_unmask(self):
        self.rect_tool.mode=not(self.unmask_action.isChecked())
"""
