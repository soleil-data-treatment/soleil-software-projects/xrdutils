"""
Created on Wed Nov 22 11:49:09 2023

@author: prevot
"""

from qtpy.QtCore import Signal
from guiqwt.curve import CurvePlot

class CurveFitPlot(CurvePlot):
    #signal emitted by plot when a shape is modified by handle
    SIG_SHAPE_CHANGED = Signal("PyQt_PyObject",int)
