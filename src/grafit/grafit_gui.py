"""
Created on Wed Sep  2 12:24:33 2020

@author: prevot
"""
import numpy as np
from os import getcwd, path
from qtpy.QtWidgets import QMainWindow,QMenuBar,QMenu,QAction,QFileDialog, QMessageBox

from grafit.io import readfileparams,readfilewindow
from grafit.widgets import DataTable
from grafit.fit1D import Fit1DWindow
from grafit.parameters import AddFitDataParam,AddFitParam

class GrafitWindow(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle("Grafit")

        self.create_menubar()
        self.fit1Dwindow = Fit1DWindow()

        self.cwd = getcwd()
        self.savename = None  #name where to save the fit
        self.readfileparams = readfileparams()
        self.addfitdataparam = AddFitDataParam()

        self.table_list=[]    #list of all tables opened (excepted resulttable which is treated separately)

    def create_menubar(self):
        self.menubar = QMenuBar(self)
        self.layout().setMenuBar(self.menubar)

        """***************File Menu*******************************"""

        self.menuFile = QMenu("File",self.menubar)

        self.actionOpen = QAction("Open",self.menuFile)
        self.actionOpen.setShortcut("Ctrl+O")

        self.actionQuit = QAction("Quit",self.menuFile)
        self.actionQuit.setShortcut("Ctrl+Q")

        self.menuFile.addActions((self.actionOpen, self.actionQuit))
        self.menubar.addMenu(self.menuFile)

        self.actionOpen.triggered.connect(self.get_open_filename)
        self.actionQuit.triggered.connect(self.close)

        """***************Display Menu*******************************"""
        self.menuDisplay = QMenu("Display",self.menubar)

        self.actionShowFitWindow = QAction("Fit window",self.menuDisplay)

        self.menuDisplay.addAction(self.actionShowFitWindow)
        self.menubar.addMenu(self.menuDisplay)


        self.actionShowFitWindow.triggered.connect(self.show_fit_window)

        """***************FitMenu*******************************"""
        self.menuFit = QMenu("Fit",self.menubar)

        self.actionAddFitData = QAction("Add data to fit",self.menuFit)

        self.menuFit.addAction(self.actionAddFitData)
        self.menubar.addMenu(self.menuFit)

        self.actionAddFitData.triggered.connect(self.set_fit_data)

    def get_open_filename(self):
        fname = QFileDialog.getOpenFileName(None, 'Save fit parameters',self.cwd)[0]
        if len(fname) == 0:
            return
        self.open_filename(fname)

    def open_filename(self,fname):
        fic=open(fname)
        ficlines= fic.readlines()
        fic.close()

        doit = readfilewindow(self.readfileparams,ficlines).exec_()

        if not doit:
            return

        self.cwd = path.dirname(fname)
        nstart = self.readfileparams.heading + self.readfileparams.title
        #creating all columns:
        columns = []
        nlabels = len(self.readfileparams.labels)

        for j in range(nlabels):
            columns.append([])

        for lig in ficlines[nstart:]:
            ll = lig.split(self.readfileparams.pattern)
            if self.readfileparams.ignore:
                ll = list(filter(None,ll))  #remove empty occurrences
            for j in range(nlabels):
                try:
                     columns[j].append(ll[j])
                except IndexError:
                    columns[j].append("")

        self.make_new_table(columns,self.readfileparams.labels,title=path.basename(fname))


    def make_new_table(self,columns,labels,title=None):
        new_data_table = DataTable()
        new_data_table.populate(columns,labels)
        self.table_list.append(new_data_table)
        if title is None or title=='':
            title = "table%d"%len(self.table_list)

        new_data_table.setWindowTitle(title)
        action = QAction(title,self.menuDisplay)

        self.menuDisplay.addAction(action)
        action.triggered.connect(new_data_table.show)
        self.menubar.addMenu(self.menuDisplay)
        new_data_table.SIG_FIT_DATA.connect(self.fit_data_table)

        new_data_table.show()

    def fit_data_table(self,data_table):
        #columns have been selected in the data_table
        sel_labels,sel_column = data_table.get_selected_column_labels()  #label is the column header, column is the column number
        n_sel = len(sel_labels)
        if n_sel == 0:
            QMessageBox.information(self,'Warning','select at least one column')
            return
        elif n_sel == 1:
            y = data_table.getColumnValues(sel_column[0])
            ny = len(y)
            x = np.arange(ny)
            y_label = sel_labels[0]
            title = data_table.windowTitle()+'_'+y_label+'=f(n)'
            self.fit1Dwindow.add_data(x,y,title=title,y_label=y_label)
        else:
            param=AddFitParam()

            param.update_choice(data_table.windowTitle())
            param.update_column_choice(sel_labels,sel_column)
            #print (column)
            param.x_label = sel_column[0]
            param.y_label = sel_column[1]
            if n_sel>2 :
                param.s_label = sel_column[2]

            #print( param)
            ok = param.edit()
            if not ok:
                return

            x_label = data_table.labels[param.x_label]
            y_label = data_table.labels[param.y_label]
            x = data_table.getColumnValues(param.x_label)
            y = data_table.getColumnValues(param.y_label)
            title = data_table.windowTitle()+'_'+y_label+'=f('+x_label+')'

            if param.sigma == 0:
                self.fit1Dwindow.add_data(x,y,title=title,x_label=x_label,y_label=y_label)
            else:
                w = data_table.getColumnValues(param.s_label)
                if param.sigma == 1:
                    self.fit1Dwindow.add_data(x,y,1./w,title=title,x_label=x_label,y_label=y_label)
                else:
                    self.fit1Dwindow.add_data(x,y,w,title=title,x_label=x_label,y_label=y_label)

    def set_fit_data(self):
        param=self.addfitdataparam
        if len(self.table_list) ==0:
            return
        choices_data=[]
        for data_table in self.table_list:
            choices_data.append([data_table.windowTitle(),data_table.labels])
        param.update_choices_data(choices_data)
        param.edit(self,apply=self.add_fit_data)

    def add_fit_data(self,param):
        c0 = param.choices_data[param.title]
        labels = c0[1]

        data_table = self.table_list[param.title]
        x_label = labels[param.x_label]
        y_label = labels[param.y_label]
        x = data_table.getColumnValues(param.x_label)
        y = data_table.getColumnValues(param.y_label)
        title = data_table.windowTitle()+'_'+y_label+'=f('+x_label+')'
        if param.sigma == 0:
            self.fit1Dwindow.add_data(x,y,title=title,x_label=x_label,y_label=y_label)
        else:
            w = data_table.getColumnValues(param.s_label)
            if param.sigma == 1:
                self.fit1Dwindow.add_data(x,y,1./w,title=title,x_label=x_label,y_label=y_label)
            else:
                self.fit1Dwindow.add_data(x,y,w,title=title,x_label=x_label,y_label=y_label)


    def show_fit_window(self):
        self.fit1Dwindow.show()

    def closeEvent(self,event):
        result = QMessageBox.question(self,
                      "Confirm Exit...",
                      "Are you sure you want to exit ?",
                      QMessageBox.Yes| QMessageBox.No)

        if result == QMessageBox.Yes:
            self.fit1Dwindow.close()
            for data_table in self.table_list:
                data_table.close()
            event.accept()
        else:
            event.ignore()


if __name__ == "__main__":

    from guidata import qapplication
    _app = qapplication()

    win = GrafitWindow()
    win.open_filename('compilation.txt')
    win.show()

    _app.exec_()
