"""
Created on Wed May 06 15:05:01 2020

@author: Prevot
"""

import numpy as np

import sys

from sys import maxsize
from qtpy.QtGui import QPainter,QBrush,QPolygonF
from qtpy.QtCore import Qt, QRectF, QPointF, QLineF
from guiqwt._scaler import _histogram
from guiqwt.baseplot import canvas_to_axes
from guiqwt.builder import PlotItemBuilder as GuiPlotItemBuilder
from guiqwt.config import _, CONF, make_title
from guiqwt.histogram import lut_range_threshold
from guiqwt.image import ImageItem,MaskedImageItem
from guiqwt.shapes import  EllipseShape,RectangleShape,PolygonShape
from guiqwt.styles import (CurveParam,ImageParam,MaskedImageParam,ErrorBarParam)
from guiqwt.transitional import QwtSymbol
from guiqwt.curve import CurveItem,ErrorBarCurveItem,vmap

DEFAULTS = {
            'plot' :
             {

              "shape/peakshape/line/style" : 'SolidLine',
              "shape/peakshape/line/color" : "#000000",
              "shape/peakshape/line/width" : 1,
              "shape/peakshape/sel_line/style" : 'SolidLine',
              "shape/peakshape/sel_line/color" : "#00ff00",
              "shape/peakshape/sel_line/width" : 1,
              "shape/peakshape/fill/style" : "NoBrush",
              "shape/peakshape/sel_fill/style" : "NoBrush",
              "shape/peakshape/symbol/marker" : "NoSymbol",
              "shape/peakshape/sel_symbol/marker" : "Rect",
              "shape/peakshape/sel_symbol/size" : 9,
              "shape/peakshape/sel_symbol/edgecolor" : "red",
              "shape/peakshape/sel_symbol/facecolor" : "black",
              "shape/peakshape/sel_symbol/alpha" : .9,

                            },

            }

CONF.update_defaults(DEFAULTS)
CURVE_COUNT = 0


class BackgroundShape(PolygonShape):
    """BackgroundShape is a polygon with n points used for fitting a background on a 2D image"""
    CLOSED = True
    def __init__(self, n = 3, shapeparam=None, model=None, image = None):
        points = np.random.rand(n,2)
        super().__init__(points, shapeparam=shapeparam)

        self.set_style('plot', "shape/peakshape")
        self.set_model(model)
        self.image = image   #use image to fit background

    def set_model(self,model):
        self.model = model

    def set_from_params(self, params):
        #set the shape from model parameters, nothing happens
        pass

    def set_params(self, params, handle=0):
        #set the model parameters from the shape
        if self.image is None:
            return
        data=[]
        x0, x1 = self.image.get_xdata()
        y0, y1 = self.image.get_ydata()
        for pt in self.points:
            if not (x0<=pt[0]<=x1 and y0<=pt[1]<=y1):
                return
            value = self.image.get_data(pt[0],pt[1])
            if not np.isfinite(value):   #there is no data at the point considered
                return
            data.append(value)
        self.model.guess_from_curve(self.points, data, params, handle=0)

    def move_point_to(self, handle, pos, ctrl=None):
        super().move_point_to(handle, pos, ctrl=ctrl)
        if self.plot():

            self.plot().SIG_SHAPE_CHANGED.emit(self, handle)

class TrueEllipseShape(PolygonShape):
    """similar to guiqwt EllipseShape but with a better modification of the shape with the handlers"""
    CLOSED = True
    def __init__(self, x1=0, y1=0, x2=0, y2=0, shapeparam=None):
        super().__init__(shapeparam=shapeparam)
        loc0=(x2-x1)/2.
        loc1=(y2-y1)/2.
        width0,width1 = abs(x2-x1)/2.,abs(y2-y1)/2.
        self.set_from_center_and_width(loc0,loc1,width0,width1)

    def set_from_drawing_points(self, plot, p0, p1, keep_b_axis = False):
        #the shape has been drawn with a tool from p0 to p1, we set its value
        x0,y0 = canvas_to_axes(self, p0)
        x1,y1 = canvas_to_axes(self, p1)
        #center position
        loc0 = (x0+x1)/2.
        loc1 = (y0+y1)/2.
        dx = (x1-x0)/2.
        dy = (y1-y0)/2.
        a = np.sqrt((x1-x0)**2+(y1-y0)**2)

        if keep_b_axis and a != 0:   #we keep the size of b axis
            x2, y2 = self.points[2]
            x3, y3 = self.points[3]
            b = np.sqrt((x3-x2)**2+(y3-y2)**2)
            dx = dx*b/a
            dy = dy*b/a
            self.set_points([[x0,y0],[x1,y1],[loc0-dy,loc1+dx],[loc0+dy,loc1-dx]])

        else:
            self.set_points([[x0,y0],[x1,y1],[loc0-dy,loc1+dx],[loc0+dy,loc1-dx]])

        #width0 = np.sqrt((x0-x1)**2+(y0-y1)**2)/2.
        #width1 = width0
        #self.set_from_center_and_width(loc0,loc1,width0,width1)

    def set_local_center(self, p0):
        x1, y1 = canvas_to_axes(self, p0)
        self.set_center(x1, y1)

    def set_center(self, x1, y1):
        x0 = (self.points[0][0]+self.points[1][0])/2.
        y0 = (self.points[0][1]+self.points[1][1])/2.

        dx = x1-x0
        dy = y1-y0
        self.points += np.array([[dx, dy]])

    def set_from_center_and_width(self,loc0,loc1,width0,width1):
        #first axis points, then second axis points
        self.set_points([[loc0-width0,loc1],[loc0+width0,loc1],[loc0,loc1-width1],[loc0,loc1+width1]])

    def get_xline(self):
        return QLineF(*(tuple(self.points[0])+tuple(self.points[1])))

    def get_yline(self):
        return QLineF(*(tuple(self.points[2])+tuple(self.points[3])))


    def set_xdiameter(self, x0, y0, x1, y1):
        """Set the coordinates of the ellipse's X-axis diameter"""
        xline = QLineF(x0, y0, x1, y1)
        yline = xline.normalVector()
        yline.translate(xline.pointAt(.5)-xline.p1())
        yline.setLength(self.get_yline().length())
        yline.translate(yline.pointAt(.5)-yline.p2())
        self.set_points([(x0, y0), (x1, y1),
                         (yline.x1(), yline.y1()), (yline.x2(), yline.y2())])

    def get_xdiameter(self):
        """Return the coordinates of the ellipse's X-axis diameter"""
        return tuple(self.points[0])+tuple(self.points[1])

    def set_ydiameter(self, x2, y2, x3, y3):
        """Set the coordinates of the ellipse's Y-axis diameter"""
        yline = QLineF(x2, y2, x3, y3)
        xline = yline.normalVector()
        xline.translate(yline.pointAt(.5)-yline.p1())
        xline.setLength(self.get_xline().length())
        xline.translate(xline.pointAt(.5)-xline.p2())
        self.set_points([(xline.x1(), xline.y1()), (xline.x2(), xline.y2()),
                         (x2, y2), (x3, y3)])

    def get_ydiameter(self):
        """Return the coordinates of the ellipse's Y-axis diameter"""
        return tuple(self.points[2])+tuple(self.points[3])

    def move_point_to(self, handle, pos, ctrl=None):
        nx, ny = pos
        if handle == 0:
            x1, y1 = self.points[1]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x0, y0 = self.points[0]
                x1, y1 = x1+x0-nx, y1+y0-ny
            self.set_xdiameter(nx, ny, x1, y1)
        elif handle == 1:
            x0, y0 = self.points[0]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x1, y1 = self.points[1]
                x0, y0 = x0+x1-nx, y0+y1-ny
            self.set_xdiameter(x0, y0, nx, ny)
        elif handle == 2:
            x3, y3 = self.points[3]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x2, y2 = self.points[2]
                x3, y3 = x3+x2-nx, y3+y2-ny
            self.set_ydiameter(nx, ny, x3, y3)
        elif handle == 3:
            x2, y2 = self.points[2]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x3, y3 = self.points[3]
                x2, y2 = x2+x3-nx, y2+y3-ny
            self.set_ydiameter(x2, y2, nx, ny)
        elif handle == -1:
            delta = (nx, ny)-self.points.mean(axis=0)
            self.points += delta


    def get_sizes_and_angle(self):
        """Return ellipse axes and angle"""
        x0, y0 = self.points[0]
        x1, y1 = self.points[1]
        a = np.sqrt((x1-x0)**2+(y1-y0)**2)
        th = np.arctan2(y1-y0,x1-x0)
        x0, y0 = self.points[2]
        x1, y1 = self.points[3]
        b = np.sqrt((x1-x0)**2+(y1-y0)**2)

        return a,b,th

    def get_bounding_rectangle(self):
        x0 = (self.points[0,0]+self.points[1,0])/2.
        y0 = (self.points[0,1]+self.points[1,1])/2.

        a, b, th = self.get_sizes_and_angle()
        c = np.cos(th)
        s = np.sin(th)
        u = np.sqrt((a*c)**2+(b*s)**2)/2.
        v = np.sqrt((a*s)**2+(b*c)**2)/2.
        x_min = x0 - u
        x_max = x0 + u
        y_min = y0 - v
        y_max = y0 + v

        return (x_min,y_min,x_max,y_max)


    def hit_test(self, pos):
        """return (dist, handle, inside)"""
        if not self.plot():
            return sys.maxsize, 0, False, None
        dist, handle, inside, other = self.poly_hit_test(self.plot(),
                                             self.xAxis(), self.yAxis(), pos)
        if not inside:
            xMap = self.plot().canvasMap(self.xAxis())
            yMap = self.plot().canvasMap(self.yAxis())
            _points, _line0, _line1, rect, angle = self.compute_elements(xMap, yMap)
            inside = rect.contains(QPointF(pos))
        return dist, handle, inside, other

    def compute_elements(self, xMap, yMap):
        """Return points, lines and ellipse rect"""
        #if aspect ratio of the plot is not 1, need to recompute axis

        u=(xMap.invTransform(1)-xMap.invTransform(0))   #u is the (signed) pixel size
        v=(yMap.invTransform(1)-yMap.invTransform(0))   #v is the (signed) pixel size
        a,b,th=self.get_sizes_and_angle()


        if a*b != 0:
            c=np.cos(th)
            s=np.sin(th)
            cs=2.*c*s

            fx2=(u*c/a)**2+(u*s/b)**2
            fy2=(v*s/a)**2+(v*c/b)**2
            fxy = u*v*(1./a**2-1./b**2)   #equal to zero if a=b
            fxycs=u*v*cs*(1./a**2-1./b**2)   #equal to zero if a=b, or a,b along x or y

            if abs(s)<1e-15:
                a=a/u
                b=b/v
            elif abs(c)<1e-15:
                a=a/v
                b=b/u
            elif abs(a/b-1.) <1e-15:
                th=np.arctan2(fxycs,fx2-fy2)/2.  #angle of the ellipse major axis and x direction
                a=np.sqrt(2./(fx2+fy2))
                b=a
            else:
                th=np.arctan2(fxycs,fx2-fy2)/2.  #angle of the ellipse major axis and x direction
                cs=2.*np.cos(th)*np.sin(th)
                a=np.sqrt(2./(fx2+fy2+fxycs/cs))
                b=np.sqrt(2./(fx2+fy2-fxycs/cs))
        #ellipse major and minor axes
        th=np.rad2deg(th)


        points = self.transform_points(xMap, yMap)

        line0 = QLineF(points[0], points[1])
        line1 = QLineF(points[2], points[3])

        rect = QRectF()
        rect.setWidth(a)
        rect.setHeight(b)
        rect.moveCenter(line0.pointAt(.5))



        return points, line0, line1, rect, th


    def draw(self, painter, xMap, yMap, canvasRect):
        points, line0, line1, rect, angle = self.compute_elements(xMap, yMap)
        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)
        #painter.drawLine(line0)
        #painter.drawLine(line1)
        painter.save()

        painter.translate(rect.center())
        painter.rotate(angle)
        painter.translate(-rect.center())
        painter.drawEllipse(rect.toRect())

        painter.restore()
        if symbol != QwtSymbol.NoSymbol:
            for i in range(points.size()):
                symbol.drawSymbol(painter, points[i].toPoint())




class PeakShape(TrueEllipseShape):
    """PeakShape is a centered ellipse used for defining a 2D fitting curve"""
    CLOSED = True
    def __init__(self, x1=0, y1=0, x2=0, y2=0, shapeparam=None, model=None):
        super().__init__(x1, y1, x2, y2, shapeparam=shapeparam)
        self.is_ellipse = True
        self.set_style('plot', "shape/peakshape")
        self.set_model(model)

    def set_model(self,model):
        self.model = model

    def set_from_drawing_points(self, plot, p0, p1):
        #the shape has been drawn with a tool from p0 to p1, we set its value
        loc0,loc1 = canvas_to_axes(self, p0)
        x1,y1 = canvas_to_axes(self, p1)

        width0,width1 = 2.*abs(x1-loc0),2*abs(y1-loc1)
        self.set_from_center_and_width(loc0,loc1,width0,width1)

    def set_from_params(self, params):
        #set the shape from model parameters
        prefix=self.model.prefix
        loc0 = params[prefix+'loc0'].value
        loc1 = params[prefix+'loc1'].value
        width0 = params[prefix+'width0'].value
        width1 = params[prefix+'width1'].value
        th = params[prefix+'th'].value
        cc=np.cos(th)/2.
        ss=np.sin(th)/2.
        cc0=cc*width0
        ss0=ss*width0
        cc1=cc*width1
        ss1=ss*width1
        self.set_points([[loc0-cc0,loc1-ss0],[loc0+cc0,loc1+ss0],[loc0+ss1,loc1-cc1],[loc0-ss1,loc1+cc1]])

    def set_params(self, params, handle=0):
        #set the model parameters from the shape
        prefix=self.model.prefix
        p0, p1, p2, p3 = self.get_points()
        loc0 = (p0[0]+p1[0])/2.
        loc1 =(p0[1]+p1[1])/2.
        width0 =np.sqrt((p1[0]-p0[0])**2+(p1[1]-p0[1])**2)
        width1 =np.sqrt((p3[0]-p2[0])**2+(p3[1]-p2[1])**2)
        th = np.arctan2(p1[1]-p0[1],p1[0]-p0[0])
        params[prefix+'loc0'].set(value = loc0)
        params[prefix+'loc1'].set(value = loc1)
        params[prefix+'width0'].set(value = width0)
        params[prefix+'width1'].set(value = width1)
        params[prefix+'th'].set(value = th)

    def set_from_tool(self, params, p0, p1):
        """set model params from tool drawing starting and ending points
        uses the function of the associated model"""
        if self.model == None:
            return
        x0, y0 = canvas_to_axes(self,p0)
        x1, y1 = canvas_to_axes(self,p1)
        tool_points = [[x0,y0],[x1,y1]]
        self.model.set_from_tool(tool_points, params)
        self.set_from_params(params)

    def move_point_to(self, handle, pos, ctrl=None):
        super().move_point_to(handle, pos, ctrl=ctrl)
        if self.plot():
            self.plot().SIG_SHAPE_CHANGED.emit(self, handle)

    def get_sizes_and_angle(self):
        """Return ellipse axes and angle"""
        x0, y0 = self.points[0]
        x1, y1 = self.points[1]
        a = np.sqrt((x1-x0)**2+(y1-y0)**2)
        th = np.arctan2(y1-y0,x1-x0)
        x0, y0 = self.points[2]
        x1, y1 = self.points[3]
        b = np.sqrt((x1-x0)**2+(y1-y0)**2)
        return a,b,th

    def hit_test(self, pos):
        """return (dist, handle, inside)"""
        if not self.plot():
            return sys.maxsize, 0, False, None
        dist, handle, inside, other = self.poly_hit_test(self.plot(),
                                             self.xAxis(), self.yAxis(), pos)
        if not inside:
            xMap = self.plot().canvasMap(self.xAxis())
            yMap = self.plot().canvasMap(self.yAxis())
            _points, _line0, _line1, rect, angle = self.compute_elements(xMap, yMap)
            inside = rect.contains(QPointF(pos))
        return dist, handle, inside, other

    def draw(self, painter, xMap, yMap, canvasRect):
        """draw only the first axis"""
        points, line0, line1, rect, angle = self.compute_elements(xMap, yMap)
        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)
        painter.drawLine(line0)
        painter.save()

        painter.translate(rect.center())
        painter.rotate(angle)
        painter.translate(-rect.center())
        painter.drawEllipse(rect.toRect())
        painter.restore()
        if symbol != QwtSymbol.NoSymbol:
            for i in range(points.size()):
                symbol.drawSymbol(painter, points[i].toPoint())




class CurveShape(PolygonShape):
    """Curve Shape is a set of points used for defining a 1D fitting curve which
    is drawn together with the points"""
    CLOSED = False
    ON_BG = False   #for positionning the shape

    def __init__(self, points=None, closed=None, shapeparam=None, model=None, precision =100):
        self.precision = precision  #number of points used for drawing the curve
        self.curve_points = np.empty((precision,0))  #points of the curve
        super().__init__(points = points, closed = closed, shapeparam=shapeparam)  #points are the handles for setting curve parameters
        self.set_model(model)

    def set_model(self,model):
        self.model = model

    def set_color(self,color):
        self.shapeparam.update_param(self)
        self.shapeparam.line.color = color
        self.shapeparam.symbol.edgecolor = color
        self.shapeparam.symbol.facecolor = color
        self.shapeparam.sel_line.color = color
        self.shapeparam.sel_symbol.edgecolor = color
        self.shapeparam.sel_symbol.facecolor = color
        self.shapeparam.update_shape(self)

    def initialize_points(self, params):
        if self.model is None or self.plot() is  None:
            return

        npoints = self.model.get_curve_points_len()
        self.points = np.zeros((npoints,2))
        try:
            xmin, xmax = self.plot().get_axis_limits("bottom")
        except:
            xmin, xmax = 0. , 1.
        self.points[:,0] = xmin+(0.5+np.arange(npoints))*(xmax-xmin)/npoints    #a priori, use a uniform repartition of points abscissae

        self.model.set_points(self.points, params)
        self.set_curve_points(params)

    def move_point_to(self, handle, pos, ctrl=None):
        super().move_point_to(handle, pos, ctrl=ctrl)
        if self.plot():
            self.plot().SIG_SHAPE_CHANGED.emit(self, handle)

    def set_params(self,params, handle=0):
        """set model params from anchor points
        uses the guess function of the associated model"""

        if self.model == None:
            return

        self.model.guess_from_curve(self.points, params, handle)  #from the points set parameters values

        self.set_curve_points(params)

    def set_from_tool(self, params, p0, p1):
        """set model params from tool drawing starting and ending points
        uses the function of the associated model"""
        if self.model == None:
            return
        x0, y0 = canvas_to_axes(self,p0)
        x1, y1 = canvas_to_axes(self,p1)
        tool_points = [[x0,y0],[x1,y1]]
        self.model.set_from_tool(tool_points, params)
        self.set_curve_points(params)

    def set_from_params(self, params):
        """set anchor points from model parameters"""
        #set the shape from shape parameters with respect to ypos, value returned by model at shape position
        if self.model == None:
            return
        self.model.set_points(self.points, params)
        self.set_curve_points(params)

    def set_curve_points(self, params):
        """set curve points form model parameters"""
        plot = self.plot()
        if not plot:
            return
        try:
            xmin, xmax = plot.get_axis_limits("bottom")
        except:
            xmin, xmax = 0., 1.
        _x = xmin+np.arange(self.precision+1)/self.precision*(xmax-xmin)
        _y = self.model.eval(params, x=_x)
        self.curve_points = np.transpose(np.array([_x,_y]))

    def poly_hit_test(self, plot, ax, ay, pos):
        """determine the minimum distance to one of the points"""
        """inside is always false"""
        pos = QPointF(pos)
        dist = maxsize
        handle = -1
        Cx, Cy = pos.x(), pos.y()
        pts = self.points
        for i in range(pts.shape[0]):
            # On calcule la distance dans le repère du canvas
            px = plot.transform(ax, pts[i, 0])
            py = plot.transform(ay, pts[i, 1])
            d = (Cx-px)**2 + (Cy-py)**2
            if d < dist:
                dist = d
                handle = i
        inside = False
        return np.sqrt(dist), handle, inside, None

    def draw(self, painter, xMap, yMap, canvasRect):
        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)
        points = self.transform_points(xMap, yMap)

        if symbol != QwtSymbol.NoSymbol:
            symbol.drawSymbols(painter, points)

        if pen.style() != 0:
            points = QPolygonF()
            for i in range(self.curve_points.shape[0]):
                points.append(QPointF(xMap.transform(self.curve_points[i, 0]),
                                  yMap.transform(self.curve_points[i, 1])))
            painter.drawPolyline(points)

class RectangleCentredShape(RectangleShape):
    CLOSED = True
    def __init__(self, xc=0, yc=0, hsize=1, vsize=1, shapeparam=None):
        super().__init__(shapeparam=shapeparam)
        self.is_ellipse = False
        self.hsize=hsize
        self.vsize=vsize
        self.set_center(xc,yc)

    def set_size(self,hsize,vsize):
        self.hsize=hsize
        self.vsize=vsize
        x0, y0, x1, y1=self.get_rect()
        xc=(x0+x1)/2.
        yc=(y0+y1)/2.
        self.set_center(xc,yc)

    def set_local_center(self,pos):
        #set center from position in canvas units
        xc,yc = canvas_to_axes(self, pos)
        self.set_center(xc,yc)

    def set_center(self,xc,yc):
        x0=xc-self.hsize/2.
        y0=yc-self.vsize/2.
        x1=xc+self.hsize/2.
        y1=yc+self.vsize/2.
        self.set_rect(x0, y0, x1, y1)



def _nanmin(data):
    if data.dtype.name in ("float32","float64", "float128"):
        return np.nanmin(data)
    else:
        return data.min()

def _nanmax(data):
    if data.dtype.name in ("float32","float64", "float128"):
        return np.nanmax(data)
    else:
        return data.max()

class AlphaMaskedArea:
    """Defines masked/alpha areas for a masked/alpha image item"""
    """geometry can be rectangular, elliptical or polygonal"""
    """mask can be applied inside or outside the shape"""
    """the shape can be used to mask or unmask"""
    """a gradient can be applied along a direction """
    def __init__(self, geometry=None, pts = None,  inside=None, mask=None, gradient =None):
        self.geometry = geometry
        self.pts = pts
        self.inside = inside
        self.mask = mask
        self.gradient = gradient

    def __eq__(self, other):
        return (self.geometry == other.geometry and np.array_equal(self.pts, other.pts) and
               self.inside == other.inside and self.mask == self.mask and
               self.gradient == self.gradient)

    def serialize(self, writer):
        """Serialize object to HDF5 writer"""
        for name in ('geometry', 'inside', 'mask', 'gradient', 'pts'):
            writer.write(getattr(self, name), name)

    def deserialize(self, reader):
        """Deserialize object from HDF5 reader"""
        self.geometry = reader.read('geometry')
        self.inside = reader.read('inside')
        self.mask = reader.read('mask')
        self.gradient = reader.read('gradient')
        self.pts =  reader.read(group_name='pts', func=reader.read_array)

class MaskedImageNan(MaskedImageItem):


    def auto_lut_scale(self):
        _min, _max = _nanmin(self.data), _nanmax(self.data)
        self.set_lut_range([_min, _max])

    def auto_lut_scale_sym(self):
        _max = max(abs(_nanmin(self.data)), abs(_nanmax(self.data)))
        self.set_lut_range([-_max, _max])

    def get_histogram(self, nbins):
        """interface de IHistDataSource"""
        if self.data is None:
            return [0,], [0,1]
        if self.histogram_cache is None or nbins != self.histogram_cache[0].shape[0]:
            #from guidata.utils import tic, toc
            if False:
                #tic("histo1")
                res = np.histogram(self.data[~np.isnan(self.data)], nbins)
                #toc("histo1")
            else:
                #TODO: _histogram is faster, but caching is buggy
                # in this version
                #tic("histo2")
                _min = _nanmin(self.data)
                _max = _nanmax(self.data)
                if self.data.dtype in (np.float64, np.float32):
                    bins = np.unique(np.array(np.linspace(_min, _max, nbins+1),
                                              dtype=self.data.dtype))
                else:
                    bins = np.arange(_min, _max+2,
                                     dtype=self.data.dtype)
                res2 = np.zeros((bins.size+1,), np.uint32)
                _histogram(self.data.flatten(), bins, res2)
                #toc("histo2")
                res = res2[1:-1], bins
            self.histogram_cache = res
        else:
            res = self.histogram_cache
        return res


    #---- RawImageItem API -----------------------------------------------------
    def set_data(self, data, lut_range=None):

        #Set Image item data
        #    * data: 2D NumPy array
        #    * lut_range: LUT range -- tuple (levelmin, levelmax)
        ImageItem.set_data(self, data, lut_range)   #data is passed as reference
        self.data = np.ma.array(data,copy=True) #data is passed as a copy so we can modify the image mask without changing the data mask
        self._mask = None # removing reference to this temporary array

        if self.imageparam.filling_value is None:
            self.imageparam.filling_value = self.data.get_fill_value()
#        self.data.harden_mask()
        self.update_mask()


    def hit_test(self, pos):
        """image is not selected for mouseclick on masked values"""
        plot = self.plot()
        ax = self.xAxis()
        ay = self.yAxis()
        _sd, _h, _i, _u = self.border_rect.poly_hit_test(plot, ax, ay, pos)
        if _i:
            #check if self.data is not nan at this point
            x0 = plot.invTransform(ax, pos.x())
            y0 = plot.invTransform(ay, pos.y())
            if not np.isfinite(self.get_data(x0, y0)):
                _i = False
        return _sd, _h, _i, _u

    def set_masked_areas(self, areas):
        """Set masked areas (see set_mask_filename)"""
        self._masked_areas = areas

    def get_masked_areas(self):
        return self._masked_areas

    def add_masked_area(self, geometry, pts, inside, mask):
        area = AlphaMaskedArea(geometry=geometry, pts=pts, inside=inside, mask=mask, gradient=None)
        for _area in self._masked_areas:
            if area == _area:
                return
        self._masked_areas.append(area)

    def update_mask(self):
        if isinstance(self.data, np.ma.MaskedArray):
            self.data.set_fill_value(self.imageparam.filling_value)

    def get_coordinates_label(self, xc, yc):
        title = self.title().text()
        z = self.get_data(xc, yc)
        return f"{title}:<br>x = {xc:g}<br>y = {yc:g}<br>z = {z:g}"

    def get_closest_coordinates(self, x, y):
        """
        Get the closest coordinates to the given point, at the center of the pixel

        Args:
            x: X coordinate
            y: Y coordinate

        Returns:
            tuple[float, float]: Closest coordinates
        """
        xmin, xmax = self.get_xdata()
        ymin, ymax = self.get_ydata()
        i, j = self.get_closest_indexes(x, y)
        xpix = xmin + (i+0.5)*(xmax-xmin)/self.data.shape[1]
        ypix = ymin + (j+0.5)*(ymax-ymin)/self.data.shape[0]

        return xpix, ypix

class ImageNan(ImageItem):


    def auto_lut_scale(self):
        _min, _max = _nanmin(self.data), _nanmax(self.data)
        self.set_lut_range([_min, _max])

    def auto_lut_scale_sym(self):
        _max = max(abs(_nanmin(self.data)), abs(_nanmax(self.data)))
        self.set_lut_range([-_max, _max])

    def get_histogram(self, nbins):
        """interface de IHistDataSource"""
        if self.data is None:
            return [0,], [0,1]
        if self.histogram_cache is None or nbins != self.histogram_cache[0].shape[0]:
            #from guidata.utils import tic, toc
            if False:
                #tic("histo1")
                res = np.histogram(self.data[~np.isnan(self.data)], nbins)
                #toc("histo1")
            else:
                #TODO: _histogram is faster, but caching is buggy
                # in this version
                #tic("histo2")
                _min = _nanmin(self.data)
                _max = _nanmax(self.data)
                if self.data.dtype in (np.float64, np.float32):
                    bins = np.unique(np.array(np.linspace(_min, _max, nbins+1),
                                              dtype=self.data.dtype))
                else:
                    bins = np.arange(_min, _max+2,
                                     dtype=self.data.dtype)
                res2 = np.zeros((bins.size+1,), np.uint32)
                _histogram(self.data.flatten(), bins, res2)
                #toc("histo2")
                res = res2[1:-1], bins
            self.histogram_cache = res
        else:
            res = self.histogram_cache
        return res

    def get_coordinates_label(self, xc, yc):
        title = self.title().text()
        z = self.get_data(xc, yc)
        return f"{title}:<br>x = {xc:g}<br>y = {yc:g}<br>z = {z:g}"

class MaskedErrorBarCurveItem(ErrorBarCurveItem):
    """
    Construct an error-bar curve `plot item`
    with the parameters *errorbarparam*
    (see :py:class:`guiqwt.styles.ErrorBarParam`)
    """
    def __init__(self, curveparam=None, errorbarparam=None):
        if errorbarparam is None:
            self.errorbarparam = ErrorBarParam(_("Error bars"),
                                               icon='errorbar.png')
        else:
            self.errorbarparam = errorbarparam
        super(ErrorBarCurveItem, self).__init__(curveparam)
        self.masked_x = None
        self.current_point = 0
        self._masked_areas = []
        self.is_mask_visible = True

    def set_data(self, x, y, dx=None, dy=None):
        """
        Set error-bar curve data:

            * x: NumPy array
            * y: NumPy array
            * mask : NumPy boolean array or None
            * dx: float or NumPy array (non-constant error bars)
            * dy: float or NumPy array (non-constant error bars)
        """
        CurveItem.set_data(self, x, y)   #if x and y are float64, self._x and self._y are non-masked view of x and y
        #set reference to x and y  for QwtPlot
        #self._x = np.array(x, copy=False)   #non masked array
        #self._y = np.array(y, copy=False)   #non masked array

        self.masked_x = np.ma.array(x, copy=True)   #too complicated to allocate memory separately for the data and the mask

        if dx is not None:
            dx = np.array(dx, copy=False)
            if dx.size == 0:
                dx = None
        if dy is not None:
            dy = np.array(dy, copy=False)
            if dy.size == 0:
                dy = None
        self._dx = dx
        self._dy = dy
        self._minmaxarrays = {}

    def _mask_changed(self):
        """Emit the :py:data:`guiqwt.baseplot.BasePlot.SIG_MASK_CHANGED` signal"""
        plot = self.plot()
        if plot is not None:
            plot.SIG_MASK_CHANGED.emit(self)

    def set_mask_visible(self, visible):
        self.is_mask_visible = visible

    def set_masked_areas(self, areas):
        """Set masked areas (see set_mask_filename)"""
        self._masked_areas = areas

    def get_masked_areas(self):
        return self._masked_areas

    def get_mask(self):
        """Return image mask"""
        return self.masked_x.mask

    def add_masked_area(self, geometry, pts, inside, mask):
        area = AlphaMaskedArea(geometry=geometry, pts=pts, inside=inside, mask=mask, gradient=None)
        for _area in self._masked_areas:
            if area == _area:
                return
        self._masked_areas.append(area)

    def unmask_all(self):
        self.masked_x.mask = False

    def drawSeries(self, painter, xMap, yMap, canvasRect,  from_, to):   #from_ = 0, to = -1 draws the whole curve
        if self.is_mask_visible:
            if to==-1:
                non_masked = np.ma.flatnotmasked_contiguous(self.masked_x[from_:])  #slices
                masked = np.ma.clump_masked(self.masked_x[from_:])   #slices
            else:
                non_masked = np.ma.flatnotmasked_contiguous(self.masked_x[from_:to])
                masked = np.ma.clump_masked(self.masked_x[from_:to])
            for _slice in non_masked:
                super().drawSeries(painter, xMap, yMap, canvasRect,  from_+_slice.start, to+_slice.stop)

            painter.save()
            painter.setOpacity(0.25)
            for _slice in masked:
                 super().drawSeries(painter, xMap, yMap, canvasRect,  from_+_slice.start, to+_slice.stop)
            painter.restore()
        else:
            super().drawSeries(painter, xMap, yMap, canvasRect,  from_, to)

    def draw(self, painter, xMap, yMap, canvasRect):
        if self._x is None or self._x.size == 0:
            return
        x, y, xmin, xmax, ymin, ymax = self.get_minmax_arrays(all_values=False)
        tx = vmap(xMap, x)
        ty = vmap(yMap, y)
        RN = list(range(len(tx)))

        if self.errorOnTop:
            self.drawSeries(painter, xMap, yMap, canvasRect, 0, -1)

        painter.save()
        painter.setPen(self.errorPen)
        cap = self.errorCap/2.

        #error bars are not masked
        if self._dx is not None and self.errorbarparam.mode == 0:
            txmin = vmap(xMap, xmin)
            txmax = vmap(xMap, xmax)
            # Classic error bars
            lines = []
            for i in RN:
                yi = ty[i]
                lines.append(QLineF(txmin[i], yi, txmax[i], yi))
            painter.drawLines(lines)
            if cap > 0:
                lines = []
                for i in RN:
                    yi = ty[i]
                    lines.append(QLineF(txmin[i], yi-cap, txmin[i], yi+cap))
                    lines.append(QLineF(txmax[i], yi-cap, txmax[i], yi+cap))
            painter.drawLines(lines)

        if self._dy is not None:
            tymin = vmap(yMap, ymin)
            tymax = vmap(yMap, ymax)
            if self.errorbarparam.mode == 0:
                # Classic error bars
                lines = []
                for i in RN:
                    xi = tx[i]
                    lines.append(QLineF(xi, tymin[i], xi, tymax[i]))
                painter.drawLines(lines)
                if cap > 0:
                    # Cap
                    lines = []
                    for i in RN:
                        xi = tx[i]
                        lines.append(QLineF(xi-cap, tymin[i], xi+cap, tymin[i]))
                        lines.append(QLineF(xi-cap, tymax[i], xi+cap, tymax[i]))
                painter.drawLines(lines)
            else:
                # Error area
                points = []
                rpoints = []
                for i in RN:
                    xi = tx[i]
                    points.append(QPointF(xi, tymin[i]))
                    rpoints.append(QPointF(xi, tymax[i]))
                points += reversed(rpoints)
                painter.setBrush(QBrush(self.errorBrush))
                painter.drawPolygon(*points)

        painter.restore()

        if not self.errorOnTop:
            self.drawSeries(painter, xMap, yMap, canvasRect, 0, -1)

    def toggle_current_point_mask(self):
        #we toggle mask of current point
        i=self.current_index

        if not np.ma.is_masked(self.masked_x):   #we create an array for the mask
            self.masked_x.mask = np.zeros_like(self.masked_x)

        if self.masked_x.mask[i]:
            self.masked_x.mask[i] = False
        else:
            self.masked_x.mask[i] = True


    def get_next_point(self):
        i = self.current_index+1
        if i >= len(self._x):
            i = len(self._x)-1
        point = self.sample(i)
        self.current_index = i
        return point.x(), point.y()

    def get_previous_point(self):
        i = self.current_index-1
        if i < 0:
            i = 0
        point = self.sample(i)
        self.current_index = i
        return point.x(), point.y()

    def get_closest_coordinates(self, x, y):
        """Renvoie les coordonnées (x',y') du point le plus proche de (x,y)
        Méthode surchargée pour ErrorBarSignalCurve pour renvoyer
        les coordonnées des pointes des barres d'erreur"""
        plot = self.plot()
        ax = self.xAxis()
        ay = self.yAxis()
        xc = plot.transform(ax, x)
        yc = plot.transform(ay, y)
        _distance, i, _inside, _other = self.hit_test(QPointF(xc, yc))
        point = self.sample(i)
        self.current_index = i
        return point.x(), point.y()

    def update_mask(self):
        if isinstance(self.data, np.ma.MaskedArray):
            self.data.set_fill_value(self.imageparam.filling_value)
        #self.mask_qcolor=self.imageparam.mask_color


class PlotItemBuilder(GuiPlotItemBuilder):
    def __init__(self):
        super().__init__()


    def imagenan(self, data=None, filename=None, title=None, alpha_mask=None,
            alpha=None, background_color=None, colormap=None,
            xdata=[None, None], ydata=[None, None],
            pixel_size=None, center_on=None,
            interpolation='linear', eliminate_outliers=None,
            xformat='%.1f', yformat='%.1f', zformat='%.1f'):
        """
        Make an image `plot item` from data
        (:py:class:`guiqwt.image.ImageItem` object or
        :py:class:`guiqwt.image.RGBImageItem` object if data has 3 dimensions)
        """
        assert isinstance(xdata, (tuple, list)) and len(xdata) == 2
        assert isinstance(ydata, (tuple, list)) and len(ydata) == 2
        param = ImageParam(title="Image", icon='image.png')
        data, filename, title = self._get_image_data(data, filename, title,
                                                     to_grayscale=True)
        assert data.ndim == 2, "Data must have 2 dimensions"
        if pixel_size is None:
            assert center_on is None, "Ambiguous parameters: both `center_on`"\
                                      " and `xdata`/`ydata` were specified"
            xmin, xmax = xdata
            ymin, ymax = ydata
        else:
            xmin, xmax, ymin, ymax = self.compute_bounds(data, pixel_size,
                                                         center_on)
        self.__set_image_param(param, title, alpha_mask, alpha, interpolation,
                               background=background_color,
                               colormap=colormap,
                               xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                               xformat=xformat, yformat=yformat,
                               zformat=zformat)

        image = ImageNan(data, param)
        image.set_filename(filename)
        if eliminate_outliers is not None:
            image.set_lut_range(lut_range_threshold(image, 256,
                                                    eliminate_outliers))
        return image

    def maskedimagenan(self, data=None, mask=None, filename=None, title=None,
                    alpha_mask=False, alpha=1.0,
                    xdata=[None, None], ydata=[None, None],
                    pixel_size=None, center_on=None,
                    background_color=None, colormap=None,
                    show_mask=False, fill_value=None, interpolation='linear',
                    eliminate_outliers=None,
                    xformat='%.1f', yformat='%.1f', zformat='%.1f'):
        """
        Make a masked image `plot item` from data
        (:py:class:`guiqwt.image.MaskedImageItem` object)
        """
        assert isinstance(xdata, (tuple, list)) and len(xdata) == 2
        assert isinstance(ydata, (tuple, list)) and len(ydata) == 2
        param = MaskedImageParam(title=_("Image"), icon='image.png')
        data, filename, title = self._get_image_data(data, filename, title,
                                                     to_grayscale=True)
        assert data.ndim == 2, "Data must have 2 dimensions"
        if pixel_size is None:
            assert center_on is None, "Ambiguous parameters: both `center_on`"\
                                      " and `xdata`/`ydata` were specified"
            xmin, xmax = xdata
            ymin, ymax = ydata
        else:
            xmin, xmax, ymin, ymax = self.compute_bounds(data, pixel_size,
                                                         center_on)
        self.__set_image_param(param, title, alpha_mask, alpha, interpolation,
                               background=background_color,
                               colormap=colormap,
                               xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                               show_mask=show_mask, fill_value=fill_value,
                               xformat=xformat, yformat=yformat,
                               zformat=zformat)
        image = MaskedImageNan(data, mask, param)
        image.set_filename(filename)
        if eliminate_outliers is not None:
            image.set_lut_range(lut_range_threshold(image, 256,
                                                    eliminate_outliers))
        return image


    def pmaskederror(self, x, y, dx, dy, curveparam, errorbarparam,
               xaxis="bottom", yaxis="left"):
        """
        Make an errorbar curve `plot item`
        based on a `guiqwt.styles.ErrorBarParam` instance
        (:py:class:`guiqwt.curve.ErrorBarCurveItem` object)

            * x: 1D NumPy array
            * y: 1D NumPy array
            * dx: None, or scalar, or 1D NumPy array
            * dy: None, or scalar, or 1D NumPy array
            * curveparam: `guiqwt.styles.CurveParam` object
            * errorbarparam: `guiqwt.styles.ErrorBarParam` object
            * xaxis, yaxis: X/Y axes bound to curve

        Usage::

            perror(x, y, dx, dy, curveparam, errorbarparam)
        """
        curve = MaskedErrorBarCurveItem(curveparam, errorbarparam)
        curve.set_data(x, y, dx, dy)
        curve.update_params()
        self.__set_curve_axes(curve, xaxis, yaxis)
        return curve

    def maskederror(self, x, y, dx, dy, title="",
              color=None, linestyle=None, linewidth=None,
              errorbarwidth=None, errorbarcap=None, errorbarmode=None,
              errorbaralpha=None, marker=None, markersize=None,
              markerfacecolor=None, markeredgecolor=None, shade=None,
              curvestyle=None, baseline=None, xaxis="bottom", yaxis="left"):
        """
        Make an errorbar curve `plot item`
        (:py:class:`guiqwt.curve.ErrorBarCurveItem` object)

            * x: 1D NumPy array
            * y: 1D NumPy array
            * dx: None, or scalar, or 1D NumPy array
            * dy: None, or scalar, or 1D NumPy array
            * color: curve color name
            * linestyle: curve line style (MATLAB-like string or attribute name
              from the :py:class:`PyQt4.QtCore.Qt.PenStyle` enum
              (i.e. "SolidLine" "DashLine", "DotLine", "DashDotLine",
              "DashDotDotLine" or "NoPen")
            * linewidth: line width (pixels)
            * marker: marker shape (MATLAB-like string or attribute name from
              the :py:class:`PyQt4.Qwt5.QwtSymbol.Style` enum (i.e. "Cross",
              "Ellipse", "Star1", "XCross", "Rect", "Diamond", "UTriangle",
              "DTriangle", "RTriangle", "LTriangle", "Star2" or "NoSymbol")
            * markersize: marker size (pixels)
            * markerfacecolor: marker face color name
            * markeredgecolor: marker edge color name
            * shade: 0 <= float <= 1 (curve shade)
            * curvestyle: attribute name from the
              :py:class:`PyQt4.Qwt5.QwtPlotCurve.CurveStyle` enum
              (i.e. "Lines", "Sticks", "Steps", "Dots" or "NoCurve")
            * baseline (float: default=0.0): the baseline is needed for filling
              the curve with a brush or the Sticks drawing style.
            * xaxis, yaxis: X/Y axes bound to curve

        Example::

            error(x, y, None, dy, marker='Ellipse', markerfacecolor='#ffffff')

        which is equivalent to (MATLAB-style support)::

            error(x, y, None, dy, marker='o', markerfacecolor='w')
        """
        basename = _("Curve")
        curveparam = CurveParam(title=basename, icon='curve.png')
        errorbarparam = ErrorBarParam(title=_("Error bars"),
                                      icon='errorbar.png')
        if not title:
            global CURVE_COUNT
            CURVE_COUNT += 1
            curveparam.label = make_title(basename, CURVE_COUNT)
        self.__set_param(curveparam, title, color, linestyle, linewidth, marker,
                         markersize, markerfacecolor, markeredgecolor,
                         shade, curvestyle, baseline)
        errorbarparam.color = curveparam.line.color
        if errorbarwidth is not None:
            errorbarparam.width = errorbarwidth
        if errorbarcap is not None:
            errorbarparam.cap = errorbarcap
        if errorbarmode is not None:
            errorbarparam.mode = errorbarmode
        if errorbaralpha is not None:
            errorbarparam.alpha = errorbaralpha
        return self.pmaskederror(x, y, dx, dy, curveparam, errorbarparam,
                           xaxis, yaxis)

make=PlotItemBuilder()








def test():
  #dessine une table de parametres
  from qtpy.QtWidgets import QApplication
  from guiqwt.plot import ImageDialog
  import sys
  app=0
  app = QApplication(sys.argv)
  dialog=ImageDialog(toolbar=True, options={'lock_aspect_ratio':False, 'yreverse':False})
  shape=CurvePeakShape(200,200,200,400)
  plot=dialog.get_active_plot()
  plot.add_item(shape)
  dialog.exec_()

if __name__ == '__main__':
  test()
