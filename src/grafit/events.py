"""
Created on Sat Jun 06 10:00:03 2020

@author: Prevot
"""
import numpy as np
from qtpy.QtCore import Qt,QPoint,Signal,QPointF

from guiqwt.events import DragHandler,RectangularSelectionHandler,KeyEventMatch
from guiqwt.baseplot import canvas_to_axes

class MoveHandler(DragHandler):

    #: Signal emitted by MoveHandler on mouse click
    SIG_START_TRACKING = Signal("PyQt_PyObject", "QEvent")

    #: Signal emitted by MoveHandler on mouse release
    SIG_STOP_TRACKING = Signal("PyQt_PyObject", "QEvent")

    #: Signal emitted by MoveHandler on mouse move
    SIG_MOVE = Signal("PyQt_PyObject", "QEvent")

    #: Signal emitted by MoveHandler on mouse release after move
    SIG_STOP_MOVING = Signal("PyQt_PyObject", "QEvent")


    def __init__(self, filter, btn, mods=Qt.NoModifier, start_state=0):
        super().__init__(filter, btn, mods,
                                                          start_state)
        self.avoid_null_shape = False

    def set_shape(self, shape, setup_shape_cb=None, avoid_null_shape=False):
        #called at initialization of the tool
        self.shape = shape
        #print "MoveHandler.set_shape",self.shape
        self.setup_shape_cb = setup_shape_cb
        self.avoid_null_shape = avoid_null_shape

    def start_tracking(self, filter, event):
        self.shape.attach(filter.plot)
        self.shape.setZ(filter.plot.get_max_z()+1)
        if self.avoid_null_shape:
            self.start -= QPoint(1, 1)
        pt=event.pos()
        self.shape.set_local_center(pt)
        if self.setup_shape_cb is not None:
            self.setup_shape_cb(self.shape)
        self.shape.show()
        filter.plot.replot()
        self.SIG_START_TRACKING.emit(filter, event)

    def stop_tracking(self, filter, event):  #when the mouse press button is released before moving
        self.shape.detach()
        self.SIG_STOP_TRACKING.emit(filter, event)
        filter.plot.replot()

    def start_moving(self, filter, event):
        pass

    def move(self, filter, event):
        pt=event.pos()
        self.shape.set_local_center(pt)
        self.SIG_MOVE.emit(filter, event)
        filter.plot.replot()

    def stop_moving(self, filter, event): #when the mouse press button is released after moving
        self.SIG_STOP_MOVING.emit(filter, event)
        self.shape.detach()
        filter.plot.replot()


class EllipseSelectionHandlerCXY(RectangularSelectionHandler):

    #: Signal emitted by RectangularSelectionHandler when ending selection
    SIG_END_RECT = Signal("PyQt_PyObject", "QPointF", "QPointF")
    SIG_KEY_PRESSED_EVENT = Signal("PyQt_PyObject", int)


    def __init__(self, filter, btn, mods=Qt.NoModifier, start_state=0):
        super().__init__(filter, btn, mods,start_state)
        filter.add_event(start_state, KeyEventMatch((42,43,45,47)),
                         self.key_press, start_state)

    def move(self, filter, event):
        """methode surchargee par la classe """
        x1,y1=canvas_to_axes(self.shape, event.pos())
        x0,y0=canvas_to_axes(self.shape, self.start)
        dx=x1-x0
        dy=y1-y0
        self.shape.points=np.array([[x0-dx,y0],[x0+dx,y0],[x0,y0-dy],[x0,y0+dy]])
        self.move_action(filter, event)
        filter.plot.replot()

    def key_press(self,filter,event):
        self.SIG_KEY_PRESSED_EVENT.emit(filter, event.key())

    def set_shape(self, shape, h0, h1, h2, h3,
                  setup_shape_cb=None, avoid_null_shape=False):
        self.shape = shape
        self.shape_h0 = h0
        self.shape_h1 = h1
        self.shape_h2 = h2
        self.shape_h3 = h3
        self.setup_shape_cb = setup_shape_cb
        self.avoid_null_shape = avoid_null_shape

class CurvePeakSelectionHandler(RectangularSelectionHandler):
    #: Signal emitted by RectangularSelectionHandler when ending selection
    SIG_END_RECT = Signal("PyQt_PyObject", "QPointF", "QPointF")
    SIG_KEY_PRESSED_EVENT = Signal("PyQt_PyObject", int)


    def __init__(self, filter, btn, mods=Qt.NoModifier, start_state=0):
        super().__init__(filter, btn, mods,start_state)

    def start_moving(self, filter, event):
        self.shape.attach(filter.plot)
        self.shape.setZ(filter.plot.get_max_z()+1)
        if self.avoid_null_shape:
            self.start -= QPointF(1, 1)

        #self.shape.move_local_point_to(self.shape_h0, self.start)
        #self.shape.move_local_point_to(self.shape_h1, event.pos())
        #self.start_moving_action(filter, event)
        x1,y0=canvas_to_axes(self.shape, event.pos())
        x0,y1=canvas_to_axes(self.shape, self.start)
        h = y1 - y0
        w = x1 - x0
        self.shape.set_rect(x0,y0,h,w)

        if self.setup_shape_cb is not None:
            self.setup_shape_cb(self.shape)
        self.shape.show()
        filter.plot.replot()

    def move(self, filter, event):
        """methode surchargee par la classe """
        "start from peak position then goes to the bottom"

        x1,y0=canvas_to_axes(self.shape, event.pos())
        x0,y1=canvas_to_axes(self.shape, self.start)
        h = y1 - y0
        w = x1 - x0
        self.shape.set_rect(x0,y0,h,w)
        filter.plot.replot()

    def set_shape(self, shape, h0, h1, h2, h3, h4,
                  setup_shape_cb=None, avoid_null_shape=False):
        self.shape = shape
        self.shape_h0 = h0
        self.shape_h1 = h1
        self.shape_h2 = h2
        self.shape_h3 = h3
        self.shape_h4 = h4
        self.setup_shape_cb = setup_shape_cb
        self.avoid_null_shape = avoid_null_shape


class RectangularSelectionHandlerCX(RectangularSelectionHandler):
    #on utilise la classe heritee dont on surcharge la methode move

    def move(self, filter, event):
        """methode surchargee par la classe """
        sympos=QPoint(2*self.start.x()-event.pos().x(),self.start.y())
        self.shape.move_local_point_to(self.shape_h0, sympos)
        self.shape.move_local_point_to(self.shape_h1, event.pos())
        self.move_action(filter, event)
        filter.plot.replot()

class RectangularSelectionHandlerCXY(RectangularSelectionHandler):
    #on utilise la classe heritee dont on surcharge la methode move

    def move(self, filter, event):
        """methode surchargee par la classe """
        sympos=QPoint(2*self.start.x()-event.pos().x(),2*self.start.y()-event.pos().y())

        self.shape.move_local_point_to(self.shape_h0, sympos)
        self.shape.move_local_point_to(self.shape_h1, event.pos())
        self.move_action(filter, event)
        filter.plot.replot()
