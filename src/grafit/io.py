"""
Created on Wed Jun 17 16:50:26 2020

@author: prevot
"""
import re


from qtpy.QtCore import (QRect, QSize, Qt, QRegExp)
from qtpy.QtWidgets import (QDialog, QTextEdit, QLabel, QHBoxLayout, QVBoxLayout, QGridLayout, QSpinBox, QCheckBox,
                              QComboBox, QRadioButton, QDialogButtonBox, QPushButton,QButtonGroup)
from qtpy.QtGui import  QSyntaxHighlighter,QTextCharFormat,QTextCursor


from guidata.hdf5io import HDF5Reader, HDF5Writer

from guiqwt.io import item_class_from_name,register_serializable_items,item_name_from_object
from grafit.models import list_of_1D_models, list_of_2D_models
register_serializable_items('grafit.datatypes',
                            ['Raw1DData','Raw2DData'])


PATTERNS={'space':' ','tab':'\\t','comma:':','}

def serialize_1D_items(items,fname):
    writer = HDF5Writer(fname)

    with writer.group('header'):  #top level
        writer.write_str('grafit_1D')

    """Save items to HDF5 file:
        * writer: :py:class:`guidata.hdf5io.HDF5Writer` object
        * items: rawdata 1D items"""
    counts = {}
    #we temporary associate a name to each item that will be stored
    item_dict = {}  #key=item, value=name

    def _get_name(item):
        basename = item_name_from_object(item)
        count = counts[basename] = counts.setdefault(basename, 0) + 1    # returns 1 if the key value is not available in the dictionary
        name = '%s_%03d' % (basename, count)
        item_dict[item] = name
        return name

    #serialize first items
    for item in items:
        with writer.group(_get_name(item)):
            item.serialize(writer)

    with writer.group('rawdata list'):   #top level
        writer.write_sequence([item_dict[item] for item in items])

    writer.close()

def deserialize_1D_items(fname):
    reader = HDF5Reader(fname)

    with reader.group('header'):
       mod = reader.read_str()
       if not mod == 'grafit_1D':
           print ('header:',mod)
           return []

    item_dict = {}  #key=name, value=item

    #loading items
    with reader.group('rawdata list'):
        item_names = reader.read_sequence()
    items = []

    for item_name in item_names:
        klass_name = re.match(r'([A-Z]+[A-Za-z0-9\_]*)\_([0-9]*)',
                              item_name).groups()[0]
        klass = item_class_from_name(klass_name)
        item = klass()
        with reader.group(item_name):
            item.deserialize(reader)
        items.append(item)
        item_dict[item_name] = item

    return items

def serialize_2D_items(items,fname):
    writer = HDF5Writer(fname)

    with writer.group('header'):  #top level
        writer.write_str('grafit_2D')

    """Save items to HDF5 file:
        * writer: :py:class:`guidata.hdf5io.HDF5Writer` object
        * items: rawdata 2D items"""
    counts = {}
    #we temporary associate a name to each item that will be stored
    item_dict = {}  #key=item, value=name

    def _get_name(item):
        basename = item_name_from_object(item)
        count = counts[basename] = counts.setdefault(basename, 0) + 1    # returns 1 if the key value is not available in the dictionary
        name = '%s_%03d' % (basename, count)
        item_dict[item] = name
        return name

    #serialize first items
    for item in items:
        with writer.group(_get_name(item)):
            item.serialize(writer)

    with writer.group('rawdata list'):   #top level
        writer.write_sequence([item_dict[item] for item in items])

    writer.close()

def deserialize_2D_items(fname):
    reader = HDF5Reader(fname)

    with reader.group('header'):
       mod = reader.read_str()
       if not mod == 'grafit_2D':
           print ('header:',mod)
           return []

    item_dict = {}  #key=name, value=item

    #loading items
    with reader.group('rawdata list'):
        item_names = reader.read_sequence()
    items = []

    for item_name in item_names:
        klass_name = re.match(r'([A-Z]+[A-Za-z0-9\_]*)\_([0-9]*)',
                              item_name).groups()[0]
        klass = item_class_from_name(klass_name)
        item = klass()
        with reader.group(item_name):
            item.deserialize(reader)
        items.append(item)
        item_dict[item_name] = item

    return items


class Highlighter(QSyntaxHighlighter):

    def __init__(self, parent, pattern):
        super().__init__(parent)
        self.highlightFormat = QTextCharFormat()
        self.highlightFormat.setForeground(Qt.blue)
        self.highlightFormat.setBackground(Qt.green)
        self.setpattern(pattern)
        self.eof=QRegExp('\\n\\r')

    def setpattern(self,pattern):
        self.expression = QRegExp(PATTERNS[pattern])

    def highlightBlock(self, text):
        index = self.expression.indexIn(text)
        while index >= 0:
            length = self.expression.matchedLength()
            self.setFormat( index, length, self.highlightFormat)
            index = self.expression.indexIn(text, index + length )




class readfileparams():
    #definit une classe pour la lecture des parametres d'un fichier a lire
    def __init__(self):
        self.heading=0   #taille de l'entete
        self.title=True  #ligne de titre
        self.delimiter='space' #separateur de nombre par defaut
        self.pattern=' '
         #self.xs=[]  #indices x column to draw
        #self.ys=[]  #indices y column to draw
        self.labels = [] #list of labels
        self.ignore = True

class readfilewindow(QDialog):
    def __init__(self,params,ficlines):
        self.params = params
        self.ficlines = ficlines
        super().__init__()  #permet l'initialisation de la fenetre sans perdre les fonctions associees
        self.setWindowTitle("Parameters for reading file")
        self.setMinimumSize(QSize(410, 320))

        #layout = QHBoxLayout(self)
        vlayout = QVBoxLayout(self)

        #layout.addLayout(vlayout)

        #checkboxes for import
        #self.xCheckBoxes = list()
        #self.yCheckBoxes = list()
        #self.selectionGrid = QGridLayout(self)
        #self.selectionGrid.addWidget(QLabel('x'),0,0)
        #self.selectionGrid.addWidget(QLabel('y'),0,1)

        #layout.addLayout(self.selectionGrid)
        #self.setLayout(layout)

        self.display=QTextEdit(self)
        self.display.setReadOnly(True)
        self.display.setLineWrapMode(0)
        self.highlighter = Highlighter(self.display.document(),self.params.delimiter)
        self.cursor=QTextCursor(self.display.document())

        vlayout.addWidget(self.display)


        hlayout0=QHBoxLayout(self)
        hlayout0.addWidget(QLabel("heading",self))

        self.heading = QSpinBox(self)
        self.heading.setMaximum(len(ficlines))
        self.heading.setValue(self.params.heading)
        hlayout0.addWidget(self.heading)
        vlayout.addLayout(hlayout0)

        self.title = QRadioButton("title",self)
        vlayout.addWidget(self.title)
        self.title.setChecked(params.title)

        hlayout1=QHBoxLayout(self)
        hlayout1.addWidget(QLabel("delimiter",self))
        self.delimiter = QComboBox(self)
        for delimiter in PATTERNS:
            self.delimiter.addItem(delimiter)
        self.delimiter.setCurrentText(params.delimiter)
        hlayout1.addWidget(self.delimiter)
        vlayout.addLayout(hlayout1)

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.heading.valueChanged.connect(self.set_heading)
        self.title.toggled.connect(self.set_title)
        self.delimiter.currentTextChanged.connect(self.set_delimiter)

        self.buttonBox.accepted.connect(self.apply)
        self.buttonBox.rejected.connect(self.closewin)

        vlayout.addWidget(self.buttonBox)

        self.selection = QGridLayout(self)
        self.set_text()



    def refresh_boxes(self,labels):
        #on met à jour la liste des cases a cocher
        n = len(labels)
        nl = self.selectionGrid.rowCount()-1  #+1 with labels!
        if n>nl:
            for i in range(nl,n):
                cbox = QCheckBox(self)
                self.selectionGrid.addWidget(cbox,i+1,0)
                self.xCheckBoxes.append(cbox)

                cbox = QCheckBox(self)
                self.selectionGrid.addWidget(cbox,i+1,1)
                self.yCheckBoxes.append(cbox)
        for i in range(n):
            self.xCheckBoxes[i].show()
            self.xCheckBoxes[i].setText(labels[i])
            self.yCheckBoxes[i].show()

        for i in range(n,nl):
            self.xCheckBoxes[i].hide()
            self.yCheckBoxes[i].hide()

    def set_heading(self,value):
        self.params.heading = value
        self.set_text()

    def set_title(self,do):
        self.params.title = self.title.isChecked()
        self.set_text()

    def set_delimiter(self,text):
        self.params.delimiter = text
        self.set_text()

    def set_text(self):
        if self.params.title:
            n0=1
        else:
            n0=0
        value=self.params.heading
        n1=min(value+n0,len(self.ficlines)-1)
        n2 =min(value+n0+20,len(self.ficlines))

        self.highlighter.setpattern(self.params.delimiter)
        self.text=""
        for i in range (n1,n2):
            self.text = self.text+self.ficlines[i]

        self.display.setText(self.text)
        if self.params.title:
            f=QTextCharFormat()

            f.setForeground(Qt.blue)
            f.setBackground(Qt.red)
            self.cursor.setPosition(0)
            self.cursor.insertText(self.ficlines[value], f)
            self.labels=self.ficlines[value].split(PATTERNS[self.params.delimiter])
        else:
            nlab = len(self.ficlines[value].split(PATTERNS[self.params.delimiter]))
            self.labels = list('col_%d'%i for i in range(nlab))
        #self.refresh_boxes(self.labels)

    def apply (self):
        #try:
        self.params.heading = self.heading.value()
        self.params.title = self.title.isChecked()
        self.params.delimiter  = self.delimiter.currentText()
        self.params.pattern=PATTERNS[self.params.delimiter]
        self.params.labels = self.labels
        self.accept()
        self.close()

    def closewin (self):
        self.reject()
        self.close()


class ParamReader():
    #a class of reader for .par file that reads fitting parameters

    def __init__(self,filename,extendedparams,dimension=2):
        self.extendedparams = extendedparams
        self.dimension = dimension
        self.filename = filename
        self.modelclasses={}
        if self.dimension == 2:
            for modelclass in list_of_2D_models:
                self.modelclasses[modelclass.NAME] = modelclass
        elif self.dimension == 1:
            for modelclass in list_of_1D_models:
                self.modelclasses[modelclass.NAME] = modelclass
        self.methods = []
        from grafit.models import minimizationmethods
        for (meth,methparam) in minimizationmethods:
            self.methods.append(meth)  #list of all methods

    def open_and_read(self):
        try:
            self.fic=open(self.filename)
        except:
            return  'Warning','Unable to open file'
        doit=True

        info1='Ok'
        info2='Model successfully imported'

        self.lig=self.fic.readline()
        #read the first comment line
        if self.lig[:17]!='grafit program %dD'%self.dimension:
            print ('error while reading line 0: ',self.lig)
            info1='Error'
            info2='File is not a grafit parameter file with correct dimension'
            doit = False

        while doit:
            try:
                self.lig=self.fic.readline()   #read next line
                ll=self.lig.split()

                if len(ll)==0:  #empty line
                    doit = False
                elif ll[0] == '_Models':
                    self.read_models(int(ll[1]))
                elif ll[0] == '_User_parameters':
                    self.read_user_params(int(ll[1]))
                elif ll[0] == '_Parameters':
                    self.read_parameters(int(ll[1]))
                elif ll[0] == '_Method':
                    self.read_method()
                elif ll[0] == '_Method_parameters':
                    self.read_method_parameters(int(ll[1]))
                elif ll[0] == '_Chi2':
                    self.read_chi2()
                else:
                    print ('unable to understand keyword: ',self.lig)
                    info1='Warning'
                    info2='Unable to understand keyword: '+self.lig
                    doit = False
            except Exception as error:
                 print (error)
                 print ('error while reading line: ',self.lig)
                 info1='Warning'
                 info2='Error while reading line: '+self.lig
                 doit = False

        self.fic.close()
        return info1,info2

    def read_models(self,n):
        self.extendedparams.remove_all_model_classes()
        for i in range(n):
            self.lig=self.fic.readline()
            modelname = self.lig[:-1]#remove \n
            self.extendedparams.add_model_class(self.modelclasses[modelname])

    def read_user_params(self,n):
        for i in range(n):
            self.lig=self.fic.readline().split()
            self.extendedparams.add_user_param(self.lig[0])

    def read_parameters(self,n):
        self.lig=self.fic.readline()  #comment line
        for i in range(n):
            self.lig=self.fic.readline()
            ll=self.lig.split()
            nv=len(ll)
            name=ll[0]

            if name in self.extendedparams.partry:
                param = self.extendedparams.partry[name]
                param.set(value=float(ll[1]))
                if nv>3:
                    if ll[3] == 'False':
                        param.set(vary=False)
                    else:
                        param.set(vary=True)
                    if nv>4:
                        param.set(min=float(ll[4]))
                        if nv>5:
                            param.set(max=float(ll[5]))
                            if nv>6:
                                if ll[6] != 'None':
                                    param.set(expr=ll[6])
            else:
               print ("parameter ",name," not found")

    def read_method(self):
        self.lig=self.fic.readline()
        method=self.lig[:-1]   #remove \n

        if method not in self.methods:
            raise KeyError

        self.extendedparams.method=method

    def read_method_parameters(self,n):
        #not implemented yet!
        for i in range(n):
            self.lig=self.fic.readline()

    def read_chi2(self):
        #not implemented yet!
        self.lig=self.fic.readline()
        self.lig=self.fic.readline()

def write_param(filename,extendedparams,dimension=2):
    fic=open(filename,'w')
    fic.write('grafit program %dD version 1.0\n'%dimension)

    fic.write('_Models %d\n'%len(extendedparams.modelclasses))
    for modelclass in extendedparams.modelclasses:
        fic.write(modelclass.NAME)
        fic.write('\n')

    fic.write('_User_parameters %d\n'%len(extendedparams.tags))
    for param in extendedparams.tags:
        fic.write(param)
        fic.write('\n')

    if extendedparams.isfitted:
        params = extendedparams.paropt
    else:
        params = extendedparams.partry

    fic.write('_Parameters %d\n'%len(params))
    fic.write('name value err restrain min max expr\n')

    for name in params:
        fic.write(name)
        param = params[name]

        f=' %f '%param.value

        if param.stderr is not None:
            s=' %f'%param.stderr
        else:
            s=' None'
        if param.expr not in ["",None]:
            r=' Expr'
        elif param.vary:
            r=' Free'
        else:
            r=' Fixed'

        fic.write(f+s+r)

        fic.write(' %f %f %s\n' %(param.min,param.max,param.expr))

    fic.write('_Method\n')
    fic.write(extendedparams.method)
    fic.write('\n')

    if extendedparams.isfitted:
        fic.write('_Chi2\n')
        fic.write('chi2opt\n')
        fic.write('%f\n'%(extendedparams.chi2opt))

    fic.close()


if __name__ == "__main__":

    from guidata import qapplication
    _app = qapplication()

    fic=open('compilation.txt')
    ficlines= fic.readlines()
    params=readfileparams()
    readfilewindow(params,ficlines)


    _app.exec_()
