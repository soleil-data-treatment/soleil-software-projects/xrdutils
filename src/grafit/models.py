"""
Created on Fri Feb 21 17:02:23 2020

@author: Prevot
"""
from collections import OrderedDict
from copy import deepcopy
import numpy as np
from scipy.integrate import dblquad
from scipy.special import erf
from scipy.optimize import curve_fit,OptimizeWarning
from lmfit import  lineshapes
from lmfit import  Model as LmfitModel
from lmfit.models import COMMON_INIT_DOC,COMMON_GUESS_DOC,update_param_vals
import operator
from grafit.calculation import guess_2D_peak
import warnings

minimizationmethods=[('leastsq','Levenberg-Marquardt'),
                     ('least_squares','Least-Squares'),
                     ('differential_evolution','Differential evolution'),
                     ('brute','Brute force'),
                     ('nelder','Nelder-Mead'),
                     ('lbfgsb','L-BFGS-B'),
                     ('powell','Powell'),
                     #('cg','Conjugate-Gradient'),   :Jacobian is required
                     #('newton','Newton-Congugate-Gradient'), Jacobian is required
                     ('cobyla','Cobyla'),
                     #('tnc','Truncate Newton'),Jacobian is required
                     #('trust-ncg','Trust Newton-Congugate-Gradient'),Jacobian is required
                     #('dogleg','Dogleg'),Jacobian is required
                     ('basinhopping','Basin-hopping'),
                     ('slsqp','Sequential Linear Squares Programming')]



wg = 0.600561204393225
dpi = 2.*np.pi
wgpi = 1.064467019431226
erf1 = erf(1)

"""*****************************************************************************************************"""
def linear1D(x,bg0=0.,bg1=0.):
    return (bg0+x*bg1)

"""*****************************************************************************************************"""
def quadratic1D(x,bg0=0.,bg1=0.,bg2=0.):
    return (bg2*x+bg1)*x+bg0

"""*****************************************************************************************************"""
def cubic1D(x,bg0=0.,bg1=0.,bg2=0.,bg3=0.):
    return ((bg3*x+bg2)*x+bg1)*x+bg0

"""*****************************************************************************************************"""
def powerlaw1D(x, amp=1., power=1., loc=0.):
    """Return the powerlaw function.

    x -> amplitude * x**exponent

    """
    if round(power) == power:
        return amp * (x-loc)**power
    else:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", RuntimeWarning)
            return amp * np.absolute(x-loc)**power

"""*****************************************************************************************************"""
def exponential1D(x,amp=1.,tau=1.):
    return amp*np.exp(-x/tau)

"""*****************************************************************************************************"""
def lorentzian1D(x, amp=1., loc=0., fwhm=1.):
    return amp/(1.+((x-loc)/0.5/fwhm)**2)

def lorentzianint(amp=1., loc=0., fwhm=1.):
    #return integral and associated integration error
    return (amp*fwhm*np.pi/2.,0.)

"""*****************************************************************************************************"""
def gaussian1D(x, amp=1., loc=0., fwhm=1.):
    return amp*np.exp(-((x-loc)/wg/fwhm)**2)

def gaussianint(amp=1., loc=0., fwhm=1.):
    #return integral and associated integration error
    return (amp*fwhm*wgpi,0.)
"""*****************************************************************************************************"""
def pseudovoigt1D(x, amp=1., loc=0., fwhm=1., eta=0.5):
    return amp*((1.-eta)*np.exp(-((x-loc)/wg/fwhm)**2)+eta/(1.+((x-loc)/0.5/fwhm)**2))

def pseudovoigtint(amp=1., loc=0., fwhm=1., eta=0.5):
    #return integral and associated integration error
    return (amp*fwhm*((1.-eta)*wgpi+eta*np.pi/2.),0.)

def step1D(x, amp=1., loc=0., slope=1.):
    return amp*(erf((x-loc)/slope))

def door1D(x, amp=1., loc=0., fwhm=1., slope=1.):
    return amp/2.*(erf((x-loc+fwhm/2.)/slope)-erf((x-loc-fwhm/2.)/slope))

def door1Dint(x, amp=1., loc=0., fwhm=1., slope=1.):
    return (amp*fwhm,0.)

def sinus1D(x, amp=1., loc=0., freq=1.):
    return amp*np.sin((x-loc)*dpi*freq)

"""*****************************************************************************************************"""
def planar2D(x, bg=0., slope_x=0.,slope_y=0.):
    return (bg+x[0]*slope_x+x[1]*slope_y)

"""*****************************************************************************************************"""
def quadratic2D(x, bg=0., slope_x=0.,slope_y=0., slope_x2=0.,slope_xy=0.,slope_y2=0.):
    return (bg+x[0]*(slope_x2*x[0]+slope_x+slope_xy*x[1])+x[1]*(slope_y2*x[1]+slope_y))

"""*****************************************************************************************************"""
def lorentzianpower2D(x, amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0., power=2.):
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x[0]-loc0
    x1=x[1]-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    if power == 2.:
        return (amp/(1.+((x0/width0)**2+((x1/width1)**2))))
    else:
        return (amp/(1.+((x0/width0)**2+((x1/width1)**2))**(power/2.)))

def lorentzianpower(y, x, amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0., power=2.):
    #same with x y coordinates for integration
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x-loc0
    x1=y-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    if power == 2.:
        return (amp/(1.+((x0/width0)**2+((x1/width1)**2))))
    else:
        return (amp/(1.+((x0/width0)**2+((x1/width1)**2))**(power/2.)))

def lorentzianpowerint(amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0., power=2.):
    return dblquad(lambda y, x: lorentzianpower(y,x,amp,loc0,loc1,width0,width1,th,power), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)

"""*****************************************************************************************************"""
def lorentziangauss2D(x, amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x[0]-loc0
    x1=x[1]-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    return (amp*np.exp(-(x1/width1)**2)/(1.+((x0/width0)**2)))

def lorentziangaussint(amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    return  (amp*np.pi*np.sqrt(np.pi)*width0*width1,0.)

"""*****************************************************************************************************"""
def gaussgauss2D(x, amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x[0]-loc0
    x1=x[1]-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    return (amp*np.exp(-((x1/width1)**2+(x0/width0)**2)))

def gaussgaussint(amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    return  (amp*np.pi*width0*width1,0.)

"""*****************************************************************************************************"""

def doublelorentziangauss2D(x, amp=1., loc0=0., loc1=0., width0=1., width1=1., width2=1., width3=1., eta=0.5, th=0.):
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x[0]-loc0
    x1=x[1]-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    return (amp*(eta*np.exp(-(x1/width1)**2)/(1.+((x0/width0)**2))+
                 (1.-eta)*np.exp(-(x0/width3)**2)/(1.+((x1/width2)**2))))

def doublelorentziangaussint(amp=1., loc0=0., loc1=0., width0=1., width1=1., width2=1., width3=1., eta=0.5, th=0.):
    return  (amp*np.pi*np.sqrt(np.pi)*(eta*width0*width1+(1.-eta)*width2*width3),0.)







"""*****************************************************************************************************"""
def lorentzianlorentzian2D(x, amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x[0]-loc0
    x1=x[1]-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    return (amp/(1.+((x1/width1)**2))/(1.+((x0/width0)**2)))

def lorentzianlorentzianint(amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    return  (amp*np.pi**2*width0*width1,0.)


"""*****************************************************************************************************"""
def lorentzianpoly2D(x, amp=1., loc0=0., loc1=0., width0=1., width1=1., eta0=1., eta1=1., th=0.):
    #eta is the ratio of lorentzian component, if eta=0, it is lorentzian, if eta=1 it is like 1/(1+x**4)
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x[0]-loc0
    x1=x[1]-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    u0=(x0/width0)**2
    u1=(x1/width1)**2
    return (amp/(1.+(1.-eta0+eta0*u0)*u0+(1.-eta1+eta1*u1)*u1))

def lorentzianpoly(y, x, amp=1., loc0=0., loc1=0., width0=1., width1=1., eta0=1., eta1=1., th=0.):
    #same with x y coordinates for integration
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x-loc0
    y0=y-loc1
    x0,y0=x0*cth+y0*sth,-x0*sth+y0*cth  #rotation
    u0=(x0/width0)**2
    u1=(y0/width1)**2
    return (amp/(1.+(1.-eta0+eta0*u0)*u0+(1.-eta1+eta1*u1)*u1))

def lorentzianpolyint(amp=1., loc0=0., loc1=0., width0=1., width1=1., eta0=1., eta1=1., th=0.):
    return dblquad(lambda y, x: lorentzianpoly(y,x,amp,loc0,loc1,width0,width1,eta0,eta1,th), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)
"""*****************************************************************************************************"""
def lorentziandoor2D(x, amp=1., loc0=0., loc1=0., width0=1., width1=1., eta=1., th=0.):
    cth=np.cos(th)
    sth=np.sin(th)
    x0=x[0]-loc0
    x1=x[1]-loc1
    x0,x1=x0*cth+x1*sth,-x0*sth+x1*cth  #rotation
    return (amp/(1.+((x0/width0)**2))*(erf((x1+width1/2.)/(eta*width1))-erf((x1-width1/2.)/(eta*width1)))/2.)

def lorentziandoorint(amp=1., loc0=0., loc1=0., width0=1., width1=1., eta=1., th=0.):
    return  (amp*np.pi*width0*width1,0.)


"""*****************************************************************************************************"""

def nointegration(*args, **kwargs):
    return (0.,0.)


#we jsut add the possibility to compute integrals for modelss
class Model(LmfitModel):
    def __init__(self, func, independent_vars=None, param_names=None,
                 nan_policy='raise', missing=None, prefix='', name=None, **kws):
        self.intfunc = nointegration
        super().__init__(func=func, independent_vars=independent_vars, param_names=param_names,
                 nan_policy=nan_policy, missing=missing, prefix=prefix, name=name, **kws)


    def set_points(self, points, params, handle = 0):
        """"set the curve anchor points from a set of params"""
        x = points[:,0]
        points[:,1] = self.eval(params, x=x)

    def get_curve_points_len(self):
        #return the number of points in the curve used for setting parameters
        return len(self.param_names)

    def set_from_tool(self, points, params):
        #to be implemented for specific models
        pass

    def guess_from_curve(self, points, params, handle = 0):
        #for 1D model, use the values y=points[:,1] at x=points[:,0] to fit  y=self.func(x)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", OptimizeWarning)
            try:
                popt, pcov = curve_fit(self.func, points[:,0], points[:,1])
            except :
                return

        for i,name in enumerate(self.param_names):
            params[name].value = popt[i]
        params.update_constraints()


    def __add__(self, other):
        """+"""
        return CompositeModel(self, other, operator.add)

    def __sub__(self, other):
        """-"""
        return CompositeModel(self, other, operator.sub)

    def __mul__(self, other):
        """*"""
        return CompositeModel(self, other, operator.mul)

    def __div__(self, other):
        """/"""
        return CompositeModel(self, other, operator.truediv)

    def __truediv__(self, other):
        """/"""
        return CompositeModel(self, other, operator.truediv)

    def eval_integral(self, params=None, **kwargs):
        #evaluate the integral of the peak, and associated scipy.integrate error (not the fit uncertainty)
        #needs to define self.intfunc first!
        return (self.intfunc(**self.make_funcargs(params, kwargs)))

    def eval_component_integrals(self, params=None, **kwargs):
        """Evaluate the model integrals with the supplied parameters.

        Parameters
        -----------
        params : Parameters, optional
            Parameters to use in Model.
        **kwargs : optional
            Additional keyword arguments to pass to model function.

        Returns
        -------
        OrderedDict
            Keys are prefixes for component model, values are value of each
            component.

        """
        key = self._prefix
        if len(key) < 1:
            key = self._name
        return {key: self.eval_integral(params=params, **kwargs)}    #will be used to update the dictionnary of a composite model

    def get_reprstring(self, params=None, *kwargs):
        "kwargs is the list of variable names to be printed"
        out=self._name+'('+kwargs[0]
        for kw in kwargs[1:]:
            out+=','+kw
        out+=')'
        return out

class CompositeModel(Model):
    """Combine two models (`left` and `right`) with a binary operator (`op`)
    into a CompositeModel.

    Normally, one does not have to explicitly create a `CompositeModel`,
    but can use normal Python operators `+`, '-', `*`, and `/` to combine
    components as in::

    >>> mod = Model(fcn1) + Model(fcn2) * Model(fcn3)

    """

    _names_collide = ("\nTwo models have parameters named '{clash}'. "
                      "Use distinct names.")
    _bad_arg = "CompositeModel: argument {arg} is not a Model"
    _bad_op = "CompositeModel: operator {op} is not callable"
    _known_ops = {operator.add: '+', operator.sub: '-',
                  operator.mul: '*', operator.truediv: '/'}

    def __init__(self, left, right, op, **kws):
        """
        Parameters
        ----------
        left : Model
            Left-hand model.
        right : Model
            Right-hand model.
        op : callable binary operator
            Operator to combine `left` and `right` models.
        **kws : optional
            Additional keywords are passed to `Model` when creating this
            new model.

        Notes
        -----
        1. The two models must use the same independent variable.

        """
        if not isinstance(left, Model):
            raise ValueError(self._bad_arg.format(arg=left))
        if not isinstance(right, Model):
            raise ValueError(self._bad_arg.format(arg=right))
        if not callable(op):
            raise ValueError(self._bad_op.format(op=op))

        self.left = left
        self.right = right
        self.op = op

        name_collisions = set(left.param_names) & set(right.param_names)
        if len(name_collisions) > 0:
            msg = ''
            for collision in name_collisions:
                msg += self._names_collide.format(clash=collision)
            raise NameError(msg)

        # we assume that all the sub-models have the same independent vars
        if 'independent_vars' not in kws:
            kws['independent_vars'] = self.left.independent_vars
        if 'nan_policy' not in kws:
            kws['nan_policy'] = self.left.nan_policy

        def _tmp(self, *args, **kws):
            pass
        Model.__init__(self, _tmp, **kws)

        for side in (left, right):
            prefix = side.prefix
            for basename, hint in list(side.param_hints.items()):
                self.param_hints[f"{prefix}{basename}"] = hint

    def _parse_params(self):
        self._func_haskeywords = (self.left._func_haskeywords or
                                  self.right._func_haskeywords)
        self._func_allargs = (self.left._func_allargs +
                              self.right._func_allargs)
        self.def_vals = deepcopy(self.right.def_vals)
        self.def_vals.update(self.left.def_vals)
        self.opts = deepcopy(self.right.opts)
        self.opts.update(self.left.opts)

    def _reprstring(self, long=False):
        return "({} {} {})".format(self.left._reprstring(long=long),
                               self._known_ops.get(self.op, self.op),
                               self.right._reprstring(long=long))

    def get_reprstring(self, params=None, *kwargs):
        return self.left.get_reprstring(params, *kwargs)+self._known_ops.get(self.op, self.op)+"("+self.right.get_reprstring(params, *kwargs)+")"



    def eval(self, params=None, **kwargs):
        """TODO: docstring in public method."""
        return self.op(self.left.eval(params=params, **kwargs),
                       self.right.eval(params=params, **kwargs))

    def eval_components(self, **kwargs):
        """Return OrderedDict of name, results for each component."""
        out = OrderedDict(self.left.eval_components(**kwargs))
        out.update(self.right.eval_components(**kwargs))
        return out

    def eval_component_integrals(self, params=None, **kwargs):
        """Return OrderedDict of name, results for each component."""
        out = OrderedDict(self.left.eval_component_integrals(params,**kwargs))
        out.update(self.right.eval_component_integrals(params,**kwargs))
        return out

    @property
    def param_names(self):
        """Return parameter names for composite model."""
        return self.left.param_names + self.right.param_names

    @property
    def components(self):
        """Return components for composite model."""
        return self.left.components + self.right.components

    def _get_state(self):
        return (self.left._get_state(),
                self.right._get_state(), self.op.__name__)

    def _set_state(self, state, funcdefs=None):
        return _buildmodel(state, funcdefs=funcdefs)

    def _make_all_args(self, params=None, **kwargs):
        """Generate **all** function arguments for all functions."""
        out = self.right._make_all_args(params=params, **kwargs)
        out.update(self.left._make_all_args(params=params, **kwargs))
        return out

def _buildmodel(state, funcdefs=None):
    """Build model from saved state.

    Intended for internal use only.

    """
    if len(state) != 3:
        raise ValueError("Cannot restore Model")
    known_funcs = {}
    for fname in lineshapes.functions:
        fcn = getattr(lineshapes, fname, None)
        if callable(fcn):
            known_funcs[fname] = fcn
    if funcdefs is not None:
        known_funcs.update(funcdefs)

    left, right, op = state
    if op is None and right is None:
        (fname, fcndef, name, prefix, ivars, pnames,
         phints, nan_policy, opts) = left
        if not callable(fcndef) and fname in known_funcs:
            fcndef = known_funcs[fname]

        if fcndef is None:
            raise ValueError("Cannot restore Model: model function not found")

        model = Model(fcndef, name=name, prefix=prefix,
                      independent_vars=ivars, param_names=pnames,
                      nan_policy=nan_policy, **opts)

        for name, hint in list(phints.items()):
            model.set_param_hint(name, **hint)
        return model
    else:
        lmodel = _buildmodel(left, funcdefs=funcdefs)
        rmodel = _buildmodel(right, funcdefs=funcdefs)
        return CompositeModel(lmodel, rmodel, getattr(operator, op))

#Models derived from the lmfit Model class
#Need to give param_names if we want to keep the correct order
"""******************************1D models*****************************************************"""
class Linear1DModel(Model):
    """Linear model, with two Parameters.

    Defined as:

    f(x; bg1,bg0) = bg0+bg1*x

    """
    EVALUATE = 'points'
    ICON = 'linear.png'
    NAME = "Linear background"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(linear1D, param_names=['bg0','bg1'], **kwargs)
        self.set_param_hint('bg0', value=0.)
        self.set_param_hint('bg1', value=0.)

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        bg1, bg0 = 0., 0.
        if x is not None:
            bg1, bg0 = np.polyfit(x, data, 1)
        pars = self.make_params(bg1=bg1, bg0=bg0)
        return update_param_vals(pars, self.prefix, **kwargs)

    def guess_from_curve(self, points, params, handle = 0):
        x0 = points[0,0]
        y0 = points[0,1]
        x1 = points[1,0]
        y1 = points[1,1]
        if x0 == x1:
            return

        bg1 = (y1-y0)/(x1-x0)
        bg0 = y0 - bg1*x0
        params[self.prefix+'bg0'].value = bg0
        params[self.prefix+'bg1'].value = bg1
        params.update_constraints()

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x=kwargs[0] #independant variable
        out = '%g+%g*%s'%(v['bg0'],v['bg1'],x)
        return out


    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

class Quadratic1DModel(Model):
    """Quadratic model, with three Parameters.

    Defined as:

    f(x; a,b,c) = bg2*x*x+bg1*x+bg0

    """
    EVALUATE = 'points'
    ICON = 'quadratic.png'
    NAME = "Quadratic background"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(quadratic1D, param_names=['bg0','bg1','bg2'], **kwargs)
        self.set_param_hint('bg2', value=0.)
        self.set_param_hint('bg1', value=0.)
        self.set_param_hint('bg0', value=0.)

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        bg2, bg1, bg0 = 0., 0., 0.
        if x is not None:
            bg2, bg1, bg0 = np.polyfit(x, data, 2)
        pars = self.make_params(bg2=bg2, bg1=bg1, bg0=bg0)
        return update_param_vals(pars, self.prefix, **kwargs)

    def guess_from_curve(self, points, params, handle = 0):
        bg2, bg1, bg0 = np.polyfit(points[:,0], points[:,1], 2)
        params[self.prefix+'bg0'].value = bg0
        params[self.prefix+'bg1'].value = bg1
        params[self.prefix+'bg2'].value = bg2
        params.update_constraints()

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x=kwargs[0] #independant variable
        out = '%g+%g*%s+%g*%s**2'%(v['bg0'],v['bg1'],x,v['bg2'],x)
        return out

class Cubic1DModel(Model):
    """Cubic model, with four Parameters.

    Defined as:

    f(x; a,b,c) = bg3*x**3+bg2*x*x+bg1*x+bg0

    """
    EVALUATE = 'points'
    ICON = 'cubic.png'
    NAME = "Cubic background"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(cubic1D, param_names=['bg0','bg1','bg2','bg3'], **kwargs)
        self.set_param_hint('bg3', value=0.)
        self.set_param_hint('bg2', value=0.)
        self.set_param_hint('bg1', value=0.)
        self.set_param_hint('bg0', value=0.)

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        bg3, bg2, bg1, bg0 = 0., 0., 0., 0.
        if x is not None:
            bg3, bg2, bg1, bg0 = np.polyfit(x, data, 3)
        pars = self.make_params(bg3=bg3, bg2=bg2, bg1=bg1, bg0=bg0)
        return update_param_vals(pars, self.prefix, **kwargs)

    def guess_from_curve(self, points, params, handle = 0):
        bg3, bg2, bg1, bg0 = np.polyfit(points[:,0], points[:,1], 3)
        params[self.prefix+'bg0'].value = bg0
        params[self.prefix+'bg1'].value = bg1
        params[self.prefix+'bg2'].value = bg2
        params[self.prefix+'bg3'].value = bg3
        params.update_constraints()

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x=kwargs[0] #independant variable
        out = '%g+%g*%s+%g*%s**2+%g*%s**3'%(v['bg0'],v['bg1'],x,v['bg2'],x,v['bg3'],x)
        return out


class PowerLaw1DModel(Model):
    """A model based on a Power Law (see https://en.wikipedia.org/wiki/Power_law),
    with three Parameters: ``amplitude`` (:math:`A`), loc and ``exponent`` (:math:`k`), in:

    .. math::

        f(x; A, loc, k) = A (x-loc)^k

    """
    EVALUATE = 'points'
    ICON = 'power.png'
    NAME = "Power law"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(powerlaw1D, **kwargs)
        self._set_paramhints_prefix()

    def _set_paramhints_prefix(self):
        self.set_param_hint('power', max=100)
        self.set_param_hint('power', min=-100)

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        #assume that min(x) = x0
        x = np.array(x)
        y = np.array(data)
        x0 = np.amin(x)
        xx = x[x > x0]
        yy = y[x > x0]

        if len(xx)==0:
            pars = self.make_params(amp = 0., power = 1., loc=x0)
            return update_param_vals(pars, self.prefix, **kwargs)

        if yy[0]<0:
            yy = -yy
            sign = -1
        else:
            sign = 1

        if yy[0]==0:
            #not possible to fit a power law with 0
            pars = self.make_params(amp = 0., power = 1., loc=x0)
            return update_param_vals(pars, self.prefix, **kwargs)

        try:
            power, lnamp = np.polyfit(np.log(xx-x0), np.log(yy), 1)
        except (TypeError, np.linalg.LinAlgError):
            power, lnamp = 1, np.log(abs(max(yy)+1.e-9))

        pars = self.make_params(amp = sign*np.exp(lnamp), power = power, loc=x0)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

    def guess_from_curve(self, points, params, handle=0):
        """Estimate initial model parameter values from curve points"""
        #middle point is at [x0,amp]
        x0 = np.amin(points[:,0])

        if handle>0 and points[handle,1]<0:
            sign = -1
        else:
            if points[1,1]<0:
                sign = -1
            else:
                sign = +1

        x1 = points[1,0] - x0
        x2 = points[2,0] - x0
        y1 = abs(points[1,1])
        y2 = abs(points[2,1])

        if x1 == 0 or x2 == 0 or y1==0 or y2==0:
            return
        lnx1 = np.log(x1)
        lnx2 = np.log(x2)
        lny1 = np.log(y1)
        lny2 = np.log(y2)
        power = (lny2-lny1)/(lnx2-lnx1)
        amp = sign*np.exp(lny2-power*lnx2)
        if np.isfinite(power) and np.isfinite(amp):
            params[self.prefix+'amp'].value = amp
            params[self.prefix+'loc'].value = x0
            params[self.prefix+'power'].value = power

            params.update_constraints()

    def set_points(self, points, params, handle=0):
        #when a curve is modified with handle, set the curve points
        x0 = params[self.prefix+'loc'].value

        points[0,0] = x0
        points[0,1] = 0.
        if points[1,0] < x0:
            points[1,0] = x0+abs(points[1,0]-x0)
        if points[2,0] < x0:
            points[2,0] = x0+abs(points[2,0]-x0)
        points[1:,1] = self.eval(params, x=points[1:,0])

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x=kwargs[0] #independant variable
        out = '%g*(%s-%g)**%g'%(v['amp'],x,v['loc'],v['power'])
        return out


class Exponential1DModel(Model):
    """A model based on an exponential decay function
    (see https://en.wikipedia.org/wiki/Exponential_decay) with two Parameters:
    ``amplitude`` (:math:`A`), and ``decay`` (:math:`\tau`), in:

    .. math::

        f(x; amp, \tau) = amp e^{-x/\tau}

    """
    EVALUATE = 'points'
    ICON = 'exp.png'
    NAME = "Exponential decay"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(exponential1D, **kwargs)

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        if len(data)==0:
            pars = self.make_params(amp = 0., tau = 1.)
            return update_param_vals(pars, self.prefix, **kwargs)

        if data[0]<0:
            data = -data
            sign = -1
        else:
            sign = 1

        try:
            sval, oval = np.polyfit(x, np.log(abs(data)+1.e-15), 1)
        except TypeError:
            sval, oval = 1., np.log(abs(max(data)+1.e-9))
        pars = self.make_params(amp=sign*np.exp(oval), tau=-1.0/sval)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

    def guess_from_curve(self, points, params, handle=0):
        """Estimate initial model parameter values from curve points"""
        #middle point is at [x0,amp]
        x0 = points[0,0]
        x1 = points[1,0]
        if x0 == x1:
            return
        y0 = points[0,1]
        y1 = points[1,1]

        if y0==y1:
            #do not try to fit if the two values have not the same sign or if one of them is zero
            return

        if y0*y1<0:
            #the user try to change sign from the handle:
            if handle ==0:
                y1 = -y1
            else:
                y0 = -y0

        lny0 = np.log(abs(y0))
        lny1 = np.log(abs(y1))
        tau = (x0-x1)/(lny1-lny0)
        amp = y0/np.exp(-x0/tau)

        params[self.prefix+'amp'].value = amp
        params[self.prefix+'tau'].value = tau

        params.update_constraints()

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x=kwargs[0] #independant variable
        out = '%g*exp(-%s/%g)'%(v['amp'],x,v['tau'])
        return out


class Abstract2DPeakModel():
    def set_from_tool(self, points, params):
        p0 = points[0]
        p1 = points[1]
        loc0 = p0[0]
        loc1 = p0[1]
        width0,width1 = 2.*abs(p1[0]-loc0),2*abs(p1[1]-loc1)
        params[self.prefix+'loc0'].value = loc0
        params[self.prefix+'loc1'].value = loc1
        if width0 !=0:
            params[self.prefix+'width0'].value = width0
        if width1 !=0:
            params[self.prefix+'width1'].value = width1

class Abstract1DPeakModel():
    """class implementing common methods for 1D peak models"""
    def get_curve_points_len(self):
        return 3

    def set_points(self, points, params, handle=0):
        #when a curve is modified with handle, set the curve points
        loc = params[self.prefix+'loc'].value
        fwhm = params[self.prefix+'fwhm'].value
        points[1,0] = loc
        points[0,0] = loc-fwhm/2
        points[2,0] = loc+fwhm/2
        points[:,1] = self.eval(params, x=points[:,0])


    def set_from_tool(self, points, params):
        """Estimate initial model parameters values from start and stop points of an action tool"""
        p0 = points[0]
        p1 = points[1]
        loc=p0[0]
        amp = p0[1] - p1[1]
        fwhm = 2*np.absolute(p1[0]-loc)

        params[self.prefix+'amp'].value = amp
        params[self.prefix+'loc'].value = loc
        if fwhm !=0:
            params[self.prefix+'fwhm'].value = fwhm
        params.update_constraints()

    def guess_from_curve(self, points, params, handle=0):
        """Estimate initial model parameter values from curve points"""
        #middle point is at [x0,amp]
        loc=points[1,0]
        amp = points[1,1]

        #fwhm depends on handle used
        if handle==0:
            fwhm = 2*np.absolute(points[0,0]-loc)
            params[self.prefix+'fwhm'].value = fwhm
        elif handle==1:
            #fwhm = np.absolute(points[2,0]-points[0,0])
            params[self.prefix+'loc'].value = loc
            params[self.prefix+'amp'].value = amp
        else:
            fwhm = 2*np.absolute(points[2,0]-loc)
            params[self.prefix+'fwhm'].value = fwhm




        params.update_constraints()

class Lorentzian1DModel(Abstract1DPeakModel, Model):
    """Lorentzian model, with three Parameters.

    Defined as:

    f(x; amp,loc,fwhm) = amp/(1.+((x-loc)/0.5/fwhm)**2)

    """
    EVALUATE = 'peak'
    ICON = 'lorentz.png'
    NAME = "Lorentzian curve"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(lorentzian1D, param_names=['amp','loc','fwhm'], **kwargs)
        self.intfunc = lorentzianint
        self._set_paramhints_prefix()

    def _set_paramhints_prefix(self):
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc', value=0.)
        self.set_param_hint('fwhm', value=1.)
        self.set_param_hint('fwhm', min=0.)

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x=kwargs[0] #independant variable
        out = '%g/(1.+((%s-%g)/%g)**2)'%(v['amp'],x,v['loc'],v['fwhm']/2.)
        return out

class Gaussian1DModel(Abstract1DPeakModel, Model):
    """Gaussian model, with three Parameters.

    Defined as:

    f(x; amp,loc,fwhm) = amp*exp(-((x-loc)/wg/fwhm)**2)

    """
    EVALUATE = 'peak'
    ICON = 'gauss.png'
    NAME = "Gaussian curve"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(gaussian1D, param_names=['amp','loc','fwhm'], **kwargs)
        self.intfunc = gaussianint
        self._set_paramhints_prefix()

    def _set_paramhints_prefix(self):
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc', value=0.)
        self.set_param_hint('fwhm', value=1.)
        self.set_param_hint('fwhm', min=0.)

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x=kwargs[0] #independant variable
        out = '%g*exp(-((%s-%g)/%g)**2)'%(v['amp'],x,v['loc'],v['fwhm']*wg)
        return out

class PseudoVoigt1DModel(Abstract1DPeakModel, Model):
    """Pseudo Voigt model, with four Parameters.

    Defined as:

    f(x; amp,loc,fwhm, eta) = amp*((1-eta)*exp(-((x-loc)/wg/fwhm)**2)+eta*exp(-((x-loc)/wg/fwhm)**2))

    """
    EVALUATE = 'peak'
    ICON = 'voigt.png'
    NAME = "PseudoVoigt curve"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(pseudovoigt1D, param_names=['amp','loc','fwhm','eta'], **kwargs)
        self.intfunc = pseudovoigtint
        self._set_paramhints_prefix()

    def _set_paramhints_prefix(self):
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc', value=0.)
        self.set_param_hint('fwhm', value=1.)
        self.set_param_hint('fwhm', min=0.)
        self.set_param_hint('eta', min=0.)
        self.set_param_hint('eta', max=1.)
        self.set_param_hint('eta', value=0.5)

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x = kwargs[0] #independant variable
        out = '%g*(%g*exp(-((%s-%g)/%g)**2)+%g/(1.+((%s-%g)/%g)**2))'%(v['amp'],1.-v['eta'],x,v['loc'],v['fwhm']*wg,
                                                                       v['eta'],x,v['loc'],v['fwhm']/2.)
        return out

class Door1DModel(Abstract1DPeakModel, Model):
    """Door model, with four Parameters.

    Defined as:

    f(x; amp,loc,fwhm, slope) =  amp/2.*(erf((x-loc+fwhm/2.)/slope)-erf((x-loc+fwhm/2.)/slope))

    """
    EVALUATE = 'peak'
    ICON = 'door.png'
    NAME = "Door curve"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(door1D, param_names=['amp','loc','fwhm','slope'], **kwargs)
        self.intfunc = door1Dint
        self._set_paramhints_prefix()

    def _set_paramhints_prefix(self):
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc', value=0.)
        self.set_param_hint('fwhm', value=1.)
        self.set_param_hint('fwhm', min=0.)
        self.set_param_hint('slope', min=0.)
        self.set_param_hint('slope', value=1.)

    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x = kwargs[0] #independant variable
        x1 = v['loc']-v['fwhm']/2.
        x2 = v['loc']+v['fwhm']/2.
        w = v['slope']
        out = '%g*(erf((%s-%g)/%g)-erf((%s-%g)/%g))'%(v['amp']/2.,x,x1,w,x,x2,w)
        return out

class Step1DModel(Model):
    """Step model, with three Parameters.

    Defined as:

    f(x; amp,loc,fwhm, slope) =  amp*(erf((x-loc)/slope))

    """
    EVALUATE = 'point'
    ICON = 'step.png'
    NAME = "Step curve"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(step1D, param_names=['amp','loc','slope'], **kwargs)
        self._set_paramhints_prefix()

    def get_curve_points_len(self):
        return 2

    def _set_paramhints_prefix(self):
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc', value=0.)
        self.set_param_hint('slope', value=1.)

    def guess_from_curve(self, points, params, handle=0):
        """Estimate initial model parameter values from curve points"""
        #first point is at [x0,amp]
        x0 = points[0,0]
        x1 = points[1,0]
        if x0 == x1:
            return

        #y0 = points[0,1] fixed at 0
        y1 = points[1,1]   #half position (i.e. erf=0.5)

        slope = x1-x0
        amp = y1/erf1

        params[self.prefix+'amp'].value = amp
        params[self.prefix+'slope'].value = slope
        params[self.prefix+'loc'].value = x0
        params.update_constraints()

    def set_points(self, points, params, handle=0):
        #when a curve is modified with handle, set the curve points
        x0 = params[self.prefix+'loc'].value

        points[0,0] = x0
        points[0,1] = 0.
        points[1,0] = x0 + params[self.prefix+'slope'].value
        points[1,1] = erf1*params[self.prefix+'amp'].value


    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x = kwargs[0] #independant variable
        out = '%g*erf((%s-%g)/%g)'%(v['amp'],x,v['loc'],v['slope'])
        return out

class Sinus1DModel(Model):
    """Sinus model, with three Parameters.

    Defined as:

    f(x; amp,loc,freq) =  amp*sin(2*pi*(x-loc)/freq)

    """
    EVALUATE = 'point'
    ICON = 'sinus.png'
    NAME = "Sinusoidal curve"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(sinus1D, param_names=['amp','loc','freq'], **kwargs)
        self._set_paramhints_prefix()

    def get_curve_points_len(self):
        return 2

    def _set_paramhints_prefix(self):
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc', value=0.)
        self.set_param_hint('freq', value=1.)

    def guess_from_curve(self, points, params, handle=0):
        """Estimate initial model parameter values from curve points"""
        #first point is at [x0,amp]
        x0 = points[0,0]
        x1 = points[1,0]
        if x0 == x1:
            return

        y1 = points[1,1]

        freq = 0.25/(x1-x0)
        amp = y1

        params[self.prefix+'amp'].value = amp
        params[self.prefix+'freq'].value = freq
        params[self.prefix+'loc'].value = x0
        params.update_constraints()

    def set_points(self, points, params, handle=0):
        #when a curve is modified with handle, set the curve points
        x0 = params[self.prefix+'loc'].value

        points[0,0] = x0
        points[0,1] = 0.
        points[1,0] = x0 + 0.25/params[self.prefix+'freq'].value
        points[1,1] = params[self.prefix+'amp'].value


    def get_reprstring(self, params, *kwargs):
        v = self.make_funcargs(params=params)
        x = kwargs[0] #independant variable
        out = '%g*sin(%g*(%s-%g))'%(v['amp'],dpi*v['freq'],x,v['loc'])
        return out

"""******************************2D models*****************************************************"""
class LorentzianPower2DModel(Abstract2DPeakModel,Model):
    """Lorentzian model with variable power, with seven Parameters.

    Defined as:

    f(x; amp, loc0, loc1, width0, width1, th) = amp/(1.+((x0/width0)**2+(x1/width1)**2)**(power/2.)))

    """
    EVALUATE = 'peak'
    ICON = 'gauss2D.jpg'
    NAME = "Lorentzian with extra power"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(lorentzianpower2D, param_names=['amp', 'loc0', 'loc1', 'width0', 'width1', 'th', 'power'], **kwargs)
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc0', value=0.)
        self.set_param_hint('loc1', value=0.)
        self.set_param_hint('width0', value=1., min=0)
        self.set_param_hint('width1', value=1., min=0)
        self.set_param_hint('th', value=0., vary=False)
        self.set_param_hint('power', value=2., vary=False)
        self.intfunc=lorentzianpowerint

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        amp,loc0,loc1,width0,width1,th,power = 1., 0., 0., 1., 1., 0.,2.

        if x is not None:
            amp,loc0,loc1,width0,width1 = guess_2D_peak(data, x)

        pars = self.make_params(amp=amp, loc0=loc0, loc1=loc1, width0=width0, width1=width1, th=th, power=power)
        return update_param_vals(pars, self.prefix, **kwargs)


    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC


class LorentzianPoly2DModel(Abstract2DPeakModel,Model):
    """Lorentzian model with polynomial, with seven Parameters.

    Defined as:

    f(x; amp, loc0, loc1, width0, width1, th) = amp/(1.+((x0/width0)**2+(x1/width1)**2)**(power/2.)))

    """
    EVALUATE = 'peak'
    ICON = 'gauss2D.jpg'
    NAME = "Lorentzian with x**4 term"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(lorentzianpoly2D, param_names=['amp', 'loc0', 'loc1', 'width0', 'width1', 'eta0', 'eta1', 'th'], **kwargs)
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc0', value=0.)
        self.set_param_hint('loc1', value=0.)
        self.set_param_hint('width0', value=1., min=0)
        self.set_param_hint('width1', value=1., min=0)
        self.set_param_hint('eta0', value=0.5, min=0, max=1)
        self.set_param_hint('eta1', value=0.5, min=0, max=1)
        self.set_param_hint('th', value=0., min=-np.pi, max=np.pi)
        self.intfunc=lorentzianpolyint

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        amp,loc0,loc1,width0,width1,th,power = 1., 0., 0., 1., 1., 0.,2.

        if x is not None:
            amp,loc0,loc1,width0,width1 = guess_2D_peak(data, x)

        pars = self.make_params(amp=amp, loc0=loc0, loc1=loc1, width0=width0, width1=width1, th=th, power=power)
        return update_param_vals(pars, self.prefix, **kwargs)

    def eval_integral(self, params=None, **kwargs):
        """Evaluate the integral of the model with supplied parameters and keyword arguments.

        Parameters
        -----------
        params : Parameters, optional
            Parameters to use in Model.
        **kwargs : optional
            Additional keyword arguments to pass to model function.

        Returns
        -------
        numpy.ndarray
            Value of model given the parameters and other arguments.

        Notes
        -----
        1. if `params` is None, the values for all parameters are
        expected to be provided as keyword arguments.  If `params` is
        given, and a keyword argument for a parameter value is also given,
        the keyword argument will be used.

        2. all non-parameter arguments for the model function, **including
        all the independent variables** will need to be passed in using
        keyword arguments.

        """
        return (self.intfunc(**self.make_funcargs(params, kwargs)))




    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

class LorentzianDoor2DModel(Abstract2DPeakModel,Model):
    """Product of Lorentzian in one direction, door (with 2 erf) in the other direction, with seven Parameters.

    Defined as:

    f(x; amp, loc0, loc1, width0, width1, eta, th) = (amp/(1.+((x0/width0)**2)*(erf((y0-width0/2)/eta)-erf((y0+width0/2)))

    """
    EVALUATE = 'peak'
    ICON = 'gauss2D.jpg'
    NAME = "Lorentzian*door"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(lorentziandoor2D, param_names=['amp', 'loc0', 'loc1', 'width0', 'width1', 'eta', 'th'], **kwargs)
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc0', value=0.)
        self.set_param_hint('loc1', value=0.)
        self.set_param_hint('width0', value=1., min=0)
        self.set_param_hint('width1', value=1., min=0)
        self.set_param_hint('eta', value=0.2, min=0)
        self.set_param_hint('th', value=0., min=-np.pi, max=np.pi)
        self.intfunc = lorentziandoorint

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        amp,loc0,loc1,width0,width1,eta, th = 1., 0., 0., 1., 1., 1.0, 0.

        if x is not None:
            amp,loc0,loc1,width0,width1 = guess_2D_peak(data, x)

        pars = self.make_params(amp=amp, loc0=loc0, loc1=loc1, width0=width0, width1=width1, eta=eta, th=th)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

class LorentzianGauss2DModel(Abstract2DPeakModel,Model):
    """Product of Lorentzian in tha main direction, gaussian in the perpendicular direction, with six Parameters.

    Defined as:

    f(x; amp, loc0, loc1, width0, width1, th) = (amp*eta/(1.+((x0/width0)**2)*np.exp(-(x1/width1)**2))
                                                                                                     )

    """
    EVALUATE = 'peak'
    ICON = 'gauss2D.jpg'
    NAME = "Lorentzian*gaussian"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(lorentziangauss2D, param_names=['amp', 'loc0', 'loc1', 'width0', 'width1', 'th'], **kwargs)
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc0', value=0.)
        self.set_param_hint('loc1', value=0.)
        self.set_param_hint('width0', value=1., min=0)
        self.set_param_hint('width1', value=1., min=0)
        self.set_param_hint('th', value=0., min=-np.pi, max=np.pi)
        self.intfunc = lorentziangaussint

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        amp,loc0,loc1,width0,width1,th = 1., 0., 0., 1., 1., 0.

        if x is not None:
            amp,loc0,loc1,width0,width1 = guess_2D_peak(data, x)

        pars = self.make_params(amp=amp, loc0=loc0, loc1=loc1, width0=width0, width1=width1, th=th)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

class DoubleLorentzianGauss2DModel(Abstract2DPeakModel,Model):
    """Product of Lorentzian in the main direction, gaussian in the perpendicular direction added to the
    rotated similar function, with nine Parameters.

    Defined as:

    f(x; amp, loc0, loc1, width0, width1, th) = (amp*(eta*np.exp(-(x1/width1)**2)/(1.+((x0/width0)**2))+
                 (1.-eta)*np.exp(-(x0/width3)**2)/(1.+((x1/width2)**2))))

    """
    EVALUATE = 'peak'
    ICON = 'gauss2D.jpg'
    NAME = "Double lorentzian*gaussian"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(doublelorentziangauss2D, param_names=['amp', 'loc0', 'loc1', 'width0', 'width1', 'width2', 'width3', 'eta', 'th'], **kwargs)
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc0', value=0.)
        self.set_param_hint('loc1', value=0.)
        self.set_param_hint('width0', value=1., min=0)
        self.set_param_hint('width1', value=1., min=0)
        self.set_param_hint('width2', value=1., min=0, expr='%swidth1' % self.prefix)
        self.set_param_hint('width3', value=1., min=0, expr='%swidth0' % self.prefix)
        self.set_param_hint('eta', value=0.5, min=0, max=1)
        self.set_param_hint('th', value=0., min=-np.pi, max=np.pi)
        self.intfunc = doublelorentziangaussint

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        amp,loc0,loc1,width0,width1,width2,width3,eta,th = 1., 0., 0., 1., 1., 0.

        if x is not None:
            amp,loc0,loc1,width0,width1 = guess_2D_peak(data, x)

        pars = self.make_params(amp=amp, loc0=loc0, loc1=loc1, width0=width0, width1=width1, width2=width2, width3=width3,eta=eta,th=th)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

class LorentzianLorentzian2DModel(Abstract2DPeakModel,Model):
    """Product of lorentzian in two directions, with six Parameters.

    Defined as:

    f(x; amp, loc0, loc1, width0, width1, th) = (amp/(1.+((x0/width0)**2)*np.exp(-(x1/width1)**2)))

    """
    EVALUATE = 'peak'
    ICON = 'gauss2D.jpg'
    NAME = "Lorentzian*lorentzian"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(lorentzianlorentzian2D, param_names=['amp', 'loc0', 'loc1', 'width0', 'width1', 'th'], **kwargs)
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc0', value=0.)
        self.set_param_hint('loc1', value=0.)
        self.set_param_hint('width0', value=1., min=0)
        self.set_param_hint('width1', value=1., min=0)
        self.set_param_hint('th', value=0., min=-np.pi, max=np.pi)
        self.intfunc = lorentzianlorentzianint

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        amp,loc0,loc1,width0,width1,th = 1., 0., 0., 1., 1., 0.

        if x is not None:
            amp,loc0,loc1,width0,width1 = guess_2D_peak(data, x)

        pars = self.make_params(amp=amp, loc0=loc0, loc1=loc1, width0=width0, width1=width1, th=th)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC

class GaussGauss2DModel(Abstract2DPeakModel,Model):
    """Product of gaussian in two directions, with six Parameters.

    Defined as:

    f(x; amp, loc0, loc1, width0, width1, th) = amp*np.exp(-((x1/width1)**2+(x0/width0)**2))

    """
    EVALUATE = 'peak'
    ICON = 'gauss2D.jpg'
    NAME = "Gaussian*gaussian"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(gaussgauss2D, param_names=['amp', 'loc0', 'loc1', 'width0', 'width1', 'th'], **kwargs)
        self.set_param_hint('amp', value=1.)
        self.set_param_hint('loc0', value=0.)
        self.set_param_hint('loc1', value=0.)
        self.set_param_hint('width0', value=1., min=0)
        self.set_param_hint('width1', value=1., min=0)
        self.set_param_hint('th', value=0., min=-np.pi, max=np.pi)
        self.intfunc = gaussgaussint

    def guess(self, data, x=None, **kwargs):
        """Estimate initial model parameter values from data."""
        amp,loc0,loc1,width0,width1,th = 1., 0., 0., 1., 1., 0.

        if x is not None:
            amp,loc0,loc1,width0,width1 = guess_2D_peak(data, x)

        pars = self.make_params(amp=amp, loc0=loc0, loc1=loc1, width0=width0, width1=width1, th=th)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC


class Planar2DModel(Model):
    """Planar model, with three Parameters.

    Defined as:

    f(x; slope_x,slope_y,bg) = bg+x[0]*slope_x+x[1]*slope_y

    """
    EVALUATE = 'points'
    ICON = 'planar.jpg'
    NAME = "Planar background"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(planar2D, param_names=['bg','slope_x','slope_y'], **kwargs)
        self.set_param_hint('bg', value=1.)
        self.set_param_hint('slope_x', value=0.)
        self.set_param_hint('slope_y', value=0.)

    def guess_from_curve(self, points, data, params, handle=0):
        """Estimate initial model parameter values from data."""

        npts=points.shape[0]
        sx=np.sum(points[:,0])
        sy=np.sum(points[:,1])
        sx2=np.sum(points[:,0]*points[:,0])
        sxy=np.sum(points[:,0]*points[:,1])
        sy2=np.sum(points[:,1]*points[:,1])
        sz=np.sum(data)
        szx=np.sum(points[:,0]*data)
        szy=np.sum(points[:,1]*data)
        a=np.array([[npts,sx,sy],
                    [sx,sx2,sxy],
                    [sy,sxy,sy2]])
        b=np.array([sz,szx,szy])
        res = np.linalg.solve(a, b)

        params[self.prefix+'bg'].value = res[0]
        params[self.prefix+'slope_x'].value = res[1]
        params[self.prefix+'slope_y'].value = res[2]

        params.update_constraints()


class Quadratic2DModel(Model):
    """Planar model, with three Parameters.

    Defined as:

    f(x,slope_x2,slope_xy,slope_y2,slope_x,slope_y,bg) = bg+x[0]*(slope_x2*x[0]+slope_x+slope_xy*x[1])+x[1]*(slope_y2*x[1]+slope_y)

    """
    EVALUATE = 'points'
    ICON = 'planar.jpg'
    NAME = "Quadratic background"

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(quadratic2D, param_names=['bg','slope_x','slope_y','slope_x2','slope_xy','slope_y2'], **kwargs)
        self.set_param_hint('bg', value=1.)
        self.set_param_hint('slope_x', value=0.)
        self.set_param_hint('slope_y', value=0.)
        self.set_param_hint('slope_x2', value=0.)
        self.set_param_hint('slope_xy', value=0.)
        self.set_param_hint('slope_y2', value=0.)

    def guess_from_curve(self, points, data, params, handle=0):
        """Estimate initial model parameter values from data."""

        npts=points.shape[0]
        sx=np.sum(points[:,0])
        sy=np.sum(points[:,1])
        x2 = points[:,0]*points[:,0]
        xy = points[:,0]*points[:,1]
        y2 = points[:,1]*points[:,1]

        sx2=np.sum(x2)
        sxy=np.sum(xy)
        sy2=np.sum(y2)
        sx3=np.sum(points[:,0]*x2)
        sx2y=np.sum(points[:,1]*x2)
        sxy2=np.sum(points[:,0]*y2)
        sy3=np.sum(points[:,1]*y2)
        sx4=np.sum(x2*x2)
        sx3y=np.sum(x2*xy)
        sx2y2=np.sum(x2*y2)
        sxy3=np.sum(xy*y2)
        sy4=np.sum(y2*y2)

        sz=np.sum(data)
        szx=np.sum(points[:,0]*data)
        szy=np.sum(points[:,1]*data)
        szx2=np.sum(x2*data)
        szxy=np.sum(xy*data)
        szy2=np.sum(y2*data)

        a=np.array([[npts,sx,sy,sx2,sxy,sy2],
                    [sx,sx2,sxy,sx3,sx2y,sxy2],
                    [sy,sxy,sy2,sx2y,sxy2,sy3],
                    [sx2,sx3,sx2y,sx4,sx3y,sx2y2],
                    [sxy,sx2y,sxy2,sx3y,sx2y2,sxy3],
                    [sy2,sxy2,sy3,sx2y2,sxy3,sy4]])
        b=np.array([sz,szx,szy,szx2,szxy,szy2])

        res = np.linalg.solve(a, b)
        params[self.prefix+'bg'].value = res[0]
        params[self.prefix+'slope_x'].value = res[1]
        params[self.prefix+'slope_y'].value = res[2]
        params[self.prefix+'slope_x2'].value = res[3]
        params[self.prefix+'slope_xy'].value = res[4]
        params[self.prefix+'slope_y2'].value = res[5]

        params.update_constraints()


list_of_models = []
list_of_2D_models = [Planar2DModel, Quadratic2DModel, LorentzianLorentzian2DModel, GaussGauss2DModel, LorentzianGauss2DModel, LorentzianDoor2DModel, LorentzianPower2DModel, DoubleLorentzianGauss2DModel]
list_of_1D_models = [Linear1DModel, Quadratic1DModel, Cubic1DModel, PowerLaw1DModel, Exponential1DModel, Lorentzian1DModel, Gaussian1DModel, PseudoVoigt1DModel, Door1DModel, Step1DModel,Sinus1DModel]
