"""
Created on Fri Feb 21 14:57:49 2020

@author: Prevot
"""
import numpy as np

def get_xywxwy_cxy(plot,p0,p1):
    #return position and width from a centered circular shape drawn from p0 to p1
    ax,ay=plot.get_axis_id("bottom"),plot.get_axis_id("left")
    x1,y1= plot.invTransform(ax, p0.x()), plot.invTransform(ay, p0.y())
    x2,y2= plot.invTransform(ax, p1.x()), plot.invTransform(ay, p1.y())
    return x1,y1,2.*abs(x2-x1),2*abs(y1-y2)

def guess_2D_peak(data, x):
    #rough guess the amplitude, position, width of a 2D peak
    n = np.argmax(data)
    p = np.argmin(data)
    amp = data[n]-data[p]
    loc0 = x[0][n]
    loc1 = x[1][n]
    width0 = np.amax(x[0])-np.amin(x[0])/2.
    width1 = np.amax(x[1])-np.amin(x[1])/2.
    if width0 == 0:
        width0 = 1.
    if width1 == 0:
        width1 = 1.
    return amp, loc0, loc1, width0, width1
