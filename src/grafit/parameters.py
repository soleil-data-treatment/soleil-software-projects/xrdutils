"""
Created on Wed May 06 09:07:49 2020

@author: Prevot
"""
import numpy as np
from lmfit import  Parameters

from guidata.dataset.datatypes import (DataSet, ObjectItem, BeginGroup,ItemProperty,
                                       EndGroup, Obj, DataSetGroup,
                                       BeginTabGroup, EndTabGroup,
                                       GetAttrProp, NotProp)
from guidata.dataset.dataitems import (ChoiceItem, BoolItem, FloatItem, IntItem,MultipleChoiceItem,
                                       ImageChoiceItem, ColorItem, StringItem,
                                       ButtonItem, FloatArrayItem, TextItem)

from qtpy.QtWidgets import QMessageBox
from guidata.dataset.qtwidgets import DataSetEditLayout,DataSetShowLayout,DataSetShowWidget
from guidata.dataset.qtitemwidgets import DataSetWidget,SliderWidget

from guiqwt.config import CONF, _

"""default parameters for new plot items (unitcell, latticegrip, spot, reconstructiongrid)"""
DEFAULTS = {
            'plot' :
             {

              "shape/curveshape/line/style" : 'DotLine',
              "shape/curveshape/line/color" : "#ff0000",
              "shape/curveshape/line/width" : 1,
              "shape/curveshape/fill/style" : "NoBrush",
              "shape/curveshape/fill/color" : "white",
              "shape/curveshape/fill/alpha" : 0.1,
              "shape/curveshape/symbol/marker" : 'Rect',
              "shape/curveshape/symbol/size" : 10,
              "shape/curveshape/symbol/edgecolor" : "#ff0000",
              "shape/curveshape/symbol/facecolor" : "#ff0000",
              "shape/curveshape/symbol/alpha" : 0.,
              "shape/curveshape/sel_line/style" : 'SolidLine',
              "shapecurveshape/sel_line/color" : "#ff0000",
              "shape/curveshape/sel_line/width" : 2,
              "shape/curveshape/sel_fill/style" : "NoBrush",
              "shape/curveshape/sel_fill/color" : "white",
              "shape/curveshape/sel_fill/alpha" : 0.1,
              "shape/curveshape/sel_symbol/marker" : 'Rect',
              "shape/curveshape/sel_symbol/size" : 10,
              "shape/curveshape/sel_symbol/edgecolor" : "#ff0000",
              "shape/curveshape/sel_symbol/facecolor" : "#ff0000",
              "shape/curveshape/sel_symbol/alpha" : .7,

              "shape/curvedrag/line/style": 'SolidLine',
              "shape/curvedrag/line/color": "#0000ff",
              "shape/curvedrag/line/width": 1,
              "shape/curvedrag/fill/style": "SolidPattern",
              "shape/curvedrag/fill/color": "white",
              "shape/curvedrag/fill/alpha": 0.1,
              "shape/curvedrag/symbol/marker": 'Rect',
              "shape/curvedrag/symbol/size": 3,
              "shape/curvedrag/symbol/edgecolor": "#0000ff",
              "shape/curvedrag/symbol/facecolor": "#0000ff",
              "shape/curvedrag/symbol/alpha": 1.,
              "shape/curvedrag/sel_line/style": 'SolidLine',
              "shape/curvedrag/sel_line/color": "#0000ff",
              "shape/curvedrag/sel_line/width": 1,
              "shape/curvedrag/sel_fill/style": "SolidPattern",
              "shape/curvedrag/sel_fill/color": "white",
              "shape/curvedrag/sel_fill/alpha": 0.1,
              "shape/curvedrag/sel_symbol/marker": 'Rect',
              "shape/curvedrag/sel_symbol/size": 3,
              "shape/curvedrag/sel_symbol/edgecolor": "#0000ff",
              "shape/curvedrag/sel_symbol/facecolor": "#0000ff",
              "shape/curvedrag/sel_symbol/alpha": .7,




                            },

            }

CONF.update_defaults(DEFAULTS)

class UpdateProp(ItemProperty):
    """An 'operator property'
    prop: ItemProperty instance
    func: function
    """
    def __init__(self, prop=None, func=None):
        self.property = prop
        self.function = func

    def __call__(self, instance, item, value):
        instance.update_choice(item,value)
        return self

    def set(self, instance, item, value):
        instance.update_choice(item,value)


class Scale2DParam(DataSet):
  __doc__ = "Parameters for scale preferences"
  maj = BeginGroup(_("Scale Preferences") )
  autoscale =  BoolItem(_("autoscale"), default=True)
  outliers = FloatItem(_("percentage of outliers suppressed"), default=0.,min=0,max=99)
  same2scale = BoolItem(_("same scale for model and fit"), default=True)
  same3scale = BoolItem(_("same scale for model, fit, difference"))
  _maj = EndGroup("end group")

class Scale1DParam(DataSet):
  __doc__ = "Parameters for scale preferences"
  maj = BeginGroup(_("Scale Preferences") )
  autoscale =  BoolItem(_("autoscale"), default=True)
  _maj = EndGroup("end group")

class MaskSizeParam(DataSet):
  __doc__ = "Parameters for mask size"
  maj = BeginGroup(_("Scale Preferences") )
  hsize = IntItem(_("horizontal size"), default=1)
  vsize = IntItem(_("vertical size"), default=1)
  _maj = EndGroup("end group")

class DrawResultParam(DataSet):
    __doc__ = "Parameters for curve drawing"
    maj = BeginGroup(_("Draw Result") )
    x_label = ChoiceItem("x", ['A', 'B'])
    y_label = ChoiceItem("y", ['A', 'B'])
    _maj = EndGroup("end group")

    def update_choice(self,labels):
        _choices = []
        for idx, choice in enumerate(labels):
            _choices.append(self._items[1]._normalize_choice(idx, choice) )
        self._items[1].set_prop("data", choices=_choices)
        self._items[2].set_prop("data", choices=_choices)

class AddFitDataParam(DataSet):
    __doc__ = "Add data to fit"
    maj = BeginGroup(_("Draw Result") )
    title = ChoiceItem("data",['No data loaded'])
    title.set_prop("display", callback=UpdateProp())
    x_label = ChoiceItem("x", ['A', 'B'])
    y_label = ChoiceItem("y", ['A', 'B'])
    sigma = ChoiceItem("add errors/weights",['None','errors','weiths'])
    s_label = ChoiceItem("e/w", ['A', 'B'])
    _maj = EndGroup("end group")

    def __init__(self, title=None, comment=None, icon=''):
        super().__init__(title,comment,icon)
        self.choices_data = []  #[[title,labels],]

    def update_column_choice(self, labels, column=None):
        # update the choice list for x_label, y_label, s_label with a list of labels
        _choices = []
        if column == None:
            for idx, choice in enumerate(labels):
                _choices.append(self._items[1]._normalize_choice(idx, choice) )
        else:
            for idx, choice in enumerate(labels):
                _choices.append(self._items[1]._normalize_choice(column[idx], choice) )
        self._items[2].set_prop("data", choices=_choices)
        self._items[3].set_prop("data", choices=_choices)
        self._items[5].set_prop("data", choices=_choices)

    def update_choice(self, item, value):
        #item is a choice_item()
        c0 = self.choices_data[value]
        _choices = []
        for idx, choice in enumerate(self.choices_data):
            _choices.append(self._items[1]._normalize_choice(idx, choice[0]) )
        self._items[1].set_prop("data", choices=_choices)

        self.update_column_choice(c0[1])

    def update_choices_data(self, choices_data):
        self.choices_data = choices_data
        if len(self.choices_data)<0:
            return
        self.update_choice(self.title,0)

class AddFitParam(DataSet):
    __doc__ = "Add data to fit"
    maj = BeginGroup(_("Draw Result") )
    title = ChoiceItem("data",['No data loaded'])
    x_label = ChoiceItem("x", ['A', 'B'])
    y_label = ChoiceItem("y", ['A', 'B'])
    sigma = ChoiceItem("add errors/weights",['None','errors','weiths'])
    s_label = ChoiceItem("e/w", ['A', 'B'])
    _maj = EndGroup("end group")

    def __init__(self, title=None, comment=None, icon=''):
        super().__init__(title,comment,icon)
        self.choices_data = []  #[[title,labels],]

    def update_column_choice(self, labels, column=None):
        # update the choice list for x_label, y_label, s_label with a list of labels
        _choices = []
        if column == None:
            for idx, choice in enumerate(labels):
                _choices.append(self._items[1]._normalize_choice(idx, choice) )
        else:
            for idx, choice in enumerate(labels):
                _choices.append(self._items[1]._normalize_choice(column[idx], choice) )
        self._items[2].set_prop("data", choices=_choices)
        self._items[3].set_prop("data", choices=_choices)
        self._items[5].set_prop("data", choices=_choices)

    def update_choice(self, title):
        self._items[1].set_prop("data", choices=[(0,title,None),])


class ExtendedParams():
    #dictionnaire de la liste des courbes pour le fit
    #utilise pour comprendre les Parameters de lmfit
    #ne pilote jamais le tracage des courbes et l'ecriture dans le tableau
    def __init__(self):
        self.reset()

    def reset(self):
        self.modelclasses=list()  #list of the Model classes that compose the Model for fitting the data
        self.models=list()  #list of the Model instances that compose the Model for fitting the data
        self.composite_model=None
        self.ncv=len(self.models)
        self.partry=Parameters()   #parametres d'essai
        self.paropt=Parameters()   #parametres optimises
        self.method='leastsq'
        self.chi2opt = None
        self.rawdata = None   #associated rawdata to be fitted
        self.isfitted = False #is the data fitted?
        self.issaved = False  #has the fit result been saved?
        self.tags={}  #tags is a dict
        self.results = [] #list of (dict_of_values, dict_of_sigma) corresponding to each fit performed
        self.result_vars = [] #list of all different parameter names

    def printparams(self):
        print("printing of parameters")
        for key in list(self.partry.keys()):
            print(key,self.partry[key].value,self.partry[key].vary)

    def get_opt_reprstring(self):
        #get a string for the mathematical expression of the fitting result
        if self.rawdata is not None:
            out = self.composite_model.get_reprstring(self.paropt,self.rawdata.x_label,self.rawdata.y_label)
            out = out.replace('--','+')
            out = out.replace('+-','-')
            return out
        else:
            return ""


    def make_composite_model(self,partry=True):
        #return a composite model based on the list of modelclasses
        #restart the building of models and make new try parameters if partry==True
        #print "make_composite_model",self.modelclasses

        if len(self.modelclasses)==0:
            self.composite_model = None
            return None
        else:
            model=self.models[0]
            model.prefix='cv0_'
            self.composite_model = self.models[0]
            if len(self.modelclasses)>=2:
                for k,models in enumerate(self.models[1:],1):
                    model=self.models[k]
                    model.prefix='cv%d_'%k
                    self.composite_model=self.composite_model+model
        if partry:
            self.partry=self.composite_model.make_params()
            self.update_tags(self.tags)
        #print "make_composite_model",self.models

    def add_model_class(self,modelclass,paramvalues=None):
        #ajoute une courbe dans la liste parametres
        self.modelclasses.append(modelclass)
        k=len(self.modelclasses)-1
        model=modelclass(prefix='cv%d_'%k)
        self.models.append(model)
        if k>0:  #there is already a model
            self.composite_model=self.composite_model+model
        else:
            self.composite_model=model

        partry=self.composite_model.make_params()

        for name,value in self.tags.items():    #put new dict
            partry.add(name, value=value, vary=False)  #we reinitialize the tag entries

        for par in self.partry:
            partry[par]=self.partry[par]
        self.partry=partry
        self.ncv=len(self.models)
        return model

    def remove_model_class(self,k):
        correspondance=list(range(k))+[-1]+list(range(k,len(self.modelclasses)-1))
        self.modelclasses.pop(k)
        self.models.pop(k)
        self.make_composite_model(partry=False)
        self.rebuild_partry(correspondance)
        self.ncv=len(self.models)

    def remove_all_model_classes(self):
        self.modelclasses=list()  #list of the Model classes that compose the Model for fitting the data
        self.models=list()  #list of the Model instances that compose the Model for fitting the data
        self.composite_model=None
        self.ncv=len(self.models)
        self.partry=Parameters()   #parametres d'essai
        self.paropt=Parameters()   #parametres optimises

    def convert_model_class(self, i, modelclass):
        model=modelclass(prefix='cv%d_'%i)

        self.modelclasses.pop(i)
        self.models.pop(i)

        self.modelclasses.insert(i, modelclass)
        self.models.insert(i, model)

        self.make_composite_model(partry=False)
        self.rebuild_partry()
        return model

    def moveup_model_class(self,k):
        correspondance=list(range(k-1))+[k,k-1]+list(range(k+1,len(self.modelclasses)))
        modelclass=self.modelclasses.pop(k)
        self.modelclasses.insert(k-1,modelclass)
        model=self.models.pop(k)
        self.models.insert(k-1,model)
        self.make_composite_model(partry=False)
        self.rebuild_partry(correspondance)

    def movedown_model_class(self,k):
        correspondance=list(range(k))+[k+1,k]+list(range(k+2,len(self.modelclasses)))
        modelclass=self.modelclasses.pop(k)
        self.modelclasses.insert(k+1,modelclass)
        model=self.models.pop(k)
        self.models.insert(k+1,model)
        self.make_composite_model(partry=False)
        self.rebuild_partry(correspondance)

    def rebuild_partry(self,correspondance=None):

        if correspondance == None:
            #one of the function has been changed into another. We suppress any reference to parameters that may have been used
            #in expressions
            parameters = self.composite_model.make_params()
            self.update_tags(self.tags)
            suppressed = [entry for entry in self.partry if entry not in parameters]

            for entry in self.partry:
                if entry in parameters:
                    parameters[entry].set(value=self.partry[entry].value,vary=self.partry[entry].vary,min=self.partry[entry].min,max=self.partry[entry].max)
                    #we check that we have no reference to suppressed entries in the expression
                    expr = self.partry[entry].expr
                    if expr is not None:
                        for entry2 in suppressed:
                            if entry2 in expr:
                                #there is a reference to a suppressed entry, we replace in the expression with the value found
                                s = '%g'%self.partry[entry].value
                                expr = expr.replace(entry2,s)
                        parameters[entry].set(expr=expr)
            self.partry = parameters

        else:

            if self.composite_model == None:
                parameters = Parameters()
                self.partry = parameters
                self.update_tags(self.tags)
                return
            parameters = self.composite_model.make_params()
            for old_entry in self.partry:
                k = self.cvnumber(old_entry)
                if k is not None:  #this is a curve
                    if correspondance[k] >= 0:       #otherwise the curve has been suppressed
                        old_txt = 'cv%d'%k
                        new_txt = 'cv%d'%correspondance[k]
                        new_entry = old_entry.replace(old_txt,new_txt)
                        parameters[new_entry].set(value=self.partry[old_entry].value,vary=self.partry[old_entry].vary,min=self.partry[old_entry].min,max=self.partry[old_entry].max)
                else:  #this is a fixed parameter (tag)
                    parameters.add(old_entry, value=self.partry[old_entry].value,vary=self.partry[old_entry].vary,min=self.partry[old_entry].min,max=self.partry[old_entry].max)
            #then we set expressions that also have to be converted
            for old_entry in self.partry:
                j = self.cvnumber(old_entry)
                if j is not None:  #this is a curve
                    if correspondance[j] >= 0:       #otherwise the curve has been suppressed
                        old_txt = 'cv%d'%j
                        new_txt = 'cv%d'%correspondance[j]
                        new_entry = old_entry.replace(old_txt,new_txt)  #new entry
                        new_expr = self.partry[old_entry].expr            #start with old expression
                        if new_expr is not None and len(new_expr)>0:
                            for k in correspondance:
                                old_txt = 'cv%d'%k
                                if old_txt in new_expr:
                                    if correspondance[k] == -1:
                                        #the curve referenced in expression has been removed! we cannot maintain the expression
                                        new_expr = ''
                                    else:
                                        new_txt = 'cv%d'%correspondance[k]
                                        new_expr = new_expr.replace(old_txt,new_txt)
                            parameters[new_entry].set(expr=new_expr)
            self.partry = parameters
            self.update_tags(self.tags)


    """

    def rebuild_partry(self,correspondance):
        #rebuild a set of try parameters from correspondance between indices
        #correspondance is the list of old indices pointing toward new ones or nothing if equal to -1

        #first we set values and restrains
        parameters=Parameters()
        for old_entry in self.partry:
            k = self.cvnumber(old_entry)
            if k is not None:  #this is a curve
                if correspondance[k] >= 0:       #otherwise the curve has been suppressed
                    parameters.add(new_entry, value=self.partry[old_entry].value,vary=self.partry[old_entry].vary,min=self.partry[old_entry].min,max=self.partry[old_entry].max)
            else:  #this is a fixed parameter (tag)
                parameters.add(old_entry, value=self.partry[old_entry].value,vary=self.partry[old_entry].vary,min=self.partry[old_entry].min,max=self.partry[old_entry].max)

        #then we set expressions that also have to be converted
        for old_entry in self.partry:
            j = self.cvnumber(old_entry)
            if j is not None:  #this is a curve
                if correspondance[j] >= 0:       #otherwise the curve has been suppressed
                    old_txt = 'cv%d'%j
                    new_txt = 'cv%d'%correspondance[j]
                    new_entry = old_entry.replace(old_txt,new_txt)  #new entry
                    new_expr = self.partry[old_entry].expr            #start with old expression
                    if new_expr is not None and len(new_expr)>0:
                        for k in correspondance:
                            old_txt = 'cv%d'%k
                            if old_txt in new_expr:
                                if correspondance[k] == -1:
                                    #the curve referenced in expression has been removed! we cannot maintain the expression
                                    new_expr = ''
                                else:
                                    new_txt = 'cv%d'%correspondance[k]
                                    new_expr = new_expr.replace(old_txt,new_txt)
                        parameters[new_entry].set(expr=new_expr)
        self.partry = parameters
        self.update_tags(self.tags)
    """
    """
    def set_shape_from_try(self,shape,k):
        #set aspect of the shape from the try parameters
        shape.set_from_params(self.partry,k)

    def set_try_from_shape(self,shape,k):
        #set try parameters values from the shape
        shape.set_params(self.partry,k)

    def set_shape_from_opt(self,shape,k):
        #set aspect of the shape from the try parameters
        shape.set_from_params(self.paropt,k)
    """

    def set_rawdata(self,rawdata):
        #set the rawdata to be fitted to data, either Raw2DData or Raw1DData instance
        self.rawdata = rawdata
        self.chi2opt = None
        self.isfitted = None
        self.issaved = None
        self.update_tags(rawdata.tags)

    def cvnumber(self,entry):
        if entry.startswith('cv'):
            #return the number of the curve
            return int(entry.split('_')[0][2:])
        else:
            #entry is not a curve parameter
            return None

    def shift_all_curves(self, dx, dy=None):
        for model in self.models:
            if dy == None:
                name = model.prefix+'loc'
                #this is a 1D curve
                try:
                    self.partry[name].value = self.partry[name].value + dx
                except:
                    pass
            else:
                name0 = model.prefix+'loc0'
                name1 = model.prefix+'loc1'
                #this is a 2D curve
                try:
                    self.partry[name0].value = self.partry[name0].value + dx
                    self.partry[name1].value = self.partry[name1].value + dy
                except:
                    pass



    def freezecurve(self,k):
        #freeze the given curve parameters
        for name in self.models[k].param_names():
            self.partry[name].vary=False

    def releasecurve(self,k):
        #release the given curve parameters
        for name in self.models[k].param_names():
            self.partry[name].vary=True

    def update_from_estimate(self,estimate):
        for p,v in estimate.items():
            if p in list(self.partry.keys()):
                self.partry[p].value = v

    def scale_from_estimate(self,estimate):
        for p,change in estimate.items():
            if p in list(self.partry.keys()):
                self.partry[p].value = change[1]*self.partry[p].value+change[0]

    def set_whole_as_try(self):
        for kk in self.partry:
            if kk in self.paropt:
                self.partry[kk].value = self.paropt[kk].value

    def eval_try(self,x):
        if self.composite_model is not None:
            return self.composite_model.eval(self.partry,x=x)

    def eval_opt(self,x):
        return self.composite_model.eval(self.paropt,x=x)

    def do_fit(self,data,x,weights):
        if self.composite_model is None:
            return
        try:
            result=self.composite_model.fit(data,params=self.partry,method=self.method,weights=weights,x=x)
        except Exception as e:
            print(e)
            return False
        self.paropt = result.params
        self.chi2opt = result.chisqr
        if result.success:
            self.isfitted = True
            self.issaved = False
        return True

    def eval_integrals_opt(self):
        #evaluate integrals of the different peaks
        return self.composite_model.eval_component_integrals(self.paropt)

    def clear_results(self):
        #remove all results from the list
        self.results=[]
        self.result_vars=[]

    def append_result(self):
        #append a saved result to the list of previous results
        #construction of two dictionnaries:
        values={kk: self.paropt[kk].value for kk in self.partry}
        sigmas={kk: self.paropt[kk].stderr for kk in self.partry}  #may contains None elements
        self.results.append((values,sigmas))
        self.result_vars += [kk for kk in self.partry if kk not in self.result_vars]

    def remove_result(self,i):
        #remove a saved result from the list of previous results
        self.results.pop(i)
        self.result_vars = []

        for result in self.results:
             self.result_vars += [kk for kk in result[0].keys() if kk not in self.result_vars]

    def recover_previous_result(self, i):
        #set try values with previously saved result values
        values = self.results[i][0]
        for key in values:
            if key in self.partry:
                self.partry[key].set(value = values[key])

    def get_result_values(self,x_label,y_label):
        #return x,y and error on y (sigma) for given (x_label,y_label)
        x=[]
        y=[]
        s=[]
        for result in self.results:
            if x_label in result[0].keys() and y_label in result[0].keys():
                x.append(result[0][x_label])
                y.append(result[0][y_label])
                s.append(result[1][y_label])
        x=np.array(x)
        y=np.array(y)
        if None in s:
            s = None
        else:
            s = np.array(s)
        return x,y,s

    def save_opt_results(self):
        #return lines describing the results that may be saved in a result file
                #we compute the integrals
        #writing names of fitting parameters
        if self.chi2opt is None:
            return

        self.append_result()

        lig="title "
        for kk in self.paropt:
            lig = lig + kk + " "

        integrals = self.eval_integrals_opt()

        print("computation of integrals",integrals)
        #OrderedDict of {prefix: (integral,error)}

        intnames = ''
        intvalues = ''
        interrors = ''

        for prefix in list(integrals.keys()):
            intnames = intnames + prefix + 'integral '
            value = integrals[prefix]
            intvalues = intvalues + '%g '%(value[0])
            interrors = interrors + '%g '%(value[1])


        lig=lig+intnames+'chi2 model \n'

        #writing names of fitting parameters
        title=self.rawdata.title

        lig = lig + title + " "

        for kk in self.partry:
            lig = lig + '%g '%(self.paropt[kk].value)


        lig = lig + intvalues + '%g '%(self.chi2opt)
        model = self.composite_model.name
        model = model.replace(" ", "")

        lig = lig + model + '\n'
        #print lig

        #on ecrit une ligne avec les erreurs
        lig = lig + "sigma "
        for kk in self.partry:
            if self.paropt[kk].stderr is None:
              lig = lig + 'None '
            else:
              lig = lig + '%g '%(self.paropt[kk].stderr)

        lig = lig + interrors + '0. '       #no uncertainty on chi2

        lig = lig + model + '\n'
        return lig

    def add_user_param(self, name, value=0.):
        if name not in list(self.partry.keys()):
            self.partry.add(name, value=value, vary=False)
            self.tags[name] = value

    def update_tags(self,tags):
        #on supprime les entrees precedentes:

        for name,value in self.tags.items():  #remove old list
            if name in list(self.partry.keys()):
                del self.partry[name] #we suppress the old tag entries

        for name,value in tags.items():    #put new list
            self.partry.add(name, value=value, vary=False)  #we reinitialize the tag entries

        self.tags=tags

    def set_expr(self,entry,txt):
        self.params[entry].expr=txt
        if txt not in ['','None']:
            self.params[entry].restrain=2
            print("self.params[",entry,"].restrain=2")

    def set_value(self,entry,txt):
        try:
            val=float(txt)
            self.params[entry].guess_value=val
        except Exception as e:
            QMessageBox.about(None, 'Error',str(e))

    def set_vary(self,entry,txt):
        #call when opening a parameter file
        if txt=='True' or txt=='Free':
            self.params[entry].restrain=0
        elif txt=='False' or txt=='Fixed':
            self.params[entry].restrain=1
        elif txt=='Expr':
            self.params[entry].restrain=2
        else:
            print('unknown vary parameter for entry',entry)


    def set_restrain(self,entry,i):
        if i==2:  #restrain is expression, we verify that expression exists
            txt=self.params[entry].expr
            if txt not in ['','None']:
                self.params[entry].restrain=2
        else:
            self.params[entry].restrain=i

    def set_min(self,entry,txt):
        try:
            val=float(txt)
        except:
            QMessageBox.about(None,"Error","for parameter "+entry+"\nMin is not a valid entry")
            return
        if self.params[entry].max>val:
            self.params[entry].min=val
        else:
            QMessageBox.about(None,"Error","for parameter "+entry+"\nMin > Max")

    def set_max(self,entry,txt):
        try:
            val=float(txt)
        except:
            QMessageBox.about(None,"Error","for parameter "+entry+"\nMax is not a valid entry")
            return
        if self.params[entry].min<val:
            self.params[entry].max=val
        else:
            QMessageBox.about(None,"Error","for parameter "+entry+"\nMax < Min")

    def set_range(self,entry,txt):
        try:
            range=abs(float(txt))
        except:
            QMessageBox.about(None,"Error","for parameter "+entry+"\nRange is not a valid entry")
            return
        val = self.params[entry].guess_value
        self.params[entry].max=val+range
        self.params[entry].min=val-range

    def set_points(self,entry,txt):
        try:
            if txt=='None' or txt=='':
                val=0
            else:
                val=int(txt)
            self.params[entry].points=val
        except:
            QMessageBox.about(None,"Error","for parameter "+entry+"\nPoints is not a valid entry")




if __name__ == "__main__":

    from guidata import qapplication
    _app = qapplication()
    #win = FitDialog()
    param = AddFitDataParam()
    param.update_choices_data([["table1",["x","y","z"]],["table2",["u","v"]]])
    param.edit()



    _app.exec_()
