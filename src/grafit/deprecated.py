"""
Created on Thu Dec  3 12:34:51 2020

@author: prevot
"""


class CurvePeakTool(RectangularActionTool):
    """a tool that changes the position of a CurveShape  for 1D plot
    draw a rectangle, the positions p0,p1 are used for determining the height, position, width of the shape
    using func(plot, p0, p1)"""
    ICON = "gauss.png"
    def __init__(self, manager, func, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 fix_orientation=False, switch_to_default_tool=None):

        super().__init__(manager, func, shape_style=shape_style, toolbar_id=toolbar_id,
                                 title=title, icon=icon, tip=tip, fix_orientation=fix_orientation,
                                 switch_to_default_tool=switch_to_default_tool)

    def setup_filter(self, baseplot):      #partie utilisee pendant le mouvement a la souris
        #utilise a l'initialisation de la toolbar
        #print "setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()
        handler = CurvePeakSelectionHandler(filter, Qt.LeftButton,      #gestionnaire du filtre
                                              start_state=start_state)
        shape, h0, h1, h2, h3, h4 = self.get_shape()
        shape.pen.setColor(QColor("#00bfff"))

        handler.set_shape(shape, h0, h1, h2, h3, h4, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_END_RECT.connect(self.end_rect)

        return setup_standard_tool_filter(filter, start_state)

    def get_shape(self):
        """Reimplemented RectangularActionTool method"""
        shape, h0, h1, h2, h3, h4 = self.create_shape()
        self.setup_shape(shape)
        return shape, h0, h1, h2, h3, h4

    def create_shape(self):
        shape = RectangleCentredShape()
        self.set_shape_style(shape)
        return shape, 0, 1, 2, 3, 4  #give the values of h0,h1,h2,h3 use to move the shape








class MultiPointTool(InteractiveTool):
    TITLE = _("Polyline")
    ICON = "polyline.png"
    CURSOR = Qt.ArrowCursor

    def __init__(self, manager, func, handle_final_shape_cb=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager, toolbar_id,
                                title=title, icon=icon, tip=tip,
                                switch_to_default_tool=switch_to_default_tool)
        self.handle_final_shape_cb = handle_final_shape_cb
        self.shape = None
        self.current_handle = None
        self.init_pos = None
        self.move_func = func
        if shape_style is not None:
            self.shape_style_sect = shape_style[0]
            self.shape_style_key = shape_style[1]
        else:
            self.shape_style_sect = "plot"
            self.shape_style_key = "shape/drag"
        self.npoints = 2

    def set_npoints(self,npoints):
        self.npoints = npoints

    def reset(self):
        self.shape = None
        self.current_handle = None

    def create_shape(self, filter, pt):
        self.shape = CurveShape(closed=False)
        filter.plot.add_item_with_z_offset(self.shape, SHAPE_Z_OFFSET)
        self.shape.setVisible(True)
        self.shape.set_style(self.shape_style_sect, self.shape_style_key)
        self.shape.add_local_point(pt)
        return self.shape.add_local_point(pt)

    def setup_filter(self, baseplot):
        filter = baseplot.filter
        # Initialisation du filtre
        start_state = filter.new_state()
        # Bouton gauche :
        handler = QtDragHandler(filter, Qt.LeftButton, start_state=start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Enter, Qt.Key_Return,
                                         Qt.Key_Space) ),
                         self.validate, start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Backspace,Qt.Key_Escape,) ),
                         self.cancel_point, start_state)
        handler.SIG_START_TRACKING.connect(self.mouse_press)
        handler.SIG_MOVE.connect(self.move)
        handler.SIG_STOP_MOVING.connect(self.mouse_release)
        return setup_standard_tool_filter(filter, start_state)

    def cancel_point(self, filter, event):
        if self.shape is None:
            return
        points = self.shape.get_points()
        if points is None:
            return
        elif len(points) < self.npoints:
            filter.plot.del_item(self.shape)
            self.reset()
        else:
            if self.current_handle:
                newh = self.shape.del_point(self.current_handle)
            else:
                newh = self.shape.del_point(-1)
            self.current_handle = newh
        filter.plot.replot()

    def mouse_press(self, filter, event):
        """We create a new shape if it's the first point
        otherwise we add a new point
        """
        if self.shape is None:
            self.init_pos = event.pos()
            self.current_handle = self.create_shape(filter, event.pos())
        else:
            self.current_handle = self.shape.add_local_point(event.pos())
            if self.current_handle >= self.npoints:
                self.shape.del_point(0)
                self.current_handle = self.npoints-1
        self.move_func(filter.plot,self.shape.points)
        filter.plot.replot()

    def move(self, filter, event):
        """moving while holding the button down lets the user
        position the last created point
        """
        if self.shape is None or self.current_handle is None:
            # Error ??
            return
        self.shape.move_local_point_to(self.current_handle, event.pos())
        self.move_func(filter.plot,self.shape.points)
        filter.plot.replot()

    def mouse_release(self, filter, event):
        """Releasing the mouse button validate the last point position"""
        if self.current_handle is None:
            return
        if self.init_pos is not None and self.init_pos == event.pos():
            self.shape.del_point(-1)
        else:
            self.shape.move_local_point_to(self.current_handle, event.pos())
            if self.current_handle == self.npoints-1:
                self.validate(filter,event)
        self.init_pos = None
        self.current_handle = None
        self.move_func(filter.plot,self.shape.points)
        filter.plot.replot()

    def deactivate(self):
        """Deactivate tool"""
        if self.shape is not None:
            self.shape.detach()
            self.get_active_plot().replot()
            self.reset()
        self.action.setChecked(False)

class CurveTool(InteractiveTool):
    TITLE = _("Polyline")
    ICON = "polyline.png"
    CURSOR = Qt.ArrowCursor

    def __init__(self, manager, func, handle_final_shape_cb=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager, toolbar_id,
                                title=title, icon=icon, tip=tip,
                                switch_to_default_tool=switch_to_default_tool)
        self.handle_final_shape_cb = handle_final_shape_cb
        self.shape = None
        self.current_handle = None
        self.init_pos = None
        self.move_func = func
        if shape_style is not None:
            self.shape_style_sect = shape_style[0]
            self.shape_style_key = shape_style[1]
        else:
            self.shape_style_sect = "plot"
            self.shape_style_key = "shape/drag"
        self.npoints = 2

    def set_npoints(self,npoints):
        self.npoints = npoints

    def reset(self):
        self.shape = None
        self.current_handle = None

    def create_shape(self, filter, pt):
        self.shape = CurveShape(closed=False)
        filter.plot.add_item_with_z_offset(self.shape, SHAPE_Z_OFFSET)
        self.shape.setVisible(True)
        self.shape.set_style(self.shape_style_sect, self.shape_style_key)
        self.shape.add_local_point(pt)
        return self.shape.add_local_point(pt)

    def setup_filter(self, baseplot):
        filter = baseplot.filter
        # Initialisation du filtre
        start_state = filter.new_state()
        # Bouton gauche :
        handler = QtDragHandler(filter, Qt.LeftButton, start_state=start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Enter, Qt.Key_Return,
                                         Qt.Key_Space) ),
                         self.validate, start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Backspace,Qt.Key_Escape,) ),
                         self.cancel_point, start_state)
        handler.SIG_START_TRACKING.connect(self.mouse_press)
        handler.SIG_MOVE.connect(self.move)
        handler.SIG_STOP_MOVING.connect(self.mouse_release)
        return setup_standard_tool_filter(filter, start_state)

    def cancel_point(self, filter, event):
        if self.shape is None:
            return
        points = self.shape.get_points()
        if points is None:
            return
        elif len(points) < self.npoints:
            filter.plot.del_item(self.shape)
            self.reset()
        else:
            if self.current_handle:
                newh = self.shape.del_point(self.current_handle)
            else:
                newh = self.shape.del_point(-1)
            self.current_handle = newh
        filter.plot.replot()

    def mouse_press(self, filter, event):
        """We create a new shape if it's the first point
        otherwise we add a new point
        """
        if self.shape is None:
            self.init_pos = event.pos()
            self.current_handle = self.create_shape(filter, event.pos())
        else:
            self.current_handle = self.shape.add_local_point(event.pos())
            if self.current_handle >= self.npoints:
                self.shape.del_point(0)
                self.current_handle = self.npoints-1
        self.move_func(filter.plot,self.shape.points)
        filter.plot.replot()

    def move(self, filter, event):
        """moving while holding the button down lets the user
        position the last created point
        """
        if self.shape is None or self.current_handle is None:
            # Error ??
            return
        self.shape.move_local_point_to(self.current_handle, event.pos())
        self.move_func(filter.plot,self.shape.points)
        filter.plot.replot()

    def mouse_release(self, filter, event):
        """Releasing the mouse button validate the last point position"""
        if self.current_handle is None:
            return
        if self.init_pos is not None and self.init_pos == event.pos():
            self.shape.del_point(-1)
        else:
            self.shape.move_local_point_to(self.current_handle, event.pos())
            if self.current_handle == self.npoints-1:
                self.validate(filter,event)
        self.init_pos = None
        self.current_handle = None
        self.move_func(filter.plot,self.shape.points)
        filter.plot.replot()

    def deactivate(self):
        """Deactivate tool"""
        if self.shape is not None:
            self.shape.detach()
            self.get_active_plot().replot()
            self.reset()
        self.action.setChecked(False)



class CircularMaskTool(CircularDrawingTool):
    TITLE = _("Circular masking brush")
    ICON = "circle_sponge.png"
    CURSOR = Qt.CrossCursor
    AVOID_NULL_SHAPE = False
    SHAPE_STYLE_SECT = "plot"
    SHAPE_STYLE_KEY = "shape/drag"

    def __init__(self, manager, handle_final_shape_cb=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager,  toolbar_id=toolbar_id,
                                title=title, icon=icon, tip=tip,
                                switch_to_default_tool=switch_to_default_tool)
        self.handle_final_shape_cb = handle_final_shape_cb
        self.shape = None
        self.masked_image = None # associated masked image item
        self.size=10
        self.mode=True

        if shape_style is not None:
            self.shape_style_sect = shape_style[0]
            self.shape_style_key = shape_style[1]
        else:
            self.shape_style_sect = "plot"
            self.shape_style_key = "shape/drag"

    def set_mode(self,mode):
        #set the action mask/unmask
        self.mode=mode

    def start(self, filter, event):
        self.masked_image=self.find_masked_image(filter.plot)
        if self.masked_image is not None:
          x0, y0, x1, y1 = self.shape.get_rect()
          if self.mode:
            self.masked_image.mask_circular_area(x0, y0, x1, y1,trace=False,inside=True)
          else:
            self.masked_image.unmask_circular_area(x0, y0, x1, y1,trace=False,inside=True)

          self.masked_image.plot().replot()

    def find_masked_image(self, plot):
        item = plot.get_active_item()
        if isinstance(item, MaskedImageItem):
            return item
        else:
            items = [item for item in plot.get_items()
                     if isinstance(item, MaskedImageItem)]
            if items:
                return items[-1]

    def move(self, filter, event):
        """moving while holding the button down lets the user
        position the last created point
        """
        if self.masked_image is not None:
          #mask = self.masked_image.get_mask()
          x0, y0, x1, y1 = self.shape.get_rect()
          if self.mode:
            self.masked_image.mask_circular_area(x0, y0, x1, y1,trace=False,inside=True)
          else:
            self.masked_image.unmask_circular_area(x0, y0, x1, y1,trace=False,inside=True)
          self.masked_image.plot().replot()
        """
                x0, y0, x1, y1 = shape.get_rect()
                self.masked_image.mask_circular_area(x0, y0, x1, y1,inside=inside)
        """
