"""
Created on Mon Aug 31 09:50:35 2020

@author: prevot
"""
import numpy as np
from scipy.ndimage import laplace,gaussian_filter

from guidata.configtools import get_icon
from qtpy.QtCore import Signal,QSize,Qt,QPoint,QPointF
from qtpy.QtWidgets import (QCheckBox,QVBoxLayout,QHBoxLayout,QGridLayout,QLabel,QWidget,QSlider,
                              QSpinBox,QDoubleSpinBox,QPushButton,QButtonGroup,QMessageBox,QToolBar,QGroupBox)
from qtpy.QtGui import QPolygonF
from guiqwt.baseplot import canvas_to_axes
#from guiqwt.config import _
from guiqwt.curve import ErrorBarCurveItem
from guiqwt.events import DragHandler,RectangularSelectionHandler,setup_standard_tool_filter,QtDragHandler,KeyEventMatch
from guiqwt.image import MaskedImageItem
from guiqwt.interfaces import IPanel
from guiqwt.tools import (CommandTool,DefaultToolbarID,InteractiveTool,MultiLineTool,
                          CircleTool,RectangularShapeTool,SHAPE_Z_OFFSET)
from guiqwt.panels import PanelWidget
from guiqwt.shapes import Marker,PointShape,RectangleShape,PolygonShape
from scipy.signal import fftconvolve
from guiqwt.config import CONF
from guiqwt.styles import ShapeParam

from grafit.plotitems import MaskedImageNan, MaskedErrorBarCurveItem, TrueEllipseShape
import weakref

ID_IMAGEMASKING = "image masking"
ID_CURVEMASKING = "curve masking"

def _(text):
  return text



"""we redefine the masking functions outside the MaskedImage definition"""

def mask_image_rectangular_area(image, shape, inside=True,
                          trace=True, do_signal=True):
    """
    Mask rectangular area
    If inside is True (default), mask the inside of the area
    Otherwise, mask the outside
    """
    x0, y0, x1, y1 = shape.get_rect()
    ix0, iy0, ix1, iy1 = image.get_closest_index_rect(x0, y0, x1, y1)
    if inside:
        image.data[iy0:iy1, ix0:ix1] = np.ma.masked
    else:
        indexes = np.ones(image.data.shape, dtype=np.bool)
        indexes[iy0:iy1, ix0:ix1] = False
        image.data[indexes] = np.ma.masked
    if trace:
        image.add_masked_area('rectangular', shape.get_points(), inside, mask=True)
    if do_signal:
        image._mask_changed()

def mask_image_elliptical_area(image, shape, inside=True,
                       trace=True, do_signal=True):
    """
    Mask elliptical area a, b, th, are the axes and angle of the ellipse, (x1,y1 and x2,y2) the bounding rectangle

    If inside is True (default), mask the inside of the area
    Otherwise, mask the outside
    """
    #print ('mask_image_elliptical_area')

    a,b,th = shape.get_sizes_and_angle()
    x1, y1, x2, y2 = shape.get_bounding_rectangle()
    #print ('shape.sizes_and_angle',a,b,th)
    #print ('shape bounding_rectangle',x1,y1,x2,y2)

    x0 = (x1+x2)/2.  #center of ellipse
    y0 = (y1+y2)/2.
    ix1, iy1, ix2, iy2 = image.get_closest_index_rect(x1, y1, x2, y2)
    x_grid,y_grid = image.get_x_values(ix1, ix2)-x0, image.get_y_values(iy1, iy2)-y0
    x_grid, y_grid =  np.meshgrid(x_grid,y_grid)  #kind of transposition since first index is for column

    c = np.cos(th)
    s = np.sin(th)

    test = ((x_grid*c+y_grid*s)/a)**2 + ((x_grid*s-y_grid*c)/b)**2 < 0.25
    if inside:
        image.data[iy1:iy2, ix1:ix2] = np.ma.masked_where(test,image.data[iy1:iy2, ix1:ix2])    #there was a problem with use of copy=False
    else:
        image.data[iy1:iy2, ix1:ix2] = np.ma.masked_where(~test,image.data[iy1:iy2, ix1:ix2])
        indexes = np.ones(image.data.shape, dtype=np.bool)
        indexes[iy1:iy2, ix1:ix2] = False
        image.data[indexes] = np.ma.masked   #values are masked outside the rectangle

    if trace:
        image.add_masked_area('elliptical', shape.get_points(), inside, mask=True)
    if do_signal:
        image._mask_changed()

def mask_image_polygonal_area(image, shape, inside=True,
                       trace=True, do_signal=True):
    """
    Mask polygonal area, inside the rectangle (x0, y0, x1, y1)
    #points is a np array of the polygon points
    """
    pts = shape.get_points()
    x0,y0=np.min(pts,axis=0)
    x1,y1=np.max(pts,axis=0)

    #we construct a QpolygonF to use the containsPoint function of PyQt
    poly = QPolygonF()

    for i in range(pts.shape[0]):
        poly.append(QPointF(pts[i, 0], pts[i, 1]))

    ix0, iy0, ix1, iy1 = image.get_closest_index_rect(x0, y0, x1, y1)

    xdata, ydata = image.get_x_values(ix0, ix1), image.get_y_values(iy0, iy1)  #values in the axis referential
    for ix in range(ix0, ix1):
        for iy in range(iy0, iy1):
            inside_poly = poly.containsPoint(QPointF(xdata[ix-ix0],ydata[iy-iy0]), Qt.OddEvenFill)

            if inside:
                if inside_poly:
                    image.data[iy, ix] = np.ma.masked
            elif not inside_poly:
                image.data[iy, ix] = np.ma.masked
    if not inside:
        indexes = np.ones(image.data.shape, dtype=np.bool)
        indexes[iy0:iy1, ix0:ix1] = False
        image.data[indexes] = np.ma.masked
    if trace:
        image.add_masked_area('polygonal', pts, inside, mask=True)
    if do_signal:
        image._mask_changed()

def unmask_image_rectangular_area(image, shape, inside=True,trace=True,do_signal=True):
    """mask a rectangular area in the curve"""
    if not np.ma.is_masked(image.data):
        return
    x0, y0, x1, y1 = shape.get_rect()
    ix0, iy0, ix1, iy1 = image.get_closest_index_rect(x0, y0, x1, y1)
    if inside:
        image.data.mask[iy0:iy1, ix0:ix1] = False
    else:
        indexes = np.ones(image.data.shape, dtype=np.bool)
        indexes[iy0:iy1, ix0:ix1] = False
        image.data.mask[indexes] = False
    if trace:
        image.add_masked_area('rectangular', shape.get_points(), inside, mask=True)
    if do_signal:
        image._mask_changed()

def unmask_image_elliptical_area(image, shape, inside=True,
                       trace=True, do_signal=True):
    """
    Mask elliptical area a, b, th, are the axes and angle of the ellipse, (x1,y1 and x2,y2) the bounding rectangle

    If inside is True (default), mask the inside of the area
    Otherwise, mask the outside
    """
    if not np.ma.is_masked(image.data):
        return
    a,b,th = shape.get_sizes_and_angle()
    x1, y1, x2, y2 = shape.get_bounding_rectangle()


    x0 = (x1+x2)/2.  #center of ellipse
    y0 = (y1+y2)/2.
    ix1, iy1, ix2, iy2 = image.get_closest_index_rect(x2, y1, 2*x0-x2, 2*y0-y1)
    x_grid,y_grid = image.get_x_values(ix1, ix2)-x0, image.get_y_values(iy1, iy2)-y0
    x_grid, y_grid =  np.meshgrid(x_grid,y_grid)  #kind of transposition since first index is for column

    c = np.cos(th)
    s = np.sin(th)

    test = ((x_grid*c+y_grid*s)/a)**2 + ((x_grid*s-y_grid*c)/b)**2 < 0.25
    if inside:
        indexes = np.zeros(image.data[iy1:iy2, ix1:ix2].shape, dtype=np.bool)
        indexes[test] = True
        image.data[iy1:iy2, ix1:ix2].mask[indexes] = False
    else:
        indexes = np.ones(image.data[iy1:iy2, ix1:ix2].shape, dtype=np.bool)
        indexes[test] = False
        image.data[iy1:iy2, ix1:ix2].mask[indexes] = False

        indexes = np.ones(image.data.shape, dtype=np.bool)
        indexes[iy1:iy2, ix1:ix2] = False
        image.data.mask[indexes] = False

    if trace:
        image.add_masked_area('elliptical', shape.get_points(), inside, mask=True)
    if do_signal:
        image._mask_changed()


def unmask_image_polygonal_area(image, shape, inside=True,
                       trace=True, do_signal=True):
    """
    Unmask polygonal area, inside the polygon
    points is a np array of the polygon points
    """
    if not np.ma.is_masked(image.data):
        return
    pts = shape.get_points()
    x0,y0=np.min(pts,axis=0)
    x1,y1=np.max(pts,axis=0)

    #we construct a QpolygonF to use the containsPoint function of PyQt
    poly = QPolygonF()

    for i in range(pts.shape[0]):
        poly.append(QPointF(pts[i, 0], pts[i, 1]))

    ix0, iy0, ix1, iy1 = image.get_closest_index_rect(x0, y0, x1, y1)

    xdata, ydata = image.get_x_values(ix0, ix1), image.get_y_values(iy0, iy1)  #values in the axis referential
    for ix in range(ix0, ix1):
        for iy in range(iy0, iy1):
            inside_poly = poly.containsPoint(QPointF(xdata[ix-ix0],ydata[iy-iy0]), Qt.OddEvenFill)

            if inside:
                if inside_poly:
                    image.data.mask[iy, ix] = False
            elif not inside_poly:
                image.data.mask[iy, ix] = False
    if not inside:
        indexes = np.ones(image.data.shape, dtype=np.bool)
        indexes[iy0:iy0, ix0:ix0] = False
        image.data.mask[indexes] = False
    if trace:
        image.add_masked_area('polygonal', pts, inside, mask=False)
    if do_signal:
        image._mask_changed()



def mask_curve_rectangular_area(curve, shape, inside=True,trace=True,do_signal=True):
    """mask a rectangular area in the curve"""
    x0, y0, x1, y1 = shape.get_rect()
    if x0 > x1:
        x0, x1 = x1, x0
    if y0 > y1:
        y0, y1 = y1, y0
    curve
    if not np.ma.is_masked(curve.masked_x):
        #if type(curve.masked_x.mask) is numpy.bool_, np.ma.masked_where does not apply
        curve.masked_x.mask = False

    condition = np.logical_and(np.logical_and(curve._x <= x1,curve._x >= x0),np.logical_and(curve._y <= y1,curve._y >= y0))
    if inside:
        np.ma.masked_where(condition, curve.masked_x,copy=False)
    else:
        np.ma.masked_where(~condition, curve.masked_x, copy=False)

    if do_signal:
        curve._mask_changed()
    if trace:
        curve.add_masked_area('rectangular', shape.get_points(), inside, mask=True)

def unmask_curve_rectangular_area(curve, shape, inside=True,trace=True,do_signal=True):
    """mask a rectangular area in the curve"""
    x0, y0, x1, y1 = shape.get_rect()
    if x0 > x1:
        x0, x1 = x1, x0
    if y0 > y1:
        y0, y1 = y1, y0

    condition = np.logical_and(np.logical_and(curve._x <= x1,curve._x >= x0),np.logical_and(curve._y <= y1,curve._y >= y0))
    if inside:
        curve.masked_x.mask = np.logical_and(curve.masked_x.mask,~condition)
    else:
        curve.masked_x.mask = np.logical_and(curve.masked_x.mask,condition)

    if do_signal:
        curve._mask_changed()
    if trace:
        curve.add_masked_area('rectangular', shape.get_points(), inside, mask=False)

def mask_curve_elliptical_area(curve, shape, inside=True,
                       trace=True, do_signal=True):
    """
    Mask elliptical area a, b, th, are the axes and angle of the ellipse, (x1,y1 and x2,y2) the bounding rectangle

    If inside is True (default), mask the inside of the area
    Otherwise, mask the outside
    """
    a,b,th = shape.get_sizes_and_angle()
    x1, y1, x2, y2 = shape.get_bounding_rectangle()

    c = np.cos(th)
    s = np.sin(th)

    x0 = (x1+x2)/2.  #center of ellipse
    y0 = (y1+y2)/2.
    for i,xi in enumerate(curve._x):
        yi = curve._y[i]
        u = xi-x0
        v = yi-y0
        inside_ellipse = ((u*c+v*s)/a)**2 + ((u*s-v*c)/b)**2 < 0.25
        if inside:
            if inside_ellipse:
                curve.masked_x[i] = np.ma.masked
        else:
            if ~inside_ellipse:
                curve.masked_x[i] = np.ma.masked

    if trace:
        curve.add_masked_area('elliptical', shape.get_points(), inside, mask=True)
    if do_signal:
        curve._mask_changed()

def unmask_curve_elliptical_area(curve,shape, inside=True,
                       trace=True, do_signal=True):
    """
    Unmask elliptical area a, b, th, are the axes and angle of the ellipse, (x1,y1 and x2,y2) the bounding rectangle

    If inside is True (default), mask the inside of the area
    Otherwise, mask the outside
    """
    a,b,th = shape.get_sizes_and_angle()
    x1, y1, x2, y2 = shape.get_bounding_rectangle()
    c = np.cos(th)
    s = np.sin(th)

    x0 = (x1+x2)/2.  #center of ellipse
    y0 = (y1+y2)/2.
    for i,xi in enumerate(curve._x):
        yi = curve._y[i]
        u = xi-x0
        v = yi-y0
        inside_ellipse = ((u*c+v*s)/a)**2 + ((u*s-v*c)/b)**2 < 0.25
        if inside:
            if inside_ellipse:
                curve.masked_x.mask[i] = False
        else:
            if ~inside_ellipse:
                curve.masked_x.mask[i] = False

    if trace:
        curve.add_masked_area('elliptical', shape.get_points(), inside, mask=False)
    if do_signal:
        curve._mask_changed()

def mask_curve_polygonal_area(curve, shape, inside=True,
                       trace=True, do_signal=True):
    """
    mask the curve points inside or outside a region defined by a PolygonShape
    """
    pts = shape.points
    x0,y0=np.min(pts,axis=0)
    x1,y1=np.max(pts,axis=0)

    #we construct a QpolygonF to use the containsPoint function of PyQt
    poly = QPolygonF()

    for i in range(pts.shape[0]):
        poly.append(QPointF(pts[i, 0], pts[i, 1]))

    for i,xi in enumerate(curve._x):
        yi = curve._y[i]
        inside_poly = poly.containsPoint(QPointF(xi,yi), Qt.OddEvenFill)
        if inside:
            if inside_poly:
                curve.masked_x[i] = np.ma.masked
        else:
            if ~inside_poly:
                curve.masked_x[i] = np.ma.masked

    if trace:
        curve.add_masked_area('polygonal', pts, inside, mask=True)
    if do_signal:
        curve._mask_changed()
    if trace:
        curve.add_masked_area('polygonal', pts, inside, mask=True)

def unmask_curve_polygonal_area(curve, shape, inside=True,
                       trace=True, do_signal=True):
    """
    unmask the curve points inside or outside a region defined by a PolygonShape
    """
    pts = shape.points

    x0,y0=np.min(pts,axis=0)
    x1,y1=np.max(pts,axis=0)

    if not curve.plot():
        return

    #we construct a QpolygonF to use the containsPoint function of PyQt
    poly = QPolygonF()

    for i in range(pts.shape[0]):
        poly.append(QPointF(pts[i, 0], pts[i, 1]))

    for i,xi in enumerate(curve._x):
        yi = curve._y[i]
        inside_poly = poly.containsPoint(QPointF(xi,yi), Qt.OddEvenFill)
        if inside:
            if inside_poly:
                curve.masked_x.mask[i] = False
        else:
            if ~inside_poly:
                curve.masked_x.mask[i] = False

    if trace:
        curve.add_masked_area('polygonal', pts, inside, mask=False)
    if do_signal:
        curve._mask_changed()







class DoubleSpinSliderBox(QWidget):
  #a convenient widget that combines a slider and a double spinbox

  SIG_DOUBLE_VALUE_CHANGED=Signal(float)
  SIG_SLIDER_PRESSED=Signal()
  SIG_SLIDER_RELEASED=Signal()

  def __init__ (self, parent = None):
    QWidget.__init__(self,parent=parent)

    self.label = QLabel(self)
    self.slider = QSlider(self)
    self.slider.setOrientation(0x1)
    self.slider.setMinimum(0)
    self.slider.setMaximum(100)
    self.spinbox = QDoubleSpinBox(self)
    self.setRange(0.,100.)
    self.setSingleStep(1.)
    self.setSingleStep(1.)

    hBox=QHBoxLayout(self)
    hBox.addWidget(self.label)
    hBox.addWidget(self.slider)
    hBox.addWidget(self.spinbox)

    self.spinbox.valueChanged.connect(self.update_from_spinbox)
    self.slider.valueChanged.connect(self.update_from_slider)
    self.slider.sliderPressed.connect(self.SIG_SLIDER_PRESSED.emit)
    self.slider.sliderReleased.connect(self.SIG_SLIDER_RELEASED.emit)

  def setText(self,text):
    self.label.setText(text)

  def setRange(self,_min,_max):
    if _max>_min:
      self.spinbox.setRange(_min,_max)
      self._min = _min
      self._max = _max
      self.scale = 100./(self._max-self._min)


  def setSingleStep(self,step):
    self.spinbox.setSingleStep(step)

  def setDecimals(self,i):
    self.spinbox.setDecimals(i)

  def update_from_spinbox(self,x):
    self.slider.blockSignals(True)
    i=int((x-self._min)*self.scale)
    self.slider.setValue(i)
    self.slider.blockSignals(False)
    self.SIG_SLIDER_PRESSED.emit()
    self.SIG_DOUBLE_VALUE_CHANGED.emit(x)
    self.SIG_SLIDER_RELEASED.emit()

  def update_from_slider(self,i):
    self.spinbox.blockSignals(True)
    x=float(i)/self.scale+self._min
    self.spinbox.setValue(x)
    self.spinbox.blockSignals(False)
    self.SIG_DOUBLE_VALUE_CHANGED.emit(x)

  def setValue(self,x):
    self.spinbox.blockSignals(True)
    self.slider.blockSignals(True)
    self.spinbox.setValue(x)
    i=int((x-self._min)*self.scale)
    self.slider.setValue(i)
    self.slider.blockSignals(False)
    self.spinbox.blockSignals(False)
    if x!=self.value():
      self.SIG_SLIDER_PRESSED.emit()
      self.SIG_DOUBLE_VALUE_CHANGED.emit(x)
      self.SIG_SLIDER_RELEASED.emit()

  def setValueWithinMax(self,x):
     #set the value to the max of x and actual value
     self.setValue(max(x,self.value()))

  def setValueWithinMin(self,x):
     #set the value to the min of x and actual value
     self.setValue(min(x,self.value()))

  def value(self):
    return self.spinbox.value()

def couple_doublespinsliders(spsl1,spsl2):
    #couple together two DoubleSpinSliderBox spsl1 and spsl2
    spsl1.SIG_DOUBLE_VALUE_CHANGED.connect(spsl2.setValueWithinMax)
    spsl2.SIG_DOUBLE_VALUE_CHANGED.connect(spsl1.setValueWithinMin)
    #spsl1.SIG_DOUBLE_VALUE_CHANGED.connect(lambda x:spsl2.setValue(max(x,spsl2.value())))
    #spsl2.SIG_DOUBLE_VALUE_CHANGED.connect(lambda x:spsl1.setValue(min(x,spsl1.value())))


class MoveHandler(DragHandler):
    #class of Handler for managing mouse events for masking tools

    #: Signal emitted by MoveHandler on mouse click
    SIG_START_TRACKING = Signal("PyQt_PyObject", "QEvent")

    #: Signal emitted by MoveHandler on mouse release
    SIG_STOP_TRACKING = Signal("PyQt_PyObject", "QEvent")

    #: Signal emitted by MoveHandler on mouse move
    SIG_MOVE = Signal("PyQt_PyObject", "QEvent")

    #: Signal emitted by MoveHandler on mouse release after move
    SIG_STOP_MOVING = Signal("PyQt_PyObject", "QEvent")

    def __init__(self, filter, btn, mods=Qt.NoModifier, start_state=0):
        super().__init__(filter, btn, mods,
                                                          start_state)
        self.avoid_null_shape = False

    def set_shape(self, shape, setup_shape_cb=None, avoid_null_shape=False):
        #called at initialization of the tool
        self.shape = shape
        self.setup_shape_cb = setup_shape_cb
        self.avoid_null_shape = avoid_null_shape

    def start_tracking(self, filter, event):
        self.shape.attach(filter.plot)
        self.shape.setZ(filter.plot.get_max_z()+1)
        if self.avoid_null_shape:
            self.start -= QPoint(1, 1)
        pt=event.pos()
        self.shape.set_local_center(pt)
        if self.setup_shape_cb is not None:
            self.setup_shape_cb(self.shape)
        self.shape.show()
        filter.plot.replot()
        self.SIG_START_TRACKING.emit(filter, event)

    def stop_tracking(self, filter, event):  #when the mouse press button is released before moving
        self.shape.detach()
        self.SIG_STOP_TRACKING.emit(filter, event)
        filter.plot.replot()

    def start_moving(self, filter, event):
        pass

    def move(self, filter, event):
        pt=event.pos()
        self.shape.set_local_center(pt)
        self.SIG_MOVE.emit(filter, event)
        filter.plot.replot()

    def stop_moving(self, filter, event): #when the mouse press button is released after moving
        self.SIG_STOP_TRACKING.emit(filter, event)
        self.shape.detach()
        filter.plot.replot()

class EllipseSelectionHandler(RectangularSelectionHandler):

    #: Signal emitted by RectangularSelectionHandler when ending selection
    SIG_END_RECT = Signal(object, object)

    def __init__(self, filter, btn, mods=Qt.NoModifier, start_state=0):
        super().__init__(filter, btn, mods,start_state)
        filter.add_event(self.state1, filter.mouse_move(btn, Qt.ShiftModifier), self.move, self.state1)
        filter.add_event(self.state1, filter.mouse_release(btn, Qt.ShiftModifier), self.stop_moving, start_state)

    def start_moving(self, filter, event):
        self.shape.attach(filter.plot)
        self.shape.setZ(filter.plot.get_max_z()+1)
        if self.avoid_null_shape:
            self.start -= QPointF(1, 1)
        self.shape.set_from_drawing_points(filter.plot,self.start,event.pos())
        self.start_moving_action(filter, event)
        if self.setup_shape_cb is not None:
            self.setup_shape_cb(self.shape)
        self.shape.show()
        filter.plot.replot()

    def move(self, filter, event):
        """methode surchargee par la classe """
        keep_b_axis = event.modifiers() == Qt.ShiftModifier
        self.shape.set_from_drawing_points(filter.plot,self.start,event.pos(),keep_b_axis)
        self.move_action(filter, event)
        filter.plot.replot()

    def set_shape(self, shape, h0, h1, h2, h3,
                  setup_shape_cb=None, avoid_null_shape=False):
        self.shape = shape
        self.shape_h0 = h0
        self.shape_h1 = h1
        self.shape_h2 = h2
        self.shape_h3 = h3
        self.setup_shape_cb = setup_shape_cb
        self.avoid_null_shape = avoid_null_shape

    def stop_moving_action(self, filter, event):
        """Les classes derivees peuvent surcharger cette methode"""
        self.SIG_END_RECT.emit(filter, self.shape)



class RectangleCentredShape(RectangleShape):
    CLOSED = True
    def __init__(self, xc=0, yc=0, hsize=1, vsize=1, shapeparam=None):
        super().__init__(shapeparam=shapeparam)
        self.is_ellipse = False
        self.hsize=hsize
        self.vsize=vsize
        self.set_center(xc,yc)

    def set_size(self,hsize,vsize):
        self.hsize=hsize
        self.vsize=vsize
        x0, y0, x1, y1=self.get_rect()
        xc=(x0+x1)/2.
        yc=(y0+y1)/2.
        self.set_center(xc,yc)

    def set_local_center(self,pos):
        #set center from position in canvas units
        xc,yc = canvas_to_axes(self, pos)
        self.set_center(xc,yc)

    def set_center(self,xc,yc):
        x0=xc-self.hsize/2.
        y0=yc-self.vsize/2.
        x1=xc+self.hsize/2.
        y1=yc+self.vsize/2.
        self.set_rect(x0, y0, x1, y1)

class EllipseCentredShape(TrueEllipseShape):
    """a centered ellipse used for masking with drawing tool, correctly drawn, with axes along x and y directions"""
    CLOSED = True
    def __init__(self, xc=0, yc=0, hsize=1, vsize=1, shapeparam=None):
        super().__init__(shapeparam=shapeparam)
        self.is_ellipse = True
        self.hsize=hsize
        self.vsize=vsize
        self.set_center(xc,yc)

    def set_from_drawing_points(self, plot, p0, p1):
        #the shape has been drawn with a tool from p0 to p1, we set its value
        loc0,loc1 = canvas_to_axes(self, p0)
        x1,y1 = canvas_to_axes(self, p1)

        width0,width1 = 2.*abs(x1-loc0),2*abs(y1-loc1)
        self.set_from_center_and_width(loc0,loc1,width0,width1)

    def set_size(self,hsize,vsize):
        self.hsize=hsize
        self.vsize=vsize
        x0, y0, x1, y1=self.get_rect()
        xc=(x0+x1)/2.
        yc=(y0+y1)/2.
        self.set_center(xc,yc)

    def set_local_center(self,pos):
        #set center from position in canvas units
        xc,yc = canvas_to_axes(self, pos)
        self.set_center(xc,yc)

    def set_center(self,xc,yc):
        x0=xc-self.hsize/2.
        y0=yc-self.vsize/2.
        x1=xc+self.hsize/2.
        y1=yc+self.vsize/2.
        self.set_points([(x0, yc), (x1, yc), (xc, y0), (xc, y1)])

    def set_rect(self,x0, y0, x1, y1):
        #we rewrite set_rect to draw an ellipse in the horizontal rectangle given by its two corners
        xc=(x0+x1)/2.
        yc=(y0+y1)/2.
        self.set_points([(x0, yc), (x1, yc), (xc, y0), (xc, y1)])

    def get_rect(self):
        x0 = self.points[0][0]
        x1 = self.points[1][0]
        y0 = self.points[2][1]
        y1 = self.points[3][1]
        return x0,y0,x1,y1


class DrawingTool(InteractiveTool):
    """an interactive tool that move a virtual shape on the screen and do the associated functions"""
    """start_func is operated on mousePress, move_func is operated on mouseMove, stop_func is operated on mouseRelease"""
    TITLE = _("Drawing")
    ICON = "pencil.png"
    CURSOR = Qt.CrossCursor
    AVOID_NULL_SHAPE = False

    def __init__(self, manager, start_func=None, move_func=None, stop_func=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager, toolbar_id,
                                title=title, icon=icon, tip=tip,
                                switch_to_default_tool=switch_to_default_tool)
        self.shape = None
        self.shapes = {}  #dictionary of shapes used for each plot

        if start_func is None:
            self.start_func = self.do_nothing
        else:
            self.start_func = start_func

        if move_func is None:
            self.move_func = self.do_nothing
        else:
            self.move_func = move_func

        if stop_func is None:
            self.stop_func = self.do_nothing
        else:
            self.stop_func = stop_func

        if shape_style is not None:
            self.shape_style_sect = shape_style[0]
            self.shape_style_key = shape_style[1]
        else:
            self.shape_style_sect = "plot"
            self.shape_style_key = "shape/drag"

    def reset(self):
        self.shape = None
        self.current_handle = None

    def setup_shape(self, shape):
        """To be reimplemented"""
        shape.setTitle(self.TITLE)

    def create_shape(self):
        shape = PointShape(0,0)
        self.setup_shape(shape)
        return shape

    def set_shape_style(self, shape):
        shape.set_style(self.shape_style_sect, self.shape_style_key)

    def setup_filter(self, baseplot):
        #filter set for each plot registered to the manager)
        #print self,baseplot,"setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()

        self.shape = self.get_shape()
        self.shapes[baseplot]=self.shape

        handler = MoveHandler(filter, Qt.LeftButton,
                                              start_state=start_state)
        handler.set_shape(self.shape, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_START_TRACKING.connect(self.start)
        handler.SIG_MOVE.connect(self.move)
        handler.SIG_STOP_MOVING.connect(self.stop)
        handler.SIG_STOP_TRACKING.connect(self.stop)
        return setup_standard_tool_filter(filter, start_state)

    def get_shape(self):
        """Reimplemented RectangularActionTool method"""
        shape = self.create_shape()
        self.setup_shape(shape)
        return shape

    def validate(self, filter, event):
        super().validate(filter, event)
        if self.handle_final_shape_cb is not None:
            self.handle_final_shape_cb(self.shape)
        self.reset()

    def start(self, filter, event):
        self.shape = self.shapes[filter.plot]
        self.start_func(self.shape)

    def move(self, filter, event):
        self.move_func(self.shape)

    def stop(self, filter, event):
        self.stop_func(self.shape)

    def do_nothing(*args):
        pass


class RectangleDrawingTool(DrawingTool):
    """an interactive tool that move a virtual centered rectangle on the screen and do the associated functions"""
    TITLE = _("Drawing")
    ICON = "brush.jpg"
    hpixelsize=1   #size in image pixels
    vpixelsize=1
    hsize=1  #size in plot coordinates
    vsize=1
    hres=1  #resolution of the image
    vres=1

    def set_pixel_size(self,hpixelsize,vpixelsize=None):
        #set the size in image.data coordinates
        self.hpixelsize=hpixelsize
        if vpixelsize is None:
            self.vpixelsize=hpixelsize  #square shape
        else:
            self.vpixelsize=vpixelsize
        self.set_size()

    def set_resolution(self,hres,vres):
        #set the size of the tool for a image with resolution hres,vres
        #size in plot coordinates
        self.hres = hres
        self.vres = vres
        self.set_size()

    def set_size(self):
        #from resolution and pixelsize, get the size of the tool shape in plot coordinates
        #we subtract 0.5 to the pixelsize since pixels are masked as soon as part of the shape enters in them
        self.hsize=(self.hpixelsize-0.5)*self.hres
        self.vsize=(self.vpixelsize-0.5)*self.vres
        #shapes are registred for each plot so we need to update all shapes
        for shape in list(self.shapes.values()):
          if shape is not None:
              shape.set_size(self.hsize,self.vsize)

    def create_shape(self):
        shape = RectangleCentredShape(0,0,self.hsize,self.vsize)
        self.setup_shape(shape)
        return shape

class EllipseDrawingTool(RectangleDrawingTool):
    """an interactive tool that move a virtual centered ellipse on the screen and do the associated functions"""
    def create_shape(self):
        shape = EllipseCentredShape(0,0,self.hsize,self.vsize)
        self.setup_shape(shape)
        return shape

class RectangleMaskTool(RectangleDrawingTool):
    TITLE = _("Square masking brush")
    ICON = "square_sponge.png"
    CURSOR = Qt.CrossCursor
    AVOID_NULL_SHAPE = False
    SHAPE_STYLE_SECT = "plot"
    SHAPE_STYLE_KEY = "shape/drag"


class EllipseMaskTool(EllipseDrawingTool):
    TITLE = _("Ellipse masking brush")
    ICON = "circle_sponge.png"
    CURSOR = Qt.CrossCursor
    AVOID_NULL_SHAPE = False
    SHAPE_STYLE_SECT = "plot"
    SHAPE_STYLE_KEY = "shape/drag"


class EllipseTool(RectangularShapeTool):
    """a tool that draws an ellipse on a plot"""
    TITLE = _("Ellipse")
    ICON = "ellipse_shape.png"

    def handle_final_shape(self, shape):
        super().handle_final_shape(shape)

    def setup_filter(self, baseplot):      #partie utilisee pendant le mouvement a la souris
        #utilise a l'initialisation de la toolbar
        #print "setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()
        handler = EllipseSelectionHandler(filter, Qt.LeftButton,      #gestionnaire du filtre
                                              start_state=start_state)
        shape, h0, h1, h2, h3 = self.get_shape()

        handler.set_shape(shape, h0, h1, h2, h3, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_END_RECT.connect(self.end_rect)

        return setup_standard_tool_filter(filter, start_state)

    def get_shape(self):
        """Reimplemented RectangularActionTool method"""
        shape, h0, h1, h2, h3 = self.create_shape()
        self.setup_shape(shape)
        return shape, h0, h1, h2, h3

    def get_final_shape(self, plot, handlershape):
        shape, h0, h1, h2, h3 = self.create_shape()
        self.setup_shape(shape)
        plot.add_item_with_z_offset(shape, SHAPE_Z_OFFSET)
        shape.set_points(handlershape.points)
        self.last_final_shape = weakref.ref(shape)
        return shape

    def add_shape_to_plot(self, plot, handlershape):
        """
        Method called when shape's ellipse
        has just been drawn on screen.
        Adding the final shape to plot and returning it.
        modified to keep the shape drawn by handler
        """
        shape = self.get_final_shape(plot, handlershape)
        self.handle_final_shape(shape)
        plot.replot()

    def end_rect(self, filter, shape):
        plot = filter.plot

        self.action_func(plot, shape)

        self.SIG_TOOL_JOB_FINISHED.emit()
        if self.switch_to_default_tool:
            shape = self.get_last_final_shape()
            plot.set_active_item(shape)


    def create_shape(self):
        shape = TrueEllipseShape(0, 0, 1, 1)
        self.set_shape_style(shape)
        return shape, 0, 1, 2, 3  #give the values of h0,h1,h2,h3 use to move the shape

class RectangleTool(RectangularShapeTool):
    """a tool that draws a rectangle on a plot"""
    TITLE = _("Rectangle")
    ICON = "rectangle.png"


class FreeFormTool(MultiLineTool):
    TITLE = _("Free form")
    ICON = "freeform.png"

    def cancel_point(self, filter, event):
        """Reimplement base class method"""
        super().cancel_point(filter, event)
        if self.shape is not None:
            self.shape.closed = len(self.shape.points) > 2

    def mouse_press(self, filter, event):
        """Reimplement base class method"""
        super().mouse_press(filter, event)
        self.shape.closed = len(self.shape.points) > 2











class RunTool(CommandTool):
    #: Signal emitted by CommandTool when activated
    SIG_VALIDATE_TOOL = Signal()

    def __init__(self, manager, title=None,
                 icon=None, tip=None, toolbar_id=DefaultToolbarID):
        if title == None:
            title = "Apply"
        if icon == None:
            icon = get_icon("apply.png")
        super().__init__(manager, title, icon, toolbar_id=toolbar_id)

    def setup_context_menu(self, menu, plot):
        pass

    def activate_command(self, plot, checked):
        """Activate tool"""
        self.SIG_VALIDATE_TOOL.emit()


"""-----------------------1D masking Tools-----------------------------------"""
class MaskPointTool(InteractiveTool):
    TITLE = _("Mask Point")
    ICON = "eraser.png"
    MARKER_STYLE_SECT = "plot"
    MARKER_STYLE_KEY = "marker/curve"
    CURSOR = Qt.PointingHandCursor

    def __init__(self, manager, mode="reuse", on_active_item=True,
                 title=None, icon=None, tip=None, end_callback=None,
                 toolbar_id=DefaultToolbarID, marker_style=None,
                 switch_to_default_tool=None):
        super().__init__(manager, toolbar_id,
                              title=title, icon=icon, tip=tip,
                              switch_to_default_tool=switch_to_default_tool)
        assert mode in ("reuse", "create")
        self.mode = mode
        self.end_callback = end_callback
        self.marker = None
        self.last_pos = None
        self.on_active_item = on_active_item
        self.maskeditem = None
        if marker_style is not None:
            self.marker_style_sect = marker_style[0]
            self.marker_style_key = marker_style[1]
        else:
            self.marker_style_sect = self.MARKER_STYLE_SECT
            self.marker_style_key = self.MARKER_STYLE_KEY

    def activate(self):
        """Activate tool"""
        for baseplot, start_state in list(self.start_state.items()):
            baseplot.filter.set_state(start_state, None)
        self.action.setChecked(True)
        self.manager.set_active_tool(self)

    def deactivate(self):
        if self.marker is not None:
            self.marker.setVisible(False)

    def find_masked_curve(self, plot):
        item = plot.get_active_item()
        from grafit.plotitems import MaskedErrorBarCurveItem
        if isinstance(item, MaskedErrorBarCurveItem):

            return item
        else:

            items = [item for item in plot.get_items()
                     if isinstance(item, ErrorBarCurveItem)]

            if items:
                return items[-1]
            else:
                return None

    def set_marker_style(self, marker):
        marker.set_style(self.marker_style_sect, self.marker_style_key)

    def setup_filter(self, baseplot):
        filter = baseplot.filter
        # Initialisation du filtre
        start_state = filter.new_state()
        # Bouton gauche :
        handler = QtDragHandler(filter, Qt.LeftButton, start_state=start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Enter, Qt.Key_Return,
                                         Qt.Key_Space) ),
                         self.validate, start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Left,) ),
                         self.move_to_previous, start_state)
        filter.add_event(start_state,
                         KeyEventMatch( (Qt.Key_Right,) ),
                         self.move_to_next, start_state)
        handler.SIG_START_TRACKING.connect(self.start)
        handler.SIG_MOVE.connect(self.move)
        handler.SIG_STOP_NOT_MOVING.connect(self.stop)
        handler.SIG_STOP_MOVING.connect(self.stop)
        return setup_standard_tool_filter(filter, start_state)

    def start(self, filter, event):
        self.maskeditem = self.find_masked_curve(filter.plot)
        if self.maskeditem  == None:
            return
        else:
            filter.plot.select_item(self.maskeditem )
        if self.marker is None:
            title = ""
            if self.TITLE:
                title = "<b>%s</b><br>" % self.TITLE
            if self.on_active_item:
                constraint_cb = filter.plot.on_active_curve
                label_cb = lambda x, y: title + \
                           filter.plot.get_coordinates_str(x, y)
            else:
                constraint_cb = None
                label_cb = lambda x, y: \
                           f"{title}x = {x:g}<br>y = {y:g}"
            self.marker = Marker(label_cb=label_cb,
                                 constraint_cb=constraint_cb)
            self.set_marker_style(self.marker)
        self.marker.attach(filter.plot)
        self.marker.setZ(filter.plot.get_max_z()+1)
        self.marker.setVisible(True)

    def stop(self, filter, event):
        self.move( filter, event )
        if self.mode != "reuse":
            self.marker.detach()
            self.marker = None
        if self.end_callback:
            self.end_callback(self)

    def move(self, filter, event):
        if self.marker is None:
            return # something is wrong ...
        self.marker.move_local_point_to( 0, event.pos() )
        filter.plot.replot()
        self.last_pos = self.marker.xValue(), self.marker.yValue()

    def get_coordinates(self):
        return self.last_pos

    def validate(self, filter, event):
        if self.maskeditem is not None:
            self.maskeditem.toggle_current_point_mask()

    def move_to_next(self, filter, event):
        if self.maskeditem is not None:
            x,y = self.maskeditem.get_next_point()
            self.marker.setValue(x,y)
            filter.plot.SIG_MARKER_CHANGED.emit(self.marker)
            filter.plot.replot()

    def move_to_previous(self, filter, event):
        if self.maskeditem is not None:
            x,y = self.maskeditem.get_previous_point()
            self.marker.setValue(x,y)
            filter.plot.SIG_MARKER_CHANGED.emit(self.marker)
            filter.plot.replot()

class CurveMaskingWidget(PanelWidget):
    """panel for masking a grafit.plotitems.MaskedErrorBarCurveItem
    the mask is found on self.masked_curve.masked_x which holds a copy of the mask in the dataset"""

    __implements__ = (IPanel,)
    PANEL_ID = ID_CURVEMASKING
    PANEL_TITLE = "curve masking"
    PANEL_ICON = None # string

    def __init__(self,parent=None):
        self._mask_shapes = {}
        self._mask_already_restored = {}

        super().__init__(parent)
        self.setMinimumSize(QSize(300, 500))


        self.masked_curve = None # associated masked image item

        self.manager = None # manager for the associated image plot

        self.toolbar = toolbar = QToolBar(self)
        toolbar.setOrientation(Qt.Horizontal)

        self.vBox=QVBoxLayout(self)  #central widget

        gbox1=QGroupBox(self)
        gbox1.setTitle(_('Mask curve with shape tools'))

        self.vBox1=QVBoxLayout(gbox1)

        self.insidebutton=QCheckBox(self)
        self.insidebutton.setText(_("Inside"))
        self.outsidebutton=QCheckBox(self)
        self.outsidebutton.setText(_("Outside"))

        self.group1=QButtonGroup(self)
        self.group1.addButton(self.insidebutton)
        self.group1.addButton(self.outsidebutton)
        self.insidebutton.setChecked(True)

        hBox1=QHBoxLayout()
        hBox1.addWidget(self.insidebutton)
        hBox1.addWidget(self.outsidebutton)

        self.maskbutton=QCheckBox(self)
        self.maskbutton.setText(_("Mask curve"))
        self.unmaskbutton=QCheckBox(self)
        self.unmaskbutton.setText(_("Unmask curve"))

        self.group2=QButtonGroup(self)
        self.group2.addButton(self.maskbutton)
        self.group2.addButton(self.unmaskbutton)
        self.maskbutton.setChecked(True)

        hBox2=QHBoxLayout()
        hBox2.addWidget(self.maskbutton)
        hBox2.addWidget(self.unmaskbutton)

        self.singleshapebutton=QCheckBox(self)
        self.singleshapebutton.setText(_("Single Shape"))
        self.singleshapebutton.setChecked(False)

        self.shapesprefs =QPushButton(self)
        self.shapesprefs.setText(_("Preferences"))

        hBox3=QHBoxLayout()
        hBox3.addWidget(self.singleshapebutton)
        hBox3.addWidget(self.shapesprefs)

        self.autoupdatebutton=QCheckBox(self)
        self.autoupdatebutton.setText(_("Auto update"))
        self.autoupdatebutton.setChecked(True)

        self.applyshapesbutton=QPushButton(self)
        self.applyshapesbutton.setText(_("Apply shapes"))

        hBox4=QHBoxLayout()
        hBox4.addWidget(self.autoupdatebutton)
        hBox4.addWidget(self.applyshapesbutton)

        self.showshapesbutton=QCheckBox(self)
        self.showshapesbutton.setText(_("Show shapes"))
        self.showshapesbutton.setChecked(True)
        self.removeshapesbutton=QPushButton(self)
        self.removeshapesbutton.setText(_("Remove shapes"))

        hBox5=QHBoxLayout()
        hBox5.addWidget(self.showshapesbutton)
        hBox5.addWidget(self.removeshapesbutton)


        self.vBox1.addWidget(toolbar)
        self.vBox1.addLayout(hBox1)
        self.vBox1.addLayout(hBox2)



        self.vBox1.addLayout(hBox3)
        self.vBox1.addLayout(hBox4)
        self.vBox1.addLayout(hBox5)

        gbox2=QGroupBox(self)
        gbox2.setTitle(_('Mask operations'))

        self.gr0=QGridLayout(gbox2)

        self.showmaskbutton=QCheckBox(_("Show mask"),self)
        self.showmaskbutton.setChecked(True)
        self.clearmaskbutton=QPushButton(_("Clear mask"),self)
        self.invertmaskbutton=QPushButton(_("Invert mask"),self)

        self.gr0.addWidget(self.invertmaskbutton,0,0)
        self.gr0.addWidget(self.clearmaskbutton,1,0)
        self.gr0.addWidget(self.showmaskbutton,2,0)

        gbox3=QGroupBox(_('Mask curve from threshold'),self)

        self.vBox3=QVBoxLayout(gbox3)   #set VBox to central widget

        self.minspinslider = DoubleSpinSliderBox(self)
        self.minspinslider.setText("Min")
        self.minspinslider.setRange(0.,100.)
        self.minspinslider.setDecimals(1)
        self.minspinslider.setSingleStep(0.1)
        self.minspinslider.setValue(0.)

        self.maxspinslider = DoubleSpinSliderBox(self)
        self.maxspinslider.setText("Max")
        self.maxspinslider.setRange(0.,100.)
        self.maxspinslider.setDecimals(1)
        self.maxspinslider.setSingleStep(0.1)
        self.maxspinslider.setValue(100.)

        couple_doublespinsliders(self.minspinslider,self.maxspinslider)

        #self.vBox2.addLayout(hBox7)
        self.vBox3.addWidget(self.minspinslider)
        self.vBox3.addWidget(self.maxspinslider)

        self.vBox.addWidget(gbox1)
        self.vBox.addWidget(gbox2)
        self.vBox.addWidget(gbox3)

        self.defaultmaskshapeparam = ShapeParam()
        self.defaultmaskshapeparam.read_config(CONF, "plot", "shape/mask")


    def register_plot(self, baseplot):
        self._mask_shapes.setdefault(baseplot, [])
        baseplot.SIG_ITEMS_CHANGED.connect(self.items_changed)
        baseplot.SIG_ITEM_SELECTION_CHANGED.connect(self.item_selection_changed)

    def register_panel(self, manager):
        """Register panel to plot manager"""
        self.manager = manager
        default_toolbar = self.manager.get_default_toolbar()
        self.manager.add_toolbar(self.toolbar, "masking shapes")
        self.manager.set_default_toolbar(default_toolbar)
        for plot in manager.get_plots():
            self.register_plot(plot)

    def configure_panel(self):
        """Configure panel"""
        self.mask_point_tool = self.manager.add_tool(MaskPointTool, toolbar_id="masking shapes")

        self.rect_tool = self.manager.add_tool(RectangleTool, toolbar_id="masking shapes",
                                     handle_final_shape_cb=lambda shape:
                                       self.handle_shape(shape),
                                     title=_("Mask rectangular area"),
                                     icon="mask_rectangle_grey.png")
        self.ellipse_tool = self.manager.add_tool(EllipseTool, toolbar_id="masking shapes",
                                     handle_final_shape_cb=lambda shape:
                                       self.handle_shape(shape),
                                     title=_("Mask elliptical area"),
                                     icon="mask_circle_grey.png")
        self.polygon_tool = self.manager.add_tool(FreeFormTool, toolbar_id="masking shapes",
                                     handle_final_shape_cb=lambda shape:
                                       self.handle_shape(shape),
                                     title=_("Mask polygonal area"),
                                     icon='mask_polygon_grey.png')
        self.run_tool = self.manager.add_tool(RunTool, title=_("Apply mask to the data"),toolbar_id="masking shapes")

        self.setup_actions()

    def setup_actions(self):
        #QObject.connect(self.maskbutton, SIG_STATE_CHANGED, self.set_mode)
        self.applyshapesbutton.clicked.connect(self.apply_shapes)
        self.showshapesbutton.stateChanged.connect(self.show_shapes)
        self.removeshapesbutton.clicked.connect(self.remove_all_shapes)
        self.shapesprefs.clicked.connect(self.set_shapes_prefs)
        self.clearmaskbutton.clicked.connect(self.clear_mask)
        self.showmaskbutton.stateChanged.connect(self.show_mask)
        self.invertmaskbutton.clicked.connect(self.invert_mask)
        self.minspinslider.SIG_DOUBLE_VALUE_CHANGED.connect(self.update_threshold)
        self.maxspinslider.SIG_DOUBLE_VALUE_CHANGED.connect(self.update_threshold)

    def set_mode(self,i):
        pass


    def update_threshold(self,x):
        plot=self.get_active_plot()
        if self.masked_curve is None:
            return
        pmin=self.minspinslider.value()
        pmax=self.maxspinslider.value()
        self.masked_curve.masked_x.mask = False

        vmin=np.percentile(self.masked_curve._y,pmin,method='nearest')
        vmax=np.percentile(self.masked_curve._y,pmax,method='nearest')

        #self.masked_image.data[0:10,0:10]=np.ma.masked
        np.ma.masked_where(self.masked_curve._y<vmin,self.masked_curve.masked_x,copy=False)
        np.ma.masked_where(self.masked_curve._y>vmax,self.masked_curve.masked_x,copy=False)
        self.show_mask(True)
        plot.replot()


    def get_active_plot(self):
        return self.manager.get_active_plot()

    def handle_shape(self, shape):
        self.defaultmaskshapeparam.update_shape(shape)
        shape.shapeparam.label='mask shape'
        shape.set_private(True)
        shape.closed = len(shape.points) > 2
        plot = self.manager.get_active_plot()
        plot.set_active_item(shape)
        if self.singleshapebutton.isChecked():
            self.remove_shapes()
            if self.masked_curve is not None:
                self.masked_curve.unmask_all()
        self._mask_shapes[plot] += [(shape, self.insidebutton.isChecked(), self.maskbutton.isChecked())]

        if self.autoupdatebutton.isChecked():
            self.apply_shape_mask(shape,self.insidebutton.isChecked(), self.maskbutton.isChecked())


    def show_mask(self, state):
        if self.masked_curve is not None:
            self.masked_curve.set_mask_visible(state)
            self.masked_curve.plot().replot()

    def invert_mask(self):
        if self.masked_curve is not None:
            self.masked_curve.masked_x.mask = ~self.masked_curve.masked_x.mask
            self.masked_curve.plot().replot()

    def apply_shape_mask(self, shape, inside, mask):
        from grafit.plotitems import (TrueEllipseShape)
        from guiqwt.shapes import (RectangleShape, PolygonShape)
        if self.masked_curve is None:
            return
        if isinstance(shape, RectangleShape):
            x0, y0, x1, y1 = shape.get_rect()
            if mask:
                mask_curve_rectangular_area(self.masked_curve, shape, inside=inside)
            else:
                unmask_curve_rectangular_area(self.masked_curve, shape, inside=inside)
        elif isinstance(shape, TrueEllipseShape):
            if mask:
                mask_curve_elliptical_area(self.masked_curve, shape, inside=inside)
            else:
                unmask_curve_elliptical_area(self.masked_curve, shape, inside=inside)
        elif isinstance(shape, PolygonShape):
            if mask:
                mask_curve_polygonal_area(self.masked_curve, shape, inside=inside)
            else:
                unmask_curve_polygonal_area(self.masked_curve, shape, inside=inside)
        plot = self.get_active_plot()
        plot.replot()

    def apply_shapes(self):
        if self.masked_curve is None:
            QMessageBox.information(self,'warning','no curve found')
            return
        plot = self.get_active_plot()
        for shape,inside, mask in self._mask_shapes[plot]:
            self.apply_shape_mask(shape, inside, mask)

        self.show_mask(True)
        #self.masked_image.set_mask(mask)
        plot.replot()
        #self.emit(SIG_APPLIED_MASK_TOOL)


    def remove_all_shapes(self):
        message = _("Do you really want to remove all masking shapes?")
        plot = self.get_active_plot()
        answer = QMessageBox.warning(plot, _("Remove all masking shapes"),
                                     message, QMessageBox.Yes | QMessageBox.No)
        if answer == QMessageBox.Yes:
            self.remove_shapes()

    def remove_shapes(self):

        plot = self.get_active_plot()
        plot.del_items([shape for shape, _inside, mask
                        in self._mask_shapes[plot]]) # remove shapes
        self._mask_shapes[plot] = []
        plot.replot()

    def show_shapes(self, state):
        plot = self.get_active_plot()
        if plot is not None:
            for shape, _inside, mask in self._mask_shapes[plot]:
                shape.setVisible(state)
            plot.replot()

    def set_shapes_prefs(self):
        self.defaultmaskshapeparam.edit()

    def create_shapes_from_masked_areas(self):
        plot = self.get_active_plot()
        self._mask_shapes[plot] = []
        for area in self.masked_curve.get_masked_areas():
            if area.geometry == 'rectangular':
                shape = RectangleShape()
                shape.set_points(area.pts)
            elif area.geometry == 'elliptical':
                shape = TrueEllipseShape()
                shape.set_points(area.pts)
            elif area.geometry == 'polygonal':
                shape = PolygonShape()
                shape.set_points(area.pts)

            self.defaultmaskshapeparam.update_shape(shape)
            shape.set_private(True)
            self._mask_shapes[plot] += [(shape, area.inside, area.mask)]
            plot.blockSignals(True)
            plot.add_item(shape)
            plot.blockSignals(False)

    def find_masked_curve(self, plot):
        item = plot.get_active_item()
        if isinstance(item, MaskedErrorBarCurveItem):
            return item
        else:
            items = [item for item in plot.get_items()
                     if isinstance(item, MaskedErrorBarCurveItem)]
            if items:
                return items[-1]

    def set_masked_curve(self, plot):
        self.masked_curve = self.find_masked_curve(plot)
        if self.masked_curve is not None:
            if not self._mask_already_restored:
                self.create_shapes_from_masked_areas()
                self._mask_already_restored = True


    def items_changed(self, plot):

        self.set_masked_curve(plot)
        self._mask_shapes[plot] = [(shape, inside, mask) for shape, inside, mask
                                   in self._mask_shapes[plot]
                                   if shape.plot() is plot]
        self.update_status(plot)

    def item_selection_changed(self, plot):
        self.set_masked_curve(plot)
        self.update_status(plot)

    def reset_mask(self,plot):
        #remove shapes prior opening a masked image with masked areas
        self._mask_shapes[plot]=[]
        self._mask_already_restored = False

    def clear_mask(self):
        if self.masked_curve is None:
            return
        message = _("Do you really want to clear the mask?")
        plot = self.get_active_plot()
        answer = QMessageBox.warning(plot, _("Clear mask"), message,
                                     QMessageBox.Yes | QMessageBox.No)
        if answer == QMessageBox.Yes:
            self.masked_curve.unmask_all()

            plot.replot()

    def update_status(self, plot):
        #self.action.setEnabled(self.masked_image is not None)
        pass

    def activate_command(self, plot, checked):
        """Activate tool"""
        pass



class ImageMaskingWidget(PanelWidget):
    __implements__ = (IPanel,)
    PANEL_ID = ID_IMAGEMASKING
    PANEL_TITLE = "image masking"
    PANEL_ICON = None # string

    #do not use get_active_plot() as it returns default plot if canvas has not the focus

    def __init__(self,parent=None, plots= 'all'):
        self.allowed_plots = plots   #tools are only registered for these plots
        self._mask_shapes = {}
        self._mask_already_restored = {}

        super().__init__(parent)
        self.setMinimumSize(QSize(300, 500))


        self.masked_image = None # associated masked image item

        self.manager = None # manager for the associated image plot

        self.toolbar = toolbar = QToolBar(self)
        toolbar.setOrientation(Qt.Horizontal)

        self.vBox=QVBoxLayout(self)  #central widget

        gbox1=QGroupBox(self)
        gbox1.setTitle(_('Mask image with shape tools'))

        self.vBox1=QVBoxLayout(gbox1)

        self.insidebutton=QCheckBox(self)
        self.insidebutton.setText(_("Inside"))
        self.outsidebutton=QCheckBox(self)
        self.outsidebutton.setText(_("Outside"))

        self.group1=QButtonGroup(self)
        self.group1.addButton(self.insidebutton)
        self.group1.addButton(self.outsidebutton)
        self.insidebutton.setChecked(True)

        hBox1=QHBoxLayout()
        hBox1.addWidget(self.insidebutton)
        hBox1.addWidget(self.outsidebutton)

        self.maskbutton=QCheckBox(self)
        self.maskbutton.setText(_("Mask image"))
        self.unmaskbutton=QCheckBox(self)
        self.unmaskbutton.setText(_("Unmask image"))

        self.group2=QButtonGroup(self)
        self.group2.addButton(self.maskbutton)
        self.group2.addButton(self.unmaskbutton)
        self.maskbutton.setChecked(True)

        hBox2=QHBoxLayout()
        hBox2.addWidget(self.maskbutton)
        hBox2.addWidget(self.unmaskbutton)

        self.brushsizelabel=QLabel(self)
        self.brushsizelabel.setText(_("Brush size"))

        self.hbrushsize=QSpinBox(self)
        self.hbrushsize.setRange(1,100)

        self.vbrushsize=QSpinBox(self)
        self.vbrushsize.setRange(1,100)

        hBox3=QHBoxLayout()
        hBox3.addWidget(self.brushsizelabel)
        hBox3.addWidget(self.hbrushsize)
        hBox3.addWidget(self.vbrushsize)

        self.singleshapebutton=QCheckBox(self)
        self.singleshapebutton.setText(_("Single Shape"))
        self.singleshapebutton.setChecked(False)

        self.shapesprefs=QPushButton(self)
        self.shapesprefs.setText(_("Preferences"))

        hBox4=QHBoxLayout()
        hBox4.addWidget(self.singleshapebutton)
        hBox4.addWidget(self.shapesprefs)

        self.autoupdatebutton=QCheckBox(self)
        self.autoupdatebutton.setText(_("Auto update"))
        self.autoupdatebutton.setChecked(True)

        self.applyshapesbutton=QPushButton(self)
        self.applyshapesbutton.setText(_("Mask with shapes"))

        hBox5=QHBoxLayout()
        hBox5.addWidget(self.autoupdatebutton)
        hBox5.addWidget(self.applyshapesbutton)

        self.showshapesbutton=QCheckBox(self)
        self.showshapesbutton.setText(_("Show shapes"))
        self.removeshapesbutton=QPushButton(self)
        self.removeshapesbutton.setText(_("Remove shapes"))

        hBox6=QHBoxLayout()
        hBox6.addWidget(self.showshapesbutton)
        hBox6.addWidget(self.removeshapesbutton)

        self.vBox1.addWidget(toolbar)
        self.vBox1.addLayout(hBox1)
        self.vBox1.addLayout(hBox2)
        self.vBox1.addLayout(hBox3)
        self.vBox1.addWidget(self.singleshapebutton)
        self.vBox1.addLayout(hBox4)
        self.vBox1.addLayout(hBox5)
        self.vBox1.addLayout(hBox6)

        gbox2=QGroupBox(self)
        gbox2.setTitle(_('Mask operations'))

        self.gr0=QGridLayout(gbox2)

        self.showmaskbutton=QCheckBox(_("Show mask"),self)
        self.clearmaskbutton=QPushButton(_("Clear mask"),self)
        self.invertmaskbutton=QPushButton(_("Invert mask"),self)
        self.dilatationbutton=QPushButton(_("Dilatation"),self)
        self.erosionbutton=QPushButton(_("Erosion"),self)

        self.sizebox=QSpinBox(self)
        self.sizebox.setRange(1,100)

        self.gr0.addWidget(self.invertmaskbutton,0,0)
        self.gr0.addWidget(self.clearmaskbutton,1,0)
        self.gr0.addWidget(self.showmaskbutton,2,0)
        self.gr0.addWidget(self.dilatationbutton,0,1)
        self.gr0.addWidget(self.erosionbutton,1,1)
        self.gr0.addWidget(self.sizebox,2,1)

        gbox3=QGroupBox(_('Mask image from threshold'),self)

        self.vBox3=QVBoxLayout(gbox3)   #set VBox to central widget

        self.minspinslider = DoubleSpinSliderBox(self)
        self.minspinslider.setText("Min")
        self.minspinslider.setRange(0.,100.)
        self.minspinslider.setDecimals(1)
        self.minspinslider.setSingleStep(0.1)
        self.minspinslider.setValue(0.)

        self.maxspinslider = DoubleSpinSliderBox(self)
        self.maxspinslider.setText("Max")
        self.maxspinslider.setRange(0.,100.)
        self.maxspinslider.setDecimals(1)
        self.maxspinslider.setSingleStep(0.1)
        self.maxspinslider.setValue(100.)

        couple_doublespinsliders(self.minspinslider,self.maxspinslider)

        #self.vBox2.addLayout(hBox7)
        self.vBox3.addWidget(self.minspinslider)
        self.vBox3.addWidget(self.maxspinslider)

        self.vBox.addWidget(gbox1)
        self.vBox.addWidget(gbox2)
        self.vBox.addWidget(gbox3)

        self.defaultmaskshapeparam = ShapeParam()
        self.defaultmaskshapeparam.read_config(CONF, "plot", "shape/mask")

    def register_plot(self, baseplot):
        self._mask_shapes.setdefault(baseplot, [])
        baseplot.SIG_ITEMS_CHANGED.connect(self.items_changed)
        baseplot.SIG_ITEM_SELECTION_CHANGED.connect(self.item_selection_changed)

    def register_panel(self, manager):
        """Register panel to plot manager"""
        self.manager = manager
        default_toolbar = self.manager.get_default_toolbar()
        self.manager.add_toolbar(self.toolbar, "masking shapes")
        self.manager.set_default_toolbar(default_toolbar)
        for plot in manager.get_plots():
            self.register_plot(plot)

    def configure_panel(self):
        """Configure panel"""
        if self.allowed_plots == 'all':
            self.ellipse_mask_tool  = self.manager.add_tool(EllipseMaskTool,start_func = self.mask_elliptical_area,
                                                            move_func = self.mask_elliptical_area, toolbar_id="masking shapes")
            self.rect_mask_tool  = self.manager.add_tool(RectangleMaskTool,start_func = self.mask_rectangular_area,
                                                            move_func = self.mask_rectangular_area, toolbar_id="masking shapes")
            self.rect_tool = self.manager.add_tool(RectangleTool, toolbar_id="masking shapes",
                                         handle_final_shape_cb=lambda shape:
                                           self.handle_shape(shape),
                                         title=_("Mask rectangular area"),
                                         icon="mask_rectangle_grey.png")
            self.ellipse_tool = self.manager.add_tool(EllipseTool, toolbar_id="masking shapes",
                                         handle_final_shape_cb=lambda shape:
                                           self.handle_shape(shape),
                                         title=_("Mask elliptical area"),
                                         icon="mask_circle_grey.png")
            self.polygon_tool = self.manager.add_tool(FreeFormTool, toolbar_id="masking shapes",
                                         handle_final_shape_cb=lambda shape:
                                           self.handle_shape(shape),
                                         title=_("Mask polygonal area"),
                                         icon='mask_polygon_grey.png')

            self.run_tool = self.manager.add_tool(RunTool, title=_("Remove masked area"),toolbar_id="masking shapes")
        else:
            self.ellipse_mask_tool  = self.manager.add_tool_for_plots(EllipseMaskTool, self.allowed_plots, start_func = self.mask_elliptical_area,
                                                            move_func = self.mask_elliptical_area, toolbar_id="masking shapes")
            self.rect_mask_tool  = self.manager.add_tool_for_plots(RectangleMaskTool, self.allowed_plots, start_func = self.mask_rectangular_area,
                                                            move_func = self.mask_rectangular_area, toolbar_id="masking shapes")
            self.rect_tool = self.manager.add_tool_for_plots(RectangleTool, self.allowed_plots, toolbar_id="masking shapes",
                                         handle_final_shape_cb=lambda shape:
                                           self.handle_shape(shape),
                                         title=_("Mask rectangular area"),
                                         icon="mask_rectangle_grey.png")
            self.ellipse_tool = self.manager.add_tool_for_plots(EllipseTool, self.allowed_plots, toolbar_id="masking shapes",
                                         handle_final_shape_cb=lambda shape:
                                           self.handle_shape(shape),
                                         title=_("Mask elliptical area"),
                                         icon="mask_circle_grey.png")
            self.polygon_tool = self.manager.add_tool_for_plots(FreeFormTool, self.allowed_plots, toolbar_id="masking shapes",
                                         handle_final_shape_cb=lambda shape:
                                           self.handle_shape(shape),
                                         title=_("Mask polygonal area"),
                                         icon='mask_polygon_grey.png')

            self.run_tool = self.manager.add_tool_for_plots(RunTool, self.allowed_plots, title=_("Remove masked area"),toolbar_id="masking shapes")




        self.setup_actions()
        self.hbrushsize.setValue(10)
        self.vbrushsize.setValue(10)
        self.set_brush_size(10)
        self.showmaskbutton.setChecked(True)
        self.showshapesbutton.setChecked(True)

    def setup_actions(self):
        #QObject.connect(self.maskbutton, SIG_STATE_CHANGED, self.set_mode)
        self.hbrushsize.valueChanged.connect(self.set_brush_size)
        self.vbrushsize.valueChanged.connect(self.set_brush_size)
        self.applyshapesbutton.clicked.connect(self.apply_shapes)
        self.showshapesbutton.stateChanged.connect(self.show_shapes)
        self.removeshapesbutton.clicked.connect(self.remove_all_shapes)
        self.shapesprefs.clicked.connect(self.set_shapes_prefs)
        self.clearmaskbutton.clicked.connect(self.clear_mask)
        self.showmaskbutton.stateChanged.connect(self.show_mask)
        self.invertmaskbutton.clicked.connect(self.invert_mask)
        self.dilatationbutton.clicked.connect(self.mask_dilatation)
        self.erosionbutton.clicked.connect(self.mask_erosion)
        self.minspinslider.SIG_DOUBLE_VALUE_CHANGED.connect(self.update_threshold)
        self.maxspinslider.SIG_DOUBLE_VALUE_CHANGED.connect(self.update_threshold)


    def set_mode(self,i):
        self.ellipse_mask_tool.set_mode(self.maskbutton.isChecked())
        self.rect_mask_tool.set_mode(self.maskbutton.isChecked())

    def set_brush_size(self,i=10):
        #self.cmt.set_size(i)
        i=self.hbrushsize.value()
        j=self.vbrushsize.value()
        self.ellipse_mask_tool.set_pixel_size(i,j)
        self.rect_mask_tool.set_pixel_size(i,j)

    def set_shapes_prefs(self):
        self.defaultmaskshapeparam.edit()


    def update_threshold(self,x):
        plot=self.masked_image.plot()
        if self.masked_image is None:
            return
        pmin=self.minspinslider.value()
        pmax=self.maxspinslider.value()
        self.masked_image.data.mask = np.ma.nomask
        vmin=np.percentile(self.masked_image.data[np.isfinite(self.masked_image.data)],pmin,method="nearest")
        vmax=np.percentile(self.masked_image.data[np.isfinite(self.masked_image.data)],pmax,method="nearest")

        np.ma.masked_less(self.masked_image.data,vmin,copy=False)
        np.ma.masked_greater(self.masked_image.data,vmax,copy=False)
        self.show_mask(True)
        plot.replot()

    def mask_dilatation(self):
        plot=self.masked_image.plot()
        if self.masked_image is None:
            return

        radius=self.sizebox.value()
        L = np.arange(-radius, radius + 1)
        X, Y = np.meshgrid(L, L)
        struct=np.array((X ** 2 + Y ** 2) <= radius ** 2, dtype=np.bool)

        self.masked_image.data.mask = fftconvolve(self.masked_image.data.mask,struct,'same')>0.5  #much better than ndimage.binary_dilatation

        self.show_mask(True)
        plot.replot()

    def mask_erosion(self):
        plot=self.masked_image.plot()

        radius=self.sizebox.value()
        L = np.arange(-radius, radius + 1)
        X, Y = np.meshgrid(L, L)
        struct=np.array((X ** 2 + Y ** 2) <= radius ** 2, dtype=np.bool)

        self.masked_image.data.mask = fftconvolve(np.logical_not(self.masked_image.data.mask),struct,'same')<=0.5  #much better than ndimage.binary_dilatation

        self.show_mask(True)
        plot.replot()

    def get_active_plot(self):
        return self.manager.get_active_plot()

    def handle_shape(self, shape):
        self.defaultmaskshapeparam.update_shape(shape)
        shape.shapeparam.label='mask shape'
        shape.set_private(True)
        shape.setVisible(self.showshapesbutton.isChecked())
        plot = self.manager.get_active_plot()
        plot.set_active_item(shape)
        if self.singleshapebutton.isChecked():
            self.remove_shapes()
            if self.masked_image is not None:
                self.masked_image.unmask_all()
        self._mask_shapes[plot] += [(shape, self.insidebutton.isChecked(), self.maskbutton.isChecked())]

        if self.autoupdatebutton.isChecked():
            self.apply_shape_mask(shape,self.insidebutton.isChecked(), self.maskbutton.isChecked())

            plot.replot()

    def show_mask(self, state):
        #show_mask is called before construction of masked image so we need to check
        if self.masked_image is not None:
            self.masked_image.set_mask_visible(state)

    def invert_mask(self):
        self.masked_image.set_mask(np.logical_not(self.masked_image.get_mask()))
        self.masked_image.plot().replot()

    def mask_rectangular_area(self, shape):
        if self.maskbutton.isChecked():
            mask_image_rectangular_area(self.masked_image, shape, inside=self.insidebutton.isChecked())
        else:
            unmask_image_rectangular_area(self.masked_image, shape, inside=self.insidebutton.isChecked())

    def mask_elliptical_area(self, shape):
        if self.maskbutton.isChecked():
            mask_image_elliptical_area(self.masked_image, shape, inside=self.insidebutton.isChecked())
        else:
            unmask_image_elliptical_area(self.masked_image, shape, inside=self.insidebutton.isChecked())

    def apply_shape_mask(self, shape, inside, mask):
        from grafit.plotitems import (TrueEllipseShape)
        from guiqwt.shapes import (RectangleShape, PolygonShape)
        if isinstance(shape, RectangleShape):
            self.masked_image.align_rectangular_shape(shape)
            x0, y0, x1, y1 = shape.get_rect()
            if mask:
                mask_image_rectangular_area(self.masked_image, shape, inside=inside)
            else:
                unmask_image_rectangular_area(self.masked_image, shape, inside=inside)
        elif isinstance(shape, TrueEllipseShape):
            if mask:
                mask_image_elliptical_area(self.masked_image, shape, inside=inside)
            else:
                unmask_image_elliptical_area(self.masked_image, shape, inside=inside)
        elif isinstance(shape, PolygonShape):
            if mask:
                mask_image_polygonal_area(self.masked_image, shape, inside=inside)
            else:
                unmask_image_polygonal_area(self.masked_image, shape, inside=inside)


    def apply_shapes(self):
        plot = self.masked_image.plot()
        for shape, inside, mask in self._mask_shapes[plot]:
            self.apply_shape_mask(shape, inside, mask)

        self.show_mask(True)
        plot.replot()

    def remove_all_shapes(self):
        message = _("Do you really want to remove all masking shapes?")
        plot = self.get_active_plot()
        answer = QMessageBox.warning(plot, _("Remove all masking shapes"),
                                     message, QMessageBox.Yes | QMessageBox.No)
        if answer == QMessageBox.Yes:
            self.remove_shapes()

    def remove_shapes(self):
        plot = self.masked_image.plot()
        plot.del_items([shape for shape, _inside, mask
                        in self._mask_shapes[plot]]) # remove shapes
        self._mask_shapes[plot] = []
        plot.replot()

    def show_shapes(self, state):
        if self.masked_image is not None:
            plot = self.masked_image.plot()
            for shape, _inside, mask in self._mask_shapes[plot]:
                shape.setVisible(state)
            plot.replot()

    def create_shapes_from_masked_areas(self):
        plot = self.masked_image.plot()
        self._mask_shapes[plot] = []
        for area in self.masked_image.get_masked_areas():
            if area.geometry == 'rectangular':
                shape = RectangleShape()
                shape.set_points(area.pts)
                self.masked_image.align_rectangular_shape(shape)
            elif area.geometry == 'elliptical':
                shape = TrueEllipseShape()
                shape.set_points(area.pts)
            elif area.geometry == 'polygonal':
                shape = PolygonShape()
                shape.set_points(area.pts)

            shape.set_style("plot", "shape/custom_mask")
            shape.set_private(True)
            self._mask_shapes[plot] += [(shape, area.inside, area.mask)]
            plot.blockSignals(True)
            plot.add_item(shape)
            plot.blockSignals(False)

    def find_masked_image(self, plot):
        item = plot.get_active_item()
        from grafit.plotitems import MaskedImageNan
        if isinstance(item, MaskedImageNan):
            return item
        else:
            items = [item for item in plot.get_items()
                     if isinstance(item, MaskedImageNan)]
            if items:
                return items[-1]

    def set_masked_image(self, plot):
        if self.allowed_plots != "all" and plot not in self.allowed_plots:
            return

        self.masked_image = self.find_masked_image(plot)
        #we recreate the masked shapes
        if self.masked_image is not None:
            xmin,xmax=self.masked_image.get_xdata()
            dx=(xmax-xmin)/float(self.masked_image.data.shape[1])
            ymin,ymax=self.masked_image.get_ydata()
            dy=(ymax-ymin)/float(self.masked_image.data.shape[0])
            self.set_pixel_size(dx, dy)
            if not self._mask_already_restored:
                self.create_shapes_from_masked_areas()
                self._mask_already_restored = True
            self.showmaskbutton.setChecked(self.masked_image.imageparam.show_mask)
        else:
            self.manager.activate_default_tool()

        enable = self.masked_image is not None
        #we enable or not tools and buttons
        self.ellipse_mask_tool.action.setEnabled(enable)
        self.rect_mask_tool.action.setEnabled(enable)
        self.rect_tool.action.setEnabled(enable)
        self.ellipse_tool.action.setEnabled(enable)
        self.polygon_tool.action.setEnabled(enable)
        self.run_tool.action.setEnabled(enable)
        self.applyshapesbutton.setEnabled(enable)
        self.clearmaskbutton.setEnabled(enable)
        self.showmaskbutton.setEnabled(enable)
        self.invertmaskbutton.setEnabled(enable)
        self.dilatationbutton.setEnabled(enable)
        self.erosionbutton.setEnabled(enable)
        self.minspinslider.setEnabled(enable)
        self.maxspinslider.setEnabled(enable)

    def set_pixel_size(self, dx, dy):
        #when a new image is set, we redifine the size of the tools according to image
        #pixel size calculation
        self.rect_mask_tool.set_resolution(dx,dy)
        self.ellipse_mask_tool.set_resolution(dx,dy)

    def items_changed(self, plot):
        self.set_masked_image(plot)
        self._mask_shapes[plot] = [(shape, inside, mask) for shape, inside, mask
                                   in self._mask_shapes[plot]
                                   if shape.plot() is plot]
        self.update_status(plot)

    def item_selection_changed(self, plot):
        self.set_masked_image(plot)
        self.update_status(plot)

    def reset_mask(self,plot):
        #remove shapes prior opening a masked image with masked areas
        self._mask_shapes[plot]=[]
        self._mask_already_restored = False

    def clear_mask(self):
        message = _("Do you really want to clear the mask?")
        plot = self.masked_image.plot()
        answer = QMessageBox.warning(plot, _("Clear mask"), message,
                                     QMessageBox.Yes | QMessageBox.No)
        if answer == QMessageBox.Yes:
            self.masked_image.unmask_all()

            plot.replot()

    def update_status(self, plot):
        #self.action.setEnabled(self.masked_image is not None)
        pass

    def activate_command(self, plot, checked):
        """Activate tool"""
        pass
