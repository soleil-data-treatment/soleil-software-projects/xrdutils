"""
Created on Wed Jun 17 16:55:31 2020

@author: prevot
"""
import numpy as np



class Raw1DData():
    #a class for describing 2D data to be fitted
    #a masked np array associated with weigths
    #contains x ranges, y ranges, values, weights
    #weights is anumpy.ndarray (or None) of weighting values to be used in fit.
    #If not None, it will be used as a multiplicative factor of the residual array,
    #so that weights*(data - fit) is minimized in the least-squares sense.
    def __init__(self,x=[],y=[],weights=None,title="",tags={},x_label='x',y_label='y'):
        self.x = np.ma.array(x,copy=True)
        self.y = np.array(y,copy=True)

        if weights is not None:
            self.weights = np.array(weights, copy=True)
        else:
            self.weights = None

        self.title = title
        self.tags = tags #dict of fixed parameters (name,value) associated to the data
        self.x_label = x_label
        self.y_label = y_label

    def serialize(self, writer):
        """Serialize object to HDF5 writer"""
        writer.write(np.array(self.x), group_name='x')
        writer.write(np.ma.is_masked(self.x), group_name='is_masked')
        if np.ma.is_masked(self.x):
            writer.write(self.x.mask, group_name='mask')
        writer.write(self.y, group_name='y')
        is_weighted = (self.weights!=None)
        writer.write(is_weighted, group_name='is_weighted')
        if is_weighted:
            writer.write(self.weights, group_name='weights')
        writer.write(self.title, group_name='title')
        #writer.write(self.tags, group_name='tags')
        writer.write(self.x_label, group_name='x_label')
        writer.write(self.y_label, group_name='y_label')
        with writer.group('tags'):
            writer.write(list(self.tags.keys()), group_name='keys')
            writer.write(list(self.tags.values()), group_name='values')

    def deserialize(self, reader):
        """Deserialize object from HDF5 reader"""
        self.x=np.ma.array(reader.read(group_name='x', func=reader.read_array))
        if (reader.read('is_masked')):
            self.x.mask=reader.read(group_name='mask', func=reader.read_array)
        self.y=reader.read('y', func=reader.read_array)
        if (reader.read('is_weighted')):
            self.weights=reader.read('weights', func=reader.read_array)
        else:
            self.weigts=None
        self.title=reader.read('title')
        #self.tags=reader.read('tags')
        self.x_label=reader.read('x_label')
        self.y_label=reader.read('y_label')
        with reader.group('tags'):
            keys = reader.read('keys', func=reader.read_sequence)
            values = reader.read('values', func=reader.read_sequence)

        self.tags=dict(zip(keys, values))


class Raw2DData():
    #a class for describing 2D data to be fitted
    #a masked np array associated with weigths
    #contains x ranges, y ranges, values, errors
    #weights is anumpy.ndarray (or None) of weighting values to be used in fit.
    #If not None, it will be used as a multiplicative factor of the residual array,
    #so that weights*(data - fit) is minimized in the least-squares sense.

    def __init__(self,data=[[]],x_range=[0,1],y_range=[0,1],weights=None,title="2D data",tags={},x_label='x',y_label='y'):
        self.data = np.ma.array(data, copy=True)
        self.x_range = x_range
        self.y_range = y_range
        self.weights = weights
        self.title = title
        self.tags = tags #dict of fixed parameters (name,value) associated to the data
        self.x_label = x_label
        self.y_label = y_label

    def serialize(self, writer):
        """Serialize object to HDF5 writer"""
        writer.write(np.array(self.data), group_name='data')
        writer.write(np.ma.is_masked(self.data), group_name='is_masked')
        if np.ma.is_masked(self.data):
            writer.write(self.data.mask, group_name='mask')
        if self.weights is None:
            is_weighted = False
        else:
            is_weighted = True
        writer.write(is_weighted, group_name='is_weighted')
        if is_weighted:
            writer.write(self.weights, group_name='weights')
        writer.write(self.x_range, group_name='x_range')
        writer.write(self.y_range, group_name='y_range')
        writer.write(self.title, group_name='title')
        writer.write(self.x_label, group_name='x_label')
        writer.write(self.y_label, group_name='y_label')

        with writer.group('tags'):
            writer.write(list(self.tags.keys()), group_name='keys')
            writer.write(list(self.tags.values()), group_name='values')


    def deserialize(self, reader):
        """Deserialize object from HDF5 reader"""
        self.data=np.ma.array(reader.read(group_name='data', func=reader.read_array))
        if (reader.read('is_masked')):
            self.data.mask=reader.read(group_name='mask', func=reader.read_array)

        if (reader.read('is_weighted')):
            self.weights=reader.read('weights', func=reader.read_array)
        else:
            self.weights=None
        self.x_range=reader.read('x_range', func=reader.read_sequence)
        self.y_range=reader.read('y_range', func=reader.read_sequence)
        self.title=reader.read('title')
        self.x_label=reader.read('x_label')
        self.y_label=reader.read('y_label')

        with reader.group('tags'):
            keys = reader.read('keys', func=reader.read_sequence)
            values = reader.read('values', func=reader.read_sequence)

        self.tags=dict(zip(keys, values))
