"""
Created on Fri Feb 21 14:15:11 2020

@author: Prevot
"""
from guidata.qt.QtCore import SIGNAL

# Emitted when a fit is performed
SIG_FIT_DONE = SIGNAL("fit_done()")

# Emitted when a fit is performed
SIG_FIT_SAVED = SIGNAL("fit_saved()")

# Emitted by parameter table when try is
SIG_TRY_CHANGED = SIGNAL("try_changed()")

SIG_TRIGGERED = SIGNAL('triggered()')

SIG_MODEL_ADDED = SIGNAL('model_added(int)')

SIG_MODEL_REMOVED = SIGNAL('model_removed(int)')

SIG_KEY_PRESSED_EVENT = SIGNAL('keyPressedEvent')

# Emitted by plot when a shapes.EllipseShape changes
SIG_SHAPE_CHANGED = SIGNAL("shape_changed(PyQt_PyObject)")

#emitted by plot when a
SIG_KEY_PRESSED = SIGNAL('keyPressed(int)')

SIG_STOP_TRACKING=SIGNAL("stop_tracking")

SIG_CLEAR_MASK=SIGNAL("clear_mask()")

SIG_SAVE_MASK=SIGNAL("save_mask()")

SIG_SHOW_MASK=SIGNAL("show_mask(bool)")

SIG_SET_MASK_SIZE=SIGNAL("set_mask_size()")
