"""
Created on Wed Nov 22 14:17:41 2023

@author: prevot
"""

from guiqwt.curve import CurvePlot
from guiqwt.builder import make

from guidata import qapplication
_app = qapplication()


a = make.curve([0,1],[2,3])
pl = CurvePlot()
pl.add_item(a)
pl.show()

_app.exec_()
