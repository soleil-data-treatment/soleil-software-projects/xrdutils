"""
Created on Tue Sep 22 11:15:16 2020

@author: prevot
"""
import numpy as np


from scipy.ndimage import laplace,gaussian_filter

xmin=-1.2
xmax=1.2  #bornes
ymin=-1.5
ymax=1.5

data=0.0*np.random.rand(150,120)
x=np.arange(xmin,xmax,0.02)
y=np.arange(ymin,ymax,0.02)
xv,yv=np.meshgrid(x,y)
data=data+5./(1.+xv*xv+0.5*yv*yv)

a = laplace(data)

np.savetxt('testlaplace.txt', a)
