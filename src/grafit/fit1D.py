"""
Created on Thu Jun 11 17:49:40 2020

@author: prevot
"""


import numpy as np
from os import path,getcwd


import warnings
warnings.simplefilter('ignore', np.RankWarning)

from lmfit import  Parameters

from qtpy.QtCore import Qt,Signal
from qtpy.QtWidgets import (QListWidget,QListWidgetItem,QTableWidget,QTableWidgetItem,
                              QLabel,QMenu,QAction,QMenuBar,
                              QMessageBox,QFileDialog,QMainWindow,QSplitter,QWidget,
                              QComboBox, QVBoxLayout,QHBoxLayout)
from qtpy.QtGui import QCursor,QColor,QPixmap
from guiqwt.baseplot import canvas_to_axes

from guiqwt.config import CONF, _
from guidata.configtools import add_image_path

from guiqwt.histogram import ContrastAdjustment,lut_range_threshold


from guiqwt.interfaces import IPanel,ITrackableItemType
from guiqwt.image import ImagePlot
from guiqwt.plot import  PlotManager
from guiqwt.tools import (SelectTool, RectZoomTool, BasePlotMenuTool, ExportItemDataTool,
                          EditItemDataTool, ItemCenterTool, DisplayCoordsTool, ItemListPanelTool)


from grafit.tools import (RunTool,SaveFitTool,RectangularActionToolCX,TextSizeTool,
                          AddModelTool,PointTool,ExportFitTool)
from grafit.models import list_of_1D_models
from grafit.parameters import Scale1DParam,MaskSizeParam,ExtendedParams,DrawResultParam,AddFitParam
from grafit.plotitems import CurveShape,make,_nanmin,_nanmax
from grafit.widgets import RawDataList,FunctionList,ParameterTable,LayoutWidget,FitWidget,ResultTable,DataTable
from grafit.datatypes import Raw1DData
from grafit.io import readfileparams,readfilewindow,serialize_1D_items,deserialize_1D_items,write_param,ParamReader
from grafit.masking import CurveMaskingWidget
from grafit.plot import CurveFitPlot

abspath=path.abspath(__file__)
dirpath=path.dirname(abspath)

#we add the current directory to have access to the images
add_image_path(dirpath)




def color_iterator():
    colorsuit = ['#4245fd','#33984e','#f9b002','#ff929d','#27bdb1','#c628c7','#77f02a','#84892e','#7b68ab','#8b482b']
    icolor = -1
    while True:
        icolor +=1
        if icolor == 10:
            icolor = 0
        yield colorsuit[icolor]

class Fit1DPlotManager(PlotManager):
    def register_standard_tools(self):
        """
        Registering basic tools for standard plot dialog
        we do not include delete item tool
        --> top of the context-menu
        """
        t = self.add_tool(SelectTool)
        self.set_default_tool(t)
        self.add_tool(RectZoomTool)
        self.add_tool(BasePlotMenuTool, "item")
        self.add_tool(ExportItemDataTool)
        try:
            import spyderlib.widgets.objecteditor  # analysis:ignore
            self.add_tool(EditItemDataTool)
        except ImportError:
            pass
        self.add_tool(ItemCenterTool)
        self.add_separator_tool()
        self.add_tool(BasePlotMenuTool, "grid")
        self.add_tool(BasePlotMenuTool, "axes")
        self.add_tool(TextSizeTool)
        self.add_tool(DisplayCoordsTool)
        if self.get_itemlist_panel():
            self.add_tool(ItemListPanelTool)

    def add_tool_for_plot(self, ToolKlass, plot, *args, **kwargs):
        """
        Register a tool to the manager
            * ToolKlass: tool's class (`guiqwt` builtin tools are defined in
              module :py:mod:`guiqwt.tools`)
            * plot: only this plot will be be registred to the tool
            * args: arguments sent to the tool's class
            * kwargs: keyword arguments sent to the tool's class

        Plot manager's registration sequence is the following:
            1. add plots
            2. add panels
            3. add tools
        """
        if self._first_tool_flag:
            # This is the very first tool to be added to this manager
            self._first_tool_flag = False
            self.configure_panels()
        tool = ToolKlass(self, *args, **kwargs)
        self.tools.append(tool)

        tool.register_plot(plot)

        if len(self.tools) == 1:
            self.default_tool = tool
        return tool



class AbstractFitWindow(QMainWindow):
    #a common class for Fit1DWindow and Fit2Dwindow
    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent = parent)
        self.setWindowTitle("Grafit")
        self.extendedparams=ExtendedParams()
        self.shape_list=[]
        self.resulttable=ResultTable()
        self.shape_list=[]
        self.configure_splitter()
        self.setGeometry(10,30,1260,950)
        self.create_menubar()

        self.color = color_iterator()
#       widget.manager.set_default_toolbar(toolbar)
        self.register_tools()
        self.create_connect()
        self.cwd = getcwd()
        self.save_widget = None
        self.drawresultparam = DrawResultParam()
        self.fit1Dwindow = None  #a fit1Dwindow used for displaying (and possibly fitting) the fit results

    def create_menubar(self):
        self.menubar = QMenuBar(self)
        self.layout().setMenuBar(self.menubar)

        """***************File Menu*******************************"""

        self.menuFile = QMenu("File",self.menubar)

        self.actionOpenhdf5 = QAction("Open hdf5 data",self.menuFile)
        self.actionOpenhdf5.setShortcut("Ctrl+O")

        self.actionImport = QAction("Import data",self.menuFile)

        self.actionOpenPar = QAction("Open model parameters",self.menuFile)

        self.actionSavehdf5 = QAction("Save data to hdf5",self.menuFile)
        self.actionSavehdf5.setShortcut("Ctrl+S")

        self.actionSetSaveFile = QAction("Set filename for saving results",self.menuFile)

        self.actionSavePar = QAction("Save model parameters",self.menuFile)

        self.actionQuit = QAction("Quit",self.menuFile)
        self.actionQuit.setShortcut("Ctrl+Q")

        self.menuFile.addActions((self.actionOpenhdf5, self.actionImport, self.actionOpenPar, self.actionSavehdf5, self.actionSetSaveFile, self.actionSavePar, self.actionQuit))
        self.menubar.addMenu(self.menuFile)

        self.actionOpenhdf5.triggered.connect(self.get_open_hdf5_filename)
        self.actionOpenPar.triggered.connect(self.get_open_par_file)
        self.actionSavehdf5.triggered.connect(self.get_save_hdf5_filename)
        self.actionImport.triggered.connect(self.get_import_filename)
        self.actionSetSaveFile.triggered.connect(self.get_save_filename)
        self.actionSavePar.triggered.connect(self.get_save_par_file)

        self.actionQuit.triggered.connect(self.close)

        """***************Fit Menu*******************************"""
        self.menuFit = QMenu("Fit",self.menubar)

        self.actionFit = QAction("Fit data",self.menuFit)
        self.actionFit.setShortcut("Ctrl+F")

        self.actionSaveFitResult = QAction("Save fit results",self.menuFit)
        self.actionSaveFitResult.setShortcut("Ctrl+S")

        self.actionSaveAndFitNext = QAction("Save and fit next data",self.menuFit)
        self.actionSaveAndFitNext.setShortcut("Ctrl+Y")

        self.actionFitNext = QAction("Don't save and fit next data",self.menuFit)
        self.actionFitNext.setShortcut("Ctrl+N")


        self.menuFit.addActions((self.actionFit, self.actionSaveFitResult, self.actionSaveAndFitNext, self.actionFitNext))
        self.menubar.addMenu(self.menuFit)

        self.actionFit.triggered.connect(self.do_fit)
        self.actionSaveFitResult.triggered.connect(self.save_fit)
        self.actionSaveAndFitNext.triggered.connect(self.save_and_fit_next)
        self.actionFitNext.triggered.connect(self.fit_next)

        """***************Display Menu*******************************"""
        self.menuDisplay = QMenu("Display",self.menubar)

        self.actionShowResultTable = QAction("Result Table",self.menuDisplay)
        self.actionDrawResult = QAction("Draw Result Parameters",self.menuDisplay)
        self.actionClearResults = QAction("Clear Result Table",self.menuDisplay)

        self.menuDisplay.addActions((self.actionShowResultTable, self.actionDrawResult, self.actionClearResults))
        self.menubar.addMenu(self.menuDisplay)

        self.actionShowResultTable.triggered.connect(self.show_result_table)
        self.actionDrawResult.triggered.connect(self.draw_result)
        self.actionClearResults.triggered.connect(self.extendedparams.clear_results)

        """***************Mask Menu*******************************"""
        self.menuMask = QMenu("Mask",self.menubar)

        self.actionApplyShapes =  QAction("Apply masking shapes",self.menuMask)
        self.actionApplyShapes.setShortcut("Ctrl+T")
        self.actionApplyShapes.triggered.connect(self.apply_shapes)

        self.actionRemoveShapes =  QAction("Remove masking shapes",self.menuMask)
        self.actionRemoveShapes.setShortcut("Ctrl+U")
        self.actionRemoveShapes.triggered.connect(self.remove_shapes)

        self.actionApplyMask =  QAction("Apply mask to data",self.menuMask)
        self.actionApplyMask.setShortcut("Ctrl+M")
        self.actionApplyMask.triggered.connect(self.apply_mask)

        self.actionClearMask =  QAction("Clear mask",self.menuMask)
        self.actionClearMask.setShortcut("Ctrl+R")
        self.actionClearMask.triggered.connect(self.clear_mask)

        self.menuMask.addActions((self.actionApplyShapes, self.actionRemoveShapes, self.actionApplyMask, self.actionClearMask))
        self.menubar.addMenu(self.menuMask)

        """***************Preferences Menu*******************************"""
        self.menuPrefs = QMenu("Preferences",self.menubar)

        self.actionScalePrefs = QAction("Scale preferences",self.menuPrefs)
        self.actionScalePrefs.triggered.connect(self.set_scale_pref)

        self.actionRestartPrefs = QAction("Restart from last parameters saved",self)
        self.actionRestartPrefs.setCheckable(True)
        self.actionRestartPrefs.setChecked(True)

        self.actionFollowCurve = QAction("Follow curves",self)
        self.actionFollowCurve.setCheckable(True)
        self.actionFollowCurve.setChecked(True)

        self.actionShowDiff = QAction("Show difference profile(s)",self)
        self.actionShowDiff.setCheckable(True)
        self.actionShowDiff.setChecked(False)
        self.actionShowDiff.triggered.connect(self.show_diff)

        menu = QMenu()
        self.menuPrefs.addActions((self.actionScalePrefs,self.actionRestartPrefs,self.actionFollowCurve,self.actionShowDiff))
        self.menubar.addMenu(self.menuPrefs)


    def register_tools(self):
        pass

    def configure_splitter(self):
        pass

    def create_connect(self):
        self.parametertable.SIG_TRY_CHANGED.connect(self.update_try)
        self.addmodeltool.SIG_MODEL_ADDED.connect(self.add_model_class)
        self.runtool.SIG_VALIDATE_TOOL.connect(self.do_fit)
        self.savefittool.SIG_VALIDATE_TOOL.connect(self.save_fit)
        self.rawdata_list.currentRowChanged.connect(self.rawdata_list_row_changed)

        self.function_list.SIG_MODEL_REMOVED.connect(self.remove_model)
        self.function_list.SIG_CONFIGURE_MODEL.connect(self.configure_model)
        self.function_list.SIG_MODEL_MOVED_UP.connect(self.moveup_model)
        self.function_list.SIG_MODEL_MOVED_DOWN.connect(self.movedown_model)
        self.function_list.SIG_MODEL_SHOWN.connect(self.show_model)
        self.function_list.SIG_MODEL_CONVERTED.connect(self.convert_model)

        self.resulttable.SIG_ROWS_DELETED.connect(self.remove_results)
        self.resulttable.SIG_ROW_EXPORTED.connect(self.recover_previous_result)
        self.maskingpanel.run_tool.SIG_VALIDATE_TOOL.connect(self.apply_mask)

    def set_cwd(self,cwd):
        self.cwd = cwd

    def add_model_class(self,i):
        #print("add_model_class",i)
        modelclass = self.addmodeltool.list_of_models[i]
        added_model = self.extendedparams.add_model_class(modelclass)
        self.function_list.add_item(added_model.name)
        self.parametertable.initparams(settry=True)

        self.current_shape_index = len(self.extendedparams.models)-1
        self.add_shape(added_model)

    def initialize_table_widgets(self):
        self.parametertable.initparams(settry=True)   #refill the parametertable with new values
        self.function_list.initialize() #refill the function table
        if len(self.extendedparams.models) > 0:
            self.update_try()

    def remove_results(self, list_of_rows):
        #list of rows must be sorted descending
        #remove results using selected rows in self.resulttable
        for row in list_of_rows.reverse():
            self.extendedparams.remove_result(row)

    def recover_previous_result(self, row):
        #list of rows must be sorted descending
        #remove results using selected rows in self.resulttable
        self.extendedparams.recover_previous_result(row)
        self.parametertable.initparams(settry=True)   #refill the parametertable with new values
        if len(self.extendedparams.models) > 0:
            self.update_try()

    def remove_model(self):
        self.manager.default_tool.activate()
        for x in self.function_list.selectedIndexes():
            i = x.row()
            self.extendedparams.remove_model_class(i)
            item=self.shape_list.pop(i)
            if item is not None:
                self.data_plot.del_item(item)

        self.initialize_table_widgets()

    def convert_model(self, j):
        """convert all selected models into a model j"""
        modelclass = self.function_list.list_of_models[j]

        self.manager.default_tool.activate()
        for x in self.function_list.selectedIndexes():
            i = x.row()
            added_model = self.extendedparams.convert_model_class(i, modelclass)
            #we suppress previous shape and replace it with the new one. Maybe we could change only if the shape class is different?
            item=self.shape_list.pop(i)
            if item is not None:
                self.data_plot.del_item(item)
            self.add_shape(added_model, i)

        self.initialize_table_widgets()

    def moveup_model(self,i):
        #take item at i and put it at i-1, we assume that i<len(self.shape_list-1)
        self.extendedparams.moveup_model_class(i)
        item=self.shape_list.pop(i)
        if item is not None:
            self.shape_list.insert(i-1,item)
        else:
            self.initialize_table_widgets()

    def movedown_model(self,i):
        #take item at i and put it at i+1, we assume that i>0
        self.extendedparams.movedown_model_class(i)
        item=self.shape_list.pop(i)
        if item is not None:
            self.shape_list.insert(i+1,item)
        else:
            self.initialize_table_widgets()

    def show_model(self, b=True):
        for x in self.function_list.selectedIndexes():
            shape = self.shape_list[x.row()]
            if shape is not None:
                shape.setVisible(b)
        self.data_plot.replot()

    def get_open_hdf5_filename(self):
        fname = QFileDialog.getOpenFileName(None, 'Open hdf5 data file',self.cwd,filter="*.hdf5")[0]
        if len(fname) == 0:
            return
        self.open_hdf5(fname)


    def get_import_filename(self):
        fname = QFileDialog.getOpenFileName(None, 'Import text data',self.cwd)[0]
        if len(fname) == 0:
            return
        self.import_filename(fname)


    def open_hdf5(self,fname):
        try:
            rawdata_list=self.deserialize_items(fname)
        except:
            QMessageBox.warning(self, 'warning', 'unable to open hdf file')
            return

        for rawdata in rawdata_list:
            self.rawdata_list.add_item(rawdata)
        self.cwd = path.dirname(path.realpath(fname))

    def get_save_hdf5_filename(self):
        fname = QFileDialog.getSaveFileName(None, 'Save data to hdf5 file',self.cwd,filter="*.hdf5")[0]
        if len(fname) == 0:
            return
        self.save_hdf5(fname)

    def save_hdf5(self,fname):
        if len(self.rawdata_list.rawdatalist)>0:
            self.serialize_items(self.rawdata_list.rawdatalist,fname)

    def get_open_par_file(self, checked=True):
        filename = QFileDialog.getOpenFileName(None,'open parameters file',self.cwd,filter="*.par")[0]
        if len(filename)==0:
            return
        self.cwd = path.dirname(path.realpath(filename))
        self.open_par_file(filename)

    def open_par_file(self,filename,message = True):
        reader = ParamReader(filename,self.extendedparams,dimension=self.DIMENSION)
        info1,info2 = reader.open_and_read()
        if message:
            QMessageBox.information(self,info1,info2)
        if info1 == 'Warning':
            return

        #we remove previous shapes
        for item in self.shape_list:
            if item is not None:
                self.data_plot.del_item(item)


        self.shape_list.clear()
        #we add new shapes
        for model in self.extendedparams.models:
            self.add_shape(model)

        self.initialize_table_widgets()

    def get_save_filename(self):
        fname = QFileDialog.getSaveFileName(None, 'Set filename for saving fit parameters',self.cwd,filter="*.txt")[0]
        if len(fname) == 0:
            return
        self.set_save_filename(fname)

    def set_save_filename(self,fname):
        self.savename = fname
        self.cwd = path.dirname(path.realpath(self.savename))

    def get_save_par_file(self):
        filename = QFileDialog.getSaveFileName(None,'save parameter file',self.cwd)[0]
        if len(filename)==0:
            return
        self.cwd = path.dirname(path.realpath(filename))
        if len(filename)>4:
            if  filename[-4] ==  '.':
                filename = filename[:-4]
        self.save_par_file(filename+'.par')

    def save_par_file(self,filename):
        write_param(filename,self.extendedparams,dimension = self.DIMENSION)


    def make_new_table(self,columns,labels,title=None):
        new_data_table = DataTable()
        new_data_table.populate(columns,labels)
        self.table_list.append(new_data_table)
        if title is None or title=='':
            title = "table%d"%len(self.table_list)

        new_data_table.setWindowTitle(title)
        action = QAction(title,self.menuDisplay)

        self.menuDisplay.addAction(action)
        action.triggered.connect(new_data_table.show)
        self.menubar.addMenu(self.menuDisplay)
        new_data_table.SIG_FIT_DATA.connect(self.fit_data_table)
        new_data_table.show()

    def fit_data_table(self):
         pass

    def do_fit(self):
        self.set_fit_indices()
        ok = self.extendedparams.do_fit(self.fitted_data,self.fitted_x,self.fitted_weights)

        if not ok:
            QMessageBox.warning(self,'error','error while trying to fit')
            return
        self.update_opt()
        self.parametertable.initparams(setopt=True)
        fit_str = self.extendedparams.get_opt_reprstring()
        print(fit_str)

    def save_fit(self):
        #save fit results
        if not self.extendedparams.isfitted:
            #no fit has been performed!
            return

        if self.savename is None:
            self.get_save_filename()
            if self.savename is None:
                return

        fic=open(self.savename,'a')
        fic.write(self.extendedparams.save_opt_results())
        fic.close()

        ific=1

        #figshortname=self.extendedparams.rawdata.title+'_%.3d.png'%(ific)
        #figname=path.join(self.cwd,figshortname)
        figname=self.savename[:-4]+'_'+self.extendedparams.rawdata.title+'_%.3d.png'%(ific)

        while path.isfile(figname):
            ific=ific+1
            #figshortname=self.extendedparams.rawdata.title+'_%.3d.png'%(ific)
            #figname=path.join(self.cwd,figshortname)
            figname=self.savename[:-4]+'_'+self.extendedparams.rawdata.title+'_%.3d.png'%(ific)

        self.saved_widget.save_widget(figname)
        print(figname,' saved')

        if self.actionRestartPrefs.isChecked():
            self.extendedparams.set_whole_as_try()
            self.parametertable.initparams(settry=True)
        self.extendedparams.issaved = True

    def save_and_fit_next(self):
        self.apply_shapes()
        self.save_fit()
        i = self.rawdata_list.rawdatalist.index(self.extendedparams.rawdata)+1
        #go to next if possible:
        if i<len(self.rawdata_list.rawdatalist):
            self.rawdata_list.setCurrentRow(i)
            self.do_fit()

    def fit_next(self):
        self.apply_shapes()
        self.extendedparams.isfitted = False  #avoid asking saving fit
        i = self.rawdata_list.rawdatalist.index(self.extendedparams.rawdata)+1
        #go to next if possible:
        if i<len(self.rawdata_list.rawdatalist):
            self.rawdata_list.setCurrentRow(i)
            self.do_fit()

    def set_scale_pref(self):
        self.scaleparam.edit()

    def rawdata_list_row_changed(self,i):
        #the current image list row has changed
        rawdata = self.rawdata_list.rawdatalist[i]
        if self.extendedparams.isfitted and not self.extendedparams.issaved:
            #a fit has been performed but not saved
            ask=QMessageBox.question(self, "save", "do you want to save the fit?", QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)

            if ask==QMessageBox.Yes:
                self.save_fit()

        self.set_current_rawdata(rawdata)

    def update_from_shape(self, shape, handle=0):
        if shape in self.shape_list:
            shape.set_params(self.extendedparams.partry, handle)  #update parameters from shape points
            self.update_try()
            self.parametertable.initparams(settry=True)

    def update_from_shapes(self, x0, y0, x1, y1):
        #: connected to Signal emitted by plot when an IBasePlotItem object was moved (args: x0, y0, x1, y1)
        for item in self.data_plot.get_selected_items():
            if item in self.shape_list:
                item.set_params(self.extendedparams.partry, 0)  #update parameters from shape points

        self.update_try()
        self.parametertable.initparams(settry=True)

    def show_result_table(self):
        self.resulttable.update_results(self.extendedparams.results,self.extendedparams.result_vars)
        self.resulttable.show()

    def draw_result(self):
        self.data_added = False   #to be sure that a data will be added even if the user forget to press apply button
        if len(self.extendedparams.result_vars)==0:
            return
        self.drawresultparam.update_choice(self.extendedparams.result_vars)
        ok = self.drawresultparam.edit(apply=self.add_result_curve)
        if ok and not self.data_added:
            self.add_result_curve(self.drawresultparam)

    def add_result_curve(self,param):

        i = param.x_label
        j = param.y_label
        x_label = self.extendedparams.result_vars[i]
        y_label = self.extendedparams.result_vars[j]
        print (x_label,y_label)
        x,y,w=self.extendedparams.get_result_values(x_label,y_label)
        #s is sigma, we convert it to weigths
        if w is not None:
            w = 1./w
        if self.fit1Dwindow == None:
            self.fit1Dwindow = Fit1DWindow()
            self.fit1Dwindow.exportfittool.action.setVisible(True)
            self.fit1Dwindow.exportfittool.SIG_VALIDATE_TOOL.connect(self.apply_fit1D_expression)

        self.fit1Dwindow.add_data(x,y,w,title=y_label+'=f('+x_label+')',x_label=x_label,y_label=y_label)
        self.data_added = True
        self.fit1Dwindow.show()

    def apply_fit1D_expression(self):

        if self.fit1Dwindow.extendedparams.isfitted is False:
            return
        fit_str = self.fit1Dwindow.extendedparams.get_opt_reprstring()
        entry = self.fit1Dwindow.extendedparams.rawdata.y_label

        self.extendedparams.partry[entry].expr=fit_str
        self.parametertable.initparams(settry=True)
        self.update_try()

    def apply_mask(self, triggered=True):
        pass

    def clear_mask(self, triggered=True):
        self.maskingpanel.clear_mask()
        self.apply_mask()

    def apply_shapes(self, triggered=True):
        self.maskingpanel.apply_shapes()
        self.apply_mask()

    def remove_shapes(self, triggered=True):
        self.maskingpanel.remove_all_shapes()


    def closeEvent(self, event):
        result = QMessageBox.question(self,
                      "You are about to quit...",
                      "Do you want to save data into an hdf5 file ?",
                      QMessageBox.Yes| QMessageBox.No | QMessageBox.Cancel)

        if result ==   QMessageBox.Cancel:
            event.ignore()
        elif result ==   QMessageBox.Yes:
            self.get_save_hdf5_filename()
            event.accept()
        else:
            event.accept()






#this window holds the toolbar and the centralwidget
class Fit1DWindow(AbstractFitWindow):
    DIMENSION = 1
    def __init__(self, parent = None):
        super().__init__(parent = parent)
        self.setWindowTitle("Fit 1D")

        self.scaleparam=Scale1DParam()  #parameters for scale

        self.set_default_curves()
        self.savename = None  #name where to save the fit
        self.readfileparams = readfileparams()
        self.table_list=[]    #list of all tables opened (excepted resulttable which is treated separately)
        self.deserialize_items = deserialize_1D_items
        self.serialize_items = serialize_1D_items
        self.saved_widget = self.data_plot  #widget that will be saved if the fit is saved
        self.set_default_params()

    def set_default_curves(self):
        #top left image contains rawdata.data array to be fitted, with masking options
                #default empty curves
        z=np.empty((0,))
        self.empty=True

        self.data_curve=make.maskederror(np.ma.array(z),z,z,z,marker="Diamond",markerfacecolor='r',markeredgecolor='r',markersize=4,linestyle="NoPen",title='data')
        self.model_curve=make.curve(z,z,color='g',title='model')
        self.difference_curve=make.curve(z,z,color='k',title='difference')
        self.data_curve.set_readonly(True)
        self.model_curve.set_readonly(True)
        self.difference_curve.set_readonly(True)

        self.data_plot.add_item(self.data_curve)
        self.data_plot.add_item(self.model_curve)
        self.data_plot.add_item(self.difference_curve)
        self.difference_curve.setVisible(self.actionShowDiff.isChecked())


    def configure_splitter(self):

        centralwidget = QSplitter(Qt.Horizontal, self)
        splitter = QSplitter(Qt.Vertical, self)
        self.setCentralWidget(centralwidget)

        centralwidget.addWidget(splitter)

        self.data_plot = CurveFitPlot(self, title='data')

        splitter.addWidget(self.data_plot)

        #adding parameter table panel
        self.parametertable= ParameterTable(self,extendedparams=self.extendedparams)
        splitter.addWidget(self.parametertable)

        #adding a vertical layout with itemlist, fitwidget, functionlist
        self.vlayoutw = LayoutWidget(self,'vertical')
        self.rawdata_list = RawDataList(self)
        self.fitwidget = FitWidget(self,self.extendedparams)
        self.function_list = FunctionList(self.extendedparams, self.shape_list, list_of_1D_models, self)
        self.vlayoutw.addWidget(self.rawdata_list)
        self.vlayoutw.addWidget(self.fitwidget)
        self.vlayoutw.addWidget(self.function_list)
        centralwidget.addWidget(self.vlayoutw)

        #adding the masking panel
        self.maskingpanel = CurveMaskingWidget(self)
        centralwidget.addWidget(self.maskingpanel)


        #registring panels to the plot manager
        self.manager = Fit1DPlotManager(self)
        self.manager.add_plot(self.data_plot)
        for panel in (self.parametertable,self.maskingpanel):
            self.manager.add_panel(panel)

        toolbar = self.addToolBar("tools")
        self.manager.add_toolbar(toolbar, id(toolbar))

    def register_tools(self):
        self.manager.register_all_curve_tools()
        self.addmodeltool = self.manager.add_tool(AddModelTool, list_of_1D_models)
        self.curvepeaktool = self.manager.add_tool_for_plot(RectangularActionToolCX,self.data_plot, self.set_curve_peak,toolbar_id=None)
        self.pointtool = self.manager.add_tool_for_plot(PointTool,self.data_plot, self.set_curve_point,toolbar_id=None)
        self.runtool = self.manager.add_tool(RunTool)
        self.exportfittool = self.manager.add_tool(ExportFitTool)
        self.savefittool = self.manager.add_tool(SaveFitTool)
        self.exportfittool.action.setVisible(False)

    def create_connect(self):
        super().create_connect()

        self.data_plot.SIG_ITEM_MOVED.connect(self.update_from_shapes)
        self.data_plot.SIG_SHAPE_CHANGED.connect(self.update_from_shape)

    def set_default_params(self):
        #create a fit with a linear bg
        self.data = None
        self.add_model_class(0)

    def add_shape(self, model, index=None):
        shape = CurveShape(model = model)
        shape.set_style("plot","shape/curveshape")
        color = next(self.color)

        shape.set_color(color)

        if index == None:
            self.shape_list.append(shape)
        else:
            self.shape_list.insert(index,shape)

        self.data_plot.add_item(shape)
        shape.initialize_points(self.extendedparams.partry)  #initialize shape anchor points from position

        if index == None:
            #we select the curve and use the relevant tool to configure it,
            #otherwise, add_shape is used to replace an already existing shape, we do nothing
            self.data_plot.select_item(shape)
            self.configure_model(self.current_shape_index)

        if self.data is not None:
            self.update_try()
        #print(self.data_plot.get_items())

    def set_curve_point(self,plot, pos, handle):
        #estimate the background from n points taken from the data_image shown in the data_plot
        i = self.current_shape_index
        shape=self.shape_list[i]

        if shape is not None:
            handle = handle % shape.points.shape[0]
            shape.move_local_point_to(handle, pos)
            shape.set_params(self.extendedparams.partry,handle)

        self.parametertable.initparams(settry=True)
        self.update_try()

    def set_curve_peak(self,plot, p0, p1):
        i = self.current_shape_index
        shape=self.shape_list[i]
        if shape is not None:
            shape.set_from_tool(self.extendedparams.partry, p0, p1)
            #shape.set_params(self.extendedparams.partry,0)
        self.parametertable.initparams(settry=True)
        self.update_try()

    def configure_model(self,i):
        #from the drawing on the data, adjust the parameters of the model
        #depending on the model, curves are set by drawing points
        #or by drawing a rectangle
        self.current_shape_index = i
        if self.extendedparams.models[i].EVALUATE=="peak":
            self.curvepeaktool.activate()
        if self.extendedparams.models[i].EVALUATE=="points":
            self.pointtool.activate()

    def import_filename(self,fname):
        #import data from a text file and creates a table
        fic=open(fname)
        ficlines= fic.readlines()
        fic.close()

        ok = readfilewindow(self.readfileparams,ficlines).exec()
        if not ok:
            return
        nstart = self.readfileparams.heading + self.readfileparams.title
        #creating all columns:
        columns = []
        nlabels = len(self.readfileparams.labels)

        for j in range(nlabels):
            columns.append([])

        for lig in ficlines[nstart:]:
            ll = lig.split(self.readfileparams.pattern)
            for j in range(nlabels):
                try:
                     columns[j].append(ll[j])
                except IndexError:
                    columns[j].append("")

        self.make_new_table(columns,self.readfileparams.labels,title=path.basename(fname))


    def fit_data_table(self,data_table):
        #from columns that have been selected in a data_table, make a dataset
        sel_labels,sel_column = data_table.get_selected_column_labels()  #label is the column header, column is the column number
        n_sel = len(sel_labels)
        if n_sel == 0:
            QMessageBox.information(self,'Warning','select at least one column')
            return
        elif n_sel == 1:
            y = data_table.getColumnValues(sel_column[0])
            ny = len(y)
            x = np.arange(ny)
            y_label = sel_labels[0]
            title = data_table.windowTitle()+'_'+y_label+'=f(n)'
            self.fit1Dwindow.add_data(x,y,title=title,y_label=y_label)
        else:
            param=AddFitParam()

            param.update_choice(data_table.windowTitle())
            param.update_column_choice(sel_labels,sel_column)
            #print (column)
            param.x_label = sel_column[0]
            param.y_label = sel_column[1]
            if n_sel>2 :
                param.s_label = sel_column[2]

            #print( param)
            ok = param.edit()
            if not ok:
                return

            x_label = data_table.labels[param.x_label]
            y_label = data_table.labels[param.y_label]
            x = data_table.getColumnValues(param.x_label)
            y = data_table.getColumnValues(param.y_label)
            title = data_table.windowTitle()+'_'+y_label+'=f('+x_label+')'

            if param.sigma == 0:
                self.add_data(x,y,title=title,x_label=x_label,y_label=y_label)
            else:
                w = data_table.getColumnValues(param.s_label)
                if param.sigma == 1:
                    self.add_data(x,y,1./w,title=title,x_label=x_label,y_label=y_label)
                else:
                    self.add_data(x,y,w,title=title,x_label=x_label,y_label=y_label)

    def add_data(self,x,y,weights=None,title="",tags={},x_label='x',y_label='y'):
        rawdata = Raw1DData(x,y,weights,title,tags,x_label,y_label)
        self.rawdata_list.add_item(rawdata)
        self.set_current_rawdata(rawdata)


    def set_current_rawdata(self,rawdata):
        #set image to fit where rawdata contains data: a 2D array, xdata and ydata: the limits, weights: 1/sigma
        #update image representation
        #self.data_curve is a plotitems.MaskedErrorBarCurveItem instance

        if self.actionFollowCurve.isChecked():
            #we shift all curves in order to keep the same position with respect to the scan center (middle of x limits)
            x, y, _dx, _dy = self.data_curve.get_data()
            if len(x)>0:
                xc = (np.amin(x)+np.amax(x))/2.
                do_shift = True
            else:
                do_shift = False
        else:
            do_shift = False

        self.extendedparams.set_rawdata(rawdata)
        if rawdata.weights is not None:
            self.data_curve.set_data(rawdata.x,rawdata.y,dy=1./rawdata.weights)
        else:
            self.data_curve.set_data(rawdata.x,rawdata.y)

        #update axis labels
        self.data_plot.set_axis_title('left',rawdata.y_label)
        self.data_plot.set_axis_title('bottom',rawdata.x_label)

        self.set_fit_indices()

        if do_shift:
            #we shift all curves in order to keep the same position with respect to the scan center (middle of x limits)
            delta_xc = (np.amin(rawdata.x)+np.amax(rawdata.x))/2.-xc
            self.extendedparams.shift_all_curves(delta_xc)

        self.parametertable.initparams(settry=True)    #if tags have changed, put them into the table


        self.update_try()
        if self.scaleparam.autoscale:
            self.do_autoscale_on_data()


    def do_autoscale_on_data(self,replot=True):
        # autoscale only on the data curve (ignoring fit curve)
        for axis_id in self.data_plot.AXIS_IDS:
            vmin, vmax = None, None

            bounds = self.data_curve.boundingRect()
            if axis_id == self.data_curve.xAxis():
                vmin, vmax = bounds.left(), bounds.right()
            elif axis_id == self.data_curve.yAxis():
                vmin, vmax = bounds.top(), bounds.bottom()

            if vmin is None or vmax is None:
                continue
            elif vmin == vmax: # same behavior as MATLAB
                vmin -= 1
                vmax += 1

            elif self.data_plot.get_axis_scale(axis_id) == 'lin':
                dv = vmax-vmin
                vmin -= .002*dv
                vmax += .002*dv

            elif vmin > 0 and vmax > 0: # log scale
                dv = np.log10(vmax)-np.log10(vmin)
                vmin = 10**(np.log10(vmin)-.002*dv)
                vmax = 10**(np.log10(vmax)+.002*dv)

            self.data_plot.set_axis_limits(axis_id, vmin, vmax)

        if replot:
            self.data_plot.replot()
        self.data_plot.SIG_PLOT_AXIS_CHANGED.emit(self.data_plot)


    def set_fit_indices(self):
        #use flatten arrays for the fit: fitted_data,fitted_weights,fitted_x
        #do not take into account masked values

        self.fitted_data = self.extendedparams.rawdata.y[~self.extendedparams.rawdata.x.mask].ravel()
        self.fitted_x = self.extendedparams.rawdata.x.compressed()

        if self.extendedparams.rawdata.weights is not None:
            self.fitted_weights = self.extendedparams.rawdata.weights[~self.extendedparams.rawdata.x.mask].ravel()      #error at indices, 1D array!
        else:
            self.fitted_weights = np.ones_like(self.fitted_x)

    def show_diff(self):
        self.difference_curve.setVisible(self.actionShowDiff.isChecked())
        self.data_plot.replot()


    def update_try(self):
        #evaluate model with try parameters and update model and difference image
        if self.extendedparams.composite_model == None:
            return
        values = self.extendedparams.eval_try(self.fitted_x).ravel()-self.fitted_data

        self.difference_curve.set_data(self.fitted_x,values)

        diff = values*self.fitted_weights
        chi2 = np.sum((diff)**2)

        di=np.sum(diff)

        self.fitwidget.set_chi2(chi2,di)

        for i,shape in enumerate(self.shape_list):
            if shape is not None:
                shape.set_from_params(self.extendedparams.partry)
                """
                if shape.ON_BG:
                    x0 = shape.get_center_position()
                    y0 = self.extendedparams.eval_try(x0)
                    shape.adjust_height(y0)
                """
        #print ('self.data_plot',self.data_plot)
        try:
            xmin, xmax = self.data_plot.get_axis_limits("bottom")
        except:
            xmin, xmax = 0., 1.
        x=np.linspace(xmin, xmax, num=101)
        values = self.extendedparams.eval_try(x).ravel()
        self.model_curve.set_data(x,values)
        self.data_plot.replot()

    def update_opt(self):
        #evaluate model with try parameters and update model and difference image
        values = self.extendedparams.eval_opt(self.fitted_x).ravel()-self.fitted_data
        self.difference_curve.set_data(self.fitted_x,values)

        diff = values*self.fitted_weights
        chi2 = np.sum((diff)**2)

        di=np.sum(diff)

        self.fitwidget.set_chi2(chi2,di)

        for i,shape in enumerate(self.shape_list):
            if shape is not None:
                shape.set_from_params(self.extendedparams.paropt)

        xmin, xmax = self.data_plot.get_axis_limits("bottom")
        x=np.linspace(xmin, xmax, num=101)
        values = self.extendedparams.eval_opt(x).ravel()
        self.model_curve.set_data(x,values)
        self.data_plot.replot()

    def apply_mask(self, triggered=True):
        self.extendedparams.rawdata.x.mask = np.array(self.data_curve.get_mask(),dtype = bool, copy=True)




def test(win):
    #make a test model
    xmin=-1.
    xmax=1.  #bornes

    data=np.random.rand(100)
    x=np.ma.arange(xmin,xmax,0.02)
    data=data+5./(1.+x*x)
    tags={'Qz':0.15}
    #x=np.ma.masked_inside(x,0,1.0)
    #print (x)
    win.add_data(x,data,weights=np.ones_like(data),title='test1',tags=tags)
    data=data+0.5*x

    win.add_data(x+1,data,title='test2',tags=tags)


if __name__ == "__main__":

    from guidata import qapplication
    _app = qapplication()

    win = Fit1DWindow()
    test(win)

    win.show()

    _app.exec_()
