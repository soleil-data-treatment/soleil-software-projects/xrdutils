"""
Created on Tue Feb 18 09:44:47 2020

@author: Prevot
"""
#we use x=np.nonzero(contributions) -> for example x[0]=h values ,x[1]=k values

import numpy as np
from os import path,getcwd

from lmfit import  Parameters

from qtpy.QtCore import Qt,Signal
from qtpy.QtGui import QCursor,QColor,QPixmap
from qtpy.QtWidgets import (QListWidget,QListWidgetItem,QTableWidget,QTableWidgetItem,
                              QLabel,QMenu,QAction,QMenuBar,
                              QMessageBox,QFileDialog,QMainWindow,QSplitter,QWidget,
                              QComboBox, QVBoxLayout,QHBoxLayout)
from guidata.dataset.dataitems import MultipleChoiceItem
from guiqwt.config import _
from guidata.configtools import add_image_path
from guiqwt.curve import CurvePlot
from guiqwt.histogram import ContrastAdjustment,lut_range_threshold


from guiqwt.interfaces import IPanel
from guiqwt.image import ImagePlot
from guiqwt.plot import  PlotManager
from guiqwt.tools import (SelectTool, RectZoomTool, BasePlotMenuTool, ExportItemDataTool, EditItemDataTool, ItemCenterTool, DisplayCoordsTool, ItemListPanelTool)
from guiqwt.io import iohandler, imread

from grafit.tools import ShowWeightsTool,RunTool,SaveFitTool,CircularActionToolCXY,AddModelTool,PointTool,TextSizeTool
from grafit.models import list_of_2D_models
from grafit.parameters import Scale2DParam,MaskSizeParam,DrawResultParam,ExtendedParams
from grafit.plotitems import PeakShape, BackgroundShape, make,_nanmin,_nanmax
from grafit.widgets import RawDataList,FunctionList,ParameterTable,LayoutWidget,FitWidget,ResultTable
from grafit.fit1D import AbstractFitWindow, Fit1DWindow
from grafit.datatypes import Raw2DData
from grafit.io import serialize_2D_items,deserialize_2D_items,write_param,ParamReader
from grafit.masking import ImageMaskingWidget

abspath=path.abspath(__file__)
dirpath=path.dirname(abspath)

#we add the current directory to have access to the images
add_image_path(dirpath)





class ImageFitPlot(ImagePlot):


    SIG_KEY_PRESSED = Signal(int) #signal emitted by plot when a key is pressed
    SIG_SHAPE_CHANGED = Signal("PyQt_PyObject",int) #signal emitted by plot when shape is modified with handle

    def keyPressEvent(self, event):
        "on redefinit l'action liee a un evenement clavier quand la fenetre a le focus"
        #the keys have been inverted for the vertical displacement because the y axis is oriented in the increasing direction
        touche=event.key()
        if touche in [42,43,45,47]:
            self.SIG_KEY_PRESSED.emit(touche)



class Fit2DPlotManager(PlotManager):
    def register_standard_tools(self):
        """
        Registering basic tools for standard plot dialog
        we do not include delete item tool
        --> top of the context-menu
        """
        t = self.add_tool(SelectTool)
        self.set_default_tool(t)
        self.add_tool(RectZoomTool)
        self.add_tool(BasePlotMenuTool, "item")
        self.add_tool(ExportItemDataTool)
        try:
            import spyderlib.widgets.objecteditor  # analysis:ignore
            self.add_tool(EditItemDataTool)
        except ImportError:
            pass
        self.add_tool(ItemCenterTool)
        self.add_separator_tool()
        self.add_tool(BasePlotMenuTool, "grid")
        self.add_tool(BasePlotMenuTool, "axes")
        self.add_tool(TextSizeTool)
        self.add_tool(DisplayCoordsTool)
        if self.get_itemlist_panel():
            self.add_tool(ItemListPanelTool)

    def add_tool_for_plot(self, ToolKlass, plot, *args, **kwargs):
        return self.add_tool_for_plots(ToolKlass, [plot,], *args, **kwargs)

    def add_tool_for_plots(self, ToolKlass, plots, *args, **kwargs):
        """
        Register a tool to the manager
            * ToolKlass: tool's class (`guiqwt` builtin tools are defined in
              module :py:mod:`guiqwt.tools`)
            * plots: only these plots will be be registred to the tool
            * args: arguments sent to the tool's class
            * kwargs: keyword arguments sent to the tool's class

        Plot manager's registration sequence is the following:
            1. add plots
            2. add panels
            3. add tools
        """
        if self._first_tool_flag:
            # This is the very first tool to be added to this manager
            self._first_tool_flag = False
            self.configure_panels()
        tool = ToolKlass(self, *args, **kwargs)
        self.tools.append(tool)
        for plot in plots:
            tool.register_plot(plot)

        if len(self.tools) == 1:
            self.default_tool = tool
        return tool




#this window holds the toolbar and the centralwidget
class Fit2DWindow(AbstractFitWindow):
    DIMENSION = 2
    def __init__(self, parent = None):
        super().__init__(parent = parent)
        self.setWindowTitle("Fit 2D")

        self.set_default_images()
        self.savename = None  #name where to save the fit
        self.scaleparam=Scale2DParam()  #parameters for scale
        self.masksizeparam=MaskSizeParam() #parameters for mask tool


        self.deserialize_items = deserialize_2D_items
        self.serialize_items = serialize_2D_items
        self.saved_widget = self.imagelayoutw  #widget that will be saved if the fit is saved
        self.set_default_params()

    def set_cwd(self,cwd):
        self.cwd = cwd

    def set_default_images(self):
        #top left image contains rawdata.data array to be fitted, with masking options
        self.data_image = make.maskedimagenan(np.ma.zeros((1,1)),title='data image',xdata=[0,1],ydata=[0,1],interpolation='nearest',show_mask=True)
        #top middle image contains model image
        self.model_image = make.imagenan(np.zeros((1,1)),title='model image',xdata=[0,1],ydata=[0,1],interpolation='nearest')
        #top right image contains the difference map
        self.difference_image = make.imagenan(np.zeros((1,1)),title='difference image',xdata=[0,1],ydata=[0,1],interpolation='nearest',colormap='RdBu')

        z=np.empty((0,))
        self.data_hprofile = make.curve(z,z,title='data horizontal profile',marker="Diamond",markerfacecolor='r',markeredgecolor='r',markersize=4,linestyle="NoPen")
        self.data_vprofile = make.curve(z,z,title='data vertical profile',marker="Diamond",markerfacecolor='r',markeredgecolor='r',markersize=4,linestyle="NoPen")

        self.model_hprofile = make.curve(z,z,title='model horizontal profile',color='k')
        self.model_vprofile = make.curve(z,z,title='model vertical profile',color='k')

        self.difference_hprofile = make.curve(z,z,title='model horizontal difference',color='b')
        self.difference_vprofile = make.curve(z,z,title='model vertical difference',color='b')

        self.difference_hprofile.setVisible(self.actionShowDiff.isChecked())
        self.difference_vprofile.setVisible(self.actionShowDiff.isChecked())

        self.data_plot.add_item(self.data_image)
        self.model_plot.add_item(self.model_image)
        self.difference_plot.add_item(self.difference_image)

        self.hprofile.add_item(self.data_hprofile)
        self.hprofile.add_item(self.model_hprofile)
        self.hprofile.add_item(self.difference_hprofile)

        self.vprofile.add_item(self.data_vprofile)
        self.vprofile.add_item(self.model_vprofile)
        self.vprofile.add_item(self.difference_vprofile)


    def configure_splitter(self):

        centralwidget = QSplitter(Qt.Horizontal, self)
        splitter = QSplitter(Qt.Vertical, self)
        self.setCentralWidget(centralwidget)

        centralwidget.addWidget(splitter)

        self.data_plot = ImageFitPlot(self,title='data',yreverse=False,lock_aspect_ratio=False,xunit='',yunit='')
        self.model_plot = ImagePlot(self,title='model',yreverse=False,lock_aspect_ratio=False,xunit='',yunit='')
        self.difference_plot = ImagePlot(self,title='difference',yreverse=False,lock_aspect_ratio=False,xunit='',yunit='')

        self.contrast = ContrastAdjustment(self)
        self.hprofile = CurvePlot(self,title='horizontal profile')
        self.vprofile = CurvePlot(self,title='vertical profile')

        #adding layout widget with 3 plots and itemlist
        self.imagelayoutw = LayoutWidget(self,'horizontal')
        self.imagelayoutw.addWidget(self.data_plot)
        self.imagelayoutw.addWidget(self.model_plot)
        self.imagelayoutw.addWidget(self.difference_plot)
        splitter.addWidget(self.imagelayoutw)

        #adding layout widget with 3 plots with profiles
        self.plotlayoutw = LayoutWidget(self,'horizontal')
        self.plotlayoutw.addWidget(self.contrast)
        self.plotlayoutw.addWidget(self.hprofile)
        self.plotlayoutw.addWidget(self.vprofile)
        splitter.addWidget(self.plotlayoutw)

        #adding parameter table panel
        self.parametertable= ParameterTable(self,extendedparams=self.extendedparams)
        splitter.addWidget(self.parametertable)

        #adding a vertical layout with itemlist, fitwidget, functionlist
        self.vlayoutw = LayoutWidget(self,'vertical')
        self.rawdata_list = RawDataList(self)
        self.fitwidget = FitWidget(self,self.extendedparams)
        self.function_list = FunctionList(self.extendedparams, self.shape_list, list_of_2D_models, self)
        self.vlayoutw.addWidget(self.rawdata_list)
        self.vlayoutw.addWidget(self.fitwidget)
        self.vlayoutw.addWidget(self.function_list)
        centralwidget.addWidget(self.vlayoutw)

        #adding the masking panel, with tools restricted to the data_plot
        self.maskingpanel = ImageMaskingWidget(self, plots = [self.data_plot,])
        centralwidget.addWidget(self.maskingpanel)

        #registring panels to the plot manager
        self.manager = Fit2DPlotManager(self, )

        for plot in (self.data_plot, self.model_plot, self.difference_plot, self.hprofile, self.vprofile):
            self.manager.add_plot(plot)
        for panel in (self.contrast,self.parametertable,self.maskingpanel):
            self.manager.add_panel(panel)

        toolbar = self.addToolBar("tools")
        self.manager.add_toolbar(toolbar, id(toolbar))

    def register_tools(self):
        self.manager.register_all_image_tools()
        self.addmodeltool = self.manager.add_tool(AddModelTool, list_of_2D_models)
        self.spottool = self.manager.add_tool_for_plot(CircularActionToolCXY,self.data_plot, self.estimate_spot,self.scale_spot,toolbar_id=None)
        self.pointtool = self.manager.add_tool_for_plot(PointTool,self.data_plot, self.estimate_bg,toolbar_id=None)
        self.runtool = self.manager.add_tool(RunTool)
        self.savefittool = self.manager.add_tool(SaveFitTool)
        self.showweightstool = self.manager.add_tool(ShowWeightsTool)


    def create_connect(self):
        super().create_connect()

        self.showweightstool.SIG_VALIDATE_TOOL.connect(self.show_weigths)
        self.data_plot.SIG_LUT_CHANGED.connect(self.do_lut_from_data)
        self.model_plot.SIG_LUT_CHANGED.connect(self.do_lut_from_model)
        self.data_plot.SIG_ITEM_MOVED.connect(self.update_from_shapes)
        self.data_plot.SIG_SHAPE_CHANGED.connect(self.update_from_shape)
        self.data_plot.SIG_KEY_PRESSED.connect(self.set_shape_intensity)

    def set_default_params(self):
        #create a fit with a planar bg
        self.data = None
        self.add_model_class(0)

    def add_shape(self, model, index=None):
        #creates and add to the plot a fitting shape associated with the model
        tool = None
        shape = None

        if model.EVALUATE == "peak":
            shape = PeakShape(0, 0, 1, 1, model = model)
            tool = self.spottool
        elif model.EVALUATE == "points":
            shape=BackgroundShape(model.get_curve_points_len(), model = model, image = self.data_image)
            tool = self.pointtool

        if shape is not None:
            self.data_plot.add_item(shape)

        if index == None:
            self.shape_list.append(shape)
            if tool is not None:
                tool.activate()
        else:
            self.shape_list.insert(index,shape)

        if self.data is not None:
            self.update_try()

    def estimate_bg(self, plot, pos, handle):
        #estimate the background from (three) points taken from the data_image shown in the data_plot
        i = self.current_shape_index
        shape=self.shape_list[i]

        if shape is not None:
            handle = handle % shape.points.shape[0]
            shape.move_local_point_to(handle, pos)
            shape.set_params(self.extendedparams.partry,0)

        self.parametertable.initparams(settry=True)
        self.update_try()

    def estimate_spot(self,plot, p0, p1):
        i = self.current_shape_index
        shape=self.shape_list[i]
        if shape is not None:
            shape.set_from_tool(self.extendedparams.partry, p0, p1)
        self.parametertable.initparams(settry=True)
        self.update_try()

    def scale_spot(self,plot,key):
        i = self.current_shape_index  #number of the curve to ajust
        prefix='cv%d_'%i

        zaxis = plot.colormap_axis
        #_min,_max=
        _min,_max= plot.get_axis_limits(zaxis)

        shift=0.
        scale=1.

        if key == 42:
            scale=1.1
        elif key == 47:
            scale=1./1.1

        if key == 43:
            shift=(_max-_min)/10.
        elif key == 45:
            shift=-(_max-_min)/10.

        estimated_values={prefix+'amp':(shift,scale),prefix+'bg':(shift,scale)}
        self.extendedparams.scale_from_estimate(estimated_values)
        self.parametertable.initparams(settry=True)
        self.update_try()

    def configure_model(self,i):
        #from the drawing on the data, adjust the parameters of the model
        self.current_shape_index = i
        if self.extendedparams.models[i].EVALUATE=="peak":
            self.spottool.activate()
        if self.extendedparams.models[i].EVALUATE=="points":
            self.pointtool.activate()

    def get_import_filename(self):
        formats = iohandler.get_filters('load')
        fname = QFileDialog.getOpenFileName(None, 'Import text data',self.cwd, filter=formats)[0]
        if len(fname) == 0:
            return
        self.import_filename(fname)


    def import_filename(self,fname):
        #import data from an image
        data = imread(fname, to_grayscale = True)
        self.add_data(data, x_range=[0,data.shape[1]], y_range=[0,data.shape[0]],title=path.basename(fname))

    def add_data(self, data,x_range=[0.,1.],y_range=[0.,1.],weights=None,title="",tags={},x_label='x',y_label='y'):
        #creates a np.ma.array from data
        try:
            if len(data.shape) !=2:
                QMessageBox.warning(self,'error','data is not a 2D array')
                return False
            if weights is not None:
                if weights.shape != data.shape:
                    QMessageBox.warning(self,'error','weights and data have not the same shape')
                    return False
        except:
            QMessageBox.warning(self,'error','data %s cannot be loaded'%title)
            return False

        rawdata = Raw2DData(data,x_range,y_range,weights,title,tags,x_label,y_label)
        self.rawdata_list.add_item(rawdata)
        self.set_current_rawdata(rawdata)
        return True

    def set_current_rawdata(self,rawdata):
        #set image to fit where rawdata contains data: a 2D array, xdata and ydata: the limits, weights: 1/sigma
        #update image representation

        self.extendedparams.set_rawdata(rawdata)

        if self.actionFollowCurve.isChecked():
            #we shift all curves in order to keep the same position with respect to the scan center (middle of x limits)
            x = self.data_image.get_xdata()
            y = self.data_image.get_ydata()
            xc = (x[0]+x[1])/2.
            yc = (y[0]+y[1])/2.

        self.data_image.set_xdata(*rawdata.x_range)
        self.data_image.set_ydata(*rawdata.y_range)
        self.data_image.set_data(rawdata.data)   #ImageItem.set_data does not make a copy

        xmin,xmax=rawdata.x_range
        dx=(xmax-xmin)/float(rawdata.data.shape[1])
        ymin,ymax=rawdata.y_range
        dy=(ymax-ymin)/float(rawdata.data.shape[0])

        self.maskingpanel.set_pixel_size(dx, dy)
        self.data_plot.set_title(rawdata.title)

        self.model_image.set_xdata(*rawdata.x_range)
        self.model_image.set_ydata(*rawdata.y_range)

        self.difference_image.set_xdata(*rawdata.x_range)
        self.difference_image.set_ydata(*rawdata.y_range)

        #update axis labels
        self.data_plot.set_axis_title('left',rawdata.y_label)
        self.data_plot.set_axis_title('bottom',rawdata.x_label)
        self.model_plot.set_axis_title('left',rawdata.y_label)
        self.model_plot.set_axis_title('bottom',rawdata.x_label)
        self.difference_plot.set_axis_title('left',rawdata.y_label)
        self.difference_plot.set_axis_title('bottom',rawdata.x_label)

        #update values to be fitted
        sx=rawdata.data.shape[1]
        sy=rawdata.data.shape[0]
        self.fx=(rawdata.x_range[1]-rawdata.x_range[0])/sx
        self.fy=(rawdata.y_range[1]-rawdata.y_range[0])/sy

        self.set_fit_indices()
        #self.model_image.data[self.fit_indices]=0.  #start from 0

        self.set_data_profiles()

        self.data_plot.update_colormap_axis(self.data_image)


        if self.actionFollowCurve.isChecked():
            #we shift all curves in order to keep the same position with respect to the scan center (middle of x limits)
            delta_xc = (rawdata.x_range[0]+rawdata.x_range[1])/2.-xc
            delta_yc = (rawdata.y_range[0]+rawdata.y_range[1])/2.-yc
            self.extendedparams.shift_all_curves(delta_xc, delta_yc)

        self.parametertable.initparams(settry=True)    #if tags have changed, put them into the table


        self.update_try()


    def set_fit_indices(self):
        #use flatten arrays for the fit: fitted_data,fitted_weights,fitted_x
        #do not take into account NaN and masked values

        arr = self.extendedparams.rawdata.data
        if np.ma.is_masked(arr):
            self.fit_indices = np.nonzero(np.isfinite(arr)*(1-arr.mask))  #indices to fit
        else:
            self.fit_indices = np.nonzero(np.isfinite(arr))  #indices to fit

        #we put first x and y in the list
        self.fitted_x=[self.fit_indices[1]*self.fx+self.extendedparams.rawdata.x_range[0]+self.fx/2.,self.fit_indices[0]*self.fy+self.extendedparams.rawdata.y_range[0]+self.fy/2.]

        dat = np.ones(arr.shape)*np.NaN
        dat[self.fit_indices] = 0.
        self.model_image.set_data(dat)       #passed as reference!
        self.difference_image.set_data(np.array(dat))


        self.fitted_data = arr[self.fit_indices]     #value at indices, 1D array!

        if self.extendedparams.rawdata.weights is not None:
            self.fitted_weights = self.extendedparams.rawdata.weights[self.fit_indices]       #weights at indices, 1D array!
        else:
            self.fitted_weights = None

    def set_data_profiles(self):
        arr = self.extendedparams.rawdata.data
        hrange = np.arange(self.extendedparams.rawdata.x_range[0]+self.fx/2,self.extendedparams.rawdata.x_range[1],self.fx)
        vrange = np.arange(self.extendedparams.rawdata.y_range[0]+self.fy/2,self.extendedparams.rawdata.y_range[1],self.fy)

        array1 = np.nan_to_num(arr)
        array2 = np.isfinite(arr)
        if np.ma.is_masked(arr):
            array1 = array1*(1.-arr.mask)
            array2 = array2*(1.-arr.mask)

        sum1=(np.sum(array1,axis=0)/np.sum(array2,axis=0))
        sum2=(np.sum(array1,axis=1)/np.sum(array2,axis=1))

        hrange=hrange[np.isfinite(sum1)]
        vrange=vrange[np.isfinite(sum2)]

        self.data_hprofile.set_data(hrange,sum1[np.isfinite(sum1)])
        self.data_vprofile.set_data(vrange,sum2[np.isfinite(sum2)])

        if self.scaleparam.autoscale:
            self.data_plot.do_autoscale(replot=True)
        else:
            self.data_plot.replot()


    def show_weigths(self):
        if self.showweightstool.action.isChecked() and self.extendedparams.rawdata.weights is not None:
            self.data_image.set_data(self.extendedparams.rawdata.weights)
        else:
            self.data_image.set_data(self.extendedparams.rawdata.data)
        self.data_plot.replot()

    def show_diff(self):
        self.difference_hprofile.setVisible(self.actionShowDiff.isChecked())
        self.difference_vprofile.setVisible(self.actionShowDiff.isChecked())
        self.hprofile.replot()
        self.vprofile.replot()

    def set_shape_intensity(self, key):
        for item in self.data_plot.get_selected_items():
            if item in self.shape_list:
                i=self.shape_list.index(item)

                prefix='cv%d_'%i

                zaxis = self.data_plot.colormap_axis
                #_min,_max=
                _min,_max= self.data_plot.get_axis_limits(zaxis)

                shift=0.
                scale=1.

                if key == 42:
                    scale=1.1
                elif key == 47:
                    scale=1./1.1

                if key == 43:
                    shift=(_max-_min)/10.
                elif key == 45:
                    shift=-(_max-_min)/10.

                estimated_values={prefix+'amp':(shift,scale)}
                self.extendedparams.scale_from_estimate(estimated_values)
        self.parametertable.initparams(settry=True)
        self.update_try()

    def update_try(self):
        #evaluate model with try parameters and update model and difference image
        try:
            values = self.extendedparams.eval_try(self.fitted_x)
        except Exception as e:
            QMessageBox.warning(self,'warning',e)
            return

        self.model_image.data[self.fit_indices] = values

        diff = values-self.fitted_data
        self.difference_image.data[self.fit_indices] = diff
        if self.fitted_weights is not None:
            diff = diff*self.fitted_weights
        chi2 = np.sum((diff)**2)

        di=np.sum(diff)

        self.fitwidget.set_chi2(chi2,di)
        for shape in self.shape_list:
            if shape is not None:
                shape.set_from_params(self.extendedparams.partry)

        self.update_images_and_profiles()



    def update_opt(self):
        #evaluate model with try parameters and update model and difference image
        values=self.extendedparams.eval_opt(self.fitted_x)
        self.model_image.data[self.fit_indices] = values

        diff = values-self.fitted_data
        self.difference_image.data[self.fit_indices] = diff
        if self.fitted_weights is not None:
            diff = diff*self.fitted_weights
        chi2 = np.sum((diff)**2)

        di=np.sum(diff)

        self.fitwidget.set_chi2(chi2,di)

        for i,shape in enumerate(self.shape_list):
            if shape is not None:
                shape.set_from_params(self.extendedparams.paropt)

        self.update_images_and_profiles()

    def update_images_and_profiles(self):

        hrange = self.data_hprofile.get_data()[0]
        vrange = self.data_vprofile.get_data()[0]

        array1 = np.nan_to_num(self.model_image.data)  #Replace NaN with zero and infinity with large finite numbers
        array2 = np.isfinite(self.model_image.data)
        try:
            sum1=(np.sum(array1,axis=0)/np.sum(array2,axis=0))
        except RuntimeWarning:
            pass
        try:
          sum2=(np.sum(array1,axis=1)/np.sum(array2,axis=1))
        except RuntimeWarning:
            pass

        self.model_hprofile.set_data(hrange[np.isfinite(sum1)],sum1[np.isfinite(sum1)])
        self.model_vprofile.set_data(vrange[np.isfinite(sum2)],sum2[np.isfinite(sum2)])

        array1 = np.nan_to_num(self.difference_image.data)
        array2 = np.isfinite(self.difference_image.data)

        try:
            sum1=(np.sum(array1,axis=0)/np.sum(array2,axis=0))
        except RuntimeWarning:
            pass
        try:
            sum2=(np.sum(array1,axis=1)/np.sum(array2,axis=1))
        except RuntimeWarning:
            pass

        self.difference_hprofile.set_data(hrange[np.isfinite(sum1)],sum1[np.isfinite(sum1)])
        self.difference_vprofile.set_data(vrange[np.isfinite(sum2)],sum2[np.isfinite(sum2)])


        if self.scaleparam.autoscale:
            self.data_plot.do_autoscale(replot=False)
            self.model_plot.do_autoscale(replot=False)
            self.difference_plot.do_autoscale(replot=False)

            if self.scaleparam.outliers == 0: #full scale

                _min_data, _max_data = _nanmin(self.data_image.data), _nanmax(self.data_image.data)
                _min_model, _max_model = _nanmin(self.model_image.data), _nanmax(self.model_image.data)
                _min_diff, _max_diff = _nanmin(self.difference_image.data), _nanmax(self.difference_image.data)
            else: #eliminate outliers
                _min_data, _max_data = lut_range_threshold(self.data_image, 256, self.scaleparam.outliers)
                _min_model, _max_model = lut_range_threshold(self.model_image, 256, self.scaleparam.outliers)
                _min_diff, _max_diff = lut_range_threshold(self.difference_image, 256, self.scaleparam.outliers)

            if self.scaleparam.same2scale or self.scaleparam.same3scale:
                _min=min(_min_data,_min_model)
                _max=max(_max_data,_max_model)
                self.data_image.set_lut_range([_min, _max])
                self.model_image.set_lut_range([_min, _max])
                if self.scaleparam.same3scale:
                    _minmax = max(abs(_min),abs(_max))
                    self.difference_image.set_lut_range([-_minmax, _minmax])
                else:
                    _minmax = max(abs(_min_diff),abs(_max_diff))
                    self.difference_image.set_lut_range([-_minmax, _minmax])

            else:
                self.data_image.set_lut_range([_min_data, _max_data])
                self.model_image.set_lut_range([_min_model, _max_model])
                _minmax = max(abs(_min_diff),abs(_max_diff))
                self.difference_image.set_lut_range([-_minmax, _minmax])

            self.hprofile.do_autoscale(replot=False)
            self.vprofile.do_autoscale(replot=False)


        self.data_plot.replot()
        self.model_plot.replot()
        self.difference_plot.replot()
        self.hprofile.replot()
        self.vprofile.replot()

        self.data_plot.update_colormap_axis(self.data_image)
        self.model_plot.update_colormap_axis(self.model_image)
        self.difference_plot.update_colormap_axis(self.difference_image)

    def do_lut_from_data(self,plot):
         if self.scaleparam.same2scale or self.scaleparam.same3scale:
            self.model_image.set_lut_range([self.data_image.min,self.data_image.max])
            self.model_plot.replot()
            self.model_plot.update_colormap_axis(self.model_image)
         if self.scaleparam.same3scale:
            _minmax = max(abs(self.data_image.min),abs(self.data_image.max))
            self.difference_image.set_lut_range([-_minmax, _minmax])
            self.difference_plot.replot()
            self.difference_plot.update_colormap_axis(self.difference_image)

    def do_lut_from_model(self,plot):
         if self.scaleparam.same2scale or self.scaleparam.same3scale:
            self.data_image.set_lut_range([self.model_image.min,self.model_image.max])
            self.data_plot.replot()
            self.data_plot.update_colormap_axis(self.data_image)
         if self.scaleparam.same3scale:
            _minmax = max(abs(self.model_image.min),abs(self.model_image.max))
            self.difference_image.set_lut_range([-_minmax, _minmax])
            self.difference_plot.replot()
            self.difference_plot.update_colormap_axis(self.difference_image)

    def show_mask(self,state):
        self.data_image.set_mask_visible(state)

    def apply_mask(self):
        self.extendedparams.rawdata.data.mask = np.array(self.data_image.get_mask(),dtype=bool)
        self.set_fit_indices()   #update list of indices of the data that will be fitted
        self.set_data_profiles()  #update profiles to take away masked values

    def clear_mask(self):
        self.extendedparams.rawdata.data.mask = False
        self.data_image.unmask_all()
        self.data_plot.replot()

    def set_mask_size(self):
        self.masksizeparam.edit()
        self.masktool.rect_tool.set_pixel_size(self.masksizeparam.hsize,self.masksizeparam.vsize)



def test(win):
    #make a test model
    xmin=-1.
    xmax=1.  #bornes
    ymin=-1.
    ymax=1.

    data=np.random.rand(100,100)
    x=np.arange(xmin,xmax,0.02)
    y=np.arange(ymin,ymax,0.02)
    xv,yv=np.meshgrid(x,y)
    data=data+10./(1.+xv*xv+yv*yv)
    w=np.ones((100,100))+0.01*xv

    tags={'Qz':0.15}
    win.add_data(data,(xmin,xmax),(ymin,ymax),weights=w,title='test1',tags=tags)

    xmin=-1.4
    xmax=1.0  #bornes
    ymin=-2.5
    ymax=0.5
    data=np.random.rand(150,120)
    x=np.arange(xmin,xmax,0.02)
    y=np.arange(ymin,ymax,0.02)
    xv,yv=np.meshgrid(x,y)
    data=data+5./(1.+xv*xv+0.5*yv*yv)
    tags={'Qz':0.3}
    win.add_data(data,(xmin,xmax),(ymin,ymax),title='test2',tags=tags)
    win.set_save_filename('test.txt')
    win.do_fit()
    win.save_fit()

if __name__ == "__main__":

    from guidata import qapplication
    _app = qapplication()
    #win = FitDialog()
    win = Fit2DWindow()
    test(win)

    """

    x=np.nonzero(data)
    fx=(xmax-xmin)/s
    fy=(ymax-ymin)/s

    xx=(np.array(x[0])+0.5)*fx+xmin
    xy=(np.array(x[1])+0.5)*fy+ymin

    models=(Linear2DModel,Lorenzian2DModel)

    win.extendedparams.add_model_class(Linear2DModel)
    win.extendedparams.add_model_class(Lorenzian2DModel)
    win.parametertable.initparams(settry=True)


    data[x]=win.extendedparams.composite_model.eval(win.extendedparams.partry,x=[xx,xy])
    image1=make.image(data,title='test',xdata=[xmin,xmax],ydata=[ymin,ymax])
    """
    win.show()
    #test=ParameterTable(extendedparams=ExtendedParams())
    #test.show()
    _app.exec_()
