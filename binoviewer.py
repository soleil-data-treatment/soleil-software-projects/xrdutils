"""
Created on Wed Dec 07 11:10:28 2016

@author: Prevot
"""
#program for visualisation of the binoculars file
#open hdf5 file with two 3D arrays (counts, contributions) and ranges and resolution
#the values correspond to the pixel centers, for example, there are 11 pixels in the [0,1] range with 0.1 resolution




import os.path as osp
import os

import guidata
import guiqwt.cross_section
# debug mode shows the ROI in the top-left corner of the image plot:
guiqwt.cross_section.DEBUG = True

from os import path

from qtpy.QtCore import Signal, QSize, Qt, QPoint, QPointF, QLineF, QRectF
from qtpy.QtWidgets import (qApp, QApplication, QCheckBox, QVBoxLayout, QHBoxLayout, QGridLayout, QLabel,
                            QLineEdit, QWidget, QSplitter, QSizePolicy, QSpinBox, QDoubleSpinBox, QComboBox,
                            QMenu, QMenuBar, QProgressBar, QTabWidget, QAction,
                              QPushButton, QButtonGroup, QDialog, QMessageBox, QFileDialog)
from qtpy.QtGui import QKeyEvent, QColor, QPainter
from qtpy import PYQT5

from guidata.configtools import add_image_path, get_icon
from guidata.dataset.datatypes import DataSet
from guidata.dataset.dataitems import IntItem, FloatItem, BoolItem

from guiqwt.baseplot import canvas_to_axes
from guiqwt.config import _, make_title
from guiqwt.curve import CurvePlot, CurveItem, PlotItemList
from guiqwt.events import setup_standard_tool_filter, RectangularSelectionHandler, KeyEventMatch
from guiqwt.image import get_plot_qrect
from guiqwt.interfaces import ICurveItemType, IImageItemType
from guiqwt.panels import PanelWidget
from guiqwt.plot import ImageDialog, PlotManager, CurveDialog, configure_plot_splitter
from guiqwt.shapes import Marker, XRangeSelection, PolygonShape
from guiqwt.styles import CurveParam
from guiqwt.tools import (OpenFileTool,CommandTool,DefaultToolbarID, RectangularActionTool,
                          BasePlotMenuTool, AntiAliasingTool, SelectTool)
from guiqwt.transitional import QwtSymbol

import tables
import sys
import numpy as np

from scipy.interpolate import griddata
from scipy.ndimage import laplace, uniform_filter, binary_erosion
from scipy import spatial
import scipy.optimize as opt

from xrdutils.latticetools import (guess_lattice, ReciprocalPlot, LatticeGridTool, ReconstructionGridTool,
                                   RectanglePeakTool, ReciprocalMarker, ReciprocalDialog)

from grafit import Fit2DWindow
from grafit import Fit1DWindow
from grafit.plotitems import TrueEllipseShape
from grafit.plotitems import PlotItemBuilder as GuiPlotItemBuilder
from grafit.masking import ImageMaskingWidget

LINESTYLES = ["SolidLine", "DashLine", "DotLine", "DashDotLine", "DashDotDotLine"]
MARKERS = ["Cross", "Ellipse", "Star1", "XCross", "Rect", "Diamond", "UTriangle",
              "DTriangle", "RTriangle", "LTriangle", "Star2"]
COLORS = ["red","green","blue","cyan","magenta","yellow","black","gray"]

def style_generator():
    """Cycling through curve styles"""
    while True:
        for linestyle in LINESTYLES:
            for marker in MARKERS:
                for color in COLORS:
                    yield (linestyle, marker, color)

CURVE_COUNT = 0
ID_IMAGEMASKING = "image masking"

def planar2D(x, slope_x=0., slope_y=0., bg=0.):
    """planar background used for fitting the structure factors in a ROI"""
    return (bg + x[0] * slope_x + x[1] * slope_y)

def gaussgauss2D(x, slope_x=0., slope_y=0., bg=0., amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    """2D gaussian curve used for fitting the structure factors"""
    cth = np.cos(th)
    sth = np.sin(th)
    x0 = x[0] - loc0
    x1 = x[1] - loc1
    x0, x1 = x0 * cth + x1 * sth, -x0 * sth + x1 * cth  #rotation
    return (bg + x[0] * slope_x + x[1] * slope_y + amp * np.exp(-((x1 / width1)**2 + (x0 / width0)**2)))

def gaussgaussint(amp=1., loc0=0., loc1=0., width0=1., width1=1., th=0.):
    """return the integral of a 2D gaussian curve"""
    return  amp * np.pi * width0 * width1


def get_indices_from_coordinates(u1, u2, v1, v2, ku, kv, values, shape):
    """from the shape and coordinates of a 2D or 3D array, get the indices of a rectangle slice given by the coordinates u1,u2,v1,v2
    iu and iv are the directions of u and v values is an array with [?,min,max,step] values for each direction"""
    print (ku,kv)
    iu1 = int(np.round((u1 - values[ku, 1]) / values[ku, 3]))
    iu2 = int(np.round((u2 - values[ku, 1]) / values[ku, 3])) + 1  #add 1 to take into account np slicing that excludes the upper limit
    iv1 = int(np.round((v1 - values[kv, 1]) / values[kv, 3]))
    iv2 = int(np.round((v2 - values[kv, 1]) / values[kv, 3])) + 1
    iu1 = max(iu1, 0)
    iu1 = min(iu1, shape[ku])
    iu2 = max(iu2, 0)
    iu2 = min(iu2, shape[ku])
    iv1 = max(iv1, 0)
    iv1 = min(iv1, shape[kv])
    iv2 = max(iv2, 0)
    iv2 = min(iv2, shape[kv])
    return iu1, iu2, iv1, iv2

def get_closest_indices(ix1, ix2, iy1, iy2, iz1, iz2, shape):
    """for a 3D array, return the indices corresponding to slices inside the array  """
    if ix1 > ix2:
        ix2, ix1 = ix1, ix2
    if iy1 > iy2:
        iy2, iy1 = iy1, iy2
    if iz1 > iz2:
        iz2, iz1 = iz1, iz2

    if ix1 < 0:
        ix1=0
    if ix2 < 0:
        ix2 = 0
    if iy1 < 0:
        iy1 = 0
    if iy2 < 0:
        iy2 = 0
    if iz1 < 0:
        iz1 = 0
    if iz2 < 0:
        iz2 = 0

    if ix1 > shape[0]:
        ix1 = shape[0]
    if ix2 > shape[0]:
        ix2 = shape[0]
    if iy1 > shape[1]:
        iy1 = shape[1]
    if iy2 > shape[1]:
        iy2 = shape[1]
    if iz1 > shape[2]:
        iz1 = shape[2]
    if iz2 > shape[2]:
        iz2 = shape[2]
    return ix1, ix2, iy1, iy2, iz1, iz2

class EllipseRoi(PolygonShape):
    """two imbricated ellipse shapes used for fitting the structure factors
    a planar background is fitted on the outer disk (between the two ellipse)
    the background-corrected intensities are summed on the inner ellipse"""
    CLOSED = True
    def __init__(self, x1=0, y1=0, x2=0, y2=0, eta=0.9, shapeparam=None):
        super().__init__(shapeparam=shapeparam)
        self.eta = eta
        self.check_collision = True
        loc0 = (x2 - x1) / 2.
        loc1 = (y2 - y1) / 2.
        width0, width1 = abs(x2 - x1) / 2., abs(y2 -y1 ) / 2.
        self.set_from_center_and_width(loc0, loc1, width0, width1)

    def set_from_drawing_points(self, plot, p0, p1):
        #the shape has been drawn with a tool from p0 to p1, we set its value
        loc0, loc1 = canvas_to_axes(self, p0)
        x1, y1 = canvas_to_axes(self, p1)

        width0, width1 = 2.*abs(x1 - loc0), 2 * abs(y1 - loc1)
        self.set_from_center_and_width(loc0, loc1, width0, width1)

    def set_local_center(self, p0):
        x1, y1 = canvas_to_axes(self, p0)
        self.set_center(x1, y1)

    def set_center(self, x1, y1):
        x0 = (self.points[4][0] + self.points[5][0]) / 2.
        y0 = (self.points[4][1] + self.points[5][1]) / 2.

        dx = x1 - x0
        dy = y1 - y0
        self.points += np.array([[dx, dy]])

    def set_from_center_and_width(self,loc0,loc1,width0,width1):
        self.set_points([[loc0-width0,loc1],[loc0+width0,loc1],[loc0,loc1-width1],[loc0,loc1+width1],
                         [loc0-width0*self.eta,loc1],[loc0+width0*self.eta,loc1],[loc0,loc1-width1*self.eta],[loc0,loc1+width1*self.eta]])

    def set_outer_points(self,points):
        self.points[0:4] = points

    def set_inner_points(self,points):
        self.points[4:8] = points

    def get_outer_xline(self):
        return QLineF(*(tuple(self.points[0])+tuple(self.points[1])))

    def get_outer_yline(self):
        return QLineF(*(tuple(self.points[2])+tuple(self.points[3])))

    def get_inner_xline(self):
        return QLineF(*(tuple(self.points[4])+tuple(self.points[5])))

    def get_inner_yline(self):
        return QLineF(*(tuple(self.points[6])+tuple(self.points[7])))

    def set_outer_xdiameter(self, x0, y0, x1, y1):
        """Set the coordinates of the ellipse's X-axis diameter"""
        xline = QLineF(x0, y0, x1, y1)
        yline = xline.normalVector()
        yline.translate(xline.pointAt(.5)-xline.p1())
        yline.setLength(self.get_outer_yline().length())
        yline.translate(yline.pointAt(.5)-yline.p2())
        self.set_outer_points([(x0, y0), (x1, y1),
                         (yline.x1(), yline.y1()), (yline.x2(), yline.y2())])

    def get_outer_xdiameter(self):
        """Return the coordinates of the ellipse's X-axis diameter"""
        return tuple(self.points[0])+tuple(self.points[1])

    def set_outer_ydiameter(self, x2, y2, x3, y3):
        """Set the coordinates of the ellipse's Y-axis diameter"""
        yline = QLineF(x2, y2, x3, y3)
        xline = yline.normalVector()
        xline.translate(yline.pointAt(.5)-yline.p1())
        xline.setLength(self.get_outer_xline().length())
        xline.translate(xline.pointAt(.5)-xline.p2())
        self.set_outer_points([(xline.x1(), xline.y1()), (xline.x2(), xline.y2()),
                         (x2, y2), (x3, y3)])

    def get_outer_ydiameter(self):
        """Return the coordinates of the ellipse's Y-axis diameter"""
        return tuple(self.points[2])+tuple(self.points[3])

    def set_inner_xdiameter(self, x0, y0, x1, y1):
        """Set the coordinates of the ellipse's X-axis diameter"""
        xline = QLineF(x0, y0, x1, y1)
        yline = xline.normalVector()
        yline.translate(xline.pointAt(.5)-xline.p1())
        yline.setLength(self.get_inner_yline().length())
        yline.translate(yline.pointAt(.5)-yline.p2())
        self.set_inner_points([(x0, y0), (x1, y1),
                         (yline.x1(), yline.y1()), (yline.x2(), yline.y2())])

    def get_inner_xdiameter(self):
        """Return the coordinates of the ellipse's X-axis diameter"""
        return tuple(self.points[4])+tuple(self.points[5])

    def set_inner_ydiameter(self, x2, y2, x3, y3):
        """Set the coordinates of the ellipse's Y-axis diameter"""
        yline = QLineF(x2, y2, x3, y3)
        xline = yline.normalVector()
        xline.translate(yline.pointAt(.5)-yline.p1())
        xline.setLength(self.get_inner_xline().length())
        xline.translate(xline.pointAt(.5)-xline.p2())
        self.set_inner_points([(xline.x1(), xline.y1()), (xline.x2(), xline.y2()),
                         (x2, y2), (x3, y3)])

    def get_inner_ydiameter(self):
        """Return the coordinates of the ellipse's Y-axis diameter"""
        return tuple(self.points[6])+tuple(self.points[7])

    def move_point_to(self, handle, pos, ctrl=None):
        nx, ny = pos
        oldpoints = np.array(self.points)
        if handle == 0:
            x1, y1 = self.points[1]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x0, y0 = self.points[0]
                x1, y1 = x1+x0-nx, y1+y0-ny
            self.set_outer_xdiameter(nx, ny, x1, y1)
        elif handle == 1:
            x0, y0 = self.points[0]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x1, y1 = self.points[1]
                x0, y0 = x0+x1-nx, y0+y1-ny
            self.set_outer_xdiameter(x0, y0, nx, ny)
        elif handle == 2:
            x3, y3 = self.points[3]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x2, y2 = self.points[2]
                x3, y3 = x3+x2-nx, y3+y2-ny
            self.set_outer_ydiameter(nx, ny, x3, y3)
        elif handle == 3:
            x2, y2 = self.points[2]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x3, y3 = self.points[3]
                x2, y2 = x2+x3-nx, y2+y3-ny
            self.set_outer_ydiameter(x2, y2, nx, ny)

        elif handle == 4:
            x1, y1 = self.points[5]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x0, y0 = self.points[4]
                x1, y1 = x1+x0-nx, y1+y0-ny
            self.set_inner_xdiameter(nx, ny, x1, y1)
        elif handle == 5:
            x0, y0 = self.points[4]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x1, y1 = self.points[5]
                x0, y0 = x0+x1-nx, y0+y1-ny
            self.set_inner_xdiameter(x0, y0, nx, ny)
        elif handle == 6:
            x3, y3 = self.points[7]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x2, y2 = self.points[6]
                x3, y3 = x3+x2-nx, y3+y2-ny
            self.set_inner_ydiameter(nx, ny, x3, y3)
        elif handle == 7:
            x2, y2 = self.points[6]
            if ctrl:
                # When <Ctrl> is pressed, the center position is unchanged
                x3, y3 = self.points[7]
                x2, y2 = x2+x3-nx, y2+y3-ny
            self.set_inner_ydiameter(x2, y2, nx, ny)
        elif handle == -1:
            delta = (nx, ny)-self.points.mean(axis=0)
            self.points += delta
        if self.collision():
            self.points = oldpoints

    def get_sizes_and_angle(self, inner = False):
        """Return ellipse axes and angle"""
        if inner:
            x0, y0 = self.points[4]
            x1, y1 = self.points[5]
            a = np.sqrt((x1-x0)**2+(y1-y0)**2)
            th = np.arctan2(y1-y0,x1-x0)
            x0, y0 = self.points[6]
            x1, y1 = self.points[7]
            b = np.sqrt((x1-x0)**2+(y1-y0)**2)
        else:
            x0, y0 = self.points[0]
            x1, y1 = self.points[1]
            a = np.sqrt((x1-x0)**2+(y1-y0)**2)
            th = np.arctan2(y1-y0,x1-x0)
            x0, y0 = self.points[2]
            x1, y1 = self.points[3]
            b = np.sqrt((x1-x0)**2+(y1-y0)**2)

        return a,b,th

    def get_bounding_rectangle(self, inner=False):
        if inner:
            x0 = (self.points[4,0]+self.points[5,0])/2.
            y0 = (self.points[4,1]+self.points[5,1])/2.
        else:
            x0 = (self.points[0,0]+self.points[1,0])/2.
            y0 = (self.points[0,1]+self.points[1,1])/2.

        a, b, th = self.get_sizes_and_angle(inner)
        c = np.cos(th)
        s = np.sin(th)
        u = np.sqrt((a*c)**2+(b*s)**2)/2.
        v = np.sqrt((a*s)**2+(b*c)**2)/2.
        x_min = x0 - u
        x_max = x0 + u
        y_min = y0 - v
        y_max = y0 + v

        return (x_min,y_min,x_max,y_max)

    def collision(self):
        """test if there is a possible collision between the inner and outer ellipse
        we restrict the points of the inner ellipse to be inside the outer elllipse
        it is not exact a true mathematical test needs a lot of computation"""

        if not self.check_collision:
            return False

        a,b,th = self.get_sizes_and_angle(inner=False)

        if a*b == 0:
            return False

        x0 = (self.points[0,0]+self.points[1,0])/2.
        y0 = (self.points[0,1]+self.points[1,1])/2.
        c = np.cos(th)
        s = np.sin(th)
        for i in range(4,8):
            x = self.points[i,0]
            y = self.points[i,1]
            u = x-x0
            v = y-y0
            xx = u*c + v*s
            yy = -u*s + v*c
            if (xx/a)**2+(yy/b)**2 > 0.25:
                return True
        return False

    def hit_test(self, pos):
        """return (dist, handle, inside)"""
        if not self.plot():
            return sys.maxsize, 0, False, None
        dist, handle, inside, other = self.poly_hit_test(self.plot(),
                                             self.xAxis(), self.yAxis(), pos)
        if not inside:
            xMap = self.plot().canvasMap(self.xAxis())
            yMap = self.plot().canvasMap(self.yAxis())
            _points, _line0, _line1, rect, angle = self.compute_elements(xMap, yMap)
            inside = rect.contains(QPointF(pos))
        return dist, handle, inside, other

    def compute_elements(self, xMap, yMap, inner=False):
        """Return points, lines and ellipse rect"""
        #if aspect ratio is not 1, need to recompute axis

        u=(xMap.invTransform(1)-xMap.invTransform(0))   #u is the (signed) pixel size
        v=(yMap.invTransform(1)-yMap.invTransform(0))   #v is the (signed) pixel size
        a,b,th=self.get_sizes_and_angle(inner)

        if a*b != 0:
            c=np.cos(th)
            s=np.sin(th)
            cs=2.*c*s

            fx2=(u*c/a)**2+(u*s/b)**2
            fy2=(v*s/a)**2+(v*c/b)**2
            fxy=u*v*cs*(1./a**2-1./b**2)

            if abs(s)<1e-15:
                a=a/u
                b=b/v
            elif abs(c)<1e-15:
                a=a/v
                b=b/u
            else:
                th=np.arctan2(fxy,fx2-fy2)/2.  #angle of the ellipse major axis and x direction
                cs=2.*np.cos(th)*np.sin(th)
                a=np.sqrt(2./(fx2+fy2+fxy/cs))
                b=np.sqrt(2./(fx2+fy2-fxy/cs))

        points = self.transform_points(xMap, yMap)
        if inner:
            line0 = QLineF(points[4], points[5])
            line1 = QLineF(points[6], points[7])
        else:
            line0 = QLineF(points[0], points[1])
            line1 = QLineF(points[2], points[3])


        rect = QRectF()
        rect.setWidth(a)
        rect.setHeight(b)
        rect.moveCenter(line0.pointAt(.5))

        #ellipse major and minor axes
        th=np.rad2deg(th)

        return points, line0, line1, rect, th


    def draw(self, painter, xMap, yMap, canvasRect):
        points, line0, line1, rect, angle = self.compute_elements(xMap, yMap, inner=False)
        pen, brush, symbol = self.get_pen_brush(xMap, yMap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)
        painter.setBrush(brush)
        #painter.drawLine(line0)
        #painter.drawLine(line1)
        painter.save()

        painter.translate(rect.center())
        painter.rotate(angle)
        painter.translate(-rect.center())
        painter.drawEllipse(rect.toRect())

        painter.restore()

        points, line0, line1, rect, angle = self.compute_elements(xMap, yMap, inner=True)

        painter.save()

        painter.translate(rect.center())
        painter.rotate(angle)
        painter.translate(-rect.center())
        painter.drawEllipse(rect.toRect())

        painter.restore()

        if symbol != QwtSymbol.NoSymbol:
            for i in range(points.size()):
                symbol.drawSymbol(painter, points[i].toPoint())



class RectSelectTool(RectangularActionTool):
    """a tool for defining by drawing a rectangle on a plot, a selection range for the data"""
    TITLE = _("Selection rectangle")
    ICON = "rectangle.png"
    SHAPE_STYLE_SECT = "plot"
    SHAPE_STYLE_KEY = "shape/rectzoom"
    AVOID_NULL_SHAPE = False

    #: Signal emitted by RectSelectTool when a rectangular_selection has been performed
    SIG_RECT_SELECTION = Signal(object, float, float, float, float)

    def __init__(self, manager, setup_shape_cb=None,
                 handle_final_shape_cb=None, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 switch_to_default_tool=None):
        super().__init__(manager,
                       self.add_shape_to_plot, shape_style,
                       toolbar_id=toolbar_id, title=title, icon=icon, tip=tip,
                       switch_to_default_tool=switch_to_default_tool)
        self.setup_shape_cb = setup_shape_cb
        self.handle_final_shape_cb = handle_final_shape_cb


    def add_shape_to_plot(self, plot, p0, p1):
        """
        Method called when shape's rectangular area
        has just been drawn on screen.
        Adding the final shape to plot and returning it.
        """
        #this code is for a ProjectionPlot only!!!
        #uc=plot.unitcell
        x0, y0, w, h = get_plot_qrect(plot, p0, p1).getRect()
        x1=x0+w
        y1=y0+h
        if x0>x1:
          x0,x1=x1,x0
        if y0>y1:
          y0,y1=y1,y0
        self.SIG_RECT_SELECTION.emit(plot, x0, x1, y0, y1)



class EllipseRoiSelectionHandlerCXY(RectangularSelectionHandler):
    """a shape handler used for drawing an EllipseRoi on a plot"""
    #: Signal emitted by RectangularSelectionHandler when ending selection
    SIG_END_RECT = Signal("PyQt_PyObject", "QPointF", "QPointF")
    SIG_KEY_PRESSED_EVENT = Signal("PyQt_PyObject", int)
    SIG_STOP_NOTMOVING_EVENT = Signal("PyQt_PyObject", "QPointF")

    def __init__(self, filter, btn, mods=Qt.NoModifier, start_state=0):
        super().__init__(filter, btn, mods,start_state)
        filter.add_event(start_state, KeyEventMatch((42,43,45,47)),
                         self.key_press, start_state)

    def move(self, filter, event):
        """methode surchargee par la classe """
        #x1,y1=canvas_to_axes(self.shape, event.pos())
        #x0,y0=canvas_to_axes(self.shape, self.start)
        #dx=x1-x0
        #dy=y1-y0
        self.shape.set_from_drawing_points(filter.plot,self.start,event.pos())
        #self.shape.points=np.array([[x0-dx,y0],[x0+dx,y0],[x0,y0-dy],[x0,y0+dy]])
        self.move_action(filter, event)
        filter.plot.replot()

    def key_press(self,filter,event):
        self.SIG_KEY_PRESSED_EVENT.emit(filter, event.key())

    def stop_notmoving(self, filter, event):
        self.SIG_STOP_NOTMOVING_EVENT.emit(filter,event.pos())

    def set_shape(self, shape, h0, h1, h2, h3,
                  setup_shape_cb=None, avoid_null_shape=False):
        self.shape = shape
        self.shape_h0 = h0
        self.shape_h1 = h1
        self.shape_h2 = h2
        self.shape_h3 = h3
        self.setup_shape_cb = setup_shape_cb
        self.avoid_null_shape = avoid_null_shape

class RectangularSelectionHandlerCXY(RectangularSelectionHandler):
    """a shape handler used for drawing a centered rectangle on a plot"""
    def move(self, filter, event):
        """methode surchargee par la classe """
        sympos=QPoint(2*self.start.x()-event.pos().x(),2*self.start.y()-event.pos().y())

        self.shape.move_local_point_to(self.shape_h0, sympos)
        self.shape.move_local_point_to(self.shape_h1, event.pos())
        self.move_action(filter, event)
        filter.plot.replot()

class RectangularActionToolCXY(RectangularActionTool):
    """a tool for drawing a centered rectangle"""
    def setup_filter(self, baseplot):      #partie utilisee pendant le mouvement a la souris
        #utilise a l'initialisation de la toolbar
        filter = baseplot.filter
        start_state = filter.new_state()
        handler = RectangularSelectionHandlerCXY(filter, Qt.LeftButton,      #gestionnaire du filtre
                                              start_state=start_state)
        shape, h0, h1 = self.get_shape()
        shape.pen.setColor(QColor("#00bfff"))

        handler.set_shape(shape, h0, h1, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_END_RECT.connect(self.end_rect)
        #self.connect(handler, SIG_CLICK_EVENT, self.start)   #a definir aussi dans RectangularSelectionHandler2
        return setup_standard_tool_filter(filter, start_state)

    def activate(self):
        """Activate tool"""

        for baseplot, start_state in list(self.start_state.items()):
            baseplot.filter.set_state(start_state, None)
        self.action.setChecked(True)
        self.manager.set_active_tool(self)
        #plot = self.get_active_plot()
        #plot.newcurve=True

    def deactivate(self):
        """Deactivate tool"""
        self.action.setChecked(False)


class CircularActionToolCXY(RectangularActionToolCXY):
    """a tool for drawing a centered ellipseRoi"""
    TITLE = _("Circle")
    ICON = "ellipse_roi.png"

    def __init__(self, manager, func1, func2, shape_style=None,
                 toolbar_id=DefaultToolbarID, title=None, icon=None, tip=None,
                 fix_orientation=False, switch_to_default_tool=None):

        self.click_func = func2  #function for keys activated with +/-
        super().__init__(manager, func1, shape_style=shape_style, toolbar_id=toolbar_id,
                                 title=title, icon=icon, tip=tip, fix_orientation=fix_orientation,
                                 switch_to_default_tool=switch_to_default_tool)


    def setup_filter(self, baseplot):      #partie utilisee pendant le mouvement a la souris
        #utilise a l'initialisation de la toolbar
        #print "setup_filter"
        filter = baseplot.filter
        start_state = filter.new_state()
        handler = EllipseRoiSelectionHandlerCXY(filter, Qt.LeftButton,      #gestionnaire du filtre
                                              start_state=start_state)
        shape, h0, h1, h2, h3 = self.get_shape()
        shape.pen.setColor(QColor("#00bfff"))

        handler.set_shape(shape, h0, h1, h2, h3, self.setup_shape,
                          avoid_null_shape=self.AVOID_NULL_SHAPE)
        handler.SIG_END_RECT.connect(self.end_rect)
        handler.SIG_KEY_PRESSED_EVENT.connect(self.key_pressed)
        handler.SIG_STOP_NOTMOVING_EVENT.connect(self.clicked)

        return setup_standard_tool_filter(filter, start_state)

    def key_pressed(self, filter, key):
        plot = filter.plot
        self.key_func(plot,key)

    def clicked(self, filter, p0):
        plot = filter.plot
        self.click_func(plot,p0)

    def get_shape(self):
        """Reimplemented RectangularActionTool method"""
        shape, h0, h1, h2, h3 = self.create_shape()
        self.setup_shape(shape)
        return shape, h0, h1, h2, h3

    def create_shape(self):
        shape = EllipseRoi(0, 0, 1, 1)
        self.set_shape_style(shape)
        return shape, 0, 1, 2, 3  #give the values of h0,h1,h2,h3 use to move the shape

class XRangeSelection2(XRangeSelection):
    """a selection range slightly modified to take into account min<max"""
    def set_range(self, _min, _max, dosignal=True):
        if _min<_max:
            super().set_range(_min, _max, dosignal=dosignal)
        else:
            super().set_range(_max, _min, dosignal=dosignal)


    def move_point_to(self, hnd, pos, ctrl=None):
        val, _ = pos
        if hnd == 0:
            if val<=self._max:
                self._min = val
        elif hnd == 1:
            if val>=self._min:
                self._max = val
        elif hnd == 2:
            move = val-(self._max+self._min)/2
            self._min += move
            self._max += move

        self.plot().SIG_RANGE_CHANGED.emit(self, self._min, self._max)

class BgRangeSelection(XRangeSelection2):
    def __init__(self, _min, _max, shapeparam=None):
        if _min > _max:
          _min,_max=_max,_min
        super().__init__(_min, _max, shapeparam=shapeparam)
        self.shapeparam.fill="#000000"
        self.shapeparam.update_range(self) # creates all the above QObjects



class BinoPlot(CurvePlot):
    MOVE = Signal(int)   #: Signal emitted by plot when an arrow key is pressed

    def keyPressEvent(self, event):
        "when the plot has focus, allows to move the range selection with keys"

        if type(event) == QKeyEvent:
            #here accept the event and do something
            touche=event.key()
            if touche == Qt.Key_Right:
                self.MOVE.emit(1)
            elif touche == Qt.Key_Left:
                self.MOVE.emit(-1)

class BinoCurve(CurveItem):
    def get_closest_x(self, xc, yc):   #need to correct an error in guiqwt!
        # We assume X is sorted, otherwise we'd need :
        # argmin(abs(x-xc))

        i = self._x.searchsorted(xc)
        n = len(self._x)
        if 0<i<n:
            if np.fabs(self._x[i-1]-xc) < np.fabs(self._x[i]-xc):
                return self._x[i-1], self._y[i-1]
        elif i == n:
            i = n-1
        return self._x[i], self._y[i]



class PlotItemBuilder(GuiPlotItemBuilder):
    def __init__(self):
        super().__init__()

    def binocurve(self, x, y, title="",
              color=None, linestyle=None, linewidth=None,
              marker=None, markersize=None, markerfacecolor=None,
              markeredgecolor=None, shade=None, curvestyle=None, baseline=None,
              xaxis="bottom", yaxis="left"):

        """
        Make a curve `plot item` from x, y, data
        (:py:class:`guiqwt.curve.CurveItem` object)

            * x: 1D NumPy array
            * y: 1D NumPy array
            * color: curve color name
            * linestyle: curve line style (MATLAB-like string or "SolidLine",
              "DashLine", "DotLine", "DashDotLine", "DashDotDotLine", "NoPen")
            * linewidth: line width (pixels)
            * marker: marker shape (MATLAB-like string or "Cross",
              "Ellipse", "Star1", "XCross", "Rect", "Diamond", "UTriangle",
              "DTriangle", "RTriangle", "LTriangle", "Star2", "NoSymbol")
            * markersize: marker size (pixels)
            * markerfacecolor: marker face color name
            * markeredgecolor: marker edge color name
            * shade: 0 <= float <= 1 (curve shade)
            * curvestyle: "Lines", "Sticks", "Steps", "Dots", "NoCurve"
            * baseline (float: default=0.0): the baseline is needed for filling
              the curve with a brush or the Sticks drawing style.
            * xaxis, yaxis: X/Y axes bound to curve

        Example::

            curve(x, y, marker='Ellipse', markerfacecolor='#ffffff')

        which is equivalent to (MATLAB-style support)::

            curve(x, y, marker='o', markerfacecolor='w')
        """

        basename = _("Curve")
        param = CurveParam(title=basename, icon='curve.png')
        if not title:
            global CURVE_COUNT
            CURVE_COUNT += 1
            title = make_title(basename, CURVE_COUNT)
        self.__set_param(param, title, color, linestyle, linewidth, marker,
                         markersize, markerfacecolor, markeredgecolor, shade,
                         curvestyle, baseline)
        curve = BinoCurve(param)
        curve.set_data(x, y)
        curve.update_params()
        self.__set_curve_axes(curve, xaxis, yaxis)
        return curve



make=PlotItemBuilder()

class SlicePrefs():
    i0=3   #slice direction
    i1=1   #scan direction
    i2=2   #raw integration direction
    absmins=[0,0,0]  #les valeurs min des points
    absmaxs=[1,1,1]  #les valeurs max des points (borne exclue)
    mins=[0,0,0]  #les valeurs min de la range pour les slices
    maxs=[0,0,0] #les valeurs max de la range pour les slices (borne exclue)
    steps=[1,1,1]  #voxel widths
    labels=['x','y','z']
    widths=[1.,1.,1.]  #widths for tilted rods
    pos1=[0.,0.,0.]  #start position of a tilted slice
    pos2=[0.,0.,1.]  #end position of a tilted slice
    stepw=1     #width of a slice
    bins=1    #number of bins
    visible = True  #only slices for rods in the axes limits are generated
    do_it=False

class Preferences(DataSet):
    new_slice_win = BoolItem("Make slices in a new window", default=True)
    keep_trace = BoolItem("Keep trace of integrated regions", default =False)

class LaplaceParam(DataSet):
    cutoff= IntItem("remove contributions smaller than", default=1, min=1)
    steps= IntItem("integration steps", default=10, min=0)
    increase = FloatItem("speed up coefficient",default=1.1,min=1.)
    decrease = FloatItem("speed down coefficient",default=0.5,nonzero=True,max=1.).set_pos(col=1)
    fillc = FloatItem("fill missing contributions with",default=10,min=1.).set_pos(col=0)
    in_convex = BoolItem("fill only inside the convex hull")
    doslice = BoolItem("apply along slice",default=False)

class FilterParam(DataSet):
    cutoff= IntItem("remove contributions smaller than", default=1, min=1)

class SetStraightScansWindow(QDialog):
    # definit une fenetre pour rentrer les parametres de dialogue de construction de map 3D
    def __init__(self,prefs):
        self.prefs=prefs
        super().__init__()  #permet l'initialisation de la fenetre sans perdre les fonctions associees
        self.setWindowTitle("Parameters for scans along a straight rod")
        self.setMinimumSize(QSize(450, 160))
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.lab0 = QLabel("Direction",self)
        self.lab1 = QLabel("Rod",self)
        self.lab2 = QLabel("Scan",self)
        self.lab3 = QLabel("Raw sum",self)
        self.lab4=  QLabel('Min (incl.)',self)
        self.lab5=  QLabel('Max (incl.)',self)

        self.xlab = QLabel(prefs.labels[0],self)
        self.ylab = QLabel(prefs.labels[1],self)
        self.zlab = QLabel(prefs.labels[2],self)

        self.lab6=  QLabel('Steps',self)
        self.lab7=  QLabel('Width',self)

        #slice direction
        self.xbox1 = QCheckBox(self)
        self.ybox1 = QCheckBox(self)
        self.zbox1 = QCheckBox(self)
        self.group1=QButtonGroup(self)
        self.group1.addButton(self.xbox1,1)
        self.group1.addButton(self.ybox1,2)
        self.group1.addButton(self.zbox1,3)
        self.group1.button(prefs.i0+1).setChecked(True)

        #scan direction
        self.xbox2 = QCheckBox(self)
        self.ybox2 = QCheckBox(self)
        self.zbox2 = QCheckBox(self)
        self.group2=QButtonGroup(self)
        self.group2.addButton(self.xbox2,1)
        self.group2.addButton(self.ybox2,2)
        self.group2.addButton(self.zbox2,3)
        self.group2.button(prefs.i1+1).setChecked(True)

        #raw sum direction
        self.xbox3 = QCheckBox(self)
        self.ybox3 = QCheckBox(self)
        self.zbox3 = QCheckBox(self)
        self.group3=QButtonGroup(self)
        self.group3.addButton(self.xbox3,1)
        self.group3.addButton(self.ybox3,2)
        self.group3.addButton(self.zbox3,3)
        self.group3.button(prefs.i2+1).setChecked(True)

        self.xmin = QDoubleSpinBox(self)
        self.xmin.setRange(prefs.absmins[0],prefs.absmaxs[0])
        self.xmin.setSingleStep(prefs.steps[0])
        self.xmin.setDecimals(max(2,int(1-np.log10(prefs.steps[0]))))
        self.xmin.setValue(prefs.mins[0])
        self.xmax = QDoubleSpinBox(self)
        self.xmax.setRange(prefs.absmins[0],prefs.absmaxs[0])
        self.xmax.setSingleStep(prefs.steps[0])
        self.xmax.setDecimals(max(2,int(1-np.log10(prefs.steps[0]))))
        self.xmax.setValue(prefs.maxs[0])

        self.ymin = QDoubleSpinBox(self)
        self.ymin.setRange(prefs.absmins[1],prefs.absmaxs[1])
        self.ymin.setSingleStep(prefs.steps[1])
        self.ymin.setDecimals(max(2,int(1-np.log10(prefs.steps[1]))))
        self.ymin.setValue(prefs.mins[1])
        self.ymax = QDoubleSpinBox(self)
        self.ymax.setRange(prefs.absmins[1],prefs.absmaxs[1])
        self.ymax.setSingleStep(prefs.steps[1])
        self.ymax.setDecimals(max(2,int(1-np.log10(prefs.steps[1]))))
        self.ymax.setValue(prefs.maxs[1])

        self.zmin = QDoubleSpinBox(self)
        self.zmin.setRange(prefs.absmins[2],prefs.absmaxs[2])
        self.zmin.setSingleStep(prefs.steps[2])
        self.zmin.setDecimals(max(2,int(1-np.log10(prefs.steps[2]))))
        self.zmin.setValue(prefs.mins[2])
        self.zmax = QDoubleSpinBox(self)
        self.zmax.setRange(prefs.absmins[2],prefs.absmaxs[2])
        self.zmax.setSingleStep(prefs.steps[2])
        self.zmax.setDecimals(max(2,int(1-np.log10(prefs.steps[2]))))
        self.zmax.setValue(prefs.maxs[2])

        stepn=int((prefs.maxs[prefs.i0]-prefs.mins[prefs.i0])/prefs.steps[prefs.i0])
        if stepn==0:
            stepn=1
        stepw=(prefs.maxs[prefs.i0]-prefs.mins[prefs.i0])/stepn

        self.stepn = QSpinBox(self)
        self.stepn.setMinimum(1)
        self.stepn.setMaximum(stepn)
        self.stepn.setValue(stepn)

        self.stepw = QLineEdit(self)
        self.stepw.setText('%f'%(stepw))

        self.OK = QPushButton(self)
        self.OK.setText("OK")

        self.Cancel = QPushButton(self)
        self.Cancel.setText("Cancel")

        self.layout.addWidget(self.lab0,0,0)
        self.layout.addWidget(self.lab1,0,1)
        self.layout.addWidget(self.lab2,0,2)
        self.layout.addWidget(self.lab3,0,3)
        self.layout.addWidget(self.lab4,0,4)
        self.layout.addWidget(self.lab5,0,5)

        self.layout.addWidget(self.xlab,1,0)
        self.layout.addWidget(self.xbox1,1,1)
        self.layout.addWidget(self.xbox2,1,2)
        self.layout.addWidget(self.xbox3,1,3)
        self.layout.addWidget(self.xmin,1,4)
        self.layout.addWidget(self.xmax,1,5)

        self.layout.addWidget(self.ylab,2,0)
        self.layout.addWidget(self.ybox1,2,1)
        self.layout.addWidget(self.ybox2,2,2)
        self.layout.addWidget(self.ybox3,2,3)
        self.layout.addWidget(self.ymin,2,4)
        self.layout.addWidget(self.ymax,2,5)

        self.layout.addWidget(self.zlab,3,0)
        self.layout.addWidget(self.zbox1,3,1)
        self.layout.addWidget(self.zbox2,3,2)
        self.layout.addWidget(self.zbox3,3,3)
        self.layout.addWidget(self.zmin,3,4)
        self.layout.addWidget(self.zmax,3,5)

        self.layout.addWidget(self.lab6,4,0)
        self.layout.addWidget(self.stepn,4,1)
        self.layout.addWidget(self.lab7,4,2)
        self.layout.addWidget(self.stepw,4,3)
        self.layout.addWidget(self.OK,4,4)
        self.layout.addWidget(self.Cancel,4,5)

        for i in range(1,4):
            self.layout.setColumnMinimumWidth(i,80)
        for i in range(6):
            self.layout.setColumnStretch(i,1)

        self.Cancel.clicked.connect(self.closewin)
        self.OK.clicked.connect(self.appl)
        for button in [self.xbox1,self.ybox1,self.zbox1,self.xbox2,self.ybox2,self.zbox2,self.xbox3,self.ybox3,self.zbox3]:
            button.clicked.connect(self.validate)
        for button in [self.xmin,self.xmax,self.ymin,self.ymax,self.zmin,self.zmax]:
            button.valueChanged.connect(self.round_values)
        self.stepn.valueChanged.connect(self.compute_step_width)
        self.stepw.textEdited.connect(self.compute_step_number)   #only when user change the text!

        self.Cancel.clicked.connect(self.closewin)
        self.round_values()

        self.exec_()

    def round_values(self,x=0):
        i1=round((self.xmin.value()-self.prefs.absmins[0])/self.prefs.steps[0])
        i2=round((self.xmax.value()-self.prefs.absmins[0])/self.prefs.steps[0])
        self.xmin.setValue(self.prefs.absmins[0]+i1*self.prefs.steps[0])
        self.xmax.setValue(self.prefs.absmins[0]+i2*self.prefs.steps[0])
        if i1>=i2:
            if i2==0:
                i1=0
                i2=1
            else:
                i1=i2-1
        i1=round((self.ymin.value()-self.prefs.absmins[1])/self.prefs.steps[1])
        i2=round((self.ymax.value()-self.prefs.absmins[1])/self.prefs.steps[1])
        if i1>=i2:
            if i2==0:
                i1=0
                i2=1
            else:
                i1=i2-1
        self.ymin.setValue(self.prefs.absmins[1]+i1*self.prefs.steps[1])
        self.ymax.setValue(self.prefs.absmins[1]+i2*self.prefs.steps[1])

        i1=round((self.zmin.value()-self.prefs.absmins[2])/self.prefs.steps[2])
        i2=round((self.zmax.value()-self.prefs.absmins[2])/self.prefs.steps[2])
        if i1>=i2:
            if i2==0:
                i1=0
                i2=1
            else:
                i1=i2-1
        self.zmin.setValue(self.prefs.absmins[2]+i1*self.prefs.steps[2])
        self.zmax.setValue(self.prefs.absmins[2]+i2*self.prefs.steps[2])
        self.compute_step_width()

    def compute_step_number(self,text=''):
        i0=self.group1.checkedId()-1
        try:
            mins=[self.xmin.value(),self.ymin.value(),self.zmin.value()]
            maxs=[self.xmax.value(),self.ymax.value(),self.zmax.value()]

            #max step possible
            stepn=int(round((maxs[i0]-mins[i0])/self.prefs.steps[i0]))
            self.stepn.setMaximum(stepn)

            stepw=abs(float(self.stepw.text()))
            stepn=int((maxs[i0]-mins[i0])/stepw)
            stepw=(maxs[i0]-mins[i0])/stepn
            self.stepn.setValue(stepn)
            self.stepw.setText('%f'%(stepw))
        except Exception:
            print('problem in compute_step_number')
            pass

    def compute_step_width(self,ii=0):
        i0=self.group1.checkedId()-1
        try:
            mins=[self.xmin.value(),self.ymin.value(),self.zmin.value()]
            maxs=[self.xmax.value(),self.ymax.value(),self.zmax.value()]

            #max step possible
            stepn=int(round((maxs[i0]-mins[i0])/self.prefs.steps[i0]))
            self.stepn.setMaximum(stepn)

            stepn=self.stepn.value()
            stepw=(maxs[i0]-mins[i0])/stepn
            self.stepw.setText('%f'%(stepw))
        except Exception:
            QMessageBox.about(self, 'Error','Input can only be a number')
            return

    def validate(self):
        i0=self.group1.checkedId()-1
        i1=self.group2.checkedId()-1
        if i1==i0:
            i1=(i0+1)%3
            self.group2.button(i1+1).setChecked(True)
        i2=3-(i0+i1)
        self.group3.button(i2+1).setChecked(True)

        mins=[self.xmin.value(),self.ymin.value(),self.zmin.value()]
        maxs=[self.xmax.value(),self.ymax.value(),self.zmax.value()]

        stepn=int((maxs[i0]-mins[i0])/self.prefs.steps[i0])
        if stepn==0:
            stepn=1
        stepw=(maxs[i0]-mins[i0])/stepn

        self.stepn.setMaximum(stepn)
        self.stepn.setValue(stepn)

        self.stepw.setText('%f'%(stepw))

    def appl (self):
        self.prefs.i0=self.group1.checkedId()-1
        self.prefs.i1=self.group2.checkedId()-1
        self.prefs.i2=self.group3.checkedId()-1

        try:
            self.prefs.mins[0] = self.xmin.value()
            self.prefs.mins[1] = self.ymin.value()
            self.prefs.mins[2] = self.zmin.value()
            self.prefs.maxs[0] = self.xmax.value()
            self.prefs.maxs[1] = self.ymax.value()
            self.prefs.maxs[2] = self.zmax.value()

            self.prefs.stepn=self.stepn.value()
            self.prefs.stepw=(self.prefs.maxs[self.prefs.i0]-self.prefs.mins[self.prefs.i0])/self.prefs.stepn


        except Exception:
            QMessageBox.about(self, 'Error','Input can only be a number')
            return

        if self.prefs.mins[0]>=self.prefs.maxs[0] or self.prefs.mins[1]>=self.prefs.maxs[1] or self.prefs.mins[2]>=self.prefs.maxs[2]:
           QMessageBox.about(self, 'Error','Minimum values must be lower than maximum ones')
           return

        self.close()
        self.prefs.do_it=True

    def closewin (self):
        self.close()
        self.prefs.do_it=False


class SetTiltedScansWindow(QDialog):
    # definit une fenetre pour rentrer les parametres de dialogue de construction de slice le long d'une rod
    def __init__(self,prefs):
        self.prefs=prefs
        super().__init__()  #permet l'initialisation de la fenetre sans perdre les fonctions associees
        self.setWindowTitle("Parameters for scans along a tilted rod")
        self.setMinimumSize(QSize(450, 160))
        self.layout=QGridLayout()
        self.setLayout(self.layout)

        self.lab01 = QLabel("Direction",self)
        self.lab02 = QLabel("Range/width",self)
        self.lab03 = QLabel("Start (incl)",self)
        self.lab04 = QLabel("Stop (incl)",self)
        self.lab05 = QLabel("Steps",self)

        self.lab10 = QLabel("Rod",self)
        self.lab20 = QLabel("Scan",self)
        self.lab30 = QLabel("Raw sum",self)

        self.lab41 = QLabel(prefs.labels[2],self)
        self.lab42 = QLabel(prefs.labels[1],self)
        self.lab43 = QLabel(prefs.labels[0],self)

        self.lab50 = QLabel("Pos. 1",self)
        self.lab60 = QLabel("Pos. 2",self)

        self.combo11 = QComboBox(self)
        self.combo21 = QComboBox(self)
        self.combo31 = QComboBox(self)

        for combo in [self.combo11,self.combo21,self.combo31]:
            combo.addItem(prefs.labels[0])
            combo.addItem(prefs.labels[1])
            combo.addItem(prefs.labels[2])

        self.box12 = QLineEdit(self)
        self.box22 = QDoubleSpinBox(self)
        self.box32 = QDoubleSpinBox(self)

        self.box13 = QDoubleSpinBox(self)  #start
        self.box14 = QDoubleSpinBox(self)  #stop
        self.box15 = QSpinBox(self)        #steps

        self.box15.setMinimum(1)

        self.entry51 = QLineEdit(self)
        self.entry52 = QLineEdit(self)
        self.entry53 = QLineEdit(self)
        self.entry61 = QLineEdit(self)
        self.entry62 = QLineEdit(self)
        self.entry63 = QLineEdit(self)

        self.OK = QPushButton(self)
        self.OK.setText("OK")

        self.Cancel = QPushButton(self)
        self.Cancel.setText("Cancel")

        self.layout.addWidget(self.lab01,0,1)
        self.layout.addWidget(self.lab02,0,2)
        self.layout.addWidget(self.lab03,0,3)
        self.layout.addWidget(self.lab04,0,4)
        self.layout.addWidget(self.lab05,0,5)
        self.layout.addWidget(self.lab10,1,0)
        self.layout.addWidget(self.lab20,2,0)
        self.layout.addWidget(self.lab30,3,0)
        self.layout.addWidget(self.lab41,4,1)
        self.layout.addWidget(self.lab42,4,2)
        self.layout.addWidget(self.lab43,4,3)
        self.layout.addWidget(self.lab50,5,0)
        self.layout.addWidget(self.lab60,6,0)

        self.layout.addWidget(self.combo11,1,1)
        self.layout.addWidget(self.combo21,2,1)
        self.layout.addWidget(self.combo31,3,1)

        self.layout.addWidget(self.box12,1,2)
        self.layout.addWidget(self.box22,2,2)
        self.layout.addWidget(self.box32,3,2)
        self.layout.addWidget(self.box13,1,3)
        self.layout.addWidget(self.box14,1,4)
        self.layout.addWidget(self.box15,1,5)

        self.layout.addWidget(self.entry51,5,1)
        self.layout.addWidget(self.entry52,5,2)
        self.layout.addWidget(self.entry53,5,3)
        self.layout.addWidget(self.entry61,6,1)
        self.layout.addWidget(self.entry62,6,2)
        self.layout.addWidget(self.entry63,6,3)

        self.layout.addWidget(self.OK,7,0)
        self.layout.addWidget(self.Cancel,7,1)

        self.combo11.setCurrentIndex(prefs.i2)
        self.combo21.setCurrentIndex(prefs.i1)
        self.combo31.setCurrentIndex(prefs.i0)

        self.set_rod_direction(prefs.i2)  #the main direction of the rod
        self.set_scan_direction(prefs.i1)  #a curve of the intensities along this direction is obtained
        self.set_raw_direction(prefs.i0)  #raw integration of all intensities along this direction

        self.entry51.setText('%f'%((self.prefs.mins[0]+self.prefs.maxs[0])/2.))
        self.entry61.setText('%f'%((self.prefs.mins[0]+self.prefs.maxs[0])/2.))
        self.entry52.setText('%f'%((self.prefs.mins[1]+self.prefs.maxs[1])/2.))
        self.entry62.setText('%f'%((self.prefs.mins[1]+self.prefs.maxs[1])/2.))
        self.entry53.setText('%f'%(self.prefs.mins[2]))
        self.entry63.setText('%f'%(self.prefs.maxs[2]))

        for i in range(1,4):
            self.layout.setColumnMinimumWidth(i,80)
        for i in range(6):
            self.layout.setColumnStretch(i,1)

        self.Cancel.clicked.connect(self.closewin)
        self.OK.clicked.connect(self.appl)
        self.combo11.activated.connect(self.set_rod_direction)
        self.combo21.activated.connect(self.set_scan_direction)
        self.combo31.activated.connect(self.set_raw_direction)
        for box in [self.box13,self.box14]:
            box.valueChanged.connect(self.round_values)
        self.box12.textEdited.connect(self.set_steps)   #only when user change the text!
        self.box15.valueChanged.connect(self.set_width)

        self.exec_()


    def set_steps(self,x):
        i0=self.combo11.currentIndex()
        width=float(self.box12.text())
        if width<self.prefs.steps[i0]:
            width=self.prefs.steps[i0]

        vmin=self.box13.value()
        vmax=self.box14.value()
        step=int((vmax-vmin)/width)
        if step<0:
            step=1
        width=(vmax-vmin)/step

        self.box15.setValue(step)
        self.box12.setText('%f'%width)

    def set_width(self,x):
        i0=self.combo11.currentIndex()
        vmin=self.box13.value()
        vmax=self.box14.value()
        step=self.box15.value()
        width=(vmax-vmin)/step

        if width<self.prefs.steps[i0]:
             step=int((vmax-vmin)/self.prefs.steps[i0])
             width=(vmax-vmin)/step
             self.box15.setValue(step)

        self.box15.setValue(step)
        self.box12.setText('%f'%width)

    def round_values(self,x=0):
        i0=self.combo11.currentIndex()
        x1=(self.box13.value()-self.prefs.absmins[i0])/self.prefs.steps[i0]
        x2=(self.box14.value()-self.prefs.absmins[i0])/self.prefs.steps[i0]

        j1=round(x1)
        j2=round(x2)
        if j1>=j2:
            if j2==0:
                j1=0
                j2=1
            else:
                j1=j2-1

        vmin=self.prefs.absmins[i0]+j1*self.prefs.steps[i0]
        vmax=self.prefs.absmins[i0]+j2*self.prefs.steps[i0]
        self.box13.setValue(vmin)
        self.box14.setValue(vmax)
        width=(vmax-vmin)/self.box15.value()

        if width<self.prefs.steps[i0]:
             step=int((vmax-vmin)/self.prefs.steps[i0])
             width=(vmax-vmin)/step
             self.box15.setValue(step)

        self.box12.setText('%f'%width)

    def set_rod_direction(self,i0):
        #called when a combobox has been changed
        stepmax=int((self.prefs.absmaxs[i0]-self.prefs.absmins[i0])/self.prefs.steps[i0])
        self.box15.setMaximum(stepmax)
        step=int((self.prefs.absmaxs[i0]-self.prefs.absmins[i0])/self.prefs.steps[i0])
        self.box15.setValue(step)

        self.box13.setRange(self.prefs.absmins[i0],self.prefs.absmaxs[i0])
        self.box13.setValue(self.prefs.mins[i0])
        self.box13.setSingleStep(self.prefs.steps[i0])
        self.box13.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i0]))))

        self.box14.setRange(self.prefs.absmins[i0],self.prefs.absmaxs[i0])
        self.box14.setValue(self.prefs.maxs[i0])
        self.box14.setSingleStep(self.prefs.steps[i0])
        self.box14.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i0]))))

        self.box12.setText('%f'%self.prefs.steps[i0])

    def set_scan_direction(self,i1):
        self.box22.setRange(0,self.prefs.absmaxs[i1]-self.prefs.absmins[i1])
        self.box22.setSingleStep(self.prefs.steps[i1])
        self.box22.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i1]))))
        self.box22.setValue(self.prefs.maxs[i1]-self.prefs.mins[i1])

    def set_raw_direction(self,i2):
        self.box32.setRange(0,self.prefs.absmaxs[i2]-self.prefs.absmins[i2])
        self.box32.setSingleStep(self.prefs.steps[i2])
        self.box32.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i2]))))
        self.box32.setValue(self.prefs.maxs[i2]-self.prefs.mins[i2])


    def appl(self):

        self.prefs.i0=self.combo11.currentIndex()  #rod integration
        self.prefs.i1=self.combo21.currentIndex() #scan direction
        self.prefs.i2=self.combo31.currentIndex() #raw sum direction

        try:
            self.prefs.widths[0] = float(self.box12.text())  #step for slice
            self.prefs.widths[1] = self.box22.value()  #range for scan
            self.prefs.widths[2] = self.box32.value()  #range for raw integration
            self.prefs.stepn = self.box15.value()
            self.prefs.mins[self.prefs.i0] = self.box13.value()  #we have minimum and maximum values for the rod direction, in the other directions, this is given by the widths at each value along the rod
            self.prefs.maxs[self.prefs.i0] = self.box14.value()

            self.prefs.pos1[0] = float(self.entry51.text())
            self.prefs.pos1[1] = float(self.entry52.text())
            self.prefs.pos1[2] = float(self.entry53.text())

            self.prefs.pos2[0] = float(self.entry61.text())
            self.prefs.pos2[1] = float(self.entry62.text())
            self.prefs.pos2[2] = float(self.entry63.text())

        except Exception:
            QMessageBox.about(self, 'Error','Input can only be a number')
            return

        if self.prefs.mins[0]>=self.prefs.maxs[0]:
             QMessageBox.about(self, 'Error','Minimum values must be lower than maximum ones')
             return

        self.close()
        print(self.prefs.stepn)
        self.prefs.do_it=True

    def closewin (self):
        self.close()
        self.prefs.do_it=False


class Set2DSliceWindow(QDialog):
    # definit une fenetre pour rentrer les parametres de dialogue de construction d'une serie de slices autour de chaque tige visible
    def __init__(self,prefs):
        self.prefs=prefs
        super().__init__()  #permet l'initialisation de la fenetre sans perdre les fonctions associees
        self.set_layout()
        self.exec_()

    def set_layout(self):
        self.setWindowTitle("Parameters for slices generation")
        self.setFixedSize(QSize(450, 190))
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.lab0 = QLabel("Direction",self)
        self.lab1 = QLabel("Slice",self)
        self.lab4=  QLabel('Min (incl.)',self)
        self.lab5=  QLabel('Max (incl.)',self)
        self.lab6=  QLabel('Bins',self)
        self.lab7=  QLabel('Width',self)

        self.group1=QButtonGroup(self)

        self.vmins = []
        self.vmaxs = []
        self.vlabs = []
        self.vboxs = []

        for i0 in range(3):
            vlab = QLabel(self.prefs.labels[i0],self)
            vbox = QCheckBox(self)

            vmin = QDoubleSpinBox(self)
            vmin.setRange(self.prefs.absmins[i0], self.prefs.absmaxs[i0])
            vmin.setSingleStep(self.prefs.steps[i0])
            vmin.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i0]))))
            vmin.setValue(self.prefs.mins[i0])

            vmax = QDoubleSpinBox(self)
            vmax.setSingleStep(self.prefs.steps[i0])
            vmax.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i0]))))
            vmax.setRange(self.prefs.absmins[i0], self.prefs.absmaxs[i0])
            vmax.setValue(self.prefs.maxs[i0])

            self.group1.addButton(vbox,i0+1)

            self.vlabs.append(vlab)
            self.vmins.append(vmin)
            self.vmaxs.append(vmax)
            self.vboxs.append(vbox)

            self.layout.addWidget(vlab,i0+1,0)
            self.layout.addWidget(vbox,i0+1,1)
            self.layout.addWidget(vmin,i0+1,4)
            self.layout.addWidget(vmax,i0+1,5)

            vbox.clicked.connect(self.validate)
            vmin.valueChanged.connect(self.round_values)
            vmax.valueChanged.connect(self.round_values)

        self.group1.button(self.prefs.i0+1).setChecked(True)

        #slice parameters
        #stepn=int(round((prefs.maxs[prefs.i0]-prefs.mins[prefs.i0])/prefs.steps[prefs.i0]))
        max_bins=int(round((self.prefs.maxs[self.prefs.i0]-self.prefs.mins[self.prefs.i0])/
                           self.prefs.steps[self.prefs.i0]))+1

        self.bins = QSpinBox(self)
        self.bins.setMinimum(1)
        self.bins.setMaximum(max_bins)
        self.bins.setValue(1)

        self.stepw = QDoubleSpinBox(self)
        self.stepw.setSingleStep(self.prefs.steps[self.prefs.i0])
        self.stepw.setDecimals(max(2,int(1-np.log10(self.prefs.steps[self.prefs.i0]))))
        self.stepw.setMinimum(self.prefs.steps[self.prefs.i0])
        self.stepw.setMaximum(self.prefs.steps[self.prefs.i0]*max_bins)

        self.OK = QPushButton(self)
        self.OK.setText("OK")

        self.Cancel = QPushButton(self)
        self.Cancel.setText("Cancel")

        self.layout.addWidget(self.lab0,0,0)
        self.layout.addWidget(self.lab1,0,1)
        self.layout.addWidget(self.lab4,0,4)
        self.layout.addWidget(self.lab5,0,5)
        self.layout.addWidget(self.lab6,4,0)
        self.layout.addWidget(self.bins,4,1)
        self.layout.addWidget(self.lab7,4,4)
        self.layout.addWidget(self.stepw,4,5)

        self.layout.addWidget(self.OK,5,4)
        self.layout.addWidget(self.Cancel,5,5)

        for i in range(1,4):
            self.layout.setColumnMinimumWidth(i,80)
        for i in range(6):
            self.layout.setColumnStretch(i,1)

        self.Cancel.clicked.connect(self.closewin)
        self.OK.clicked.connect(self.appl)

        self.bins.valueChanged.connect(self.compute_step_width)
        self.stepw.valueChanged.connect(self.compute_bins)   #only when user change the text!

        self.Cancel.clicked.connect(self.closewin)



    def round_values(self, x=0):
        i0 = self.prefs.i0
        i1=round((self.vmins[i0].value()-self.prefs.absmins[i0])/self.prefs.steps[i0])
        i2=round((self.vmaxs[i0].value()-self.prefs.absmins[i0])/self.prefs.steps[i0])
        if i1>=i2:
            if i2==0:
                i1=0
                i2=1
            else:
                i1=i2-1
        self.vmins[i0].setValue(self.prefs.absmins[i0]+i1*self.prefs.steps[i0])
        self.vmaxs[i0].setValue(self.prefs.absmins[i0]+i2*self.prefs.steps[i0])
        self.compute_step_width()

    def compute_bins(self,text=''):
        #step width has changed
        i0=self.group1.checkedId()-1
        block = self.blockSignals(True)
        self.bins.setValue(int(round(self.stepw.value()/self.prefs.steps[i0])))
        self.blockSignals(block)

    def compute_step_width(self,ii=0):
        #minimum or maximum values of slices have changed, or bin value has changed
        i0=self.group1.checkedId()-1
        block = self.blockSignals(True)

        try:
            #max step possible
            max_bins=int(round((self.vmaxs[i0].value()-self.vmins[i0].value())/self.prefs.steps[i0]))+1
            self.bins.setMaximum(max_bins)
            self.stepw.setValue(self.prefs.steps[i0]*self.bins.value())

        except Exception:
            QMessageBox.about(self, 'Error','Input can only be a number')
            return

        self.blockSignals(block)

    def validate(self):
        i0 = self.group1.checkedId()-1
        self.prefs.i0 = i0

        block = self.blockSignals(True)

        print ('validate',i0,self.prefs.steps[i0])

        #max step possible
        max_bins=int(round((self.vmaxs[i0].value()-self.vmins[i0].value())/self.prefs.steps[i0]))+1

        self.bins.setMaximum(max_bins)
        self.stepw.setValue(self.prefs.steps[i0]*self.bins.value())

        self.stepw.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i0]))))
        self.stepw.setMinimum(self.prefs.steps[i0])
        self.stepw.setMaximum(self.prefs.steps[i0]*max_bins)

        self.blockSignals(block)

    def appl (self):
        #when "OK" is pressed
        self.prefs.i0=self.group1.checkedId()-1

        for i0 in range(3):
            self.prefs.mins[i0] = self.vmins[i0].value()
            self.prefs.maxs[i0] = self.vmaxs[i0].value()

        self.prefs.stepw=self.stepw.value()
        self.prefs.bins=self.bins.value()

        if self.prefs.mins[0]>=self.prefs.maxs[0] or self.prefs.mins[1]>=self.prefs.maxs[1] or self.prefs.mins[2]>=self.prefs.maxs[2]:
           QMessageBox.about(self, 'Error','Minimum values must be lower than maximum ones')
           return

        self.close()
        self.prefs.do_it=True

    def closewin (self):
        self.close()
        self.prefs.do_it=False

class Set2DSlicesWindow(Set2DSliceWindow):
    # definit une fenetre pour rentrer les parametres de dialogue de construction d'une serie de slices autour de chaque tige visible
    def __init__(self, prefs):
        super().__init__(prefs)  #permet l'initialisation de la fenetre sans perdre les fonctions associees

    def set_layout(self):
        super().set_layout()
        self.visible = QCheckBox('visible', self)
        self.visible.setChecked(self.prefs.visible)
        self.layout.addWidget(self.visible,5,0)
        self.set_ranges()
        self.round_values()

    def set_ranges(self):
        i0 = self.prefs.i0
        for i in range(3):
            if i==i0:
                self.vmins[i].setRange(self.prefs.absmins[i],self.prefs.absmaxs[i])
                self.vmins[i].setValue(self.prefs.mins[i])
                self.vmaxs[i].setRange(self.prefs.absmins[i],self.prefs.absmaxs[i])
                self.vmaxs[i].setValue(self.prefs.maxs[i])
            else:
                dvm = (self.prefs.absmaxs[i]-self.prefs.absmins[i])
                dv = round((self.prefs.maxs[i]-self.prefs.mins[i])/2/self.prefs.steps[i])*self.prefs.steps[i]
                self.vmins[i].setRange(-dvm,0)
                self.vmins[i].setValue(-dv)
                self.vmaxs[i].setRange(0,dvm)
                self.vmaxs[i].setValue(dv)

    def validate(self):
        i0 = self.group1.checkedId()-1
        self.prefs.i0 = i0

        self.set_ranges()
        block = self.blockSignals(True)

        print ('validate',i0,self.prefs.steps[i0])

        #max step possible
        max_bins=int(round((self.vmaxs[i0].value()-self.vmins[i0].value())/self.prefs.steps[i0]))+1

        self.bins.setMaximum(max_bins)
        self.stepw.setValue(self.prefs.steps[i0]*self.bins.value())

        self.stepw.setDecimals(max(2,int(1-np.log10(self.prefs.steps[i0]))))
        self.stepw.setMinimum(self.prefs.steps[i0])
        self.stepw.setMaximum(self.prefs.steps[i0]*max_bins)

        self.blockSignals(block)

class FitTool(CommandTool):
    #: Signal emitted by CommandTool when activated
    SIG_VALIDATE_TOOL = Signal('PyQt_PyObject')

    def __init__(self, manager, title=None,
                 icon=None, tip=None, toolbar_id=None):
        if title == None:
            title = "Fit plot item"
        super().__init__(manager, title, icon, toolbar_id=toolbar_id)

    def activate_command(self, plot, checked):
        """Activate tool"""
        self.SIG_VALIDATE_TOOL.emit(plot)



class RunTool(CommandTool):
    #: Signal emitted by CommandTool when activated
    SIG_VALIDATE_TOOL = Signal()

    def __init__(self, manager, title=None,
                 icon=None, tip=None, toolbar_id=DefaultToolbarID):
        if title == None:
            title = "Apply"
        if icon == None:
            icon = get_icon("apply.png")
        super().__init__(manager, title, icon, toolbar_id=toolbar_id)

    def setup_context_menu(self, menu, plot):
        pass

    def activate_command(self, plot, checked):
        """Activate tool"""
        self.SIG_VALIDATE_TOOL.emit()



#in this module we define a widget for showing the progression of calculations
class ProgressBar(QWidget):
    def __init__(self,title):
        QWidget.__init__(self)
        self.setWindowTitle(title)
        self.setFixedSize(QSize(200, 80))

        self.progressbar = QProgressBar(self)
        self.progressbar.setGeometry(10, 10, 180, 30)

        self.cancelbtn = QPushButton("Cancel", self)
        self.cancelbtn.setGeometry(10, 40, 100, 30)

        self.cancelbtn.clicked.connect(self.cancelwin)
        self.stop=False

    def cancelwin(self):
        self.stop=True

    def update_progress(self,x):
        self.progressbar.setValue(x*100)
        qApp.processEvents()


class ReciprocalImageWidget(QSplitter):
    """
    Construct a BaseImageWidget object, which includes:
        * A plot (:py:class:`guiqwt.curve.CurvePlot`)
        * An `item list` panel (:py:class:`guiqwt.curve.PlotItemList`)
        * A `contrast adjustment` panel
          (:py:class:`guiqwt.histogram.ContrastAdjustment`)
        * An `X-axis cross section` panel
          (:py:class:`guiqwt.histogram.XCrossSection`)
        * An `Y-axis cross section` panel
          (:py:class:`guiqwt.histogram.YCrossSection`)

    This object does nothing in itself because plot and panels are not
    connected to each other.
    See children class :py:class:`guiqwt.plot.ImageWidget`
    """
    def __init__(self, parent = None, title = "",
                 xlabel = ("", ""), ylabel = ("", ""), zlabel = None,
                 xunit = ("", ""), yunit = ("", ""), zunit = None, yreverse = True,
                 colormap = "jet", aspect_ratio = 1.0, lock_aspect_ratio = True,
                 show_contrast = False, show_itemlist = False, show_xsection = False,
                 show_ysection = False, xsection_pos = "top", ysection_pos = "right",
                 gridparam = None, curve_antialiasing = None, **kwargs):
        if PYQT5:
            super().__init__(parent, **kwargs)
            self.setOrientation(Qt.Vertical)
        else:
            QSplitter.__init__(self, Qt.Vertical, parent)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.sub_splitter = QSplitter(Qt.Horizontal, self)
        self.tab = QTabWidget(self)
        self.projectionplots = []
        for i in range(3):
            plot = ReciprocalPlot(parent = self, title = title,
                              xlabel = xlabel, ylabel = ylabel, zlabel = zlabel,
                              xunit = xunit, yunit = yunit, zunit = zunit,
                              yreverse = yreverse, aspect_ratio = aspect_ratio,
                              lock_aspect_ratio = lock_aspect_ratio,
                              gridparam = gridparam)
            if curve_antialiasing is not None:
                plot.set_antialiasing(curve_antialiasing)
            self.projectionplots.append(plot)
            self.tab.addTab(plot, 'projection %d'%i)

        from guiqwt.cross_section import YCrossSection
        self.ycsw = YCrossSection(self, position = ysection_pos,
                                  xsection_pos = xsection_pos)
        self.ycsw.setVisible(show_ysection)

        from guiqwt.cross_section import XCrossSection
        self.xcsw = XCrossSection(self)
        self.xcsw.setVisible(show_xsection)

        self.xcsw.SIG_VISIBILITY_CHANGED.connect(self.xcsw_is_visible)

        self.xcsw_splitter = QSplitter(Qt.Vertical, self)
        if xsection_pos ==   "top":
            self.xcsw_splitter.addWidget(self.xcsw)
            self.xcsw_splitter.addWidget(self.tab)
        else:
            self.xcsw_splitter.addWidget(self.tab)
            self.xcsw_splitter.addWidget(self.xcsw)
        self.xcsw_splitter.splitterMoved.connect(
                                 lambda pos, index: self.adjust_ycsw_height())

        self.ycsw_splitter = QSplitter(Qt.Horizontal, self)
        if ysection_pos ==   "left":
            self.ycsw_splitter.addWidget(self.ycsw)
            self.ycsw_splitter.addWidget(self.xcsw_splitter)
        else:
            self.ycsw_splitter.addWidget(self.xcsw_splitter)
            self.ycsw_splitter.addWidget(self.ycsw)

        configure_plot_splitter(self.xcsw_splitter,
                                decreasing_size = xsection_pos ==   "bottom")
        configure_plot_splitter(self.ycsw_splitter,
                                decreasing_size = ysection_pos ==   "right")

        self.sub_splitter.addWidget(self.ycsw_splitter)

        self.itemlist = PlotItemList(self)
        self.itemlist.setVisible(show_itemlist)
        self.sub_splitter.addWidget(self.itemlist)

        # Contrast adjustment (Levels histogram)
        from guiqwt.histogram import ContrastAdjustment
        self.contrast = ContrastAdjustment(self)
        self.contrast.setVisible(show_contrast)
        self.addWidget(self.contrast)

        configure_plot_splitter(self)
        configure_plot_splitter(self.sub_splitter)
        self.tab.currentChanged.connect(self.current_plot_changed)

    def current_plot_changed(self, index):
        #when tabwidget current plot has changed
        plot = self.projectionplots[index]
        plot.SIG_ITEMS_CHANGED.emit(plot)


    def adjust_ycsw_height(self, height = None):
        if height is None:
            height = self.xcsw.height()-self.ycsw.toolbar.height()
        self.ycsw.adjust_height(height)
        if height:
            QApplication.processEvents()

    def set_labels(self,labels):
        #change the labels of the projection tabs
        for i in range(3):
            self.tab.setTabText(i,labels[i])

    def xcsw_is_visible(self, state):
        if state:
            QApplication.processEvents()
            self.adjust_ycsw_height()
        else:
            self.adjust_ycsw_height(0)


#we cannot use lambda functions in a loop, we replace with more sophisticated version
def methodFactory(curve):
    def get_closest_x(x,y):
        return curve.get_closest_x(x,y)
    return get_closest_x

class ProjectionWidget(PanelWidget):
    PANEL_ID=999

    SIG_PROJECTION_CHANGED = Signal()
    SIG_ACTIVE_PLOT_CHANGED = Signal()

    def __init__(self, parent=None, labels = ['X', 'Y', 'Z']):
        super().__init__(parent)

        self.manager = None # manager for the associated image plot
        self.local_manager = PlotManager(self) # local manager for the histogram plot

        self.setMinimumWidth(250)
        self.dockwidget=None
        self.set_plots(labels)

        #all buttons and plot are stacked in HBox stacked in a VBox, which is the Layout
        VBox = QVBoxLayout()
        self.setLayout(VBox)

        #--------------------first line----------------------
        self.show_intensities = QCheckBox('intensities',self)
        self.show_intensities.setChecked(True)
        self.show_counts = QCheckBox('counts',self)
        self.show_contributions = QCheckBox('contributions',self)
        self.intgroup=QButtonGroup(VBox)
        self.intgroup.setExclusive(True)
        self.intgroup.addButton(self.show_intensities, id=0)
        self.intgroup.addButton(self.show_counts, id=1)
        self.intgroup.addButton(self.show_contributions, id=2)
        HBox = QHBoxLayout()
        HBox.addWidget(self.show_intensities)
        HBox.addWidget(self.show_counts)
        HBox.addWidget(self.show_contributions)
        VBox.addLayout(HBox)

        #--------------------second line----------------------
        self.zlog = QCheckBox('log scale',self)
        self.swap = QCheckBox('swap',self)
        self.swap.setChecked(True)
        self.autoscale = QCheckBox('autoscale',self)
        self.autoscale.setChecked(True)
        HBox = QHBoxLayout()
        HBox.addWidget(self.zlog)
        HBox.addWidget(self.swap)
        HBox.addWidget(self.autoscale)
        VBox.addLayout(HBox)

        #--------------------plots and associated buttons etc...----------------------
        self.labels = []
        self.selectplanes = []
        self.bgsubtracts = []
        self.autoscales = []
        self.minvalues = []
        self.maxvalues = []

        for axis in range(3):
             HBox=QHBoxLayout()

             label = QLabel(labels[axis],self)
             self.labels.append(label)
             HBox.addWidget(label)

             chbox1 = QCheckBox('select plane',self)
             self.selectplanes.append(chbox1)
             HBox.addWidget(chbox1)

             chbox2 = QCheckBox('subtract bg',self)
             self.bgsubtracts.append(chbox2)
             HBox.addWidget(chbox2)

             VBox.addLayout(HBox)

             HBox=QHBoxLayout()

             autoscale = QPushButton('<->',self)
             self.autoscales.append(autoscale)
             HBox.addWidget(autoscale)

             minvalue = QLineEdit(self)
             self.minvalues.append(minvalue)
             HBox.addWidget(minvalue)

             maxvalue = QLineEdit(self)
             self.maxvalues.append(maxvalue)
             HBox.addWidget(maxvalue)

             VBox.addLayout(HBox)

             VBox.addWidget(self.plots[axis])

        #adding tools to the local manager
        self.local_manager.add_tool(SelectTool)
        self.local_manager.add_tool(BasePlotMenuTool, "item")
        self.local_manager.add_tool(BasePlotMenuTool, "axes")
        self.local_manager.add_tool(BasePlotMenuTool, "grid")
        self.local_manager.add_tool(AntiAliasingTool)
        self.fittool = self.local_manager.add_tool(FitTool)
        self.local_manager.get_default_tool().activate()
        self.setup_connect()
        self.active_plot=self.plots[0]
        self.prefs=SlicePrefs()

    def set_plots(self, labels = ['X', 'Y', 'Z']):
        self.plots = []
        self.curves = []
        self.selectranges = []
        self.bgranges1 = []
        self.bgranges2 = []
        self.markers = []

        for axis in range(3):
            plot = BinoPlot()
            self.local_manager.add_plot(plot)
            self.plots.append(plot)
            curve = make.binocurve([0,1],[0,0])
            curve.x_label = labels[axis]
            self.curves.append(curve)
            r0 =  XRangeSelection2(0,1)
            self.selectranges.append(r0)
            r1 = BgRangeSelection(0,1)  #range selection for background subtraction left side
            self.bgranges1.append(r1)
            r2 = BgRangeSelection(0,1)  #range selection for background subtraction right side
            self.bgranges2.append(r2)

            r1.setVisible(False)
            r2.setVisible(False)

            #cannot use lambda function in a loop
            marker = Marker(constraint_cb = methodFactory(curve))

            self.markers.append(marker)
            marker.set_markerstyle('|')
            marker.setVisible(False)   #default projection along h

            plot.add_item(r1)
            plot.add_item(r2)
            plot.add_item(r0)
            plot.add_item(curve)
            plot.add_item(marker)
            plot.replot()

    def keyPressEvent(self, event):
        if type(event) == QKeyEvent:
            #here accept the event and do something
            event.accept()
        else:
            event.ignore()

    def get_plot(self):
        return self.manager.get_active_plot()


    def register_panel(self,manager):
        self.manager = manager


    def configure_panel(self):
        pass

    def setup_connect(self):
        for axis in range(3):
            plot = self.plots[axis]
            plot.SIG_RANGE_CHANGED.connect(self.selection_changed)
            plot.SIG_MARKER_CHANGED.connect(self.selection_changed)
            plot.SIG_ITEM_SELECTION_CHANGED.connect(self.set_active_plot)
            plot.MOVE.connect(lambda value,axis=axis: self.move_cursor(value,axis))

            self.minvalues[axis].returnPressed.connect(self.value_changed)
            self.maxvalues[axis].returnPressed.connect(self.value_changed)
            self.selectplanes[axis].clicked.connect(lambda checked,axis=axis: self.plane_changed(checked, axis))
            self.bgsubtracts[axis].clicked.connect(lambda checked,axis=axis: self.bg_changed(checked, axis))
            self.autoscales[axis].clicked.connect(lambda checked,axis=axis: self.do_autoscale(axis))

        self.zlog.clicked.connect(self.log_changed)
        self.swap.clicked.connect(self.other_changed)
        self.intgroup.buttonClicked.connect(self.int_changed)

    def log_changed(self):
        if self.zlog.isChecked():
            for plot in self.plots:
                plot.set_axis_scale('left','log')
                plot.replot()
        else:
            for plot in self.plots:
                plot.set_axis_scale('left','lin')
                plot.replot()

        self.SIG_PROJECTION_CHANGED.emit()

    def update_bg_range(self, axis):
        #we show background for the projected curve
        #we update position of the bg to be in adequation with the range selected

        bgrange1 = self.bgranges1[axis]
        bgrange2 = self.bgranges2[axis]

        dxs2 = self.spacings[axis]/2.

        if self.selectplanes[axis].isChecked():
            marker = self.markers[axis].xValue()
            xrmin, xrmax = marker-dxs2, marker+dxs2
        else:
            xrmin, xrmax = self.selectranges[axis].get_range()

        xdata = self.curves[axis].get_data()[0]

        bgrange1.setVisible(True)
        bgrange2.setVisible(True)

        if len(xdata) > 0:
            xmin = min(xdata)
            xmax = max(xdata)
            if xmin > xrmin:  #instead of starting from first point of the curve, we start from a lowest value
                xmin = xrmin
            if xmax < xrmax:
                xmax = xrmax
            bgrange1.set_range(xmin-dxs2, xrmin, dosignal=False)
            bgrange2.set_range(xmax+dxs2, xrmax, dosignal=False)
        else:  #no data, we choose arbitrary 10% tail
            bgrange1.set_range(xrmin-0.1*(xrmax-xrmin), xrmin, dosignal=False)
            bgrange2.set_range(xrmax, xrmax+0.1*(xrmax-xrmin), dosignal=False)


    def bg_changed(self, checked, axis):
        if checked:
            #we show background for the projected curve
            self.update_bg_range(axis)

        else:
            self.bgranges1[axis].setVisible(False)
            self.bgranges2[axis].setVisible(False)

        self.SIG_PROJECTION_CHANGED.emit()

    def other_changed(self, i=0):
        self.SIG_PROJECTION_CHANGED.emit()

    def set_active_plot(self, plot):
        self.active_plot = plot
        self.SIG_ACTIVE_PLOT_CHANGED.emit()

    def selection_changed(self, plot):
        #a range selection has been modified
        ranges = []
        for axis in range(3):
            vmin, vmax = self.selectranges[axis].get_range()
            ranges.append([vmin,vmax])
        self.set_values(np.array(ranges))
        self.SIG_PROJECTION_CHANGED.emit()

    def value_changed(self):
        #a range value has been entered
        ranges = []
        for axis in range(3):
            vmin, vmax = float(self.minvalues[axis].text()), float(self.maxvalues[axis].text())
            ranges.append([vmin,vmax])
        self.set_ranges(np.array(ranges))
        self.SIG_PROJECTION_CHANGED.emit()
        pass

    def do_autoscale(self, axis):
        vmin=self.ranges[axis][0]
        vmax=self.ranges[axis][1]
        self.minvalues[axis].setText('%f'%vmin)
        self.maxvalues[axis].setText('%f'%vmax)
        self.value_changed()

    def plane_changed(self, checked, axis):
        #change between range and plane selection modes
        if self.bgsubtracts[axis].isChecked():
            self.update_bg_range(axis)

        if checked:
            self.selectranges[axis].setVisible(False)
            self.set_marker_position(axis)
        else:
            self.markers[axis].setVisible(False)
            self.selectranges[axis].setVisible(True)

        self.plots[axis].replot()
        self.SIG_PROJECTION_CHANGED.emit()

    def move_cursor(self, i, axis):
        if self.selectplanes[axis].isChecked():
            xdata, ydata = self.curves[axis].get_data()
            if len(xdata) > 0:  #put marker at the middle of the curve
                x, y = self.markers[axis].get_pos()
                x, y = self.curves[axis].get_closest_x(x+i*self.spacings[axis], y)
                self.markers[axis].setValue(x, y)
                self.SIG_PROJECTION_CHANGED.emit()
        else:   #we move the range
            _min, _max=self.selectranges[axis].get_range()
            self.selectranges[axis].set_range(_min+i*self.spacings[axis], _max+i*self.spacings[axis])

    def set_marker_position(self, axis):
        self.markers[axis].setVisible(True)
        self.plots[axis].select_some_items([self.markers[axis]])
        xdata,ydata=self.curves[axis].get_data()
        if len(xdata) > 0:  #put marker at the middle of the curve
            x, y = self.markers[axis].get_pos()
            x, y = self.curves[axis].get_closest_x(x,y)
            self.markers[axis].setValue(x, y)

    def int_changed(self,):
        self.SIG_PROJECTION_CHANGED.emit()

    def set_ranges_and_values(self, ranges, labels=None, spacings=[1,1,1]):
        #adjust ranges and values with new data
        if labels is not None:
            #print (labels,self.labels, self.curves)
            for axis in range(3):
                self.labels[axis].setText(labels[axis])
                self.curves[axis].x_label = labels[axis]

        #ranges for complete data
        self.ranges = np.array(ranges)
        self.spacings = spacings #intervals between points along x,y,z

        if self.autoscale.isChecked():
            self.set_ranges(ranges)
            self.set_values(ranges)

    def set_ranges(self, ranges, spacings=None, dosignal=False):
        for axis in range(3):
            #range selection
            self.selectranges[axis].set_range(ranges[axis][0]-self.spacings[axis]/2.,ranges[axis][1]+self.spacings[axis]/2.,dosignal=dosignal)

            #background left
            self.bgranges1[axis].set_range(ranges[axis][0]-3.*self.spacings[axis]/2.,ranges[axis][0]-self.spacings[axis]/2.,dosignal=dosignal)

            #background right
            self.bgranges2[axis].set_range(ranges[axis][1]+self.spacings[axis]/2.,ranges[axis][1]+3.*self.spacings[axis]/2.,dosignal=dosignal)

            self.plots[axis].replot()

    def set_values(self, ranges):
        for axis in range(3):
            self.minvalues[axis].setText('%f'%ranges[axis][0])
            self.maxvalues[axis].setText('%f'%ranges[axis][1])

    def get_ranges(self):
        #return selection range
        ranges = []
        for axis in range(3):
            if self.selectplanes[axis].isChecked():
                #just select plane
                v = self.markers[axis].xValue()
                ranges.append([v-self.spacings[axis]/2.,v+self.spacings[axis]/2.])
            else:
                vmin, vmax = self.selectranges[axis].get_range()
                ranges.append([vmin,vmax])
        return np.array(ranges)

    def set_projected_range(self, x0, x1, y0, y1, axis = 0):
        #update range and values from range (x0,x1,y0,y1) of the projected plot
        if axis==0:
            if self.swap.isChecked():
                self.selectranges[1].set_range(x0,x1,dosignal=False)
                self.selectranges[2].set_range(y0,y1,dosignal=True)
            else:
                self.selectranges[1].set_range(y0,y1,dosignal=False)
                self.selectranges[2].set_range(x0,x1,dosignal=True)
        elif axis==1:
            if self.swap.isChecked():
                self.selectranges[0].set_range(x0,x1,dosignal=False)
                self.selectranges[2].set_range(y0,y1,dosignal=True)
            else:
                self.selectranges[0].set_range(y0,y1,dosignal=False)
                self.selectranges[2].set_range(x0,x1,dosignal=True)
        else:
            if self.swap.isChecked():
                self.selectranges[0].set_range(x0,x1,dosignal=False)
                self.selectranges[1].set_range(y0,y1,dosignal=True)
            else:
                self.selectranges[0].set_range(y0,y1,dosignal=False)
                self.selectranges[1].set_range(x0,x1,dosignal=True)




class Image3DDialog(ImageDialog):
    def __init__(self, parent=None,):
        defaultoptions={"show_contrast":True,"show_xsection":False,"show_ysection":False,"lock_aspect_ratio":False,"yreverse":False}
        ImageDialog.__init__(self, edit=False, toolbar=True,options=defaultoptions)
        self.fittool = self.add_tool(FitTool)
        self.fittool.SIG_VALIDATE_TOOL.connect(self.do_fit)

        #3D data associated
        self.data = []

        self.sliceprefs = SlicePrefs()
        self.laplaceparam = LaplaceParam()
        self.filterparam = FilterParam()

        self.create_menubar()
        self.preferences = Preferences()
        self.make_default_images()
        self.create_connects()

        self.fit1Dwindow = None #a window for fitting 1D slices
        self.fit2Dwindow = None #a window for fitting 2D slices
        self.roiwindow = None #a window for showing ROI integration results
        self.reciprocalwindow = None  # a window for making copy of maps

        self.plot_widget.current_plot_changed(self.plot_widget.tab.currentIndex()) #set masked_image to current displayed tab widget

        self.roi_shape = None  # a shape for ROI integration
        self.savefilename = None

        self.cwd = os.getcwd()
        self.style_generator = style_generator()

    def register_image_tools(self):
        ImageDialog.register_image_tools(self)
        self.openfiletool = self.add_tool(OpenFileTool)
        self.openfiletool.SIG_OPEN_FILE.connect(self.open_file)
        self.rectselecttool = self.add_tool(RectSelectTool, icon='range_selection.png')
        self.rectselecttool.SIG_RECT_SELECTION.connect(self.set_projected_range)
        self.lgt = self.add_tool(LatticeGridTool)
        self.rgt = self.add_tool(ReconstructionGridTool)
        self.add_tool(RectanglePeakTool)
        self.spottool = self.add_tool(CircularActionToolCXY,self.draw_roi,self.move_roi)

    def create_menubar(self):
        self.menubar = QMenuBar(self)
        self.layout().setMenuBar(self.menubar)

        """***************File Menu*******************************"""

        self.menuFile = QMenu("File",self.menubar)

        self.actionOpen = QAction("Open",self.menuFile)
        self.actionOpen.setShortcut("Ctrl+O")

        self.actionSetSaveFile = QAction("Set filename for saving ROI parameters",self.menuFile)
        self.actionSetSaveFile.setShortcut("Ctrl+S")

        self.actionQuit = QAction("Quit",self.menuFile)
        self.actionQuit.setShortcut("Ctrl+Q")

        self.menuFile.addActions((self.actionOpen, self.actionSetSaveFile, self.actionQuit))

        """***************Operations Menu************************"""

        self.menuOperations = QMenu("Operations",self.menubar)

        self.menuSlices = QMenu("Slices",self.menuOperations)
        self.action2D = QAction("Slice rod (2D integration)",self.menuSlices)
        self.action2DNodes = QAction("Slices along lattice rods (2D integration)",self.menuSlices)
        self.actionStraight = QAction("Scan straight rod (1D int)",self.menuSlices)
        self.actionTilted = QAction("Scan tilted rod (1D int)",self.menuSlices)
        self.menuSlices.addActions((self.action2D,self.action2DNodes,self.actionStraight,self.actionTilted))
        self.actionIntegrate = QAction("ROI integration",self.menuOperations)
        self.actionIntegrate.setShortcut("Ctrl+I")
        self.actionIntegrateAll = QAction("ROI integration along lattice spots",self.menuOperations)

        self.menuInterpolation = QMenu("Interpolation",self.menuOperations)
        self.actionGriddata = QAction("Scipy griddata",self.menuInterpolation)
        self.actionLaplace = QAction("Laplace equation",self.menuInterpolation)
        self.actionFilling = QAction("Filling",self.menuInterpolation)
        self.menuInterpolation.addActions((self.actionGriddata,self.actionLaplace,self.actionFilling))

        self.actionFilter = QAction("Filter data",self.menuOperations)
        self.actionExportImage = QAction("Export current image", self.menuOperations)

        self.menuOperations.addMenu(self.menuSlices)
        self.menuOperations.addAction(self.actionFilter)
        self.menuOperations.addAction(self.actionIntegrate)
        self.menuOperations.addAction(self.actionIntegrateAll)
        self.menuOperations.addMenu(self.menuInterpolation)
        self.menuOperations.addAction(self.actionExportImage)

        """***************Show Menu************************"""
        self.menuShow = QMenu("Show",self.menubar)
        self.actionshowFit1D = QAction("Curve fitting",self.menuShow)
        self.menuShow.addAction(self.actionshowFit1D)
        self.actionshowFit2D = QAction("Image fitting",self.menuShow)
        self.menuShow.addAction(self.actionshowFit2D)
        self.actionshowRoi = QAction("ROI integration result",self.menuShow)
        self.menuShow.addAction(self.actionshowRoi)
        self.actionshowReciprocal = QAction("Reciprocal space",self.menuShow)
        self.menuShow.addAction(self.actionshowReciprocal)

        """***************Settings Menu************************"""
        self.menuSettings = QMenu("Settings",self.menubar)
        self.actionPreferences = QAction("Preferences",self.menuSettings)
        self.menuSettings.addAction(self.actionPreferences)


        """**********************************************"""

        self.menubar.addMenu(self.menuFile)
        self.menubar.addMenu(self.menuOperations)
        self.menubar.addMenu(self.menuShow)
        self.menubar.addMenu(self.menuSettings)

        self.actionOpen.triggered.connect(self.get_open_filename)
        self.actionSetSaveFile.triggered.connect(self.get_save_filename)
        self.actionQuit.triggered.connect(self.close)
        self.action2D.triggered.connect(self.make_2D_slices)
        self.action2DNodes.triggered.connect(self.make_2D_slices_around_nodes)
        self.actionStraight.triggered.connect(self.make_straight_scans)
        self.actionTilted.triggered.connect(self.make_tilted_scans)
        self.actionGriddata.triggered.connect(self.griddata_interpolation)
        self.actionLaplace.triggered.connect(self.laplace_interpolation)
        self.actionFilling.triggered.connect(self.filling)
        self.actionFilter.triggered.connect(self.do_filter)
        self.actionIntegrate.triggered.connect(self.integrate_single_roi)
        self.actionIntegrateAll.triggered.connect(self.integrate_all_rois)
        self.actionExportImage.triggered.connect(self.export_image)
        self.actionshowFit1D.triggered.connect(self.show_fit1Dwindow)
        self.actionshowFit2D.triggered.connect(self.show_fit2Dwindow)
        self.actionshowRoi.triggered.connect(self.show_roiwindow)
        self.actionshowReciprocal.triggered.connect(self.show_reciprocalwindow)

        self.actionPreferences.triggered.connect(self.set_preferences)

    def create_plot(self, options, row=0, column=0, rowspan=1, columnspan=1):

        self.plot_widget = ReciprocalImageWidget(self, **options)
        self.plot_layout.addWidget(self.plot_widget,
                                   row, column, rowspan, columnspan)


        self.imageplots = self.plot_widget.projectionplots
        # Configuring plot manager
        for plot in self.plot_widget.projectionplots:
            self.add_plot(plot)


        self.add_panel(self.plot_widget.itemlist)
        self.add_panel(self.plot_widget.xcsw)
        self.add_panel(self.plot_widget.ycsw)
        self.add_panel(self.plot_widget.contrast)


        #ImageDialog.create_plot(self, options, row, column, rowspan, columnspan)
        #self.customise_plots()
        #ra_panel = ObliqueCrossSection(self)
        #splitter = self.plot_widget.xcsw_splitter
        #splitter.addWidget(ra_panel)
        #splitter.setStretchFactor(splitter.count()-1, 1)
        #splitter.setSizes(list(splitter.sizes())+[2])
        #self.add_panel(ra_panel)

        self.p_panel = ProjectionWidget(self)
        self.imagemasking = ImageMaskingWidget(self)

        splitter = self.plot_widget.ycsw_splitter
        splitter.addWidget(self.p_panel)
        splitter.addWidget(self.imagemasking)

        splitter.setStretchFactor(0,1)
        splitter.setStretchFactor(1,0)
        splitter.setStretchFactor(2,0)
        splitter.setSizes(list(splitter.sizes())+[2])

        self.add_panel(self.p_panel)
        self.add_panel(self.imagemasking)



    def create_connects(self):
        self.p_panel.SIG_PROJECTION_CHANGED.connect(self.update_images)
        self.p_panel.fittool.SIG_VALIDATE_TOOL.connect(self.do_fit)
        self.imagemasking.run_tool.SIG_VALIDATE_TOOL.connect(self.delete_masked_values)

    def get_projection(self):
        #return the index of the projection displayed
        return self.plot_widget.tab.currentIndex()

    def customise_plots(self):
        for plot in self.imageplots:
            plot.cross_marker.detach()
            plot.cross_marker = ReciprocalMarker()
            plot.cross_marker.set_style("plot", "marker/cross")
            plot.cross_marker.setVisible(False)
            plot.cross_marker.attach(plot)

    def make_default_images(self):
        self.images = []
        for i in range(3):
            image = make.maskedimagenan(np.zeros((1,1)), show_mask=True, interpolation='nearest', xdata=[0,1], ydata=[0,1])
            image.set_readonly(True)
            self.imageplots[i].add_item(image)
            self.images.append(image)


    def update_images(self):

        intensity = self.p_panel.intgroup.checkedId()  #determine whether intensity, counts or contributions are shown
        bg = [self.p_panel.bgsubtracts[i].isChecked() for i in range(3)]
        if self.data is not None:
            self.make_projection(intensity=intensity, bg=bg) #show integrated intensity for selected range
            if intensity == 0:
                for image in self.images:
                    image.setTitle("intensities")
            elif intensity ==1:
                for image in self.images:
                    image.setTitle("counts")
            else:
                for image in self.images:
                    image.setTitle("contributions")


    def export_image(self):
        if self.reciprocalwindow is None:
            self.init_reciprocalwindow()

        title = str(self.windowTitle())
        axis = self.get_projection()
        self.reciprocalwindow.import_image(self.images[axis], title=title)
        self.reciprocalwindow.show()


    def get_open_filename(self):
        self.openfiletool.activate_command(self.imageplots[0], True)


    def open_file(self,filename):
        #open a hdf5 file generated from diffraction data by binoviewer
        #file contains two 3D arrays: counts and contributions. Intensity = counts/contributions
        #limits are the center of the voxels

        print(filename)
        print(self.openfiletool.directory)

        try:
            hfile = tables.open_file(filename)
        except Exception as err:

            QMessageBox.about(self, 'Error','unable to open file')
            print (err)
            return

        print(hfile.root.binoculars.counts.shape)
        #print( hfile)
        try:
            self.data=np.array(hfile.root.binoculars.counts.read(),np.float32)
            self.contributions=np.array(hfile.root.binoculars.contributions.read(),np.float32)

        except tables.exceptions.NoSuchNodeError:
            QMessageBox.about(self, 'Error','file is empty')
            return

        except MemoryError:
            try:
                self.data = np.array(hfile.root.binoculars.counts.read(),np.float16)
                self.contributions = np.array(hfile.root.binoculars.contributions.read(),np.float16)

            except MemoryError:
                QMessageBox.about(self, 'Error','file is too big')


        axes = hfile.root.binoculars.axes

        self.labels = []
        #print (self.axes._v_children,labels)
        for leaf in axes._f_list_nodes():
            self.labels.append(leaf.name)

        #print( self.axes._f_listNodes())
        self.x_label,self.y_label,self.z_label = self.labels
        self.x_values = axes._v_children[self.x_label].read()
        self.y_values = axes._v_children[self.y_label].read()
        self.z_values = axes._v_children[self.z_label].read()

        self.values = np.array([self.x_values,self.y_values,self.z_values])
        ranges = self.values[:,1:3]   #min   =   center of the first pixel, max = center of the last pixel -> number of points = (max-min)/interval + 1
        spacings = self.values[:,3]
        self.p_panel.set_ranges_and_values(ranges,self.labels,spacings)
        self.plot_widget.set_labels(self.labels)






        self.update_images()


        self.setWindowTitle(filename.split("/")[-1])


        mins = [round(self.x_values[1],12),round(self.y_values[1],12),round(self.z_values[1],12)]
        steps = [round(self.x_values[3],12),round(self.y_values[3],12),round(self.z_values[3],12)]
        maxs = [round(self.x_values[2],12),round(self.y_values[2],12),round(self.z_values[2],12)]


        self.update_sliceprefs(absranges=[mins,maxs], ranges=[mins,maxs], steps=steps, labels = self.labels)

        #we need to update masking tool size with pixel size of the image
        self.plot_widget.current_plot_changed(self.plot_widget.tab.currentIndex()) #set masked_image to current displayed tab widget

        #self.slicetool2.update_pref(absranges=[mins,maxs],steps=steps,labels=self.labels)

        #interpolation de self.data
    def get_save_filename(self):
        fname = QFileDialog.getSaveFileName(None, 'Save ROI integrals', self.cwd, filter="*.txt")[0]
        if len(fname) == 0:
            return
        self.set_save_filename(fname)

    def set_save_filename(self,fname):
        self.savefilename = fname
        self.cwd = path.dirname(path.realpath(self.savefilename))
        fic = open(self.savefilename,'a')
        fic.write('xname yname zname x0 y0 z0 lattice hx kx h k w_Fexp err_w_Fexp nw_Fexp err_nw_Fexp ')
        fic.write('w_integral nw_integral error_integral total_counts bg_counts total_cont shell_integral shell_cont ')
        fic.write('core_voxels shell_voxels ')
        fic.write('bg_slope_x bg_slope_y bg_0 err_bg_slope_x err_bg_slope_y err_bg_0 chi2_bg ')
        fic.write('ROI_shape xmin xmax ymin ymax p0x p0y p1x p1y p2x p2y p3x p3y p4x p4y p5x p5y p6x p6y p7x p7y ')
        fic.write('int_bg_slope_x int_bg_slope_y int_bg_0 int_amp int_loc0 int_loc1 int_width0 int_width1 int_th int_Fexp int_chi2 ')
        fic.write('\n')
        fic.close()

    def set_preferences(self):
        self.preferences.edit()

    def delete_masked_values(self):

        axis = self.get_projection()

        ix0,ix1 = self.showndatalimits[0]
        iy0,iy1 = self.showndatalimits[1]
        iz0,iz1 = self.showndatalimits[2]

        mask = self.images[axis].data.mask
        if self.p_panel.swap.isChecked():
            mask = mask.transpose()
        if axis == 0:
            for ix in range(ix0,ix1):
                self.data[ix,iy0:iy1,iz0:iz1] = np.where(mask,0.,self.data[ix,iy0:iy1,iz0:iz1])
                self.contributions[ix,iy0:iy1,iz0:iz1] = np.where(mask,0.,self.contributions[ix,iy0:iy1,iz0:iz1])
        elif axis==1:
            for iy in range(iy0,iy1):
                self.data[ix0:ix1,iy,iz0:iz1] = np.where(mask,0.,self.data[ix0:ix1,iy,iz0:iz1])
                self.contributions[ix0:ix1,iy,iz0:iz1] = np.where(mask,0.,self.contributions[ix0:ix1,iy,iz0:iz1])
        else:
            for iz in range(iz0,iz1):
                self.data[ix0:ix1,iy0:iy1,iz] = np.where(mask,0.,self.data[ix0:ix1,iy0:iy1,iz])
                self.contributions[ix0:ix1,iy0:iy1,iz] = np.where(mask,0.,self.contributions[ix0:ix1,iy0:iy1,iz])
        self.update_images()

    def laplace_interpolation(self):

        #first we select points to interpolate inside a convex hull
        #https://gist.github.com/stuarteberg/8982d8e0419bea308326933860ecce30

        doit = self.laplaceparam.edit()
        if not doit:
            return

        if self.laplaceparam.doslice:

            i0 = self.laplaceparam.cutoff



            sld = np.array(self.data)  #copy to save
            slc = np.array(self.contributions)  #copy to save
            sle = np.nan_to_num(self.data/slc)   #normalized intensity values, 0 if no contributions

            condition = slc<i0   #non measured points

            print ('number of non measured voxels: ', np.sum(condition))
            kmax = self.data.shape[2]

            #first fitting slices with average value
            progressbar = ProgressBar('first fill slices with average...')
            progressbar.show()

            for k in range(kmax):
                progressbar.update_progress(k/float(kmax))

                if np.sum(~condition[:,:,k])>2:
                    if self.laplaceparam.in_convex :
                        points = np.argwhere(~condition[:,:,k]).astype(np.int16)  #pixels measured: array of shape:(number of non measured pixels,2)
                        hull = spatial.ConvexHull(points)
                        deln = spatial.Delaunay(points[hull.vertices])

                        mask = np.zeros_like(self.data[:,:,k], dtype=bool)
                        points = np.argwhere(condition[:,:,k]).astype(np.int16) #pixels with no measurements: array of shape:(number of non measured pixels,3)

                        s = deln.find_simplex(points) #return array of indices of vertices with -1 if outside the hull
                        s = np.nonzero(s+1)

                        points = points[s] #pixels with no measurementsthat are inside the hull
                        mask[points[:,0],points[:,1]] = True    #true for pixels with no measurements inside the hull
                        condition[:,:,k] = mask

                    mpoints = np.nonzero(condition[:,:,k])  #values to interpolate
                    mpointsc = np.nonzero(condition[:,:,k])  #copy
                    x=len(mpoints[0])


                    while x>0:

                        z = uniform_filter(self.contributions[:,:,k])[mpoints]
                        y = uniform_filter(sle[:,:,k]*self.contributions[:,:,k])[mpoints]

                        sle[:,:,k][mpoints] = y/z  #replace with mean values
                        sle[:,:,k][mpoints] = np.nan_to_num(sle[:,:,k][mpoints])

                        self.contributions[:,:,k][mpoints] = np.nan_to_num(z/z)*self.laplaceparam.fillc

                        if self.laplaceparam.in_convex:
                            mpoints = np.nonzero(np.logical_and(self.contributions[:,:,k]==0,mask))  #values to interpolate
                        else:
                            mpoints = np.nonzero(self.contributions[:,:,k]==0)

                        if len(mpoints[0]) == x:
                            #there is an error
                            return
                        x=len(mpoints[0])
                else:
                    condition[:,:,k] = False

            progressbar.close()
            print ('number of voxel to interpolate: ', np.sum(condition))

            #no solve laplace equation
            dt = np.ones((kmax,))*0.5
            doit = True

            while doit:
                progressbar=ProgressBar('Laplace resolution...')
                progressbar.show()
                max1 = np.zeros(kmax)

                for k in range(kmax):

                    progressbar.update_progress(k/float(kmax))

                    mpoints = np.nonzero(condition[:,:,k])  #values to interpolate
                    if len(mpoints[0])>0:

                        delta = laplace(sle[:,:,k],mode='nearest')
                        #print (mpoints,len(mpoints))
                        #print (delta[mpoints])
                        max0 = np.amax(np.abs(delta[mpoints]))

                        sle[:,:,k][mpoints] += delta[mpoints]*dt[k]

                        for i in range(self.laplaceparam.steps):
                            delta = laplace(sle[:,:,k],mode='nearest')
                            max1[k] = np.amax(np.abs(delta[mpoints]))

                            if max1[k]<max0:
                                dt[k] = dt[k]*self.laplaceparam.increase
                            else:
                                dt[k] = dt[k]*self.laplaceparam.decrease

                            sle[:,:,k][mpoints] += delta[mpoints]*dt[k]

                    if progressbar.stop:
                        break

                self.data=sle*self.contributions
                self.update_images()
                txt='Maximum laplacian value=%f, continue?'%np.amax(max1)
                i=QMessageBox.question(self,"Continue Laplace",txt,QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel )

                if i == QMessageBox.Cancel:      #do not change and stop iterating
                    self.data = sld
                    self.contributions = slc
                    self.update_images()
                    doit = False

                elif i == QMessageBox.No:
                    doit = False   #stop iterating


        else:

            i0 = self.laplaceparam.cutoff

            dt = 0.5

            sld = np.array(self.data)  #copy to save
            slc = np.array(self.contributions)

            condition = slc<i0   #non measured points
            print ('number of non measured voxels: ', np.sum(condition))

            if self.laplaceparam.in_convex :
                #compute convexhull
                points = np.argwhere(~condition).astype(np.int16)   #points inside the hull
                hull = spatial.ConvexHull(points)
                deln = spatial.Delaunay(points[hull.vertices])

                # Instead of allocating a giant array for all indices in the volume,
                # just iterate over the slices one at a time.
                idx_2d = np.indices(self.data.shape[1:], np.int16)
                idx_2d = np.moveaxis(idx_2d, 0, -1)

                idx_3d = np.zeros((*self.data.shape[1:], 3), np.int16)
                idx_3d[:, :, 1:] = idx_2d

                mask = np.zeros_like(self.data, dtype=bool)
                for z in range(self.data.shape[0]):   #slicing other the first dimension
                    idx_3d[:,:,0] = z
                    s = deln.find_simplex(idx_3d)
                    mask[z, (s != -1)] = 1    #mask = 0 outside the hull

                condition = np.logical_and(condition,mask)   #condition=True if non measured points and inside the hull

            mpoints = np.nonzero(condition)  #values to interpolate
            mpointsc = np.nonzero(condition)  #copy
            x=len(mpoints[0])
            print ('number of voxel to interpolate: ', x)

            if x==0:
                QMessageBox.Warning(self,'all contributions are above the cutoff', 'no laplacian performed')
                return

            sle=np.nan_to_num(self.data/slc)   #normalized intensity values, 0 if no contributions

            x0 = x
            progressbar=ProgressBar('filling first with average...')
            progressbar.show()

            while x>0:
                progressbar.update_progress(1.-x/x0)
                z = uniform_filter(self.contributions)[mpoints]
                y = uniform_filter(sle*self.contributions)[mpoints]

                sle[mpoints] = y/z  #replace with mean values
                sle[mpoints] = np.nan_to_num(sle[mpoints])

                self.contributions[mpoints] = np.nan_to_num(z/z)*self.laplaceparam.fillc  #replace with default value fillc

                if self.laplaceparam.in_convex:
                    mpoints = np.nonzero(np.logical_and(self.contributions==0,mask))  #values to interpolate
                else:
                    mpoints = np.nonzero(self.contributions==0)

                x = len(mpoints[0])
                if progressbar.stop:
                    x = 0

            progressbar.close()

            mpoints = mpointsc
            delta = laplace(sle, mode='nearest')
            max0 = np.max(np.abs(delta[mpoints]))

            sle[mpoints] += delta[mpoints] * dt

            while doit:
                progressbar = ProgressBar('Laplace resolution...')
                progressbar.show()
                for i in range(self.laplaceparam.steps):
                    delta = laplace(sle, mode = 'nearest')
                    max1 = np.max(np.abs(delta[mpoints]))

                    if max1<max0:
                        dt = dt*self.laplaceparam.increase
                    else:
                        dt = dt*self.laplaceparam.decrease

                    sle[mpoints] += delta[mpoints]*dt

                    x = i/float(self.laplaceparam.steps)
                    progressbar.update_progress(x)

                    if progressbar.stop:
                        break

                self.data = np.absolute(sle*self.contributions)
                self.update_images()
                txt='Maximum laplacian value=%f, continue?'%max1
                i=QMessageBox.question(self,"Continue Laplace",txt,QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel )

                if i == QMessageBox.Cancel:      #do not change and stop iterating
                    self.data = sld
                    self.contributions = slc
                    self.update_images()
                    doit=False

                elif i == QMessageBox.No:
                    doit = False   #stop iterating

    def filling(self):
        #fill pixels with no contributions with pixels where

        #select one plane
        slc=np.array(self.contributions)
        sle=np.nan_to_num(self.data/slc)

        a=sle.shape
        #a=float(a[0]*a[1]*a[2])
        a=float(a[0]*a[1]*a[2])

        progressbar=ProgressBar('filling...')
        progressbar.show()

        mpoints = np.nonzero(slc==0)  #values to interpolate
        x=len(mpoints[0])

        while x>0:
            progressbar.update_progress(1.-x/a)
            z=uniform_filter(slc)[mpoints]
            y=uniform_filter(sle*slc)[mpoints]

            sle[mpoints]=y/z  #replace with mean values
            sle[mpoints]=np.nan_to_num(sle[mpoints])

            slc[mpoints]=10*z/z  #replace with default value=10
            slc[mpoints]=np.nan_to_num(slc[mpoints])

            mpoints = np.nonzero(slc==0)  #values to interpolate
            x=len(mpoints[0])
            if progressbar.stop:
                x=0

        self.contributions=slc
        self.data=sle*slc
        self.update_images()



    def griddata_interpolation(self):
        """new version: 3D interpolation"""
        x,y,z = np.indices(self.data.shape)

        sle=self.data/self.contributions  #intensite normalisees
        struct = np.ones((3,3,3))
        nocont = self.contributions==0
        edge = ~(binary_erosion(self.contributions, struct, border_value=1)
                 +nocont)

        #interpolate contributions

        print("interpolate contributions")
        self.contributions[nocont]=griddata((x[edge], y[edge],z[edge]),  # points we know
                                        self.contributions[edge],                    # values we know
                                        (x[nocont], y[nocont], z[nocont]),
                                        method='linear',fill_value=0.)  #fill with 0 for points outside of the convex hull of the input points
        #interpolate normalized intensities and multiply by contributions
        print("interpolate counts")
        self.data[nocont]=self.contributions[edge]*griddata((x[edge], y[edge],z[edge]), # points we know
                                        sle[edge],                    # values we know
                                        (x[nocont], y[nocont], z[nocont]),
                                        method='linear',fill_value=0.)
        self.update_images()


    def do_filter(self):
        doit=self.filterparam.edit()
        if not doit:
            return

        mpoints= np.nonzero(self.contributions<self.filterparam.cutoff)  #values to remove

        self.contributions[mpoints]=0
        self.data[mpoints]=0
        self.update_images()

    def set_projected_range(self, plot, x0, x1, y0, y1):
        #if a selection rectangle has been drawn on a plot, set the projected range
        axis = self.imageplots.index(plot)
        self.p_panel.set_projected_range(x0, x1, y0, y1, axis)

    def make_projection(self, intensity = 0, bg=[0,0,0]):
        #axis: axis along which the projection will be made
        #intensity: 0: sum(counts)/sum(contributions), 1: sum(counts), 2: sum(contributions)
        #bg: subtract background for each projection (each axis has its own bg subtraction)
        with np.errstate(invalid='ignore'):
            ranges = self.p_panel.get_ranges()

            xmin=ranges[0,0]
            xmax=ranges[0,1]
            ymin=ranges[1,0]
            ymax=ranges[1,1]
            zmin=ranges[2,0]
            zmax=ranges[2,1]
            ix1=int(np.floor((xmin-self.x_values[1])/self.x_values[3])+1)  #first integer above the cursor
            ix2=int(np.floor((xmax-self.x_values[1])/self.x_values[3])+1)  #first integer above the cursor
            iy1=int(np.floor((ymin-self.y_values[1])/self.y_values[3])+1)
            iy2=int(np.floor((ymax-self.y_values[1])/self.y_values[3])+1)
            iz1=int(np.floor((zmin-self.z_values[1])/self.z_values[3])+1)
            iz2=int(np.floor((zmax-self.z_values[1])/self.z_values[3])+1)

            ix1,ix2,iy1,iy2,iz1,iz2 = get_closest_indices(ix1,ix2,iy1,iy2,iz1,iz2,self.data.shape)  #slice inside the array
            if (ix2-ix1)*(iy2-iy1)*(iz2-iz1) == 0:   #no values in the selected subarray
                return

            if intensity == 2:
                proj_x=np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=0)
                proj_y=np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=1)
                proj_z=np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=2)
                projx=np.sum(self.contributions[:,iy1:iy2,iz1:iz2],axis=(1,2))
                projy=np.sum(self.contributions[ix1:ix2,:,iz1:iz2],axis=(0,2))
                projz=np.sum(self.contributions[ix1:ix2,iy1:iy2,:],axis=(0,1))
                proj = [proj_x, proj_y, proj_z]
                proj2 = [projx, projy, projz]

            elif intensity == 1:
                proj_x=np.sum(self.data[ix1:ix2,iy1:iy2,iz1:iz2],axis=0)
                proj_y=np.sum(self.data[ix1:ix2,iy1:iy2,iz1:iz2],axis=1)
                proj_z=np.sum(self.data[ix1:ix2,iy1:iy2,iz1:iz2],axis=2)
                projx=np.sum(self.data[:,iy1:iy2,iz1:iz2],axis=(1,2))
                projy=np.sum(self.data[ix1:ix2,:,iz1:iz2],axis=(0,2))
                projz=np.sum(self.data[ix1:ix2,iy1:iy2,:],axis=(0,1))
                proj = [proj_x, proj_y, proj_z]
                proj2 = [projx, projy, projz]
            else:
                proj_x=np.sum(self.data[ix1:ix2,iy1:iy2,iz1:iz2],axis=0)/np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=0)
                proj_y=np.sum(self.data[ix1:ix2,iy1:iy2,iz1:iz2],axis=1)/np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=1)
                proj_z=np.sum(self.data[ix1:ix2,iy1:iy2,iz1:iz2],axis=2)/np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=2)
                projx=np.sum(self.data[:,iy1:iy2,iz1:iz2],axis=(1,2))/np.sum(self.contributions[:,iy1:iy2,iz1:iz2],axis=(1,2))
                projy=np.sum(self.data[ix1:ix2,:,iz1:iz2],axis=(0,2))/np.sum(self.contributions[ix1:ix2,:,iz1:iz2],axis=(0,2))
                projz=np.sum(self.data[ix1:ix2,iy1:iy2,:],axis=(0,1))/np.sum(self.contributions[ix1:ix2,iy1:iy2,:],axis=(0,1))

                proj = [proj_x, proj_y, proj_z]
                proj2 = [projx, projy, projz]

                for axis in range(3):
                    if bg[axis]:
                        #we subtract background:
                        if axis==0:
                            vv=self.x_values
                            x3,x4=self.p_panel.bgranges1[0].get_range()
                            x5,x6=self.p_panel.bgranges2[0].get_range()
                        elif axis==1:
                            vv=self.y_values
                            x3,x4=self.p_panel.bgranges1[1].get_range()
                            x5,x6=self.p_panel.bgranges2[1].get_range()
                        else:
                            vv=self.z_values
                            x3,x4=self.p_panel.bgranges1[2].get_range()
                            x5,x6=self.p_panel.bgranges1[2].get_range()
                        ibgs=[]
                        for x in [x3,x4,x5,x6]:
                            ibg=int(np.floor((x-vv[1])/vv[3])+1)
                            if ibg<0:
                                ibg=0
                            if ibg>self.data.shape[axis]:
                                ibg=self.data.shape[axis]
                            ibgs.append(ibg)
                        i3,i4,i5,i6=ibgs
                        if i3>i4:
                            i3,i4=i4,i3
                        if i5>i6:
                            i5,i6=i6,i5

                        if axis==0:
                            bg_value=((np.sum(self.data[i3:i4,iy1:iy2,iz1:iz2],axis=axis)+np.sum(self.data[i5:i6,iy1:iy2,iz1:iz2],axis=axis))
                                    /(np.sum(self.contributions[i3:i4,iy1:iy2,iz1:iz2],axis=axis)+np.sum(self.contributions[i5:i6,iy1:iy2,iz1:iz2],axis=axis)))
                        elif axis==1:
                            bg_value=((np.sum(self.data[ix1:ix2,i3:i4,iz1:iz2],axis=axis)+np.sum(self.data[ix1:ix2,i5:i6,iz1:iz2],axis=axis))
                                    /(np.sum(self.contributions[ix1:ix2,i3:i4,iz1:iz2],axis=axis)+np.sum(self.contributions[ix1:ix2,i5:i6,iz1:iz2],axis=axis)))
                        else:
                            bg_value=((np.sum(self.data[ix1:ix2,iy1:iy2,i3:i4],axis=axis)+np.sum(self.data[ix1:ix2,iy1:iy2,i5:i6],axis=axis))
                                    /(np.sum(self.contributions[ix1:ix2,iy1:iy2,i3:i4],axis=axis)+np.sum(self.contributions[ix1:ix2,iy1:iy2,i5:i6],axis=axis)))
                        bg_value=np.nan_to_num(bg_value)  #replace nan numbers with 0.
                        proj[axis]=proj[axis]-bg_value


            xmin=self.x_values[1]+(ix1-0.5)*self.x_values[3]     #x_values are at the center of the pixels
            xmax=self.x_values[1]+(ix2-0.5)*self.x_values[3]
            ymin=self.y_values[1]+(iy1-0.5)*self.y_values[3]
            ymax=self.y_values[1]+(iy2-0.5)*self.y_values[3]
            zmin=self.z_values[1]+(iz1-0.5)*self.z_values[3]
            zmax=self.z_values[1]+(iz2-0.5)*self.z_values[3]

            self.showndatalimits=[[ix1,ix2],[iy1,iy2],[iz1,iz2]]   #limits shown

            xpts=self.x_values[1]+np.arange(self.data.shape[0])*self.x_values[3]
            ypts=self.y_values[1]+np.arange(self.data.shape[1])*self.y_values[3]
            zpts=self.z_values[1]+np.arange(self.data.shape[2])*self.z_values[3]
            pts = [xpts, ypts, zpts]

            for axis in range(3):
                if axis==0:
                    xdata,ydata=(zmin,zmax),(ymin,ymax)
                    x_label,y_label=self.z_label,self.y_label
                elif axis==1:
                    xdata,ydata=(zmin,zmax),(xmin,xmax)
                    x_label,y_label=self.z_label,self.x_label
                else:
                    xdata,ydata=(ymin,ymax),(xmin,xmax)
                    x_label,y_label=self.y_label,self.x_label



                if self.p_panel.zlog.isChecked():
                #    v=np.absolute(np.nanmin(proj[np.array(proj,bool)]))  #minimum of the nonzero values of the array
                     u = np.nanmin(proj[axis])
                     if u<=0:
                         proj[axis] = proj[axis] - u
                         v = np.nanmin(proj[axis][proj[axis]>0]) #minimum value above 0
                         proj[axis]=np.log(proj[axis]+v)  #should avoid inf values
                     else:
                         proj[axis]=np.log(proj[axis])

                lutrange=self.images[axis].get_lut_range()
                if self.p_panel.swap.isChecked():
                    self.imageplots[axis].set_axis_title('bottom',y_label)
                    self.imageplots[axis].set_axis_title('left',x_label)
                    self.images[axis].set_data(np.transpose(proj[axis]))
                    self.images[axis].set_xdata(ydata[0],ydata[1])
                    self.images[axis].set_ydata(xdata[0],xdata[1])
                else:
                    self.imageplots[axis].set_axis_title('bottom',x_label)
                    self.imageplots[axis].set_axis_title('left',y_label)
                    self.images[axis].set_data(proj[axis])
                    self.images[axis].set_xdata(xdata[0],xdata[1])
                    self.images[axis].set_ydata(ydata[0],ydata[1])

                self.images[axis].imageparam.update_param(self.images[axis])
                self.images[axis].imageparam.update_image(self.images[axis])

                if self.p_panel.autoscale.isChecked():
                    self.imageplots[axis].do_autoscale()
                else:
                    self.images[axis].set_lut_range(lutrange)
                    self.imageplots[axis].replot()
                #send signal connected to histogram to reflect changes
                self.imageplots[axis].SIG_ITEM_SELECTION_CHANGED.emit(self.imageplots[axis])

                v_abs = pts[axis]
                v_ord = proj2[axis]

                self.p_panel.curves[axis].set_data(v_abs[np.isfinite(v_ord)],v_ord[np.isfinite(v_ord)])
                if self.p_panel.autoscale.isChecked():
                    self.p_panel.plots[axis].do_autoscale()
                else:
                    self.p_panel.plots[axis].replot()

            self.plot_widget.current_plot_changed(self.plot_widget.tab.currentIndex()) #set masked_image to current displayed tab widget


    def update_sliceprefs(self, absranges=None, ranges=None, steps=None, labels=None ):
        if absranges is not None:
            #fixe les bornes des spinbox
            self.sliceprefs.absmins=absranges[0]
            self.sliceprefs.absmaxs=absranges[1]
        if ranges is not None:
            #determine les valeurs des spinbox
            self.sliceprefs.mins=ranges[0]
            self.sliceprefs.maxs=ranges[1]
        if steps is not None:
            self.sliceprefs.steps=steps
        if labels is not None:
            self.sliceprefs.labels=labels
        print ('mins, maxs, steps')
        print(self.sliceprefs.mins,self.sliceprefs.maxs,self.sliceprefs.steps)

    def get_2D_slices_prefs(self, nodes=False):
        ranges = self.p_panel.get_ranges()
        xmin=ranges[0,0]
        xmax=ranges[0,1]
        ymin=ranges[1,0]
        ymax=ranges[1,1]
        zmin=ranges[2,0]
        zmax=ranges[2,1]
        mins=[round(xmin,12),round(ymin,12),round(zmin,12)]
        maxs=[round(xmax,12),round(ymax,12),round(zmax,12)]

        self.update_sliceprefs(ranges=[mins,maxs])


        self.sliceprefs.i0 = self.get_projection()

        if nodes:
            Set2DSlicesWindow(self.sliceprefs)  #ouvre une fenetre de dialogue
        else:
            Set2DSliceWindow(self.sliceprefs)  #ouvre une fenetre de dialogue

    def make_2D_slices(self):
        self.get_2D_slices_prefs(nodes=False)
        prefs = self.sliceprefs

        if not prefs.do_it:
            return

        i0=prefs.i0   #slice direction
        tag1=self.labels[i0]  #axis name

        ix1=int(round((prefs.mins[0]-self.x_values[1])/self.x_values[3]))  #limit included
        ix2=int(round((prefs.maxs[0]-self.x_values[1])/self.x_values[3]))+1  #limit included
        iy1=int(round((prefs.mins[1]-self.y_values[1])/self.y_values[3]))
        iy2=int(round((prefs.maxs[1]-self.y_values[1])/self.y_values[3]))+1
        iz1=int(round((prefs.mins[2]-self.z_values[1])/self.z_values[3]))
        iz2=int(round((prefs.maxs[2]-self.z_values[1])/self.z_values[3]))+1

        ix1,ix2,iy1,iy2,iz1,iz2 = get_closest_indices(ix1,ix2,iy1,iy2,iz1,iz2, self.data.shape)
        if (ix2-ix1)*(iy2-iy1)*(iz2-iz1) == 0:   #no values in the selected subarray
            print("no values in slices")
            return

        mins=[self.x_values[1],self.y_values[1],self.z_values[1]]
        maxs=[self.x_values[2],self.y_values[2],self.z_values[2]]
        steps=[self.x_values[3],self.y_values[3],self.z_values[3]]

        print("mins,maxs (pixel centers)")
        print(mins)
        print(maxs)
        print("-------------")
        print("slices bounds indices:")
        print(ix1,ix2,iy1,iy2,iz1,iz2)

        i_deb = [ix1,iy1,iz1]
        i_fin = [ix2,iy2,iz2]

        if i0 == 0:
            #indices for x and y values of each slice
            i1 = 1
            i2 = 2
        elif i0 == 1:
            i1 = 0
            i2 = 2
        else:
            i1 = 0
            i2 = 1

        x_range = [mins[i1]+steps[i1]*i_deb[i1],mins[i1]+steps[i1]*i_fin[i1]]
        y_range = [mins[i2]+steps[i2]*i_deb[i2],mins[i2]+steps[i2]*i_fin[i2]]

        #slice limits (included)
        vmin=prefs.mins[i0]
        vmax=prefs.maxs[i0]

        jmin=int(round((vmin-mins[i0])/steps[i0]))  #borne [
        jmax=int(round((vmax-mins[i0])/steps[i0])) #borne ]

        x_label, y_label = self.labels[i1], self.labels[i2]

        tag2 = x_label
        tag3 = y_label

        if self.preferences.new_slice_win:
            #we make the slices in a new window
            window = Fit2DWindow()
            window.set_cwd(self.openfiletool.directory)
            window.show()
        else:
            self.show_fit2Dwindow()
            window = self.fit2Dwindow

        title0 = self.windowTitle()
        title0 = title0.replace(' ','_')

        ask=True

        #we make slices by binning the data using self.bins
        #
        bins = self.sliceprefs.bins
        for j1 in range(jmin, jmax+1, bins):

            j2 = j1 + bins

            if j2 > self.data.shape[i0]:
                j2 = self.data.shape[i0]

            print("slice:", j1, j2)

            if i0==0:
                proj=np.sum(self.data[j1:j2,iy1:iy2,iz1:iz2],axis=0)  #we do not multiply by stepw in order to have the density along the rod, as usual
                projcont=np.sum(self.contributions[j1:j2,iy1:iy2,iz1:iz2],axis=0)
            elif i0==1:
                proj=np.sum(self.data[ix1:ix2,j1:j2,iz1:iz2],axis=1)  #we do not multiply by stepw in order to have the density along the rod, as usual
                projcont=np.sum(self.contributions[ix1:ix2,j1:j2,iz1:iz2],axis=1)
            else:
                proj=np.sum(self.data[ix1:ix2,iy1:iy2,j1:j2],axis=2)  #we do not multiply by stepw in order to have the density along the rod, as usual
                projcont=np.sum(self.contributions[ix1:ix2,iy1:iy2,j1:j2],axis=2)

            if np.amax(projcont) > 0:   #otherwise slice is empty

                data = proj/projcont
                #sigmas = np.sqrt(proj+1)/projcont    = statistical error on the counts detected
                weights = projcont/np.sqrt(proj+1)

                Q=mins[i0]+float(j1+j2-1)*steps[i0]/2.   #first point is mins[2]+j1*steps[2], last is mins[2]+(j2-1)*steps[2]
                title = title0+'_'+tag1+'=%f'%(Q)
                tags={}
                tags[tag1] = Q
                tags[tag2] = (x_range[0]+x_range[-1])/2.
                tags[tag3] = (y_range[0]+y_range[-1])/2.

                if self.p_panel.swap.isChecked():   #xlabel is for image columns (data is transposed)
                    ok = window.add_data(np.transpose(data),x_range,y_range,np.transpose(weights),title=title,x_label=x_label,y_label=y_label,tags=tags)
                else:    #xlabel is for image rows = ordinate, not abscissa
                    ok = window.add_data(data,y_range,x_range,weights,title=title,x_label=y_label,y_label=x_label,tags=tags)

                j1=j2

                #if there is a systematic error, better to stop!
                if ok==False and ask:
                    answer = QMessageBox.warning(self, _("Error while making slices"),
                                 "continue?", QMessageBox.Yes | QMessageBox.No)
                    if answer == QMessageBox.No:
                        return
                    else:
                        ask=False

    def make_2D_slices_around_nodes(self):
        self.get_2D_slices_prefs(nodes=True)
        prefs = self.sliceprefs

        if not prefs.do_it:
            return

        #slice direction
        i0 = prefs.i0
        #print('prefs.mins',prefs.mins)
        #print('prefs.maxs',prefs.maxs)

        if prefs.visible:
            #we find the node in the rectangle given by plot axes limits
            x1, x2 = self.imageplots[i0].get_axis_limits("bottom")
            y1, y2 = self.imageplots[i0].get_axis_limits("left")
        else:
            bound = self.images[i0].boundingRect()
            x1, x2 = bound.left(), bound.right()
            y1, y2 = bound.top(), bound.bottom()

        nodes = self.imageplots[i0].get_visible_nodes(x1, x2, y1, y2)
        print ("find nodes in the rectangle",x1,x2,y1,y2)

        if i0 == 2:
            kx, ky = 1, 0           #kx is the direction of the x_axis of the ImagePlot, ky, of the y_axis of the ImagePlot
        elif i0 ==1:
            kx, ky = 2, 0
        else:
            kx, ky = 2, 1
        if self.p_panel.swap.isChecked():
            kx, ky = ky, kx    #slice limits


        if self.preferences.new_slice_win:
            #we make the slices in a new window
            window = Fit2DWindow()
            window.set_cwd(self.openfiletool.directory)
            window.show()
        else:
            self.show_fit2Dwindow()
            window = self.fit2Dwindow

        #slice limits (included)
        vmin=prefs.mins[i0]
        vmax=prefs.maxs[i0]

        mins=[self.x_values[1],self.y_values[1],self.z_values[1]]
        maxs=[self.x_values[2],self.y_values[2],self.z_values[2]]
        steps=[self.x_values[3],self.y_values[3],self.z_values[3]]

        jmin=int(round((vmin-mins[i0])/steps[i0]))  #borne [
        jmax=int(round((vmax-mins[i0])/steps[i0])) #borne ]
        bins = self.sliceprefs.bins

        for node in nodes:   #[(lattice,idom,h,k,x,y)]
            lattice = node[0]
            idom = node[1]
            h = node[2]
            k = node[3]
            qxnode = node[4]
            qynode = node[5]
            label = lattice.shapeparam.label.replace(' ','_')
            label = lattice.shapeparam.label+'_dom_%d_h=%d_k=%d'%(idom,h,k)

            if i0 == 2:
                if self.p_panel.swap.isChecked():
                    xc = node[4]
                    yc = node[5]
                else:
                    yc = node[4]
                    xc = node[5]
            elif i0 ==1:
                if self.p_panel.swap.isChecked():
                    xc = node[4]
                    zc = node[5]
                else:
                    zc = node[4]
                    xc = node[5]
            else:
                kx, ky = 2, 1
                if self.p_panel.swap.isChecked():
                    yc = node[4]
                    zc = node[5]
                else:
                    zc = node[4]
                    yc = node[5]

            tag1=self.labels[i0]  #axis name

            if i0 == 0:   #projection along x
                ix1=int(round((prefs.mins[0]-self.x_values[1])/self.x_values[3]))  #limit included
                ix2=int(round((prefs.maxs[0]-self.x_values[1])/self.x_values[3]))+1  #limit included
                iy1=int(round((yc+prefs.mins[1]-self.y_values[1])/self.y_values[3]))
                iy2=int(round((yc+prefs.maxs[1]-self.y_values[1])/self.y_values[3]))+1
                iz1=int(round((zc+prefs.mins[2]-self.z_values[1])/self.z_values[3]))
                iz2=int(round((zc+prefs.maxs[2]-self.z_values[1])/self.z_values[3]))+1


            elif i0 == 1:    #projection along y
                ix1=int(round((xc+prefs.mins[0]-self.x_values[1])/self.x_values[3]))  #limit included
                ix2=int(round((xc+prefs.maxs[0]-self.x_values[1])/self.x_values[3]))+1  #limit included
                iy1=int(round((prefs.mins[1]-self.y_values[1])/self.y_values[3]))
                iy2=int(round((prefs.maxs[1]-self.y_values[1])/self.y_values[3]))+1
                iz1=int(round((zc+prefs.mins[2]-self.z_values[1])/self.z_values[3]))
                iz2=int(round((zc+prefs.maxs[2]-self.z_values[1])/self.z_values[3]))+1

            else:    #projection along z
                ix1=int(round((xc+prefs.mins[0]-self.x_values[1])/self.x_values[3]))  #limit included
                ix2=int(round((xc+prefs.maxs[0]-self.x_values[1])/self.x_values[3]))+1  #limit included
                iy1=int(round((yc+prefs.mins[1]-self.y_values[1])/self.y_values[3]))
                iy2=int(round((yc+prefs.maxs[1]-self.y_values[1])/self.y_values[3]))+1
                iz1=int(round((prefs.mins[2]-self.z_values[1])/self.z_values[3]))
                iz2=int(round((prefs.maxs[2]-self.z_values[1])/self.z_values[3]))+1


            ix1,ix2,iy1,iy2,iz1,iz2 = get_closest_indices(ix1,ix2,iy1,iy2,iz1,iz2,self.data.shape)
            #
            #print (ix1,ix2,iy1,iy2,iz1,iz2)

            if (ix2-ix1)*(iy2-iy1)*(iz2-iz1) != 0:   #otherwise no values in the selected subarray
                print ('node,',lattice.shapeparam.label,h,k,node[4],node[5])
                mins=[self.x_values[1],self.y_values[1],self.z_values[1]]
                maxs=[self.x_values[2],self.y_values[2],self.z_values[2]]
                steps=[self.x_values[3],self.y_values[3],self.z_values[3]]

                #print("mins,maxs")
                #print(mins)
                #print(maxs)
                #print("-------------")

                if i0==0:
                    x_range=[mins[1]+steps[1]*iy1,mins[1]+steps[1]*iy2]   #y
                    y_range=[mins[2]+steps[2]*iz1,mins[2]+steps[2]*iz2]   #z
                    vmin=prefs.mins[0]
                    v1=vmin
                    j1=int(round((v1-mins[0])/steps[0]))  #borne [
                    x_label,y_label=self.y_label,self.z_label

                elif i0==1:
                    x_range=[mins[0]+steps[0]*ix1,mins[0]+steps[0]*ix2]   #x
                    y_range=[mins[2]+steps[2]*iz1,mins[2]+steps[2]*iz2]   #z
                    vmin=prefs.mins[1]
                    v1=vmin
                    j1=int(round((v1-mins[1])/steps[1]))  #borne [
                    x_label,y_label=self.x_label,self.z_label

                else:
                    x_range=[mins[0]+steps[0]*ix1,mins[0]+steps[0]*ix2]   #x
                    y_range=[mins[1]+steps[1]*iy1,mins[1]+steps[1]*iy2]   #y
                    vmin=prefs.mins[2]
                    v1=vmin
                    j1=int(round((v1-mins[2])/steps[2]))  #borne [
                    x_label,y_label=self.x_label,self.y_label

                tag2 = x_label
                tag3 = y_label

                qtheta = np.arctan2(qynode,qxnode)
                qpar = np.sqrt(qxnode**2+qynode**2)

                #we check if there are other nodes in the rectangle of selection defined by x_range and y_range
                if self.p_panel.swap.isChecked():
                    xx1, xx2 = x_range
                    yy1, yy2 = y_range
                else:
                    xx1, xx2 = y_range
                    yy1, yy2 = x_range

                o_nodes = self.imageplots[i0].get_visible_nodes(xx1, xx2, yy1, yy2)

                o_tags = {}
                o_i = 1
                for o_node in o_nodes:   #[(lattice,idom,h,k,x,y)]
                    o_lattice = o_node[0]
                    o_idom = o_node[1]
                    o_h = o_node[2]
                    o_k = o_node[3]
                    o_qxnode = o_node[4]
                    o_qynode = o_node[5]
                    if o_lattice != lattice or o_idom != idom or o_h != h or o_k != k:
                        #there is another node in the slice window, we add it to the future list of tags
                        o_tags[x_label+'_%d'%o_i] = o_qxnode
                        o_tags[y_label+'_%d'%o_i] = o_qynode
                        o_tags['qpar'+'_%d'%o_i] = np.sqrt(o_qxnode**2+o_qynode**2)
                        o_tags['qtheta'+'_%d'%o_i] = np.arctan2(o_qynode,o_qxnode)



                title0 = self.windowTitle()
                title0 = title0.replace(' ','_')

                ask=True

                for j1 in range(jmin, jmax+1, bins):

                    j2 = j1 + bins

                    if j2 > self.data.shape[i0]:
                        j2 = self.data.shape[i0]

                    if i0==0:
                        proj=np.sum(self.data[j1:j2,iy1:iy2,iz1:iz2],axis=0)  #we do not multiply by stepw in order to have the density along the rod, as usual
                        projcont=np.sum(self.contributions[j1:j2,iy1:iy2,iz1:iz2],axis=0)
                    elif i0==1:
                        proj=np.sum(self.data[ix1:ix2,j1:j2,iz1:iz2],axis=1)  #we do not multiply by stepw in order to have the density along the rod, as usual
                        projcont=np.sum(self.contributions[ix1:ix2,j1:j2,iz1:iz2],axis=1)
                    else:
                        proj=np.sum(self.data[ix1:ix2,iy1:iy2,j1:j2],axis=2)  #we do not multiply by stepw in order to have the density along the rod, as usual
                        projcont=np.sum(self.contributions[ix1:ix2,iy1:iy2,j1:j2],axis=2)

                    if np.amax(projcont)>0:
                        data = proj/projcont
                        #sigmas = np.sqrt(proj+1)/projcont    = statistical error on the counts detected
                        weights = projcont/np.sqrt(proj+1)

                        Q=mins[2]+float(j1+j2-1)*steps[2]/2.
                        title = title0+'_'+label+'_'+tag1+'=%f'%(Q)
                        tags={}
                        tags[tag1] = Q
                        tags[tag2] = qxnode
                        tags[tag3] = qynode
                        tags['qpar'] = qpar
                        tags['qtheta'] = qtheta

                        tags.update(o_tags)  #add other tags for other nodes




                        if self.p_panel.swap.isChecked():   #x along
                            ok = window.add_data(np.transpose(data),x_range,y_range,np.transpose(weights),title=title,x_label=x_label,y_label=y_label,tags=tags)
                        else:
                            ok = window.add_data(data,y_range,x_range,weights,title=title,x_label=x_label,y_label=y_label,tags=tags)

                        j1=j2

                        #if there is a systematic error, better to stop!
                        if ok==False and ask:
                            answer = QMessageBox.warning(self, _("Error while making slices"),
                                         "continue?", QMessageBox.Yes | QMessageBox.No)
                            if answer == QMessageBox.No:
                                return
                            else:
                                ask=False

    def make_straight_scans(self):
        ranges = self.p_panel.get_ranges()
        xmin=ranges[0,0]
        xmax=ranges[0,1]
        ymin=ranges[1,0]
        ymax=ranges[1,1]
        zmin=ranges[2,0]
        zmax=ranges[2,1]

        mins=[round(xmin,12),round(ymin,12),round(zmin,12)]
        maxs=[round(xmax,12),round(ymax,12),round(zmax,12)]

        self.update_sliceprefs(ranges=[mins,maxs])
        prefs=self.sliceprefs
        SetStraightScansWindow(self.sliceprefs)  #ouvre une fenetre de dialogue
        if not prefs.do_it:
          return

        #prefs.i0 = #rod integration
        #prefs.i1 = #scan direction
        #prefs.i2 = #raw sum direction

        i0,i1,i2=prefs.i0,prefs.i1,prefs.i2

        ix1=int(round((prefs.mins[0]-self.x_values[1])/self.x_values[3]))  #limit included
        ix2=int(round((prefs.maxs[0]-self.x_values[1])/self.x_values[3]))+1  #limit included
        iy1=int(round((prefs.mins[1]-self.y_values[1])/self.y_values[3]))
        iy2=int(round((prefs.maxs[1]-self.y_values[1])/self.y_values[3]))+1
        iz1=int(round((prefs.mins[2]-self.z_values[1])/self.z_values[3]))
        iz2=int(round((prefs.maxs[2]-self.z_values[1])/self.z_values[3]))+1

        ix1,ix2,iy1,iy2,iz1,iz2 = get_closest_indices(ix1,ix2,iy1,iy2,iz1,iz2,self.data.shape)
        if (ix2-ix1)*(iy2-iy1)*(iz2-iz1) == 0:   #no values in the selected subarray
            return

        imins=[ix1,iy1,iz1]
        imaxs=[ix2,iy2,iz2]
        mins=[self.x_values[1],self.y_values[1],self.z_values[1]]
        maxs=[self.x_values[2],self.y_values[2],self.z_values[2]]
        steps=[self.x_values[3],self.y_values[3],self.z_values[3]]

        print("mins,maxs")
        print(mins)
        print(maxs)
        print("-------------")


        ni2=imaxs[i2]-imins[i2]  #number of points along the raw sum direction
        #We do raw integration along direction i2, keeping a 3D array
        proj2=np.sum(self.data[ix1:ix2,iy1:iy2,iz1:iz2],axis=i2)
        #we have to take into account the number of time the pixels have been counted
        cont2=np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=i2)
        axes=[0,1,2]
        axes.remove(i2)
        #np.sum(self.contributions[ix1:ix2,iy1:iy2,iz1:iz2],axis=(prefs.i3),keepdims=True)
        #print ('ix1,ix2:',ix1,ix2)
        #print ('iy1,iy2:',iy1,iy2)
        #print ('iz1,iz2:',iz1,iz2)

        #print ("proj3",proj3.shape)
        ii0=axes.index(i0)  #index of the axis for 2D array to make sum
        #better would be to transpose at the beginning...
        if ii0==1:
            proj2=proj2.transpose()
            cont2=cont2.transpose()

        #we make slices along i1, now first direction of the 2D array
        vmin=prefs.mins[i0]
        vmax=prefs.maxs[i0]
        v1=vmin
        j1=int(round((v1-mins[i0])/steps[i0]))  #borne [

        #scan abscisse
        wstep=steps[i1]
        wmin=mins[i1]+imins[i1]*wstep
        wmax=mins[i1]+imaxs[i1]*wstep
        pts=np.arange(wmin,wmax-wstep/2.,wstep)  #we put wmax-wstep to be sure not to have wmax included


        print('scan direction: ',i2)
        print('scan pts',wmin,wmax,pts.shape)   #points along the scan direction i2

        tag1=self.labels[i0]
        tag2=self.labels[i2]+'_min'
        tag3=self.labels[i2]+'_max'

        if self.preferences.new_slice_win:
            #we make the slices in a new window
            window = Fit1DWindow()
            window.set_cwd(self.openfiletool.directory)
            window.show()
        else:
            self.show_fit1Dwindow()
            window = self.fit1Dwindow

        for i in range(prefs.stepn+1):
            v2=vmin+(i+1)*prefs.stepw
            j2=int(round((v2-mins[i0])/steps[i0]))  #borne [
            proj1=np.sum(proj2[j1:j2], axis=0)  #we do not multiply by stepw in order to have the density along the rod, as usual
            #print ('proj1',proj1.shape)
            cont1=np.sum(cont2[j1:j2], axis=0)
            ydata=np.ma.masked_where(cont1==0, proj1)
            ydata=ydata*(prefs.steps[i2]*ni2)/cont1        #as counts are divided by contributions, we obtained a density, so we need
                                                            #to multiply by the window size (prefs.steps[i2]*ni2) to obtain integrated values

            #sigmas = np.sqrt(proj1+1)*(prefs.steps[i3]*ni3)/cont1   statistical error on the counts
            weights = cont1/(np.sqrt(proj1+1)*prefs.steps[i2]*ni2)
            weights = np.ma.masked_where(cont1==0, weights)

            xdata=np.ma.masked_where(cont1==0,pts)

            Q=mins[i0]+float(j1+j2-1)*steps[i0]/2.   #position along the rod
            title=self.labels[i0]+'=%f'%(Q)

            x_label=self.labels[i1]  #scan label

            tags={tag1:Q,tag2:prefs.mins[i2],tag3:prefs.maxs[i2]}

            window.add_data(xdata.compressed(),ydata.compressed(),weights.compressed(),title=title,x_label=x_label,y_label='intensity',tags=tags)

            j1=j2
            v1=v2


    def make_tilted_scans(self):
        ranges = self.p_panel.get_ranges()
        xmin=ranges[0,0]
        xmax=ranges[0,1]
        ymin=ranges[1,0]
        ymax=ranges[1,1]
        zmin=ranges[2,0]
        zmax=ranges[2,1]
        mins=[round(xmin,12),round(ymin,12),round(zmin,12)]
        maxs=[round(xmax,12),round(ymax,12),round(zmax,12)]

        self.update_sliceprefs(ranges=[mins,maxs])
        prefs=self.sliceprefs
        SetTiltedScansWindow(prefs)  #ouvre une fenetre de dialogue
        if not prefs.do_it:
          return

        print(prefs.i0,prefs.i1,prefs.i2)    #indices of directions
        print(prefs.widths)                  #range for raw_sum/range for scan/width og a slice
        print(prefs.stepn)                   #number of slices
        print(prefs.mins[prefs.i0],prefs.maxs[prefs.i0])   #limit of slices
        print(prefs.pos1)                    #1 position in the rod
        print(prefs.pos2)                    #2 position in the rod


        #print (prefs.mins)
        #print (prefs.maxs)
        #print (prefs.stepn,prefs.stepw)
        #print (prefs.i1,prefs.i2,prefs.i3)
        ix,iy,iz = prefs.i2,prefs.i1,prefs.i0    #i0 slice direction,i1 scan direction, i2 raw sum direction
        wx = prefs.widths[2]
        wy = prefs.widths[0]
        if ix==iy or ix==iz or iy==iz:
            print("two indices equal!!!")
            return

        pos1=np.array(prefs.pos1)
        pos2=np.array(prefs.pos2)
        if pos1[iz]==pos2[iz]:
            print("same starting and ending position%f"%pos1[iz])
            return
        #we make a slice
        #mins=[self.x_values[1],self.y_values[1],self.z_values[1]]
        #maxs=[self.x_values[2],self.y_values[2],self.z_values[2]]
        #steps=[self.x_values[3],self.y_values[3],self.z_values[3]]

        #we swap the array to have i0,i1,i2 coprresponding to axes 2,1,0

        data=self.data
        cont=self.contributions
        if iz!=2:  #rod direction
            data=np.swapaxes(data,iz,2)    # a view of data is returned
            cont=np.swapaxes(cont,iz,2)
        if iy!=1:  #scan direction
            data=np.swapaxes(data,iy,1)
            cont=np.swapaxes(cont,iy,1)
        #that should be OK now! we have rod=last axes,raw_sum=first axe

        #number of integration points:
        kx=int(wx/prefs.steps[ix])
        #number of scan values
        ky=int(wy/prefs.steps[iy])
        print('number of points along raw_sum and scan directions',kx,ky)

        zmin=prefs.mins[iz]
        zmax=prefs.maxs[iz]
        z1=zmin
        k1=int(round((z1-prefs.absmins[iz])/prefs.steps[iz]))    #borne inf [

        if self.preferences.new_slice_win:
            #we make the slices in a new window
            window = Fit1DWindow()
            window.set_cwd(self.openfiletool.directory)
            window.show()
        else:
            self.show_fit1Dwindow()
            window = self.fit1Dwindow

        for istep in range(prefs.stepn+1):
            z2=zmin+(istep+1)*(zmax-zmin)/prefs.stepn
            k2=int(round((z2-prefs.absmins[iz])/prefs.steps[iz]))          #borne sup [

            scan_values=np.zeros(ky)   #a scan
            scan_conts=np.zeros(ky)    #number of contributing original pixels

            print("slice between",k1,k2)

            fx = (pos2[ix]-pos1[ix])/(pos2[iz]-pos1[iz])  #x shift factor
            fy = (pos2[iy]-pos1[iy])/(pos2[iz]-pos1[iz])
            for k in range(k1,k2):
                #center of the rod
                data_sli=data[:,:,k]
                cont_sli=cont[:,:,k]
                z=prefs.absmins[iz]+k*prefs.steps[iz]
                #center of the rod
                x=pos1[ix]+fx*(z-pos1[iz])
                y=pos1[iy]+fy*(z-pos1[iz])
                x1=x-wx/2
                x2=x+wx/2
                y1=y-wy/2
                y2=y+wy/2

                ix1=int(round((x1-prefs.absmins[ix])/prefs.steps[ix]))
                ix2=int(round((x2-prefs.absmins[ix])/prefs.steps[ix]))
                iy1=int(round((y1-prefs.absmins[iy])/prefs.steps[iy]))
                iy2=int(round((y2-prefs.absmins[iy])/prefs.steps[iy]))
                print('k=',k,'z=',z,'ix1,ix2,iy1,iy2=',ix1,ix2,iy1,iy2)

                if k==k1:
                    ix1deb=ix1
                    ix2deb=ix2
                    iy1deb=iy1
                    iy2deb=iy2
                    zdeb=z
                if k==k2-1:
                    ix1fin=ix1
                    ix2fin=ix2
                    iy1fin=iy1
                    iy2fin=iy2
                    zfin=z


                if ix1<0:
                    ix1=0  #the slice will be smaller
                if ix2>data_sli.shape[0]:
                    ix2=data_sli.shape[0]  #the slice will be smaller



                if iy1>=0 and iy2<=data_sli.shape[1]:
                    scan_values=scan_values+np.sum(data_sli[ix1:ix2,iy1:iy2],axis=0)*(ix2-ix1)*wx
                    scan_conts=scan_conts+np.sum(cont_sli[ix1:ix2,iy1:iy2],axis=0)
                else:
                    #we have to ajust the position of the slice
                    if iy1<0:
                        idec1=-iy1
                        iy1=0
                    else:
                        idec1=0

                    if iy2>data_sli.shape[1]:
                        idec2=ky-(iy2-data_sli.shape[1])
                        iy2=data_sli.shape[1]
                    else:
                        idec2=ky

                    scan_values[idec1:idec2]=scan_values[idec1:idec2]+np.sum(data_sli[ix1:ix2,iy1:iy2],axis=0)*(ix2-ix1)*wx
                    scan_conts[idec1:idec2]=scan_conts[idec1:idec2]+np.sum(cont_sli[ix1:ix2,iy1:iy2],axis=0)


            #xvalues
            x1deb=prefs.absmins[ix]+ix1deb*prefs.steps[ix]
            x1fin=prefs.absmins[ix]+ix1fin*prefs.steps[ix]
            x2deb=prefs.absmins[ix]+(ix2deb-1)*prefs.steps[ix]
            x2fin=prefs.absmins[ix]+(ix2fin-1)*prefs.steps[ix]
            x1=(x1deb+x1fin)/2.
            x2=(x2deb+x2fin)/2.
            xcen=(x1+x2)/2.

            #yvalues
            y1deb=prefs.absmins[iy]+iy1deb*prefs.steps[iy]
            y1fin=prefs.absmins[iy]+iy1fin*prefs.steps[iy]
            y2deb=prefs.absmins[iy]+(iy2deb-1)*prefs.steps[iy]
            y2fin=prefs.absmins[iy]+(iy2fin-1)*prefs.steps[iy]
            y1=(y1deb+y1fin)/2.
            y2=(y2deb+y2fin)/2.
            ycen=(y1+y2)/2.

            zcen=(zdeb+zfin)/2.

            abscissae=np.arange(ky)*prefs.steps[iy]+y1
            title='scan_'+self.labels[iz]+'=%f'%zcen

            tag1=self.labels[ix]
            tag2=self.labels[iy]
            tag3=self.labels[iz]
            tag4=self.labels[ix]+'_range'

            ydata=np.ma.masked_where(scan_conts==0,scan_values)
            ydata=ydata/scan_conts
            xdata=np.ma.masked_where(scan_conts==0,abscissae)

            #to be done: determine weights...
            x_label=self.labels[iy]  #label for the scan direction

            tags={tag1:xcen,
                       tag2:ycen,
                       tag3:zcen,
                       tag4:wx}

            window.add_data(xdata.compressed(),ydata.compressed(),title=title,x_label=x_label,y_label='intensity',tags=tags)

            k1=k2

    def draw_roi(self, plot, p0, p1):
        if self.roi_shape is None:
            self.roi_shape = EllipseRoi(0, 0, 1, 1)
            self.roi_shape.set_style("plot", "shape/drag")
            self.roi_shape.shapeparam.label='ROI'
            self.roi_shape.shapeparam.update_shape(self.roi_shape)
            plot.add_item(self.roi_shape)
        else:
            oldplot = self.roi_shape.plot()
            if oldplot is None:
                plot.add_item(self.roi_shape)
            elif oldplot != plot:
                oldplot.del_item(self.roi_shape)
                plot.add_item(self.roi_shape)
        self.roi_shape.setVisible(True)
        self.roi_shape.set_from_drawing_points(plot,p0, p1)
        plot.replot()

    def move_roi(self, plot, p0):
        #move the center of the shape to p0
        self.roi_shape.set_local_center(p0)
        self.roi_shape.setVisible(True)
        plot.replot()

    def integrate_single_roi(self):
        axis = self.get_projection()
        title = self.integrate_roi()
        if self.preferences.keep_trace:
            shape = TrueEllipseShape(0, 0, 1, 1)
            shape.set_points(self.roi_shape.points)
            shape.shapeparam.fill.color ="white"
            shape.shapeparam.fill.alpha = 0.25
            shape.shapeparam.label = title
            shape.shapeparam.update_shape(shape)

            self.imageplots[axis].add_item(shape)


    def integrate_roi(self):
        axis = self.get_projection()
        if self.savefilename is None:
            self.get_save_filename()
        fic = open(self.savefilename,'a')

        if self.roi_shape is None or self.roi_shape.plot() != self.imageplots[axis]:
            QMessageBox.warning(self,'Warning','First draw a ROI')
            return

        #first we extract the data, given the current projection

        x1, y1, x2, y2 = self.roi_shape.get_bounding_rectangle()

        x0 = (x1+x2)/2.
        y0 = (y1+y2)/2.

        dxs2 = np.abs((x2-x1)/2.)
        dys2 = np.abs((y2-y1)/2.)

        kz = self.get_projection()   #kz is the direction along which Intensity=f(u) will be plotted
        if kz == 2:
            kx, ky = 1, 0           #kx is the direction of the x_axis of the ImagePlot, ky, of the y_axis of the ImagePlot
        elif kz ==1:
            kx, ky = 2, 0
        else:
            kx, ky = 2, 1
        if self.p_panel.swap.isChecked():
            kx, ky = ky, kx    #slice limits

        lattice_opt, idom_opt = guess_lattice(self.imageplots[axis], x0, y0)  #check among visible lattices/reconstruction if one can find the peak coordinate
        if lattice_opt is None:
            title = self.windowTitle()
            title = title.replace(' ', '_')
            title += '_at(%s=%g,_%s=%g)'%(self.labels[kx],x0,self.labels[ky],y0)
            h_real, k_real = 0., 0.
            h, k = 0, 0
            qxnode = x0
            qynode = y0
        else:
            h_real, k_real = lattice_opt.get_node_coordinates(x0, y0, idom_opt)
            h, k = np.round(h_real), np.round(k_real)
            title = lattice_opt.shapeparam.label
            title = title.replace(' ', '_')
            title += '_dom_%d_(%d,%d)'%(idom_opt, h, k)
            qxnode, qynode = lattice_opt.get_plot_coordinates(h, k, idom_opt)

        self.show_roiwindow()

        qtheta = np.arctan2(qynode,qxnode)
        qpar = np.sqrt(qxnode**2+qynode**2)


        ix1,ix2,iy1,iy2 = get_indices_from_coordinates(x1,x2,y1,y2,kx,ky,self.values,self.data.shape)

        a,b,th=self.roi_shape.get_sizes_and_angle(inner=False)
        c = np.cos(th)
        s = np.sin(th)
        #recompute ellipse dimensions in array referential
        #compute a meshgrid for the array self.data[ix1,ix2,iy1,iy2]
        x1 = self.values[kx,1] + ix1 * self.values[kx,3]
        x2 = self.values[kx,1] + ix2 * self.values[kx,3]
        y1 = self.values[ky,1] + iy1 * self.values[ky,3]
        y2 = self.values[ky,1] + iy2 * self.values[ky,3]
        x_grid = np.arange(x1-x0,x2-x0-self.values[kx,3]/2.,self.values[kx,3])
        y_grid = np.arange(y1-y0,y2-y0-self.values[ky,3]/2.,self.values[ky,3])

        y_grid, x_grid =  np.meshgrid(y_grid,x_grid)  #kind of transposition since first index is for column

        if kz == 2:
            if self.p_panel.swap.isChecked():
                data = np.array(self.data[ix1:ix2,iy1:iy2,:])
                cont = np.array(self.contributions[ix1:ix2,iy1:iy2,:])
            else:
                data = np.swapaxes(np.array(self.data[iy1:iy2,ix1:ix2,:]),0,1)
                cont = np.swapaxes(np.array(self.contributions[iy1:iy2,ix1:ix2,:]),0,1)
        elif kz == 1:
            if self.p_panel.swap.isChecked():
                data = np.array(self.data[ix1:ix2,:,iy1:iy2])
                cont = np.array(self.contributions[ix1:ix2,:,iy1:iy2])
            else:
                data = np.swapaxes(np.array(self.data[iy1:iy2,:,ix1:ix2]),0,2)
                cont = np.swapaxes(np.array(self.contributions[iy1:iy2,:,ix1:ix2]),0,2)
        else:
            if self.p_panel.swap.isChecked():
                data = np.array(self.data[:,ix1:ix2,iy1:iy2])
                cont = np.array(self.contributions[:,ix1:ix2,iy1:iy2])
            else:
                data = np.swapaxes(np.array(self.data[:,iy1:iy2,ix1:ix2]),1,2)
                cont = np.swapaxes(np.array(self.contributions[:,iy1:iy2,ix1:ix2]),1,2)


        #data are the counts detected, cont is the number of pixels of the detector that contribute to the voxel considered
        with np.errstate(invalid='ignore'):
            intensity = data/cont
            sigma = np.sqrt(data+1)/cont
        outside = ((x_grid*c+y_grid*s)/a)**2 + ((x_grid*s-y_grid*c)/b)**2 > 0.25

        a,b,th=self.roi_shape.get_sizes_and_angle(inner=True)
        c = np.cos(th)
        s = np.sin(th)

        inside = ((x_grid*c+y_grid*s)/a)**2 + ((x_grid*s-y_grid*c)/b)**2 < 0.25
        both = np.logical_or(inside,outside)
        #print (outside.shape,data.shape)

        x_shell = np.ma.masked_where(both,x_grid)
        y_shell = np.ma.array(y_grid,mask=x_shell.mask)

        x_core = np.ma.masked_where(~inside,x_grid)
        y_core = np.ma.array(y_grid,mask=x_core.mask)

        x_cv = []  #to be plotted
        y_cv = []
        s_cv = []

        for l in range(data.shape[kz]):
            #first compute background inn the shell
            if kz == 2:
                x = np.ma.masked_where(cont[:,:,l]==0,x_shell)
                _intens = np.ma.array(intensity[:,:,l],mask=x.mask)
                _sigma = np.ma.array(sigma[:,:,l],mask=x.mask)
                _data = np.ma.array(data[:,:,l],mask=x.mask)
                _cont = np.ma.array(cont[:,:,l],mask=x.mask)
            elif kz ==1:
                x = np.ma.masked_where(cont[:,l,:]==0,x_shell)
                _intens = np.ma.array(intensity[:,l,:],mask=x.mask)
                _sigma = np.ma.array(sigma[:,l,:],mask=x.mask)
                _data = np.ma.array(data[:,l,:],mask=x.mask)
                _cont = np.ma.array(cont[:,l,:],mask=x.mask)
            else:
                x = np.ma.masked_where(cont[l,:,:]==0,x_shell)
                _intens = np.ma.array(intensity[l,:,:],mask=x.mask)
                _sigma = np.ma.array(sigma[l,:,:],mask=x.mask)
                _data = np.ma.array(data[l,:,:],mask=x.mask)
                _cont = np.ma.array(cont[l,:,:],mask=x.mask)

            y = np.ma.array(y_shell,mask=x.mask)
            z0 = self.values[kz,1] + l * self.values[kz,3]

            _sigma = _sigma.compressed()
            _intens = _intens.compressed()

            shell_cont = np.ma.sum(_cont)  #contributions in the shell for bg subtraction
            shell_integral = np.ma.sum(_data)  #total counts in the shell
            bg_voxels = _data.count() #voxels in the shell

            x = x.compressed()
            y = y.compressed()

            if len(x)>3:
                p0 = [0.,0.,np.ma.mean(_intens)]
                if np.amin(x)<np.amax(x) and np.amin(y)<np.amax(y):
                    p0, pcov = opt.curve_fit(planar2D, (x, y), _intens,  p0=p0, sigma=_sigma)
                    pvar = np.diag(pcov)  #variances
                    chi2_bg = np.mean(((planar2D((x, y),p0[0],p0[1],p0[2])-_intens)/_sigma)**2)

                else:
                    print('not enough points for planar bg subtraction')
                    pvar = [0.,0.,0.]
                    chi2_bg = 0
            else:
                p0 = [0.,0.,0.]
                pvar = [0.,0.,0.]
                chi2_bg = 0
                print('no bg found for z=',z0)
            #compute bg contribution in the core

            if kz == 2:
                x = np.ma.masked_where(cont[:,:,l]==0,x_core)
                _data = np.ma.array(data[:,:,l],mask=x.mask)
                _cont = np.ma.array(cont[:,:,l],mask=x.mask)
            elif kz ==1:
                x = np.ma.masked_where(cont[:,l,:]==0,x_core)
                _data = np.ma.array(data[:,l,:],mask=x.mask)
                _cont = np.ma.array(cont[:,l,:],mask=x.mask)
            else:
                x = np.ma.masked_where(cont[l,:,:]==0,x_core)
                _data = np.ma.array(data[l,:,:],mask=x.mask)
                _cont = np.ma.array(cont[l,:,:],mask=x.mask)

            y = np.ma.array(y_core,mask=x.mask)
            bg = (p0[2] + p0[0]*x + p0[1]*y)
            if len(x)>0:
                bg_error = np.sqrt(1./np.ma.sum(_cont)+pvar[2])     #squared sum of statistical errors and fit variance (I am not sure that it should be summed)
            else:
                bg_error = 0

            total_cont = np.ma.sum(_cont)      #sum of contributions in the inner ROI  (core)
            total_voxels = _data.count()  #voxels in the core

            if total_cont>0:
                total_counts = np.ma.sum(_data)    #sum of measured counts (core)
                bg_counts = np.ma.sum(_cont*bg)    #sum of bg intensities estimated in the inner ROI, multiplied by contributions


                w_integral = (total_counts - bg_counts)/total_cont  #weighted integral
                w_integral = w_integral*_data.count()*self.values[kx,3]*self.values[ky,3]   #integral is multiplied by the integrated surface

                nw_integral = np.ma.sum(_data/_cont-bg)   #non weighted integral
                nw_integral = nw_integral*self.values[kx,3]*self.values[ky,3] #integral is multiplied by area of the data cell

                error_integral = np.sqrt(total_counts+(bg_counts*bg_error)**2)  #sqrt(statistical error on the intensity)**2 + (error on bg subtraction)**2)
                error_integral = error_integral/total_cont*_data.count()*self.values[kx,3]*self.values[ky,3]  #to scale with intensity

                if w_integral>0:
                    w_Fexp = np.sqrt(w_integral)
                else:
                    w_Fexp = 0.
                if nw_integral>0:
                    nw_Fexp = np.sqrt(nw_integral)
                else:
                    nw_Fexp = 0.

                err_w_Fexp = np.sqrt(max(0,w_integral)+error_integral)-w_Fexp
                err_nw_Fexp = np.sqrt(max(0,nw_integral)+error_integral)-nw_Fexp

                #first remove elements with missing contributions
                if kz == 2:
                    x = np.ma.masked_where(cont[:,:,l]==0,x_grid)
                    _intens = np.ma.array(intensity[:,:,l],mask=x.mask)
                    _sigma = np.ma.array(sigma[:,:,l],mask=x.mask)
                    _data = np.ma.array(data[:,:,l],mask=x.mask)
                    _cont = np.ma.array(cont[:,:,l],mask=x.mask)
                elif kz ==1:
                    x = np.ma.masked_where(cont[:,l,:]==0,x_grid)
                    _intens = np.ma.array(intensity[:,l,:],mask=x.mask)
                    _sigma = np.ma.array(sigma[:,l,:],mask=x.mask)
                    _data = np.ma.array(data[:,l,:],mask=x.mask)
                    _cont = np.ma.array(cont[:,l,:],mask=x.mask)
                else:
                    x = np.ma.masked_where(cont[l,:,:]==0,x_grid)
                    _intens = np.ma.array(intensity[l,:,:],mask=x.mask)
                    _sigma = np.ma.array(sigma[l,:,:],mask=x.mask)
                    _data = np.ma.array(data[l,:,:],mask=x.mask)
                    _cont = np.ma.array(cont[l,:,:],mask=x.mask)

                #remove missing contributions and flatten array for everybody
                y = np.ma.array(y_grid,mask=x.mask)
                x = x.compressed()    #relative values
                y = y.compressed()
                _sigma = _sigma.compressed()
                _intens = _intens.compressed()
                _data = _data.compressed()
                _cont = _cont.compressed()

                #estimate amplitude of the gaussian*lorenztian from the 1% highest intensity
                n = len(x)/100
                if n<=1:
                    amp = np.amax(_intens)
                else:
                    ind_max = np.argpartition(_intens,-10)[-10:]
                    dsum = np.sum(_data[ind_max])
                    csum = np.sum(_cont[ind_max])
                    amp = dsum/csum

                slope_x = p0[0]
                slope_y = p0[1]
                bg = p0[2]
                amp = amp - bg #subtract background
                if amp<0:
                    amp = 0

                loc0 = 0.
                loc1 = 0.
                width0 = 0.01
                width1 = 0.01
                qz = z0
                th = 1.27429847*qtheta -0.16812245*qpar+0.04701741*qz**3-0.41606092*qz**2+1.43500816*qz+0.29321067

                p1 = [slope_x,slope_y,bg,amp,loc0,loc1,width0,width1,th]

                try:
                    bounds=([-np.inf,-np.inf,0.,0.,-dxs2,-dys2,0.,0.,th-1e-10],[np.inf,np.inf,np.inf,np.inf,dxs2,dys2,2*dxs2,2*dys2,th+1e-10])
                    p1, pcov1 = opt.curve_fit(gaussgauss2D, (x, y), _intens,  p0=p1, sigma=_sigma, bounds=bounds)

                    bounds=([-np.inf,-np.inf,0.,0.,-dxs2,-dys2,0.,0.,th-np.pi/4.],[np.inf,np.inf,np.inf,np.inf,dxs2,dys2,2*dxs2,2*dys2,th+np.pi/4.])
                    p1, pcov1 = opt.curve_fit(gaussgauss2D, (x, y), _intens,  p0=p1, sigma=_sigma, bounds=bounds)

                except ValueError:
                    print ("ValueError",p1,bounds)
                    p1 = np.zeros(9)

                except RuntimeError:
                    p1 = np.zeros(9)

                chi2_gg = np.mean(((gaussgauss2D((x, y),p1[0],p1[1],p1[2],p1[3],p1[4],p1[5],p1[6],p1[7],p1[8])-_intens)/_sigma)**2)
                int_Fexp = np.sqrt(gaussgaussint(p1[3],p1[4],p1[5],p1[6],p1[7],p1[8]))

                fic.write('%s %s %s '%(self.labels[kx],self.labels[ky],self.labels[kz]))
                fic.write('%g %g %g '%(x0,y0,z0))
                fic.write('%s '%title)
                fic.write('%g %g '%(h_real, k_real))
                fic.write('%d %d '%(h, k))
                fic.write('%g %g '%(w_Fexp,err_w_Fexp))
                fic.write('%g %g '%(nw_Fexp,err_nw_Fexp))
                fic.write('%g %g %g '%(w_integral,nw_integral,error_integral))
                fic.write('%d %f %d '%(total_counts,bg_counts,total_cont))
                fic.write('%d %d '%(shell_integral, shell_cont))
                fic.write('%d %d '%(total_voxels,bg_voxels))
                fic.write('%g %g %g '%(p0[0],p0[1],p0[2]))
                fic.write('%g %g %g %g '%(pvar[0],pvar[1],pvar[2],chi2_bg))
                fic.write('ellipse ')
                fic.write('%g %g %g %g '%(x1, x2, y1, y2))
                for p in self.roi_shape.points:
                    fic.write('%g %g '%(p[0],p[1]))

                fic.write('%g %g %g %g %g %g %g %g %g %g %g'%(p1[0],p1[1],p1[2],p1[3],p1[4],p1[5],p1[6],p1[7],p1[8],int_Fexp,chi2_gg))

                fic.write('\n')

                print (z0,w_Fexp,err_w_Fexp)

                x_cv.append(z0)
                y_cv.append(w_Fexp)
                s_cv.append(err_w_Fexp)

            else:
                print('no bg found')

        linestyle, marker, color = next(self.style_generator)

        curve = make.error(x_cv, y_cv, np.ones_like(x_cv)*self.values[kz,3]/2.,s_cv,title=title,
                           color=color, linestyle=linestyle, linewidth=1,errorbarwidth=1,
                           marker=marker, markersize=8,markerfacecolor=color, markeredgecolor='black')
        self.roiwindow.get_plot().add_item(curve)
        fic.close()
        return title

    def integrate_all_rois(self):
        axis = self.get_projection()
        if self.savefilename is None:
            self.get_save_filename()


        if self.roi_shape is None or self.roi_shape.plot() != self.imageplots[axis]:
            QMessageBox.warning(self,'Warning','First draw a ROI')
            return

        bound = self.images[axis].boundingRect()
        x1, x2 = bound.left(), bound.right()
        y1, y2 = bound.top(), bound.bottom()
        nodes = self.imageplots[axis].get_visible_nodes(x1, x2, y1, y2)
        for node in nodes:
            self.roi_shape.set_center(node[4],node[5])
            self.integrate_roi()



    def init_roiwindow(self):
        self.roiwindow = CurveDialog()
        self.roiwindow.move(10,500)
        self.roiwindow.show()

    def show_roiwindow(self):
       if self.roiwindow is None:
           self.init_roiwindow()
       else:
           self.roiwindow.show()

    def init_fit1Dwindow(self):
        self.fit1Dwindow=Fit1DWindow()
        self.fit1Dwindow.set_cwd(self.openfiletool.directory)
        self.fit1Dwindow.show()

    def show_fit1Dwindow(self):
       if self.fit1Dwindow is None:
           self.init_fit1Dwindow()
       else:
           self.fit1Dwindow.show()

    def init_fit2Dwindow(self):
        self.fit2Dwindow=Fit2DWindow()
        self.fit2Dwindow.set_cwd(self.openfiletool.directory)
        self.fit2Dwindow.show()

    def show_fit2Dwindow(self):
       if self.fit2Dwindow is None:
           self.init_fit2Dwindow()
       else:
           self.fit2Dwindow.show()

    def init_reciprocalwindow(self):
        self.reciprocalwindow = ReciprocalDialog()
        self.reciprocalwindow.set_cwd(self.openfiletool.directory)
        self.reciprocalwindow.show()

    def show_reciprocalwindow(self):
        if self.reciprocalwindow is None:
            self.init_reciprocalwindow()
        else:
            self.reciprocalwindow.show()

    def do_fit(self,plot):
        images = plot.get_selected_items(item_type=IImageItemType)

        if len(images)>0:
            image = images[0]
            self.show_fit2Dwindow()
            x_range = [image.xmin, image.xmax]
            y_range = [image.ymin, image.ymax]
            ok = self.fit2Dwindow.add_data(image.data,x_range=x_range,y_range=y_range,title=self.windowTitle(),x_label=plot.get_axis_title('bottom'),y_label=plot.get_axis_title('left'))
            print( ok)
            return

        curves = plot.get_selected_items(item_type=ICurveItemType)
        if len(curves)>0:
            curve = curves[0]
            self.show_fit1Dwindow()
            x, y = curve.get_data()
            self.fit1Dwindow.add_data(x,y,title=self.windowTitle(),x_label=plot.get_axis_title('bottom'),y_label=plot.get_axis_title('left'))

            return


    def reject(self):
        QMessageBox.about(self,"Warning","Use ctr+Q to close window")

    def closeEvent(self,event):
        result = QMessageBox.question(self,
                      "Confirm Exit...",
                      "Are you sure you want to exit ?",
                      QMessageBox.Yes| QMessageBox.No)

        if result == QMessageBox.Yes:
            for window in [self.roiwindow, self.fit1Dwindow, self.fit2Dwindow]:
                if window is not None:
                    window.close()

        else:
            event.ignore()

def test0():
    """Test"""
    # -- Create QApplication
    import guidata
    _app = guidata.qapplication()
    # --
    win = Image3DDialog()
    #win.get_plot().manager.get_tool(AspectRatioTool).lock_action.setChecked(False)
    for plot in win.imageplots:
        plot.set_aspect_ratio(lock=True)
    win.resize(1200, 800)

    win.setGeometry(10,35,1250,900)
    win.open_file('D:/Documents Geoffroy PREVOT/ZrO2/SIXS-mars 2024/binoculars/sample2/mesh_qx_qy_qz_440-440_nolimits.hdf5')

    win.show()

    #SetSliceWindow2(win.slicetool.prefs)
    _app.exec_()
    _app.quit()

def test():
    """Test"""
    # -- Create QApplication
    import guidata
    _app = guidata.qapplication()
    # --
    win = Image3DDialog()
    #win.get_plot().manager.get_tool(AspectRatioTool).lock_action.setChecked(False)
    plot = win.imageplots[0]
    plot.set_aspect_ratio(lock=True)
    win.resize(1200, 800)
    #win.open_file('D:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL Juillet 2020/binoculars/GeAg111_sample2_760-767_hkl.hdf5')
    win.openfiletool.directory='D:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL Juillet 2020/binoculars/depot 2'
    win.open_file('D:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL Juillet 2020/binoculars/depot 2/GeAg111_sample2_219-410_QxQyQz_[0.9-2.5,m2.2-m0.5,0.0-4.5].hdf5')
    #win.openfiletool.directory='G:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL nov 2019/binoculars depot3/hdf5 depot3'
    #win.open_file('G:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL nov 2019/binoculars depot3/hdf5 depot3/Al111_depot3_688-714_2.hdf5')
    win.set_save_filename('D:/Documents Geoffroy PREVOT/ANR-Germanene-2017/Manip SOLEIL Juillet 2020/analyses/test_integration2.txt')

    from xrdutils.latticetools import LatticeGrid,ReconstructionGrid
    grid = LatticeGrid(O = [0,0], u = [2.228601635880728,-1.1595186883141404], v = [2.11847345818322,1.3502662874311975], sg = 'p3m1', centered = False)
    grid.set_style("plot", "shape/latticegrid")
    reconstruction = ReconstructionGrid(A = [12,7], B = [-7,5], master = grid, sym = True)
    reconstruction.set_style("plot", "shape/reconstructiongrid")
    reconstruction.shapeparam.label = '(V107xV107)'
    reconstruction.setTitle('(V107xV107)')
    plot.add_item(grid)
    plot.add_item(reconstruction)

    win.setGeometry(10,35,1250,900)

    win.show()
    #SetSliceWindow2(win.slicetool.prefs)
    _app.exec_()
    _app.quit()

def test2():
    """Test"""
    # -- Create QApplication
    import guidata
    _app = guidata.qapplication()
    # --

    win = Image3DDialog()
    #win.get_plot().manager.get_tool(AspectRatioTool).lock_action.setChecked(False)
    plot = win.imageplots[0]
    plot.set_aspect_ratio(lock=True)
    win.resize(1200, 800)
    win.openfiletool.directory='D:/Documents Geoffroy PREVOT/Ag-Si/SIXS-SiAg(110)-Juillet2019/binoculars/sample2b 1051-1097'
    win.openfiletool.directory='D:/Documents Geoffroy PREVOT/Silicene/Au-Si/SIXS-SiAu(111)-Juin2017/prevot/binoculars'
    win.show()
    from os import listdir
    from os.path import join

    fname = join(win.openfiletool.directory,"Au111qxqyqz_385-402 (1).hdf5")
    win.open_file(fname)
    """
    for filename in listdir(win.openfiletool.directory):
      print (filename)
      if (filename.startswith("sample2b") and filename.endswith(".hdf5")):
          fname = join(win.openfiletool.directory,filename)
          win.open_file(fname)
          win.p_panel.set_ranges([[1.1,1.52],[-0.001,0.001],[0.055,0.195]],dosignal=True)
          win.p_panel.xplot.select_item(win.p_panel.xcurve)
          win.do_fit(win.p_panel.xplot)
    """
    from xrdutils.latticetools import LatticeGrid,ReconstructionGrid
    grid = LatticeGrid(O = [0,0], u = [0.1763194275295802,-0.359198384010342], v = [0.39923463931606434,-0.02690208858382523], sg = 'p3m1', centered = False)
    grid.set_style("plot", "shape/latticegrid")
    reconstruction = ReconstructionGrid(A = [2.9033,1.96], B = [-0.74,2.8], master = grid, sym = True)
    reconstruction.set_style("plot", "shape/reconstructiongrid")
    plot.add_item(grid)
    plot.add_item(reconstruction)
    #win.set_save_filename('D:/Documents Geoffroy PREVOT/Ag-Si/SIXS-SiAg(110)-Juillet2019/test.txt')
    #SetSliceWindow2(win.slicetool.prefs)
    _app.exec_()
    _app.quit()


def main():
    abspath = osp.abspath(__file__)
    dirpath = osp.dirname(abspath)
    add_image_path(dirpath)

    _app = guidata.qapplication()

    win = Image3DDialog()
    plot = win.imageplots[0]
    plot.set_aspect_ratio(lock=True)
    win.resize(1200, 800)
    win.show()
    _app.exec_()
    _app.quit()

if __name__ == "__main__":
    main()
